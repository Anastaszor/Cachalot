<?php

// procedural
@date_default_timezone_set('Europe/Paris');

function now()
{
	$time = explode(' ', microtime());
	return date('H:i:s').'.'.substr($time[1], 0, 6).' ';
}

echo now()." php composer.phar update...\n";
exec("php composer.phar update");
echo now()." done;\n";

// require composer autoloader
$bp = __DIR__;
$testfolder = $bp.DIRECTORY_SEPARATOR.'runtime';
require $bp.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

echo now()." Cachalot Test Setup Ok;\n";

echo now()." Cleaning up directory...\n";

CUtil::removeRecursiveDir($testfolder);
mkdir($testfolder);
chmod($testfolder, 0777);

echo now()." done;\n\n";

$cachalot = Cachalot::connect("file://".$testfolder);

echo "Welcome to Cachalot CLI !\n";

$cachalot->exec("create database test");
$cachalot->exec("use test");

$command = "";
while(true)
{
	echo "Please type an sql command and it will be executed (or exit to quit):\n\n";
	
	$line = trim(fgets(STDIN));
	if($line === 'exit' 
		|| $line === 'q'
		|| $line === 'quit'
		|| $line === ':wq'
		|| $line === ':q!'
		|| $line === false
	) break;	// break goes to exit
	
	try
	{
		$before = new DateTime();
		
		$query = $cachalot->query($line);
		$success = $query->execute();
		echo "\nExecution status : ".($success ? 'success' : 'fail')."\n";
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		if($result !== array())
			echo var_export($result, true).";\n\n";
		else 
			echo "The query returned no data.\n\n";
		
		$after = new DateTime();
		echo now().$after->diff($before, true)->format('execution took %h hours, %m minutes and %s seconds')."\n\n\n";
	}
	catch(CachalotException $e)
	{
		echo "\n".now().$e->getMessage()."\n";
		echo $e->getTraceAsString()."\n\n";
	}
}

echo "See ya later..";
exit(0);	// bye bye


$use = new UseStatement($cachalot);
$use->setDatabaseName('newdb');
$result = $use->execute();
echo CUtil::now().$use->toSQL()."\n\n";
// var_export($result);echo "\n\n";

$createtb = new CreateTableStatement($cachalot);
$createtb->setTableName('newtb');
	$coldef = new CreateDefinitionStatement();
	$coldef->setColumnName('column1');
		$coltype = new ColumnDefinitionStatement();
			$coldata = new DecimalTypeStatement();
			$coldata->setLength(10);
			$coldata->setDecimals(2);
			$coldata->setUnsigned(false);
			$coldata->setZerofill(true);
		$coltype->setDataType($coldata);
	$coldef->setColumnType($coltype);
$createtb->addColumnDefinitionStatement($coldef);
	$coldef = new CreateDefinitionStatement();
	$coldef->setColumnName('column2');
		$coltype = new ColumnDefinitionStatement();
			$coldata = new CharTypeStatement();
			$coldata->setBinary(false);
			$coldata->setCharset(new Utf8Charset());
			$coldata->setCollation(new Utf8_general_csCollation());
			$coldata->setLength(3);
		$coltype->setDataType($coldata);
		$coltype->setComment('A simili 3-char key');
		$coltype->setNotNull(true);
	$coldef->setColumnType($coltype);
$createtb->addColumnDefinitionStatement($coldef);
$result = $createtb->execute();
echo CUtil::now().$createtb->toSQL()."\n\n";
// var_export($result);echo "\n\n";

// $createtb2 = new CreateTableStatement($cachalot);
// $createtb2->setTableName('newtb2');
// $result = $createtb2->execute();
// echo CUtil::now().$createtb2->toSQL()."\n\n";
// var_export($result);echo "\n\n";

// $show = new ShowTablesStatement($cachalot);
// $result = $show->execute();
// echo CUtil::now().$show->toSQL()."\n\n";
// var_export($result);echo "\n\n";

// $deletetb = new DropTableStatement($cachalot);
// $deletetb->addTableName('newtb2');
// $result = $deletetb->execute();
// echo CUtil::now().$deletetb->toSQL()."\n\n";
// var_export($result);echo "\n\n";

$show = new ShowTablesStatement($cachalot);
$result = $show->execute();
echo CUtil::now().$show->toSQL()."\n\n";
var_export($result);echo "\n\n";



echo CUtil::now()." Cachalot Test Finished... Success;\n";

# Cachalot
A DBMS written in PHP that uses the files as db/tables/records (This is not an 
n-th connector, this is the DBMS itself).


## Supported Dialects and Statements

- s : sql rendering partial
- S : sql rendering complete
- p : parsed partial
- P : parsed complete
- e : partial execution possible
- E : full execution possible
- t : partial tests
- T : full tests

| Statement             | Mysql 5.0 |
|:----------------------|:---------:|
| ALTER DATABASE        | ST        |
| ALTER FUNCTION        | St        |
| ALTER PROCEDURE       | St        |
| ALTER TABLE           | ST        |
| ALTER VIEW            | ST        |
| CREATE DATABASE       | ST        |
| CREATE FUNCTION       | St        |
| CREATE INDEX          | St        |
| CREATE PROCEDURE      | St        |
| CREATE TABLE          |  s        |
| CREATE TRIGGER        | S         |
| CREATE VIEW           | S         |
| DROP DATABASE         | S         |
| DROP FUNCTION         | S         |
| DROP INDEX            | S         |
| DROP PROCEDURE        | S         |
| DROP TABLE            |  s        |
| DROP TRIGGER          | S         |
| DROP VIEW             | S         |
| RENAME TABLE          |  s        |
| TRUNCATE TABLE        |  s        |
| CALL                  |  s        |
| DELETE                |  s        |
| DO                    |  s        |
| HANDLER               |  s        |
| INSERT                |  s        |
| LOAD DATA INFILE      |  s        |
| REPLACE               |  s        |
| SELECT                |  s        |
| UPDATE                |  s        |
| START TRANSACTION     |  s        |
| COMMIT                |  s        |
| ROLLBACK              |  s        |
| SAVEPOINT             |  s        |
| ROLLBACK TO SAVEPOINT |  s        |
| RELEASE SAVEPOINT     |  s        |
| LOCK TABLES           |  s        |
| UNLOCK TABLES         |  s        |
| SET TRANSACTION       |  s        |
| PREPARE               |  s        |
| EXECUTE               |  s        |
| DEALLOCATE PREPARE    |  s        |
| ANALYSE TABLE         |  s        |
| BACKUP TABLE          |  s        |
| CHECK TABLE           |  s        |
| CHECKSUM TABLE        |  s        |
| OPTIMIZE TABLE        |  s        |
| REPAIR TABLE          |  s        |
| RESTORE TABLE         |  s        |
| SET                   |  s        |
| SHOW BINARY LOGS      |  s        |
| SHOW BINLOGS EVENTS   |  s        |
| SHOW CHARACTER SET    |  s        |
| SHOW COLLATION        |  s        |
| SHOW COLUMNS          |  s        |
| SHOW CREATE DATABASE  |  s        |
| SHOW CREATE FUNCTION  |  s        |
| SHOW CREATE PROCEDURE |  s        |
| SHOW CREATE TABLE     |  s        |
| SHOW CREATE VIEW      |  s        |
| SHOW DATABASES        |  s        |
| SHOW ENGINE           |  s        |
| SHOW ENGINES          |  s        |
| SHOW ERRORS           |  s        |
| SHOW FUNCTION CODE    |  s        |
| SHOW FUNCTION STATUS  |  s        |
| SHOW GRANTS           |  s        |
| SHOW INDEX            |  s        |
| SHOW LOGS             |  s        |
| SHOW MASTER STATUS    |  s        |
| SHOW MUTEX STATUS     |  s        |
| SHOW OPEN TABLES      |  s        |
| SHOW PRIVILEGES       |  s        |
| SHOW PROCEDURE CODE   |  s        |
| SHOW PROCEDURE STATUS |  s        |
| SHOW PROCESS LIST     |  s        |
| SHOW PROFILE          |  s        |
| SHOW PROFILES         |  s        |
| SHOW STATUS           |  s        |
| SHOW TABLE STATUS     |  s        |
| SHOW TABLES           |  s        |
| SHOW TRIGGERS         |  s        |
| SHOW VARIABLES        |  s        |
| SHOW WARNINGS         |  s        |
| CACHE INDEX           |  s        |
| FLUSH                 |  s        |
| KILL                  |  s        |
| LOAD INDEX INTO CACHE |  s        |
| RESET                 |  s        |
| EXPLAIN               |  s        |
| HELP                  | S         |
| USE                   | S         |


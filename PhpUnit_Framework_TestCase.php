<?php

/**
 * The purpose of this abstract class is to provide autocompletion in IDE when
 * phpunit is installed via phpunit.phar.
 * 
 * This class should never be loaded. Only the real class should be.
 * 
 * To define a TestCase:
 *
 * 1) Implement a subclass of PHPUnit_Framework_TestCase.
 * 2) Define instance variables that store the state of the fixture.
 * 3) Initialize the fixture state by overriding setUp().
 * 4) Clean-up after a test by overriding tearDown().
 * 
 * Compatible PhpUnit 4.8 until 5.2
 */
abstract class PhpUnit_Framework_TestCase
{
	
	// From PhpUnit_Framework_SelfDescribing
	/**
	 * Returns a string representation of the object.
	 *
	 * @return string
	 */
	public abstract function toString();
	
	// From PhpUnit_Framework_Test
	/**
	 * Runs a test and collects its result in a TestResult instance.
	 *
	 * @param PHPUnit_Framework_TestResult $result
	 *
	 * @return PHPUnit_Framework_TestResult
	 */
	public abstract function run(PHPUnit_Framework_TestResult $result = null);
	
	// From PhpUnit_Framework_Assert
	/**
	 * Asserts that an array has a specified key.
	 *
	 * @param mixed $key
	 * @param array|ArrayAccess $array
	 * @param string $message
	 * @since Method available since Release 3.0.0
	 */
	public static abstract function assertArrayHasKey($key, $array, string $message = '');
	
	/**
	 * Asserts that an array has a specified subset.
	 *
	 * @param array|ArrayAccess $subset
	 * @param array|ArrayAccess $array
	 * @param bool $strict Check for object identity
	 * @param string $message
	 * @since Method available since Release 4.4.0
	 */
	public static abstract function assertArraySubset($subset, $array, $strict = false, string $message = '');
	
	/**
	 * Asserts that an array does not have a specified key.
	 *
	 * @param mixed $key
	 * @param array|ArrayAccess $array
	 * @param string $message
	 * @since Method available since Release 3.0.0
	 */
	public static abstract function assertArrayNotHasKey($key, $array, string $message = '');
	
	/**
	 * Asserts that a haystack contains a needle.
	 *
	 * @param mixed $needle
	 * @param mixed $haystack
	 * @param string $message
	 * @param bool $ignoreCase
	 * @param bool $checkForObjectIdentity
	 * @param bool $checkForNonObjectIdentity
	 * @since Method available since Release 2.1.0
	 */
	public static abstract function assertContains($needle, $haystack, string $message = '', 
		$ignoreCase = false, $checkForObjectIdentity = true, $checkForNonObjectIdentity = false);
	
	/**
	 * Asserts that a haystack that is stored in a static attribute of a class
	 * or an attribute of an object contains a needle.
	 *
	 * @param mixed $needle
	 * @param string $haystackAttributeName
	 * @param mixed $haystackClassOrObject
	 * @param string $message
	 * @param bool $ignoreCase
	 * @param bool $checkForObjectIdentity
	 * @param bool $checkForNonObjectIdentity
	 * @since Method available since Release 3.0.0
	 */
	public static abstract function assertAttributeContains($needle, $haystackAttributeName, 
		$haystackClassOrObject, string $message = '', $ignoreCase = false, 
		$checkForObjectIdentity = true, $checkForNonObjectIdentity = false);
	
	/**
	 * Asserts that a haystack does not contain a needle.
	 *
	 * @param mixed $needle
	 * @param mixed $haystack
	 * @param string $message
	 * @param bool $ignoreCase
	 * @param bool $checkForObjectIdentity
	 * @param bool $checkForNonObjectIdentity
	 * @since Method available since Release 2.1.0
	 */
	public static abstract function assertNotContains($needle, $haystack, string $message = '', 
		$ignoreCase = false, $checkForObjectIdentity = true, $checkForNonObjectIdentity = false);
	
	
	/**
	 * Asserts that a haystack that is stored in a static attribute of a class
	 * or an attribute of an object does not contain a needle.
	 *
	 * @param mixed $needle
	 * @param string $haystackAttributeName
	 * @param mixed $haystackClassOrObject
	 * @param string $message
	 * @param bool $ignoreCase
	 * @param bool $checkForObjectIdentity
	 * @param bool $checkForNonObjectIdentity
	 * @since Method available since Release 3.0.0
	 */
	public static abstract function assertAttributeNotContains($needle, $haystackAttributeName, 
		$haystackClassOrObject, string $message = '', $ignoreCase = false, 
		$checkForObjectIdentity = true, $checkForNonObjectIdentity = false);
	
	/**
	 * Asserts that a haystack contains only values of a given type.
	 *
	 * @param string $type
	 * @param mixed $haystack
	 * @param bool $isNativeType
	 * @param string $message
	 * @since Method available since Release 3.1.4
	 */
	public static abstract function assertContainsOnly(string $type, $haystack, 
		$isNativeType = null, string $message = '');
	
	/**
	 * Asserts that a haystack contains only instances of a given classname
	 *
	 * @param string $classname
	 * @param array|Traversable $haystack
	 * @param string $message
	 */
	public static abstract function assertContainsOnlyInstancesOf($classname, $haystack, string $message = '');
	
	/**
	 * Asserts that a haystack that is stored in a static attribute of a class
	 * or an attribute of an object contains only values of a given type.
	 *
	 * @param string $type
	 * @param string $haystackAttributeName
	 * @param mixed $haystackClassOrObject
	 * @param bool $isNativeType
	 * @param string $message
	 * @since Method available since Release 3.1.4
	 */
	public static abstract function assertAttributeContainsOnly($type, $haystackAttributeName, 
		$haystackClassOrObject, $isNativeType = null, string $message = '');
	
	/**
	 * Asserts that a haystack does not contain only values of a given type.
	 *
	 * @param string $type
	 * @param mixed $haystack
	 * @param bool $isNativeType
	 * @param string $message
	 * @since Method available since Release 3.1.4
	 */
	public static abstract function assertNotContainsOnly($type, $haystack, $isNativeType = null, string $message = '');
	
	/**
	 * Asserts that a haystack that is stored in a static attribute of a class
	 * or an attribute of an object does not contain only values of a given
	 * type.
	 *
	 * @param string $type
	 * @param string $haystackAttributeName
	 * @param mixed $haystackClassOrObject
	 * @param bool $isNativeType
	 * @param string $message
	 * @since Method available since Release 3.1.4
	 */
	public static abstract function assertAttributeNotContainsOnly($type, $haystackAttributeName, 
		$haystackClassOrObject, $isNativeType = null, string $message = '');
	
	/**
	 * Asserts the number of elements of an array, Countable or Traversable.
	 *
	 * @param int $expectedCount
	 * @param mixed $haystack
	 * @param string $message
	 */
	public static abstract function assertCount(int $expectedCount, $haystack, string $message = '');
	
	/**
	 * Asserts the number of elements of an array, Countable or Traversable
	 * that is stored in an attribute.
	 *
	 * @param int $expectedCount
	 * @param string $haystackAttributeName
	 * @param mixed $haystackClassOrObject
	 * @param string $message
	 * @since Method available since Release 3.6.0
	 */
	public static abstract function assertAttributeCount($expectedCount, $haystackAttributeName, 
		$haystackClassOrObject, string $message = '');
	
	/**
	 * Asserts the number of elements of an array, Countable or Traversable.
	 *
	 * @param int $expectedCount
	 * @param mixed $haystack
	 * @param string $message
	 */
	public static abstract function assertNotCount(int $expectedCount, $haystack, string $message = '');
	
	/**
	 * Asserts the number of elements of an array, Countable or Traversable
	 * that is stored in an attribute.
	 *
	 * @param int $expectedCount
	 * @param string $haystackAttributeName
	 * @param mixed $haystackClassOrObject
	 * @param string $message
	 * @since Method available since Release 3.6.0
	 */
	public static abstract function assertAttributeNotCount($expectedCount, $haystackAttributeName, 
		$haystackClassOrObject, string $message = '');
	
	/**
	 * Asserts that two variables are equal.
	 *
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param string $message
	 * @param float $delta
	 * @param int $maxDepth
	 * @param bool $canonicalize
	 * @param bool $ignoreCase
	 */
	public static abstract function assertEquals($expected, $actual, string $message = '', 
		$delta = 0.0, $maxDepth = 10, $canonicalize = false, $ignoreCase = false);
	
	/**
	 * Asserts that a variable is equal to an attribute of an object.
	 *
	 * @param mixed $expected
	 * @param string $actualAttributeName
	 * @param string $actualClassOrObject
	 * @param string $message
	 * @param float $delta
	 * @param int $maxDepth
	 * @param bool $canonicalize
	 * @param bool $ignoreCase
	 */
	public static abstract function assertAttributeEquals($expected, $actualAttributeName, 
		$actualClassOrObject, string $message = '', $delta = 0.0, $maxDepth = 10, 
		$canonicalize = false, $ignoreCase = false);
	
	/**
	 * Asserts that two variables are not equal.
	 *
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param string $message
	 * @param float $delta
	 * @param int $maxDepth
	 * @param bool $canonicalize
	 * @param bool $ignoreCase
	 * @since Method available since Release 2.3.0
	 */
	public static abstract function assertNotEquals($expected, $actual, string $message = '', 
		$delta = 0.0, $maxDepth = 10, $canonicalize = false, $ignoreCase = false);
	
	/**
	 * Asserts that a variable is not equal to an attribute of an object.
	 *
	 * @param mixed $expected
	 * @param string $actualAttributeName
	 * @param string $actualClassOrObject
	 * @param string $message
	 * @param float $delta
	 * @param int $maxDepth
	 * @param bool $canonicalize
	 * @param bool $ignoreCase
	 */
	public static abstract function assertAttributeNotEquals($expected, $actualAttributeName, 
		$actualClassOrObject, string $message = '', $delta = 0.0, $maxDepth = 10, 
		$canonicalize = false, $ignoreCase = false);
	
	/**
	 * Asserts that a variable is empty.
	 *
	 * @param mixed $actual
	 * @param string $message
	 * @throws PHPUnit_Framework_AssertionFailedError
	 */
	public static abstract function assertEmpty($actual, string $message = '');
	
	/**
	 * Asserts that a static attribute of a class or an attribute of an object
	 * is empty.
	 *
	 * @param string $haystackAttributeName
	 * @param mixed $haystackClassOrObject
	 * @param string $message
	 * @since Method available since Release 3.5.0
	 */
	public static abstract function assertAttributeEmpty($haystackAttributeName, 
		$haystackClassOrObject, string $message = '');
	
	/**
	 * Asserts that a variable is not empty.
	 *
	 * @param mixed $actual
	 * @param string $message
	 * @throws PHPUnit_Framework_AssertionFailedError
	 */
	public static abstract function assertNotEmpty($actual, string $message = '');
	
	/**
	 * Asserts that a static attribute of a class or an attribute of an object
	 * is not empty.
	 *
	 * @param string $haystackAttributeName
	 * @param mixed $haystackClassOrObject
	 * @param string $message
	 * @since Method available since Release 3.5.0
	 */
	public static abstract function assertAttributeNotEmpty($haystackAttributeName, 
		$haystackClassOrObject, string $message = '');
	
	/**
	 * Asserts that a value is greater than another value.
	 *
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param string $message
	 * @since Method available since Release 3.1.0
	 */
	public static abstract function assertGreaterThan($expected, $actual, string $message = '');
	
	/**
	 * Asserts that an attribute is greater than another value.
	 *
	 * @param mixed $expected
	 * @param string $actualAttributeName
	 * @param string $actualClassOrObject
	 * @param string $message
	 * @since Method available since Release 3.1.0
	 */
	public static abstract function assertAttributeGreaterThan($expected, $actualAttributeName, 
		$actualClassOrObject, string $message = '');
	
	/**
	 * Asserts that a value is greater than or equal to another value.
	 *
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param string $message
	 * @since Method available since Release 3.1.0
	 */
	public static abstract function assertGreaterThanOrEqual($expected, $actual, string $message = '');
	
	/**
	 * Asserts that an attribute is greater than or equal to another value.
	 *
	 * @param mixed $expected
	 * @param string $actualAttributeName
	 * @param string $actualClassOrObject
	 * @param string $message
	 * @since Method available since Release 3.1.0
	 */
	public static abstract function assertAttributeGreaterThanOrEqual($expected, $actualAttributeName, 
		$actualClassOrObject, string $message = '');
	
	/**
	 * Asserts that a value is smaller than another value.
	 *
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param string $message
	 * @since Method available since Release 3.1.0
	 */
	public static abstract function assertLessThan($expected, $actual, string $message = '');
	
	/**
	 * Asserts that an attribute is smaller than another value.
	 *
	 * @param mixed $expected
	 * @param string $actualAttributeName
	 * @param string $actualClassOrObject
	 * @param string $message
	 * @since Method available since Release 3.1.0
	 */
	public static abstract function assertAttributeLessThan($expected, $actualAttributeName, 
		$actualClassOrObject, string $message = '');
	
	/**
	 * Asserts that a value is smaller than or equal to another value.
	 *
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param string $message
	 * @since Method available since Release 3.1.0
	 */
	public static abstract function assertLessThanOrEqual($expected, $actual, string $message = '');
	
	/**
	 * Asserts that an attribute is smaller than or equal to another value.
	 *
	 * @param mixed $expected
	 * @param string $actualAttributeName
	 * @param string $actualClassOrObject
	 * @param string $message
	 * @since Method available since Release 3.1.0
	 */
	public static abstract function assertAttributeLessThanOrEqual($expected, $actualAttributeName, 
		$actualClassOrObject, string $message = '');
	
	/**
	 * Asserts that the contents of one file is equal to the contents of another
	 * file.
	 *
	 * @param string $expected
	 * @param string $actual
	 * @param string $message
	 * @param bool $canonicalize
	 * @param bool $ignoreCase
	 * @since Method available since Release 3.2.14
	 */
	public static abstract function assertFileEquals($expected, $actual, string $message = '', 
		$canonicalize = false, $ignoreCase = false);
	
	/**
	 * Asserts that the contents of one file is not equal to the contents of
	 * another file.
	 *
	 * @param string $expected
	 * @param string $actual
	 * @param string $message
	 * @param bool $canonicalize
	 * @param bool $ignoreCase
	 * @since Method available since Release 3.2.14
	 */
	public static abstract function assertFileNotEquals($expected, $actual, string $message = '', 
		$canonicalize = false, $ignoreCase = false);
	
	/**
	 * Asserts that the contents of a string is equal
	 * to the contents of a file.
	 *
	 * @param string $expectedFile
	 * @param string $actualString
	 * @param string $message
	 * @param bool $canonicalize
	 * @param bool $ignoreCase
	 * @since Method available since Release 3.3.0
	 */
	public static abstract function assertStringEqualsFile($expectedFile, $actualString, 
		string $message = '', $canonicalize = false, $ignoreCase = false);
	
	/**
	 * Asserts that the contents of a string is not equal
	 * to the contents of a file.
	 *
	 * @param string $expectedFile
	 * @param string $actualString
	 * @param string $message
	 * @param bool $canonicalize
	 * @param bool $ignoreCase
	 * @since Method available since Release 3.3.0
	 */
	public static abstract function assertStringNotEqualsFile($expectedFile, $actualString, 
		string $message = '', $canonicalize = false, $ignoreCase = false);
	
	/**
	 * Asserts that a file exists.
	 *
	 * @param string $filename
	 * @param string $message
	 * @since Method available since Release 3.0.0
	 */
	public static abstract function assertFileExists(string $filename, string $message = '');
	
	/**
	 * Asserts that a file does not exist.
	 *
	 * @param string $filename
	 * @param string $message
	 * @since Method available since Release 3.0.0
	 */
	public static abstract function assertFileNotExists(string $filename, string $message = '');
	
	/**
	 * Asserts that a condition is true.
	 *
	 * @param bool $condition
	 * @param string $message
	 * @throws PHPUnit_Framework_AssertionFailedError
	 */
	public static abstract function assertTrue($condition, string $message = '');
	
	/**
	 * Asserts that a condition is not true.
	 *
	 * @param bool $condition
	 * @param string $message
	 * @throws PHPUnit_Framework_AssertionFailedError
	 */
	public static abstract function assertNotTrue($condition, string $message = '');
	
	/**
	 * Asserts that a condition is false.
	 *
	 * @param bool $condition
	 * @param string $message
	 * @throws PHPUnit_Framework_AssertionFailedError
	 */
	public static abstract function assertFalse($condition, string $message = '');
	
	/**
	 * Asserts that a condition is not false.
	 *
	 * @param bool $condition
	 * @param string $message
	 * @throws PHPUnit_Framework_AssertionFailedError
	 */
	public static abstract function assertNotFalse($condition, string $message = '');
	
	/**
	 * Asserts that a variable is not null.
	 *
	 * @param mixed $actual
	 * @param string $message
	 */
	public static abstract function assertNotNull($actual, string $message = '');
	
	/**
	 * Asserts that a variable is null.
	 *
	 * @param mixed $actual
	 * @param string $message
	 */
	public static abstract function assertNull($actual, string $message = '');
	
	/**
	 * Asserts that a variable is finite.
	 *
	 * @param mixed $actual
	 * @param string $message
	 */
	public static abstract function assertFinite($actual, string $message = '');
	
	/**
	 * Asserts that a variable is infinite.
	 *
	 * @param mixed $actual
	 * @param string $message
	 */
	public static abstract function assertInfinite($actual, string $message = '');
	
	/**
	 * Asserts that a variable is nan.
	 *
	 * @param mixed $actual
	 * @param string $message
	 */
	public static abstract function assertNan($actual, string $message = '');
	
	/**
	 * Asserts that a class has a specified attribute.
	 *
	 * @param string $attributeName
	 * @param string $className
	 * @param string $message
	 * @since Method available since Release 3.1.0
	 */
	public static abstract function assertClassHasAttribute(string $attributeName, 
		string $className, string $message = '');
	
	/**
	 * Asserts that a class does not have a specified attribute.
	 *
	 * @param string $attributeName
	 * @param string $className
	 * @param string $message
	 * @since Method available since Release 3.1.0
	 */
	public static abstract function assertClassNotHasAttribute(string $attributeName, 
		string $className, string $message = '');
	
	/**
	 * Asserts that a class has a specified static attribute.
	 *
	 * @param string $attributeName
	 * @param string $className
	 * @param string $message
	 * @since Method available since Release 3.1.0
	 */
	public static abstract function assertClassHasStaticAttribute(string $attributeName, 
		string $className, string $message = '');
	
	/**
	 * Asserts that a class does not have a specified static attribute.
	 *
	 * @param string $attributeName
	 * @param string $className
	 * @param string $message
	 * @since Method available since Release 3.1.0
	 */
	public static abstract function assertClassNotHasStaticAttribute(string $attributeName, 
		string $className, string $message = '');
	
	/**
	 * Asserts that an object has a specified attribute.
	 *
	 * @param string $attributeName
	 * @param object $object
	 * @param string $message
	 * @since Method available since Release 3.0.0
	 */
	public static abstract function assertObjectHasAttribute(string $attributeName, $object, string $message = '');
	
	/**
	 * Asserts that an object does not have a specified attribute.
	 *
	 * @param string $attributeName
	 * @param object $object
	 * @param string $message
	 * @since Method available since Release 3.0.0
	 */
	public static abstract function assertObjectNotHasAttribute(string $attributeName, $object, string $message = '');
	
	/**
	 * Asserts that two variables have the same type and value.
	 * Used on objects, it asserts that two variables reference
	 * the same object.
	 *
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param string $message
	 */
	public static abstract function assertSame($expected, $actual, string $message = '');
	
	/**
	 * Asserts that a variable and an attribute of an object have the same type
	 * and value.
	 *
	 * @param mixed $expected
	 * @param string $actualAttributeName
	 * @param object $actualClassOrObject
	 * @param string $message
	 */
	public static abstract function assertAttributeSame($expected, string $actualAttributeName, 
		$actualClassOrObject, string $message = '');
	
	/**
	 * Asserts that two variables do not have the same type and value.
	 * Used on objects, it asserts that two variables do not reference
	 * the same object.
	 *
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param string $message
	 */
	public static abstract function assertNotSame($expected, $actual, string $message = '');
	
	/**
	 * Asserts that a variable and an attribute of an object do not have the
	 * same type and value.
	 *
	 * @param mixed $expected
	 * @param string $actualAttributeName
	 * @param object $actualClassOrObject
	 * @param string $message
	 */
	public static abstract function assertAttributeNotSame($expected, $actualAttributeName, 
		$actualClassOrObject, string $message = '');
	
	/**
	 * Asserts that a variable is of a given type.
	 *
	 * @param string $expected
	 * @param mixed $actual
	 * @param string $message
	 * @since Method available since Release 3.5.0
	 */
	public static abstract function assertInstanceOf(string $expected, $actual, string $message = '');
	
	/**
	 * Asserts that an attribute is of a given type.
	 *
	 * @param string $expected
	 * @param string $attributeName
	 * @param mixed $classOrObject
	 * @param string $message
	 * @since Method available since Release 3.5.0
	 */
	public static abstract function assertAttributeInstanceOf($expected, $attributeName, $classOrObject, string $message = '');
	
	/**
	 * Asserts that a variable is not of a given type.
	 *
	 * @param string $expected
	 * @param mixed $actual
	 * @param string $message
	 * @since Method available since Release 3.5.0
	 */
	public static abstract function assertNotInstanceOf(string $expected, $actual, string $message = '');
	
	/**
	 * Asserts that an attribute is of a given type.
	 *
	 * @param string $expected
	 * @param string $attributeName
	 * @param mixed $classOrObject
	 * @param string $message
	 * @since Method available since Release 3.5.0
	 */
	public static abstract function assertAttributeNotInstanceOf($expected, $attributeName, $classOrObject, string $message = '');
	
	/**
	 * Asserts that a variable is of a given type.
	 *
	 * @param string $expected
	 * @param mixed $actual
	 * @param string $message
	 * @since Method available since Release 3.5.0
	 */
	public static abstract function assertInternalType(string $expected, $actual, string $message = '');
	
	/**
	 * Asserts that an attribute is of a given type.
	 *
	 * @param string $expected
	 * @param string $attributeName
	 * @param mixed $classOrObject
	 * @param string $message
	 * @since Method available since Release 3.5.0
	 */
	public static abstract function assertAttributeInternalType($expected, $attributeName, 
		$classOrObject, string $message = '');
	
	/**
	 * Asserts that a variable is not of a given type.
	 *
	 * @param string $expected
	 * @param mixed $actual
	 * @param string $message
	 * @since Method available since Release 3.5.0
	 */
	public static abstract function assertNotInternalType(string $expected, $actual, string $message = '');
	
	/**
	 * Asserts that an attribute is of a given type.
	 *
	 * @param string $expected
	 * @param string $attributeName
	 * @param mixed $classOrObject
	 * @param string $message
	 * @since Method available since Release 3.5.0
	 */
	public static abstract function assertAttributeNotInternalType($expected, $attributeName, 
		$classOrObject, string $message = '');
	
	/**
	 * Asserts that a string matches a given regular expression.
	 *
	 * @param string $pattern
	 * @param string $string
	 * @param string $message
	 */
	public static abstract function assertRegExp(string $pattern, string $string, string $message = '');
	
	/**
	 * Asserts that a string does not match a given regular expression.
	 *
	 * @param string $pattern
	 * @param string $string
	 * @param string $message
	 * @since Method available since Release 2.1.0
	 */
	public static abstract function assertNotRegExp(string $pattern, string $string, string $message = '');
	
	/**
	 * Assert that the size of two arrays (or `Countable` or `Traversable` objects)
	 * is the same.
	 *
	 * @param array|Countable|Traversable $expected
	 * @param array|Countable|Traversable $actual
	 * @param string $message
	 */
	public static abstract function assertSameSize($expected, $actual, string $message = '');
	
	/**
	 * Assert that the size of two arrays (or `Countable` or `Traversable` objects)
	 * is not the same.
	 *
	 * @param array|Countable|Traversable $expected
	 * @param array|Countable|Traversable $actual
	 * @param string $message
	 */
	public static abstract function assertNotSameSize($expected, $actual, string $message = '');
	
	/**
	 * Asserts that a string matches a given format string.
	 *
	 * @param string $format
	 * @param string $string
	 * @param string $message
	 * @since Method available since Release 3.5.0
	 */
	public static abstract function assertStringMatchesFormat(string $format, string $string, 
		string $message = '');
	
	/**
	 * Asserts that a string does not match a given format string.
	 *
	 * @param string $format
	 * @param string $string
	 * @param string $message
	 * @since Method available since Release 3.5.0
	 */
	public static abstract function assertStringNotMatchesFormat(string $format, string $string, 
		string $message = '');
	
	/**
	 * Asserts that a string matches a given format file.
	 *
	 * @param string $formatFile
	 * @param string $string
	 * @param string $message
	 * @since Method available since Release 3.5.0
	 */
	public static abstract function assertStringMatchesFormatFile(string $formatFile, string $string, 
		string $message = '');
	
	/**
	 * Asserts that a string does not match a given format string.
	 *
	 * @param string $formatFile
	 * @param string $string
	 * @param string $message
	 * @since Method available since Release 3.5.0
	 */
	public static abstract function assertStringNotMatchesFormatFile(string $formatFile, 
		string $string, string $message = '');
	
	/**
	 * Asserts that a string starts with a given prefix.
	 *
	 * @param string $prefix
	 * @param string $string
	 * @param string $message
	 * @since Method available since Release 3.4.0
	 */
	public static abstract function assertStringStartsWith(string $prefix, string $string, 
		string $message = '');
	
	/**
	 * Asserts that a string starts not with a given prefix.
	 *
	 * @param string $prefix
	 * @param string $string
	 * @param string $message
	 * @since Method available since Release 3.4.0
	 */
	public static abstract function assertStringStartsNotWith(string $prefix, string $string, 
		string $message = '');
	
	/**
	 * Asserts that a string ends with a given suffix.
	 *
	 * @param string $suffix
	 * @param string $string
	 * @param string $message
	 * @since Method available since Release 3.4.0
	 */
	public static abstract function assertStringEndsWith(string $suffix, string $string, 
		string $message = '');
	
	/**
	 * Asserts that a string ends not with a given suffix.
	 *
	 * @param string $suffix
	 * @param string $string
	 * @param string $message
	 * @since Method available since Release 3.4.0
	 */
	public static abstract function assertStringEndsNotWith(string $suffix, string $string, 
		string $message = '');
	
	/**
	 * Asserts that two XML files are equal.
	 *
	 * @param string $expectedFile
	 * @param string $actualFile
	 * @param string $message
	 * @since Method available since Release 3.1.0
	 */
	public static abstract function assertXmlFileEqualsXmlFile($expectedFile, $actualFile, 
		string $message = '');
	
	/**
	 * Asserts that two XML files are not equal.
	 *
	 * @param string $expectedFile
	 * @param string $actualFile
	 * @param string $message
	 * @since Method available since Release 3.1.0
	 */
	public static abstract function assertXmlFileNotEqualsXmlFile($expectedFile, $actualFile, 
		string $message = '');
	
	/**
	 * Asserts that two XML documents are equal.
	 *
	 * @param string $expectedFile
	 * @param string $actualXml
	 * @param string $message
	 * @since Method available since Release 3.3.0
	 */
	public static abstract function assertXmlStringEqualsXmlFile($expectedFile, $actualXml, 
		string $message = '');
	
	/**
	 * Asserts that two XML documents are not equal.
	 *
	 * @param string $expectedFile
	 * @param string $actualXml
	 * @param string $message
	 * @since Method available since Release 3.3.0
	 */
	public static abstract function assertXmlStringNotEqualsXmlFile($expectedFile, $actualXml, 
		string $message = '');
	
	/**
	 * Asserts that two XML documents are equal.
	 *
	 * @param string $expectedXml
	 * @param string $actualXml
	 * @param string $message
	 * @since Method available since Release 3.1.0
	 */
	public static abstract function assertXmlStringEqualsXmlString($expectedXml, $actualXml, 
		string $message = '');
	
	/**
	 * Asserts that two XML documents are not equal.
	 *
	 * @param string $expectedXml
	 * @param string $actualXml
	 * @param string $message
	 * @since Method available since Release 3.1.0
	 */
	public static abstract function assertXmlStringNotEqualsXmlString($expectedXml, $actualXml, 
		string $message = '');
	
	/**
	 * Asserts that a hierarchy of DOMElements matches.
	 *
	 * @param DOMElement $expectedElement
	 * @param DOMElement $actualElement
	 * @param bool $checkAttributes
	 * @param string $message
	 * @since Method available since Release 3.3.0
	 */
	public static abstract function assertEqualXMLStructure(DOMElement $expectedElement, 
		DOMElement $actualElement, $checkAttributes = false, string $message = '');
	
	/**
	 * Asserts that a string is a valid JSON string.
	 *
	 * @param string $actualJson
	 * @param string $message
	 * @since Method available since Release 3.7.20
	 */
	public static abstract function assertJson(string $actualJson, string $message = '');
	
	/**
	 * Asserts that two given JSON encoded objects or arrays are equal.
	 *
	 * @param string $expectedJson
	 * @param string $actualJson
	 * @param string $message
	 */
	public static abstract function assertJsonStringEqualsJsonString($expectedJson, $actualJson, 
		string $message = '');
	
	/**
	 * Asserts that two given JSON encoded objects or arrays are not equal.
	 *
	 * @param string $expectedJson
	 * @param string $actualJson
	 * @param string $message
	 */
	public static abstract function assertJsonStringNotEqualsJsonString($expectedJson, 
		$actualJson, string $message = '');
	
	/**
	 * Asserts that the generated JSON encoded object and the content of the given file are equal.
	 *
	 * @param string $expectedFile
	 * @param string $actualJson
	 * @param string $message
	 */
	public static abstract function assertJsonStringEqualsJsonFile($expectedFile, $actualJson, 
		string $message = '');
	
	/**
	 * Asserts that the generated JSON encoded object and the content of the given file are not equal.
	 *
	 * @param string $expectedFile
	 * @param string $actualJson
	 * @param string $message
	 */
	public static abstract function assertJsonStringNotEqualsJsonFile($expectedFile, $actualJson, 
		string $message = '');
	
	/**
	 * Asserts that two JSON files are not equal.
	 *
	 * @param string $expectedFile
	 * @param string $actualFile
	 * @param string $message
	 */
	public static abstract function assertJsonFileNotEqualsJsonFile($expectedFile, $actualFile, 
		string $message = '');
	
	/**
	 * Asserts that two JSON files are equal.
	 *
	 * @param string $expectedFile
	 * @param string $actualFile
	 * @param string $message
	 */
	public static abstract function assertJsonFileEqualsJsonFile($expectedFile, $actualFile, 
		string $message = '');
	
	/**
	 * Mark the test as incomplete.
	 *
	 * @param string $message
	 * @throws PHPUnit_Framework_IncompleteTestError
	 * @since Method available since Release 3.0.0
	 */
	public static abstract function markTestIncomplete($message = '');
	
	/**
	 * Mark the test as skipped.
	 *
	 * @param string $message
	 * @throws PHPUnit_Framework_SkippedTestError
	 * @since Method available since Release 3.0.0
	 */
	public static abstract function markTestSkipped($message = '');
	
	// From PhpUnit_Framework_TestCase
	
	/**
	 * @param string $expectedRegex
	 *
	 * @since Method available since Release 3.6.0
	 *
	 * @throws PHPUnit_Framework_Exception
	 */
	public abstract function expectOutputRegex($expectedRegex);
	
	/**
	 * @param string $expectedString
	 *
	 * @since Method available since Release 3.6.0
	 */
	public abstract function expectOutputString($expectedString);
	
	/**
	 * @param mixed $exceptionName
	 * @param string $exceptionMessage
	 * @param int $exceptionCode
	 *
	 * @since Method available since Release 3.2.0
	 */
	public abstract function setExpectedException($exceptionName, $exceptionMessage = '', 
		$exceptionCode = null);
	
	/**
	 * @param mixed $exceptionName
	 * @param string $exceptionMessageRegExp
	 * @param int $exceptionCode
	 *
	 * @since Method available since Release 4.3.0
	 */
	public abstract function setExpectedExceptionRegExp($exceptionName, $exceptionMessageRegExp = '', 
		$exceptionCode = null);
	
	/**
	 * Returns a mock object for the specified class.
	 *
	 * @param string $originalClassName Name of the class to mock.
	 * @param array|null $methods When provided, only methods whose names are in the array
	 * are replaced with a configurable test double. The behavior
	 * of the other methods is not changed.
	 * Providing null means that no methods will be replaced.
	 * @param array $arguments Parameters to pass to the original class' constructor.
	 * @param string $mockClassName Class name for the generated test double class.
	 * @param bool $callOriginalConstructor Can be used to disable the call to the original class' constructor.
	 * @param bool $callOriginalClone Can be used to disable the call to the original class' clone constructor.
	 * @param bool $callAutoload Can be used to disable __autoload() during the generation of the test double class.
	 * @param bool $cloneArguments
	 * @param bool $callOriginalMethods
	 *
	 * @return PHPUnit_Framework_MockObject_MockObject
	 *
	 * @throws PHPUnit_Framework_Exception
	 *
	 * @since Method available since Release 3.0.0
	 */
	public abstract function getMock($originalClassName, $methods = [], array $arguments = [], 
		$mockClassName = '', $callOriginalConstructor = true, $callOriginalClone = true, 
		$callAutoload = true, $cloneArguments = false, $callOriginalMethods = false);
	
	/**
	 * Returns a mock object for the specified class.
	 *
	 * @param string $originalClassName
	 *
	 * @return PHPUnit_Framework_MockObject_MockObject
	 *
	 * @throws PHPUnit_Framework_Exception
	 *
	 * @since Method available since Release 5.0.0
	 */
	public abstract function getMockWithoutInvokingTheOriginalConstructor($originalClassName);
	
	/**
	 * Mocks the specified class and returns the name of the mocked class.
	 *
	 * @param string $originalClassName
	 * @param array $methods
	 * @param array $arguments
	 * @param string $mockClassName
	 * @param bool $callOriginalConstructor
	 * @param bool $callOriginalClone
	 * @param bool $callAutoload
	 * @param bool $cloneArguments
	 *
	 * @return string
	 *
	 * @throws PHPUnit_Framework_Exception
	 *
	 * @since Method available since Release 3.5.0
	 */
	protected abstract function getMockClass($originalClassName, $methods = [], array $arguments = [], 
		$mockClassName = '', $callOriginalConstructor = false, $callOriginalClone = true, 
		$callAutoload = true, $cloneArguments = false);
	
	/**
	 * Returns a mock object for the specified abstract class with all abstract
	 * methods of the class mocked. Concrete methods are not mocked by default.
	 * To mock concrete methods, use the 7th parameter ($mockedMethods).
	 *
	 * @param string $originalClassName
	 * @param array $arguments
	 * @param string $mockClassName
	 * @param bool $callOriginalConstructor
	 * @param bool $callOriginalClone
	 * @param bool $callAutoload
	 * @param array $mockedMethods
	 * @param bool $cloneArguments
	 *
	 * @return PHPUnit_Framework_MockObject_MockObject
	 *
	 * @since Method available since Release 3.4.0
	 *
	 * @throws PHPUnit_Framework_Exception
	 */
	public abstract function getMockForAbstractClass($originalClassName, array $arguments = [], 
		$mockClassName = '', $callOriginalConstructor = true, $callOriginalClone = true, 
		$callAutoload = true, $mockedMethods = [], $cloneArguments = false);
	
	/**
	 * Returns a mock object based on the given WSDL file.
	 *
	 * @param string $wsdlFile
	 * @param string $originalClassName
	 * @param string $mockClassName
	 * @param array $methods
	 * @param bool $callOriginalConstructor
	 * @param array $options An array of options passed to SOAPClient::_construct
	 *
	 * @return PHPUnit_Framework_MockObject_MockObject
	 *
	 * @since Method available since Release 3.4.0
	 */
	protected abstract function getMockFromWsdl($wsdlFile, $originalClassName = '', $mockClassName = '', 
		array $methods = [], $callOriginalConstructor = true, array $options = []);
	
	/**
	 * Returns a mock object for the specified trait with all abstract methods
	 * of the trait mocked. Concrete methods to mock can be specified with the
	 * `$mockedMethods` parameter.
	 *
	 * @param string $traitName
	 * @param array $arguments
	 * @param string $mockClassName
	 * @param bool $callOriginalConstructor
	 * @param bool $callOriginalClone
	 * @param bool $callAutoload
	 * @param array $mockedMethods
	 * @param bool $cloneArguments
	 *
	 * @return PHPUnit_Framework_MockObject_MockObject
	 *
	 * @since Method available since Release 4.0.0
	 *
	 * @throws PHPUnit_Framework_Exception
	 */
	public abstract function getMockForTrait($traitName, array $arguments = [], $mockClassName = '', 
		$callOriginalConstructor = true, $callOriginalClone = true, $callAutoload = true, 
		$mockedMethods = [], $cloneArguments = false);
	
	/**
	 * Returns an object for the specified trait.
	 *
	 * @param string $traitName
	 * @param array $arguments
	 * @param string $traitClassName
	 * @param bool $callOriginalConstructor
	 * @param bool $callOriginalClone
	 * @param bool $callAutoload
	 * @param bool $cloneArguments
	 *
	 * @return object
	 *
	 * @since Method available since Release 3.6.0
	 *
	 * @throws PHPUnit_Framework_Exception
	 */
	protected abstract function getObjectForTrait($traitName, array $arguments = [], $traitClassName = '', 
		$callOriginalConstructor = true, $callOriginalClone = true, $callAutoload = true, 
		$cloneArguments = false);
	
	/**
	 * This method is called before the first test of this test class is run.
	 *
	 * @since Method available since Release 3.4.0
	 */
	public static abstract function setUpBeforeClass();
	
	/**
	 * Sets up the fixture, for example, open a network connection.
	 * This method is called before a test is executed.
	 */
	protected abstract function setUp();
	
	/**
	 * Performs assertions shared by all tests of a test case.
	 *
	 * This method is called before the execution of a test starts
	 * and after setUp() is called.
	 *
	 * @since Method available since Release 3.2.8
	 */
	protected abstract function assertPreConditions();
	
	/**
	 * Performs assertions shared by all tests of a test case.
	 *
	 * This method is called before the execution of a test ends
	 * and before tearDown() is called.
	 *
	 * @since Method available since Release 3.2.8
	 */
	protected abstract function assertPostConditions();
	
	/**
	 * Tears down the fixture, for example, close a network connection.
	 * This method is called after a test is executed.
	 */
	protected abstract function tearDown();
	
	/**
	 * This method is called after the last test of this test class is run.
	 *
	 * @since Method available since Release 3.4.0
	 */
	public static abstract function tearDownAfterClass();
	
}

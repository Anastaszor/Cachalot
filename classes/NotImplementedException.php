<?php

/**
 * NotImplementedException class file.
 *
 * Dummy exception thrown everywhere that a functionnality is not implemented.
 *
 * @author Anasatszor
 */
class NotImplementedException extends CachalotException
{
	
	/**
	 * Constructs a new N.I.X.
	 * @param string $message
	 * @param string $code
	 * @param string $previous
	 */
	public function __construct ($message = null, $code = null, $previous = null)
	{
		if($message === null)
		{
			$message = 'This method ({method}) is not implemented.';
			$trace = debug_backtrace();
			if(isset($trace[1]))
			{
				$previous = $trace[1];
				$func_call = '';
				if(isset($previous['class']) && isset($previous['type']))
				{
					$func_call .= $previous['class'].$previous['type'];
				}
				if(isset($previous['function']))
				{
					$func_call .= $previous['function'];
				}
				$message = str_replace('{method}', $func_call, $message);
			}
		}
		parent::__construct($message, $code, $previous);
	}
	
}

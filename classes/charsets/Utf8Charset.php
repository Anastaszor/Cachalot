<?php

/**
 * Utf8Charset class file.
 * 
 * @author Anastaszor
 */
class Utf8Charset extends CachalotObject implements ICharset
{
	
	/**
	 * (non-PHPdoc)
	 * @see ICharset::getName()
	 */
	public function getName()
	{
		return 'utf-8';
	}
	
}

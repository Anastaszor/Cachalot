<?php

/**
 * CharsetFactory class file.
 * 
 * @author Anastaszor
 */
class CharsetFactory extends CachalotObject implements ICharsetFactory
{
	
	private $_names = array(
		'utf8' => 'Utf8Charset',
		'utf-8' => 'Utf8Charset',
		'utf_8' => 'Utf8Charset',
	);
	
	/**
	 * (non-PHPdoc)
	 * @see ICharsetFactory::getDefaultCharset()
	 */
	public function getDefaultCharset()
	{
		return new Utf8Charset();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICharsetFactory::getCharset()
	 */
	public function getCharset($name)
	{
		$sname = strtolower($name);
		if(isset($this->_names[$sname]))
		{
			$classname = $this->_names[$sname];
			return new $classname();
		}
		return null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICharsetFactory::getCharsets()
	 */
	public function getCharsets()
	{
		$objects = array();
		foreach(array_unique(array_values($this->_names)) as $name)
		{
			$objects[] = new $name();
		}
		return $objects;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICharsetFactory::isCharsetAvailable()
	 */
	public function isCharsetAvailable($name)
	{
		$sname = strtolower($name);
		return isset($this->_names[$sname]);
	}
	
}

<?php

/**
 * CUtil class file.
 * 
 * @author Anastaszor
 */
final class CUtil
{
	
	public static function removeRecursiveDir($folder)
	{
		if(is_dir($folder) && is_writable($folder))	// cleanup of all previous tests
		{
			$it = new RecursiveDirectoryIterator($folder, RecursiveDirectoryIterator::SKIP_DOTS);
			$files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
			foreach($files as $file)
			{
				if ($file->isDir())
				{
					rmdir($file->getRealPath());
				}
				else
				{
					unlink($file->getRealPath());
				}
			}
			return rmdir($folder);
		}
		if(is_file($folder) && is_writable($folder))
			return unlink($folder);
		return false;
	}
	
}

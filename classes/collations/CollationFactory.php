<?php

/**
 * CollationFactory class file.
 * 
 * @author Anastaszor
 */
class CollationFactory extends CachalotObject implements ICollationFactory
{
	
	private $_names = array(
		'utf8_general_cs' => 'Utf8_general_csCollation',
	);
	
	/**
	 * (non-PHPdoc)
	 * @see ICollationFactory::getDefaultCollation()
	 */
	public function getDefaultCollation()
	{
		return new Utf8_general_csCollation();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICollationFactory::getCollation()
	 */
	public function getCollation($name)
	{
		$sname = strtolower($name);
		if(isset($this->_names[$sname]))
		{
			$classname = $this->_names[$sname];
			return new $classname();
		}
		return null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICollationFactory::getCollations()
	 */
	public function getCollations(ICharset $charset = null)
	{
		if($charset === null)
		{
			$objects = array();
			foreach(array_unique(array_values($this->_names)) as $name)
			{
				$objects[] = new $name();
			}
			return $objects;
		}
		$sname = strtolower($charset->getName());
		$objects = array();
		foreach($this->getCollations(null) as $collation)
		{
			/* @var $collation ICollation */
			if(strpos(strtolower($collation->getName()), $sname) !== false)
			{
				$objects[] = $collation;
			}
		}
		return $objects;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICollationFactory::isCollationAvailable()
	 */
	public function isCollationAvailable($name)
	{
		$sname = strtolower($name);
		return isset($this->_names[$sname]);
	}
	
}

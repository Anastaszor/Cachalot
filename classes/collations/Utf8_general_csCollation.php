<?php

/**
 * Utf8_general_csCollation class file.
 * 
 * @author Anastaszor
 */
class Utf8_general_csCollation extends CachalotObject implements ICollation
{
	
	/**
	 * (non-PHPdoc)
	 * @see ICollation::getName()
	 */
	public function getName()
	{
		return 'utf8_general_cs';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICollation::getCharset()
	 */
	public function getCharset()
	{
		return new Utf8Charset();
	}
	
}

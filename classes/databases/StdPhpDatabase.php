<?php

/**
 * StdPhpDatabase class file.
 *
 * @author Anastaszor
 */
class StdPhpDatabase extends CachalotObject implements IDatabase
{
	
	/**
	 * 
	 * @var string
	 */
	private $_base_path = null;
	/**
	 * 
	 * @var string
	 */
	private $_name = null;
	/**
	 * 
	 * @var IEngine
	 */
	private $_collation = null;
	/**
	 * 
	 * @var ICharset
	 */
	private $_charset = null;
	/**
	 * 
	 * @var IEngine
	 */
	private $_engine = null;
	
	/**
	 * (non-PHPdoc)
	 * @see IDatabase::getDatabaseManager()
	 */
	public function getDatabaseManager()
	{
		return $this->cachalot->getDatabaseManager();
	}
	/**
	 * (non-PHPdoc)
	 * @see IDatabase::getTableManager()
	 */
	public function getTableManager()
	{
		if($this->_tablemgr === null)
		{
			$this->_tablemgr = $this->_engine->getTableManager($this);
		}
		return $this->_tablemgr;
	}
	/**
	 * (non-PHPdoc)
	 * @see IDatabase::getPath()
	 */
	public function getPath()
	{
		return $this->_base_path.DIRECTORY_SEPARATOR.$this->getHash();
	}
	
	/**
	 * 
	 * @param string $path
	 * @throws CachalotException
	 */
	public function setBasePath($path)
	{
		if(!is_dir($path))
			throw new CachalotException(Cachalot::t(
				'The directory {path} does not exists.', 
				array('{path}'=>$path)
			));
		$this->_base_path = $path;
	}
	
	/**
	 * Gets the hash value for a database name. This way, we are sure that
	 * every filesystem will accept the hash as folder name, without fear to
	 * find a reserved word (like "com", i'm lookin' at you windows...) and
	 * without fear of any non ascii chars like chinese or stgh.
	 * @param string $name of the database
	 * @return string reduced name of the database
	 */
	public function getHash()
	{
		// assuming a hash function is completely random, probability of a
		// collision is lower to :
		// p < n(n-1)/(2^(k+1))		(@see birthday paradox)
		// where n is the cardinal of the set where to find collisions
		// and where k is the number of bits of data
		// by truncating in quarter the length of the data here, k=120/4=30
		// probability of a collision for a set of 1000 dbnames < 4.65E-4
		return substr(sha1($this->_name, false), 0, 10);
	}
	
	public function setName($name)
	{
		$this->_name = $name;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDatabase::getName()
	 */
	public function getName()
	{
		return $this->_name;
	}
	
	public function setCharacterSet($characterSet)
	{
		$this->_charset = $characterSet;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDatabase::getCharacterSet()
	 */
	public function getCharacterSet()
	{
		return $this->_charset;
	}
	
	public function setCollation($collation)
	{
		$this->_collation = $collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDatabase::getCollation()
	 */
	public function getCollation()
	{
		return $this->_collation;
	}
	
	public function setEngine(IEngine $engine)
	{
		$this->_engine = $engine;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDatabase::getEngine()
	 */
	public function getEngine()
	{
		return $this->_engine;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CachalotObject::equals()
	 */
	public function equals(CachalotObject $object)
	{
		if($object instanceof self)
		{
			return $this->_name == $object->_name;
		}
		return false;
	}
	
}

<?php

/**
 * DatabaseFactory class file.
 * 
 * @author Anastaszor
 */
class DatabaseFactory extends CachalotObject implements IDatabaseFactory
{
	/**
	 * Dependancy injection.
	 * @var Cachalot
	 */
	public $cachalot = null;
	/**
	 * The file where all metadata about the databases will be stored.
	 * @var string
	 */
	public $fileName = 'db_repository';
	
	/**
	 * All the Databases Objects.
	 * @var IDatabase[]
	 */
	private $_databaseMap = array();
	
	/**
	 * (non-PHPdoc)
	 * @see CachalotObject::init()
	 */
	public function init()
	{
		$this->_databaseMap = array();	// reinit
		if(file_exists($this->getRepositoryPath()))
		{
			$map = require $this->getRepositoryPath();
		}
		else
			$this->updateRepositoryFile();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDatabaseFactory::getDatabase()
	 */
	public function getDatabase($dbname)
	{
		foreach($this->_databaseMap as $database)
		{
			if($database->getName() == $dbname)
				return $database;
		}
		return null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDatabaseFactory::getDatabases()
	 */
	public function getDatabases()
	{
		return $this->_databaseMap;
	}
	
	/**
	 * 
	 */
	protected function updateRepositoryFile()
	{
		$strToWrite = "<?php return ".var_export($this->_databaseMap, true).";\n";
		file_put_contents($this->getRepositoryPath(), $strToWrite, LOCK_EX);
	}
	
	/**
	 * 
	 * @return ICharset
	 */
	protected function getDefaultCharset()
	{
		return $this->cachalot->getCharsetFactory()->getDefaultCharset();
	}
	
	/**
	 * 
	 * @return ICollation
	 */
	protected function getDefaultCollation()
	{
		return $this->cachalot->getCollationFactory()->getDefaultCollation();
	}
	
	/**
	 * 
	 * @return IEngine
	 */
	protected function getDefaultEngine()
	{
		return $this->cachalot->getEngineFactory()->getDefaultEngine();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDatabaseFactory::getPath()
	 */
	public function getPath()
	{
		return $this->cachalot->getBasePath();
	}
	
	/**
	 * Gets the full path of the repository file.
	 * @throws CachalotException
	 * @return string
	 */
	protected function getRepositoryPath()
	{
		return $this->getPath().DIRECTORY_SEPARATOR.$this->fileName.'.php';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDatabaseFactory::createDatabase()
	 */
	public function createDatabase(ICreateDatabaseStatement $statement)
	{
		$database = new StdPhpDatabase();
		$database->setName($statement->getDatabaseName());
		if($statement->getCharacterSet() !== null)
			$database->setCharacterSet($statement->getCharacterSet());
		else
			$database->setCharacterSet($this->getDefaultCharset());
		if($statement->getCollation() !== null)
			$database->setCollation($statement->getCollation());
		else
			$database->setCollation($this->getDefaultCollation());
		if($statement->getEngine() !== null)
			$database->setEngine($statement->getEngine());
		else
			$database->setEngine($this->getDefaultEngine());
		$database->setBasePath($this->cachalot->getBasePath());
		$result = new StatementExecutionResult();
		foreach($this->_databaseMap as $refdb)
		{
			if($database->equals($refdb))
			{
				if($statement->getIfNotExists())
				{
					// database already exists and IF NOT EXISTS is specified
					// >> do nothing, success
					$result->success();
					return $result;
				}
				else
				{
					// database already exists and IF NOT EXISTS not specified
					// >> fail
					$result->fail();
					return $result;
				}
			}
		}
		// database does not exists, create it
		$path = $database->getPath();
		if(is_dir($path) || mkdir($path))
		{
			$this->_databaseMap[] = $database;
			$this->updateRepositoryFile();
			$result->success();
			return $result;
		}
		// cant be created
		$result->fail();
		return $result;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDatabaseFactory::dropDatabase()
	 */
	public function dropDatabase(IDropDatabaseStatement $statement)
	{
		$result = new StatementExecutionResult();
		
		foreach($this->_databaseMap as $key => $database)
		{
			if($database->getName() == $statement->getDatabaseName())
			{
				// database exists : drop it !
				$folder = $database->getPath();
				CUtil::removeRecursiveDir($folder);
				unset($this->_databaseMap[$key]);
				$this->updateRepositoryFile();
				$result->success();
				return $result;
			}
		}
		// database does not exists
		if($statement->getIfExists())
		{
			// if exists is specified : do nothing
			$result->success();
		}
		else
		{
			// if exists not specified : fail
			$result->fail();
		}
		return $result;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDatabaseFactory::showDatabases()
	 */
	public function showDatabases(IShowDatabasesStatement $statement)
	{
		$result = new StatementExecutionResult();
		foreach($this->_databaseMap as $database)
		{
			// TODO EVAL LIKE STATEMENT
			// TODO EVAL WHERE STATEMENT
			$result->addResult(array('name'=>$database->getName()));
		}
		$result->success();
		return $result;
	}
	
}

<?php

/**
 * StdPhpTableManager interface file.
 *
 * @author Anastaszor
 */
class StdPhpTableManager__OLD extends CachalotObject implements ITableManager
{
	
	/**
	 * Dependancy Injection !
	 * @var Cachalot
	 */
	public $cachalot = null;
	
	public $fileName = 'tb_repository';
	
	/**
	 * 
	 * @var IEngine
	 */
	private $_engine = null;
	/**
	 * (non-PHPdoc)
	 * @see ITableManager::setEngine()
	 */
	public function setEngine(IEngine $engine)
	{
		$this->_engine = $engine;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableManager::getEngine()
	 */
	public function getEngine()
	{
		return $this->_engine;
	}
	
	/**
	 * 
	 * @var IDatabase
	 */
	private $_database = null;
	/**
	 * (non-PHPdoc)
	 * @see ITableManager::setDatabase()
	 */
	public function setDatabase(IDatabase $database)
	{
		$this->_database = $database;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableManager::getDatabase()
	 */
	public function getDatabase()
	{
		return $this->_database;
	}
	
	/**
	 * All the Table Objects.
	 * @var ITable[]
	 */
	private $_tableMap = array();
	/**
	 * All the Tables that are temporary. They will be deleted once the 
	 * request has finished processing.
	 * @var string[]
	 */
	private $_temporary_tables = array();
	/**
	 * 
	 * @throws CachalotException
	 * @return string
	 */
	public function getRepositoryPath()
	{
		if($this->getDatabase() === null)
		{
			throw new CachalotException(Cachalot::t('The database is not set.'));
		}
		return $this->getDatabase()->getPath().DIRECTORY_SEPARATOR.$this->fileName.'.php';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CachalotObject::init()
	 */
	public function init()
	{
		$this->_tableMap = array();	// reinit
		if(file_exists($this->getRepositoryPath()))
		{
			$map = require $this->getRepositoryPath();
			foreach($map as $object)
			{
				if(!isset($object['engine']))
				{
					throw new CachalotException(Cachalot::t('The engine is not set.'));
				}
				$name = $object['engine'];
				/* @var $engine IEngine */
				$engine = $this->cachalot->getEngineManager()->getEngine($name);
				if($engine === null)
				{
					throw new CachalotException(Cachalot::t('The base path is not set.'));
				}
				$object['engine'] = $engine;
				$table = $engine->createNewTable();
				/* @var $table CachalotObject */
				$table->__fromArray($object);
				/* @var $table ITable */
				$table->setTableManager($this);
				$this->_tableMap[] = $table;
			}
		}
		else
			$this->updateRepositoryFile();
	}
	
	protected function updateRepositoryFile()
	{
		$tbs = array();
		foreach(array_merge($this->_tableMap, $this->_temporary_tables) as $tb)
		{
			/* @var $tb CachalotObject */
			$tbs[] = $tb->__toArray();
		}
		$strToWrite = "<?php return ".var_export($tbs, true).";\n";
		
		$file = fopen($this->getRepositoryPath(), 'w');
		flock($file, LOCK_EX);
		fwrite($file, $strToWrite);
		flock($file, LOCK_UN);
		fclose($file);
	}
	
	protected function getTable($tablename)
	{
		foreach($this->_tableMap as $table)
		{
			if($table->getName() === $tablename)
			{
				return $table;
			}
		}
		return $this->getTemporaryTable($tablename);
	}
	
	protected function getTemporaryTable($tablename)
	{
		foreach($this->_temporary_tables as $table)
		{
			if($table->getName() === $tablename)
			{
				return $table;
			}
		}
		return null;
	}
	
	protected function removeTable(ITable $ttremove)
	{
		foreach($this->_tableMap as $key => $table)
		{
			if($table->getName() === $ttremove->getName())
			{
				unset($this->_tableMap[$key]);
			}
		}
		foreach($this->_temporary_tables as $key => $table)
		{
			if($table->getName() === $ttremove->getName())
			{
				unset($this->_temporary_tables[$key]);
			}
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ITableManager::create()
	 */
	public function create(ICreateTableStatement $statement)
	{
		if(($name = $statement->getTableOptionsStatement()->getEngine()) !== null)
		{
			$engine = $this->cachalot->getEngineManager()->getEngine($name);
		}
		else
		{
			$engine = $this->cachalot->getEngineManager()->getDefaultEngine();
		}
		/* @var $table ITable */
		$table = $engine->createNewTable();
		$table->setTableManager($this);
		$table->setName($statement->getTableName());
		$options = $statement->getTableOptionsStatement();
		
		$table->setAutoIncrement($options->getAutoIncrement());
		$table->setAverageRowLength($options->getAverageRowLength());
		$table->setChecksum($options->getChecksum());
		$table->setComment($options->getComment());
		$table->setConnectionString($options->getConnectionString());
		$table->setDataDirectory($options->getDataDirectory());
		if(($val = $options->getDefaultCharacterSet()) !== null)
		{
			if($this->cachalot->getCharsetManager()->isCharsetAvailable($val))
			{
				$table->setCharset($val);
			}
			else
			{
				throw new CachalotException(Cachalot::t(
					'Impossible to find engine "{name}".', 
					array('{name}'=>$val)
				));
			}
		}
		else
		{
			$table->setCharset($this->cachalot->getCharsetManager()->getDefaultCharset());
		}
		if(($val = $options->getDefaultCollation()) !== null)
		{
			if($this->cachalot->getCollationManager()->isCollationAvailable($val))
			{
				$table->setCollation($val);
			}
			else
			{
				throw new CachalotException(Cachalot::t(
					'Impossible to find collation "{name}".',
					array('{name}'=>$val)
				));
			}
		}
		else
		{
			$table->setCollation($this->cachalot->getCollationManager()->getDefaultCollation());
		}
		$table->setDelayKeyWrite($options->getDelayKeyWrite());
		$table->setIndexDirectory($options->getIndexDirectory());
		$table->setInsertMethod($options->getInsertMethod());
		$table->setKeyBlockSize($options->getKeyBlockSize());
		$table->setMaxRows($options->getMaxRows());
		$table->setMinRows($options->getMinRows());
		$table->setPackKeys($options->getPackKeys());
		$table->setPassword($options->getPassword());
		$table->setRowFormat($options->getRowFormat());
		$table->setTablespace($options->getTablespace());
		$table->setTablespaceStorage($options->getTablespaceStorage());
		foreach($options->getUnionTableNames() as $uniontablename)
		{
			throw new NotImplementedException();
		}
		$tbpath = $table->getPath();
		if(file_exists($tbpath))
		{
			if($statement->getIfNotExists())
			{
				// CREATE DATABASE IF NOT EXISTS : database exists, do nothing
			}
			else
			{
				throw new CachalotException(Cachalot::t(
						'Table {tbname} already exists !',
						array('{tbname}'=>$database->getName())
				));
			}
		}
		else
		{
			mkdir($tbpath);
			$this->_tableMap[] = $table;
			$this->updateRepositoryFile();
		}
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ITableManager::show()
	 */
	public function show(IShowTablesStatement $statement)
	{
		if($statement->getShowLikeStatement() !== null)
		{
			throw new NotImplementedException();
		}
		if($statement->getWhereStatement() !== null)
		{
			throw new NotImplementedException();
		}
		$tables = array_merge($this->_tableMap, $this->_temporary_tables);
		$tablenames = array();
		foreach($tables as $table)
		{
			$tablenames[] = $table->getName();
		}
		return array('Tables_in_'.$this->getDatabase()->getName() => $tablenames);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ITableManager::drop()
	 */
	public function drop(IDropTableStatement $statement)
	{
		// always existant, foreach from database manager
		$tablename = $statement->getFirstTableName();
		if($statement->getTemporary())
		{
			$table = $this->getTemporaryTable($tablename);
		}
		else
		{
			$table = $this->getTable($tablename);
		}
		if($table === null)
		{
			if($statement->getIfExists())
			{
				// does nothing
				return;
			}
			else
			{
				throw new CachalotException(Cachalot::t(
					'Impossible to find table "{table}".',
						array('{table}'=>$tablename)
				));
			}
		}
		$folder = $table->getPath();
		$this->removeTable($table);
		$this->updateRepositoryFile();
		return CUtil::removeRecursiveDir($folder);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CachalotObject::__destruct()
	 */
	public function __destruct()
	{
		foreach($this->_temporary_tables as $temptablename)
		{
			$statement = new DropTableStatement();
			$statement->setTemporary(true);
			$statement->addTableName($temptablename);
			$this->drop($statement);
		}
		parent::__destruct();
	}
	
}

<?php

/**
 * StatementExecutionResult class file.
 * 
 * @author Anastaszor
 */
class StatementExecutionResult 
		extends CachalotObject implements IStatementExecutionResult
{
	
	/**
	 * 
	 * @var boolean
	 */
	private $_success = null;
	/**
	 * 
	 * @var string
	 */
	private $_message = null;
	/**
	 * 
	 * @var string[][]
	 */
	private $_results = array();
	
	public function success()
	{
		$this->_success = true;
	}
	
	public function fail()
	{
		$this->_success = false;
	}
	
	public function setMessage($message)
	{
		$this->_message = $message;
	}
	
	public function addResult(array $data)
	{
		$this->_results[] = $data;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatementExecutionResult::isSuccessful()
	 */
	public function isSuccessful()
	{
		return $this->_success;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatementExecutionResult::getMessage()
	 */
	public function getMessage()
	{
		return $this->_message;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatementExecutionResult::getResultIterator()
	 */
	public function getResultIterator()
	{
		return new ResultIterator(new ArrayIterator($this->_results));
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatementExecutionResult::getValues()
	 */
	public function getValues()
	{
		return $this->_results;
	}
	
}

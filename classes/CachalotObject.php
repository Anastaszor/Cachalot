<?php

/**
 * CachalotObject class file.
 * 
 * CachalotObject is the base class for all components.
 *
 * A property is defined by a getter method, and/or a setter method.
 * Properties can be accessed in the way like accessing normal object members.
 * Reading or writing a property will cause the invocation of the corresponding
 * getter or setter method, e.g
 * <pre>
 * $a=$object->text;     // equivalent to $a=$object->getText();
 * $object->text='abc';  // equivalent to $object->setText('abc');
 * </pre>
 * The signatures of getter and setter methods are as follows,
 * <pre>
 * // getter, defines a readable property 'text'
 * public function getText() { ... }
 * // setter, defines a writable property 'text' with $value to be set to the property
 * public function setText($value) { ... }
 * </pre>
 *
 * Property names are case-insensitive.
 *
 * @author Anastaszor
 */
class CachalotObject implements ArrayAccess, Countable, Serializable
{
	
	/**
	 * Returns a property value, an event handler list or a behavior based on its name.
	 * Do not call this method. This is a PHP magic method that we override
	 * to allow using the following syntax to read a property or obtain event handlers:
	 * <pre>
	 * $value=$object->propertyName;
	 * $handlers=$object->eventName;
	 * </pre>
	 * @param string $name the property name or event name
	 * @return mixed the property value, event handlers attached to the event, or the named behavior
	 * @throws CachalotException if the property or event is not defined
	 * @see __set
	 */
	public function __get($name)
	{
		$getter='get'.ucfirst($name);
		if(method_exists($this,$getter))
			return $this->$getter();
		throw new CachalotException(Cachalot::t('Property "{class}.{property}" is not defined.',
				array('{class}'=>get_class($this), '{property}'=>$name)));
	}
	
	/**
	 * Sets value of a component property.
	 * Do not call this method. This is a PHP magic method that we override
	 * to allow using the following syntax to set a property or attach an event handler
	 * <pre>
	 * $this->propertyName=$value;
	 * $this->eventName=$callback;
	 * </pre>
	 * @param string $name the property name or the event name
	 * @param mixed $value the property value or callback
	 * @return mixed
	 * @throws CachalotException if the property/event is not defined or the property is read only.
	 * @see __get
	 */
	public function __set($name, $value)
	{
		$setter='set'.ucfirst($name);
		if(method_exists($this,$setter))
			return $this->$setter($value);
		if(method_exists($this,'get'.ucfirst($name)))
			throw new CachalotException(Cachalot::t('Property "{class}.{property}" is read only.',
				array('{class}'=>get_class($this), '{property}'=>$name)));
		else
			throw new CachalotException(Cachalot::t('Property "{class}.{property}" is not defined.',
				array('{class}'=>get_class($this), '{property}'=>$name)));
	}
	
	/**
	 * Checks if a property value is null.
	 * Do not call this method. This is a PHP magic method that we override
	 * to allow using isset() to detect if a component property is set or not.
	 * @param string $name the property name or the event name
	 * @return boolean
	 */
	public function __isset($name)
	{
		$getter='get'.ucfirst($name);
		if(method_exists($this,$getter))
			return $this->$getter()!==null;
		return false;
	}
	
	/**
	 * Sets a component property to be null.
	 * Do not call this method. This is a PHP magic method that we override
	 * to allow using unset() to set a component property to be null.
	 * @param string $name the property name or the event name
	 * @throws CachalotException if the property is read only.
	 * @return mixed
	 */
	public function __unset($name)
	{
		$setter='set'.ucfirst($name);
		if(method_exists($this,$setter))
			$this->$setter(null);
		elseif(method_exists($this,'get'.ucfirst($name)))
			throw new CachalotException(Cachalot::t('Property "{class}.{property}" is read only.',
				array('{class}'=>get_class($this), '{property}'=>$name)));
	}
	
	/**
	 * Calls the named method which is not a class method.
	 * Do not call this method. This is a PHP magic method that we override
	 * to implement the behavior feature.
	 * @param string $name the method name
	 * @param array $parameters method parameters
	 * @throws CachalotException if the closure does not exists
	 * @return mixed the method return value
	 */
	public function __call($name, $parameters)
	{
		if(class_exists('Closure', false) && $this->canGetProperty($name) && $this->$name instanceof Closure)
			return call_user_func_array($this->$name, $parameters);
		throw new CachalotException(Cachalot::t('{class} do not have a method or closure named "{name}".',
				array('{class}'=>get_class($this), '{name}'=>$name)));
	}
	
	/**
	 * Gets an object which is a copy of this, whenever possible. Read-only 
	 * vars will not be copied.
	 * @return CachalotObject
	 */
	public function __clone()
	{
		$obj = new self();
		$vars = get_object_vars($this);
		foreach($vars as $varname => $varvalue)
		{
			try
			{
				$obj->$varname = $varvalue;
			}
			catch(CachalotException $e) { /* silent */ }
		}
		return $obj;
	}
	
	/**
	 * Gets a string representation of this object which is human-readable
	 * @return string
	 */
	public function __toString()
	{
		return $this->serialize();
	}
	/**
	 * Uncompress the argument string and sets the fields of this object.
	 * @param string $string
	 * @return CachalotObject
	 */
	public function __fromString($string)
	{
		return $this->unserialize($string);
	}
	
	/**
	 * Gets an array representation of this object which is human-readable.
	 * @return mixed[]
	 */
	public function __toArray()
	{
		return get_object_vars($this);
	}
	/**
	 * Put all variables into the values array into the fields of this object.
	 * @param array $values
	 */
	public function __fromArray(array $values=array())
	{
		$keys = array_keys(get_object_vars($this));
		foreach($values as $key => $value)
		{
			if(in_array($key, $keys))
			{
				$this->$key = $value;
			}
		}
	}
	
	/**
	 * Determines whether a property can be read.
	 * A property can be read if the class has a getter method
	 * for the property name. Note, property name is case-insensitive.
	 * @param string $name the property name
	 * @return boolean whether the property can be read
	 * @see canSetProperty
	 */
	public function canGetProperty($name)
	{
		return method_exists($this,'get'.ucfirst($name)) || propertyexists($this, $name);
	}
	
	/**
	 * Determines whether a property can be set.
	 * A property can be written if the class has a setter method
	 * for the property name. Note, property name is case-insensitive.
	 * @param string $name the property name
	 * @return boolean whether the property can be written
	 * @see canGetProperty
	 */
	public function canSetProperty($name)
	{
		return method_exists($this,'set'.ucfirst($name)) || propertyexists($this, $name);
	}
	
	/**
	 * Evaluates a PHP expression or callback under the context of this component.
	 *
	 * Valid PHP callback can be class method name in the form of
	 * array(ClassName/Object, MethodName), or anonymous function (only available in PHP 5.3.0 or above).
	 *
	 * If a PHP callback is used, the corresponding function/method signature should be
	 * <pre>
	 * function foo($param1, $param2, ..., $object) { ... }
	 * </pre>
	 * where the array elements in the second parameter to this method will be passed
	 * to the callback as $param1, $param2, ...; and the last parameter will be the component itself.
	 *
	 * If a PHP expression is used, the second parameter will be "extracted" into PHP variables
	 * that can be directly accessed in the expression. See {@link http://us.php.net/manual/en/function.extract.php PHP extract}
	 * for more details. In the expression, the component object can be accessed using $this.
	 *
	 * @param mixed $_expression_ a PHP expression or PHP callback to be evaluated.
	 * @param array $_data_ additional parameters to be passed to the above expression/callback.
	 * @return mixed the expression result
	 */
	public function evaluateExpression($_expression_,$_data_=array())
	{
		if(is_string($_expression_))
		{
			extract($_data_);
			return eval('return '.$_expression_.';');
		}
		else
		{
			$_data_[]=$this;
			return call_user_func_array($_expression_, $_data_);
		}
	}
	
	/**
	 * Initializes this object after construction is completed.
	 * This method should be overridden by child classes.
	 */
	public function init()
	{
		// empty
	}
	
	/**
	 * Checks if two objects are equals. Basic implementation get says two
	 * objects are equals if their class are the same and if they hold the
	 * same values.
	 * @param CachalotObject $object
	 * @return boolean
	 */
	public function equals(CachalotObject $object)
	{
		$a = get_object_vars($this);
		$b = get_object_vars($object);
		return get_class($this) == get_class($object)
			&& array_diff($a, $b)===array() && array_diff($b, $a)===array();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ArrayAccess::offsetExists()
	 */
	public function offsetExists($offset)
	{
		return $this->__isset($offset);
	}
	/**
	 * (non-PHPdoc)
	 * @see ArrayAccess::offsetGet()
	 */
	public function offsetGet($offset)
	{
		return $this->__get($offset);
	}
	/**
	 * (non-PHPdoc)
	 * @see ArrayAccess::offsetSet()
	 */
	public function offsetSet($offset, $value)
	{
		return $this->__set($offset, $value);
	}
	/**
	 * (non-PHPdoc)
	 * @see ArrayAccess::offsetUnset()
	 */
	public function offsetUnset($offset)
	{
		return $this->__unset($offset);
	}
	/**
	 * (non-PHPdoc)
	 * @see Countable::count()
	 */
	public function count()
	{
		$count = 0;
		$vars = get_object_vars($this);
		foreach($vars as $var)
		{
			$count += ((int)($var !== null));
		}
		return $count;
	}
	/**
	 * (non-PHPdoc)
	 * @see Serializable::serialize()
	 */
	public function serialize() 
	{
		return @serialize($this);
	}
	/**
	 * (non-PHPdoc)
	 * @see Serializable::unserialize()
	 */
	public function unserialize($str)
	{
		return @unserialize($str);
	}
	
	public function __destruct()
	{
		$vars = get_object_vars($this);
		foreach($vars as $name => $value)
		{
			$this->cleanup($this, $name, $value);
		}
	}
	
	protected function cleanup($object, $name, $value, $level=0)
	{
		if(is_array($value))
		{
			if($level < 75)
			{
				foreach($value as $key => $var)
				{
					$this->cleanup($value, $key, $var, $level++);
				}
			} // prevent infinite recursion
		}
		if(is_array($object))
		{
			unset($object[$name]);
		}
		else
		{
			$object->$name = null;
		}
	}
	
}

// scope outside object : get_object_vars give only public properties
function propertyexists($object, $name)
{
	return in_array($name, array_keys(get_object_vars($object)));
}

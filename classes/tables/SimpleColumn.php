<?php

/**
 * SimpleColumn class file.
 * 
 * @author Anastaszor
 */
class SimpleColumn extends Column implements ISimpleColumn
{
	
	/**
	 * 
	 * @var boolean
	 */
	private $_auto_increment = false;
	/**
	 * 
	 * @var enum('FIXED', 'DYNAMIC', 'DEFAULT')
	 */
	private $_column_format = null;
	/**
	 * 
	 * @var IReference
	 */
	private $_reference = null;
	
	/**
	 * Sets whether this column has an auto-increment sequence.
	 * @param boolean $bool
	 */
	public function setAutoIncrement($bool)
	{
		$this->_auto_increment = true === $bool;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ISimpleColumn::getAutoIncrement()
	 */
	public function getAutoIncrement()
	{
		return $this->_auto_increment;
	}
	
	/**
	 * Sets the column format for this column.
	 * @param enum('FIXED', 'DYNAMIC', 'DEFAULT') $format
	 */
	public function setColumnFormat($format)
	{
		if($format === null 
			|| $format === self::COLUMN_FORMAT_DEFAULT
			|| $format === self::COLUMN_FORMAT_DYNAMIC
			|| $format === self::COLUMN_FORMAT_FIXED
		) {
			$this->_column_format = $format;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ISimpleColumn::getColumnFormat()
	 */
	public function getColumnFormat()
	{
		return $this->_column_format;
	}
	
	/**
	 * Sets the reference for this column.
	 * @param IReference $reference
	 */
	public function setReference(IReference $reference = null)
	{
		$this->_reference = $reference;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ISimpleColumn::getReference()
	 */
	public function getReference()
	{
		return $this->_reference;
	}
	
}

<?php

/**
 * StdPhpTable class file.
 *
 * @author Anastaszor
 */
class StdPhpTable extends CachalotObject implements ITable
{
	/**
	 * 
	 * @var ICharset
	 */
	private $_charset = null;
	/**
	 * 
	 * @var ICollation
	 */
	private $_collation = null;
	/**
	 * 
	 * @var IEngine
	 */
	private $_engine = null;
	
	/**
	 * 
	 * @var string
	 */
	private $_base_path = null;
	/**
	 * 
	 * @var string
	 */
	private $_name = null;
	private $_autoIncrement = null;
	private $_avgRowLength = null;
	private $_checksum = null;
	private $_comment = null;
	private $_connectionString = null;
	private $_dataDirectory = null;
	private $_delayKeyWrite = null;
	private $_indexDirectory = null;
	private $_insertMethod = null;
	private $_keyBlockSize = null;
	private $_maxRows = null;
	private $_minRows = null;
	private $_packKeys = null;
	private $_password = null;
	private $_rowFormat = null;
	private $_tablespace = null;
	private $_tablespaceStorage = null;
	/**
	 * 
	 * @var IColumn[]
	 */
	private $_columns = array();
	
	/**
	 * Gets the hash value for a database name. This way, we are sure that
	 * every filesystem will accept the hash as folder name, without fear to
	 * find a reserved word (like "com", i'm lookin' at you windows...) and
	 * without fear of any non ascii chars like chinese or stgh.
	 * @param string $name of the database
	 * @return string reduced name of the database
	 */
	public function getHash()
	{
		// assuming a hash function is completely random, probability of a
		// collision is lower to :
		// p < n(n-1)/(2^(k+1))		(@see birthday paradox)
		// where n is the cardinal of the set where to find collisions
		// and where k is the number of bits of data
		// by truncating in quarter the length of the data here, k=120/4=30
		// probability of a collision for a set of 1000 dbnames < 4.65E-4
		return substr(sha1($this->_name, false), 0, 10);
	}
	
	/**
	 * 
	 * @param string $path
	 */
	public function setBasePath($path)
	{
		$this->_base_path = $path;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->_name;
	}
	/**
	 *
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->_name = $name;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getCharset()
	{
		return $this->_charset;
	}
	/**
	 *
	 * @param string $value
	 */
	public function setCharset(ICharset $charset)
	{
		$this->_charset = $charset;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getCollation()
	{
		return $this->_collation;
	}
	/**
	 *
	 * @param string $value
	 */
	public function setCollation(ICollation $collation)
	{
		$this->_collation = $collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ITable::getEngine()
	 */
	public function getEngine()
	{
		return $this->_engine;
	}
	/**
	 * 
	 * @param IEngine $engine
	 */
	public function setEngine(IEngine $engine)
	{
		$this->_engine = $engine;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ITable::getPath()
	 */
	public function getPath()
	{
		return $this->_base_path.DIRECTORY_SEPARATOR.$this->getHash();
	}
	
	/**
	 *
	 * @param int $value
	 */
	public function setAutoIncrement($value)
	{
		if(((int) $value) > 0)
		{
			$this->_autoIncrement = (int) $value;
		}
	}
	/**
	 *
	 * @return int
	 */
	public function getAutoIncrement()
	{
		if($this->_autoIncrement === null)
			return $this->_engine->getDefaultTableAutoIncrement();
		return $this->_autoIncrement;
	}
	
	/**
	 *
	 * @param int $value
	 */
	public function setAverageRowLength($value)
	{
		if(((int) $value) >= 0)
		{
			$this->_avgRowLength = (int) $value;
		}
	}
	/**
	 *
	 * @return int
	 */
	public function getAverageRowLength()
	{
		if($this->_avgRowLength === null)
			return $this->_engine->getDefaultTableAverageRowLength();
		return $this->_avgRowLength;
	}
	
	/**
	 *
	 * @param boolean $value
	 */
	public function setChecksum($value)
	{
		$this->_checksum = true === $value;
	}
	/**
	 *
	 * @return boolean
	 */
	public function getChecksum()
	{
		if($this->_checksum === null)
			return $this->_engine->getDefaultTableChecksum();
		return $this->_checksum;
	}
	
	/**
	 *
	 * @param string $value
	 */
	public function setComment($value)
	{
		$this->_comment = $value;
	}
	/**
	 *
	 * @return string
	 */
	public function getComment()
	{
		if($this->_comment === null)
			return $this->_engine->getDefaultTableComment();
		return $this->_comment;
	}
	
	/**
	 *
	 * @param string $value
	 */
	public function setConnectionString($value)
	{
		$this->_connectionString = $value;
	}
	/**
	 *
	 * @return string
	 */
	public function getConnectionString()
	{
		if($this->_connectionString === null)
			return $this->_engine->getDefaultTableConnectionString();
		return $this->_connectionString;
	}
	
	/**
	 *
	 * @param string $value
	 */
	public function setDataDirectory($value)
	{
		if(preg_match('#^([A-Z]:\\|/)?[\w\d\-_]((\\|/)[\w\d\-_]+)*$#', $value))
		{
			$this->_dataDirectory = $value;
		}
	}
	/**
	 *
	 * @return string
	 */
	public function getDataDirectory()
	{
		if($this->_dataDirectory === null)
			return $this->_engine->getDefaultTableDataDirectory();
		return $this->_dataDirectory;
	}
	
	/**
	 *
	 * @param boolean $value
	 */
	public function setDelayKeyWrite($value)
	{
		$this->_delayKeyWrite = (bool) $value;
	}
	/**
	 *
	 * @return boolean
	 */
	public function getDelayKeyWrite()
	{
		if($this->_delayKeyWrite === null)
			return $this->_engine->getDefaultTableDelayKeyWrite();
		return $this->_delayKeyWrite;
	}
	
	/**
	 *
	 * @param string $value
	 */
	public function setIndexDirectory($value)
	{
		if(preg_match('#^([A-Z]:\\|/)?[\w\d\-_]((\\|/)[\w\d\-_]+)*$#', $value))
		{
			$this->_indexDirectory = $value;
		}
	}
	/**
	 * 
	 * @return string
	 */
	public function getIndexDirectory()
	{
		if($this->_indexDirectory === null)
			return $this->_engine->getDefaultTableIndexDirectory();
		return $this->_indexDirectory;
	}
	
	/**
	 *
	 * @param enum('NO', 'FIRST', 'LAST') $value
	 */
	public function setInsertMethod($value)
	{
		if($value === self::INSERT_METHOD_FIRST
			|| $value === self::INSERT_METHOD_LAST
			|| $value === self::INSERT_METHOD_NO
		) {
			$this->_insertMethod = $value;
		}
	}
	/**
	 *
	 * @return enum('NO', 'FIRST', 'LAST')
	 */
	public function getInsertMethod()
	{
		if($this->_insertMethod === null)
			return $this->_engine->getDefaultTableInsertMethod();
		return $this->_insertMethod;
	}
	
	/**
	 *
	 * @param int $value
	 */
	public function setKeyBlockSize($value)
	{
		if(((int) $value) >= 0)
		{
			$this->_keyBlockSize = (int) $value;
		}
	}
	/**
	 *
	 * @return int
	 */
	public function getKeyBlockSize()
	{
		if($this->_keyBlockSize === null)
			return $this->_engine->getDefaultTableKeyBlockSize();
		return $this->_keyBlockSize;
	}
	
	/**
	 *
	 * @param int $value
	 */
	public function setMaxRows($value)
	{
		if(((int) $value) >= 0)
		{
			$this->_maxRows = (int) $value;
		}
	}
	/**
	 *
	 * @return int
	 */
	public function getMaxRows()
	{
		if($this->_maxRows === null)
			return $this->_engine->getDefaultTableMaxRows();
		return $this->_maxRows;
	}
	
	/**
	 *
	 * @param int $value
	 */
	public function setMinRows($value)
	{
		if(((int) $value) >= 0)
		{
			$this->_minRows = (int) $value;
		}
	}
	/**
	 *
	 * @return int
	 */
	public function getMinRows()
	{
		if($this->_minRows === null)
			return $this->_engine->getDefaultTableMinRows();
		return $this->_minRows;
	}
	
	/**
	 *
	 * @param enum(true, false, 'DEFAULT') $value
	 */
	public function setPackKeys($value)
	{
		if($value == self::PACK_KEYS_ON)
			$this->_packKeys = true;
		if($value == self::PACK_KEYS_OFF)
			$this->_packKeys = false;
		if($value === self::PACK_KEYS_DEFAULT)
			$this->_packKeys = self::PACK_KEYS_DEFAULT;
	}
	/**
	 *
	 * @return enum(true, false, 'DEFAULT')
	 */
	public function getPackKeys()
	{
		if($this->_packKeys === null)
			return $this->_engine->getDefaultTablePackKeys();
		return $this->_packKeys;
	}
	
	/**
	 *
	 * @param string $value
	 */
	public function setPassword($value)
	{
		$this->_password = $value;
	}
	/**
	 *
	 * @return string
	 */
	public function getPassword()
	{
		return $this->_password;
	}
	
	/**
	 *
	 * @param enum('COMPRESSED', 'REDUNDANT', 'COMPACT', 'DEFAULT', 'DYNAMIC', 'FIXED') $value
	 */
	public function setRowFormat($value)
	{
		if($value === self::ROW_FORMAT_COMPACT
			|| $value === self::ROW_FORMAT_COMPRESSED
			|| $value === self::ROW_FORMAT_DEFAULT
			|| $value === self::ROW_FORMAT_DYNAMIC
			|| $value === self::ROW_FORMAT_FIXED
			|| $value === self::ROW_FORMAT_REDUNDANT
		) {
			$this->_rowFormat = $value;
		}
	}
	/**
	 *
	 * @return enum('COMPRESSED', 'REDUNDANT', 'COMPACT', 'DEFAULT', 'DYNAMIC', 'FIXED')
	 */
	public function getRowFormat()
	{
		if($this->_rowFormat === null)
			return $this->_engine->getDefaultTableRowFormat();
		return $this->_rowFormat;
	}
	
	/**
	 *
	 * @param string $value
	 */
	public function setTablespace($value)
	{
		$this->_tablespace = $value;
	}
	/**
	 *
	 * @return string
	 */
	public function getTablespace()
	{
		if($this->_tablespace === null)
			return $this->_engine->getDefaultTablespace();
		return $this->_tablespace;
	}
	
	/**
	 *
	 * @param enum('DISK', 'MEMORY', 'DEFAULT')
	 */
	public function setTablespaceStorage($value)
	{
		if($value === self::TABLESPACE_STORAGE_DEFAULT
			|| $value === self::TABLESPACE_STORAGE_DISK
			|| $value === self::TABLESPACE_STORAGE_MEMORY
		) {
			$this->_tablespaceStorage = $value;
		}
	}
	/**
	 *
	 * @return enum('DISK', 'MEMORY', 'DEFAULT')
	 */
	public function getTablespaceStorage()
	{
		if($this->_tablespaceStorage === null)
			return $this->_engine->getDefaultTablespaceStorage();
		return $this->_tablespaceStorage;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CachalotObject::__toArray()
	 */
	public function __toArray()
	{
		return array(
			'name' => $this->getName(),
			'engine' => $this->getEngine()->getName(),
			'autoIncrement' => $this->getAutoIncrement(),
			'avgRowLength' => $this->getAverageRowLength(),
			'checksum' => $this->getChecksum(),
			'comment' => $this->getComment(),
			'connectionString' => $this->getConnectionString(),
			'dataDirectory' => $this->getDataDirectory(),
			'charset' => $this->getCharset(),
			'collation' => $this->getCollation(),
			'delayKeyWrite' => $this->getDelayKeyWrite(),
			'indexDirectory' => $this->getIndexDirectory(),
			'insertMethod' => $this->getInsertMethod(),
			'keyBlockSize' => $this->getKeyBlockSize(),
			'maxRows' => $this->getMaxRows(),
			'minRows' => $this->getMinRows(),
			'packKeys' => $this->getPackKeys(),
			'password' => $this->getPassword(),
			'rowFormat' => $this->getRowFormat(),
			'tablespace' => $this->getTablespace(),
			'tablespaceStorage' => $this->getTablespaceStorage(),
		);
	}
	
	/**
	 * Fill this table object from the array of options given by __toArray()
	 * @param array $values
	 * @return StdPhpTable
	 */
	public function __fromArray(array $values=array())
	{
		if(isset($values['name']))
			$this->setName($values['name']);
		if(isset($values['engine']))
		{
			$em = $this->cachalot->getEngineManager();
			$engine = $em->getEngine($values['engine']);
			if($engine === null)
			{
				$this->setEngine($em->getDefaultEngine());
			}
			else
			{
				$this->setEngine($engine);
			}
		}
		if(isset($values['autoIncrement']))
			$this->setAutoIncrement($values['autoIncrement']);
		if(isset($values['avgRowLength']))
			$this->setAverageRowLength($values['avgRowLength']);
		if(isset($values['checksum']))
			$this->setChecksum($values['checksum']);
		if(isset($values['comment']))
			$this->setComment($values['comment']);
		if(isset($values['connectionString']))
			$this->setConnectionString($values['connectionString']);
		if(isset($values['dataDirectory']))
			$this->setDataDirectory($values['dataDirectory']);
		if(isset($values['charset']))
			$this->setCharset($values['charset']);
		if(isset($values['collation']))
			$this->setCollation($values['collation']);
		if(isset($values['delayKeyWrite']))
			$this->setDelayKeyWrite($values['delayKeyWrite']);
		if(isset($values['indexDirectory']))
			$this->setIndexDirectory($values['indexDirectory']);
		if(isset($values['insertMethod']))
			$this->setInsertMethod($values['insertMethod']);
		if(isset($values['keyBlockSize']))
			$this->setKeyBlockSize($values['keyBlockSize']);
		if(isset($values['maxRows']))
			$this->setMaxRows($values['maxRows']);
		if(isset($values['minRows']))
			$this->setMinRows($values['minRows']);
		if(isset($values['packKeys']))
			$this->setPackKeys($values['packKeys']);
		if(isset($values['password']))
			$this->setPassword($values['password']);
		if(isset($values['rowFormat']))
			$this->setRowFormat($values['rowFormat']);
		if(isset($values['tablespace']))
			$this->setTablespace($values['tablespace']);
		if(isset($values['tablespaceStorage']))
			$this->setTablespaceStorage($values['tablespaceStorage']);
		return $this;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CachalotObject::equals()
	 */
	public function equals(CachalotObject $object)
	{
		if($object instanceof self)
		{
			return $this->getName() == $object->getName();
		}
		return false;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ITable::getColumns()
	 */
	public function getColumns()
	{
		return $this->_columns;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ITable::addColumn()
	 */
	public function addColumn(IColumn $column)
	{
		$this->_columns[] = $column;
	}
	
}

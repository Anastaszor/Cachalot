<?php

/**
 * TableFactory class file.
 * 
 * @author Anastaszor
 */
class TableFactory extends CachalotObject implements ITableFactory
{
	
	/**
	 * Dependancy injection.
	 * @var Cachalot
	 */
	public $cachalot = null;
	/**
	 * The file where all metadata about the databases will be stored.
	 * @var string
	 */
	public $fileName = 'tb_repository';
	
	/**
	 * 
	 * @var ITable[][]
	 */
	private $_tables = array();
	
	/**
	 * (non-PHPdoc)
	 * @see CachalotObject::init()
	 */
	public function init()
	{
		$this->_tables = array();	// reinit
		foreach($this->cachalot->getDatabaseFactory()->getDatabases() as $db)
		{
			if(file_exists($this->getRepositoryPath($db)))
			{
				$map = require $this->getRepositoryPath($db);
			}
			else
				$this->updateRepositoryFile($db);
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ITableFactory::getTable()
	 */
	public function getTable(IDatabase $database, $tbname)
	{
		foreach($this->getTables($database) as $table)
		{
			if($table->getName() == $tbname)
				return $table;
		}
		return null;
	}
	
	/**
	 *
	 * @param IDatabase $database
	 * @return ITable[]
	 */
	public function getTables(IDatabase $database)
	{
		if(isset($this->_tables[$database->getHash()]))
		{
			return $this->_tables[$database->getHash()];
		}
		return array();
	}
	
	/**
	 * 
	 * @param IDatabase $database
	 * @param ITable $table
	 */
	protected function addTable(IDatabase $database, ITable $table)
	{
		if(!isset($this->_tables[$database->getHash()]))
			$this->_tables[$database->getHash()] = array();
		$this->_tables[$database->getHash()][] = $table;
	}
	
	/**
	 * 
	 * @param IDatabase $database
	 */
	protected function updateRepositoryFile(IDatabase $database)
	{
		$tables = $this->getTables($database);
		
		$strToWrite = "<?php return ".var_export($tables, true).";\n";
		
		file_put_contents($this->getRepositoryPath($database), $strToWrite, LOCK_EX);
	}
	
	/**
	 * 
	 * @param IDatabase $database
	 * @return string
	 */
	protected function getRepositoryPath(IDatabase $database)
	{
		return $database->getPath().DIRECTORY_SEPARATOR.$this->fileName.'.php';
	}
	
	/**
	 *
	 * @return ICharset
	 */
	protected function getDefaultCharset()
	{
		return $this->cachalot->getCharsetFactory()->getDefaultCharset();
	}
	
	/**
	 *
	 * @return ICollation
	 */
	protected function getDefaultCollation()
	{
		return $this->cachalot->getCollationFactory()->getDefaultCollation();
	}
	
	/**
	 *
	 * @return IEngine
	 */
	protected function getDefaultEngine()
	{
		return $this->cachalot->getEngineFactory()->getDefaultEngine();
	}
	
	
	/**
	 * (non-PHPdoc)
	 * @see ITableFactory::createTable()
	 */
	public function createTable(IDatabase $database, ICreateTableStatement $statement)
	{
		var_dump($statement->toSQL(new MysqlDialect()));
		$table = new StdPhpTable();
		$table->setName($statement->getTableName());
		if($statement->getTableOptionsStatement()->getDefaultCharacterSet() !== null)
			$table->setCharset($statement->getTableOptionsStatement()->getDefaultCharacterSet());
		else 
			$table->setCharset($this->getDefaultCharset());
		if($statement->getTableOptionsStatement()->getDefaultCollation() !== null)
			$table->setCollation($statement->getTableOptionsStatement()->getDefaultCollation());
		else 
			$table->setCollation($this->getDefaultCollation());
		if($statement->getTableOptionsStatement()->getEngine() !== null)
			$table->setEngine($statement->getTableOptionsStatement()->getEngine());
		else
			$table->setEngine($this->getDefaultEngine());
		$table->setBasePath($database->getPath());
		
		foreach($statement->getCreateDefinitionStatements() as $columnsStt)
		{
			/* @var $columnsStt ICreateDefinitionStatement */
			$typecol = $columnsStt->getColumnDefinition();
			/* @var $column Column */
			if($typecol instanceof ISimpleColumnDefinitionStatement)
			{
				$column = new SimpleColumn();
				$column->setAutoIncrement($typecol->getAutoIncrement());
				$column->setColumnFormat($typecol->getColumnFormat());
				$column->setReference($this->buildReference($typecol->getReferenceStatement()));
			}
			elseif($typecol instanceof IGeneratedColumnDefinitionStatement)
			{
				$column = new GeneratedColumn();
			}
			// else we have a real problem
			$column->setName($columnsStt->getColumnName());
			foreach($columnsStt->getConstraints() as $constraint)
			{
				$column->addConstraint($constraint);
			}
			$column->setGenericDataType($typecol->getDataType()->toDataType());
			$column->setNotNull($typecol->getNotNull());
			$column->setUnique($typecol->getUnique());
			$column->setPrimary($typecol->getPrimary());
			$column->setComment($typecol->getComment());
			$table->addColumn($column);
		}
		
		$result = new StatementExecutionResult();
		foreach($this->getTables($database) as $reftb)
		{
			if($reftb->equals($table))
			{
				if($statement->getIfNotExists())
				{
					// table exists and IF NOT EXISTS is specified
					// >> do nothing, success
					$result->success();
					return $result;
				}
				else
				{
					// table exists and IF NOT EXISTS not specified
					// >> fail
					$result->fail();
					return $result;
				}
			}
		}
		// table does not exists, create it.
		$path = $table->getPath();
		if(is_dir($path) || mkdir($path))
		{
			$this->addTable($database, $table);
			$this->updateRepositoryFile($database);
			$result->success();
			return $result;
		}
		$result->fail();
		return $result;
	}
	
	public function buildReference(IReferenceStatement $statement = null)
	{
		if($statement === null)
			return null;
		// TODO BUILD REFERENCE STATEMENT
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ITableFactory::dropTable()
	 */
	public function dropTable(IDatabase $database, $key, IDropTableStatement $statement)
	{
		$result = new StatementExecutionResult();
		$tabcoord = $statement->getTableNames();
		$tablename = $tabcoord[$key]['table'];
		foreach($this->getTables($database) as $key => $table)
		{
			if($table->getName() == $tablename)
			{
				// table exists, delete it !
				$folder = $table->getPath();
				CUtil::removeRecursiveDir($folder);
				unset($this->_tables[$database->getHash()][$key]);
				$this->updateRepositoryFile($database);
				$result->success();
				return $result;
			}
		}
		if($statement->getIfExists())
		{
			// table does not exists, IF EXISTS is specified
			// >> do nothing, success
			$result->success();
		}
		else
		{
			// table does not exists, IF EXISTS not specified
			// >> fail
			$result->fail();
		}
		return $result;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ITableFactory::showTables()
	 */
	public function showTables(IDatabase $database, IShowTablesStatement $statement)
	{
		$result = new StatementExecutionResult();
		foreach($this->getTables($database) as $table)
		{
			// TODO EVAL LIKE STATEMENT
			// TODO EVAL WHERE STATEMENT
			$result->addResult(array('name'=>$table->getName()));
		}
		$result->success();
		return $result;
	}
	
}

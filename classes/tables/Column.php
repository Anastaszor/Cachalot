<?php

/**
 * Column class file.
 * 
 * @author Anastaszor
 */
abstract class Column extends CachalotObject implements IColumn
{
	
	/**
	 * 
	 * @var string
	 */
	private $_name = null;
	/**
	 * 
	 * @var IGenericDataType
	 */
	private $_type = null;
	/**
	 * 
	 * @var boolean
	 */
	private $_nullable = false;
	/**
	 * 
	 * @var boolean
	 */
	private $_unique = false;
	/**
	 * 
	 * @var boolean
	 */
	private $_primary = false;
	/**
	 * 
	 * @var string
	 */
	private $_comment = null;
	/**
	 * 
	 * @var IConstraint[]
	 */
	private $_constraints = array();
	
	/**
	 * Sets the name of this column
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->_name = (string) $name;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IColumn::getName()
	 */
	public function getName()
	{
		return $this->_name;
	}
	
	/**
	 * Sets the type of this column
	 * @param IGenericDataType $type
	 */
	public function setGenericDataType(IGenericDataType $type)
	{
		$this->_type = $type;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IColumn::getGenericDataType()
	 */
	public function getGenericDataType()
	{
		return $this->_type;
	}
	
	/**
	 * Sets whether the NULL value is allowed in the record
	 * @param string $bool
	 */
	public function setNotNull($bool)
	{
		$this->_nullable = !(true === $bool);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IColumn::getNotNull()
	 */
	public function getNotNull()
	{
		return !$this->_nullable;
	}
	
	/**
	 * Sets whether the column should be in an unique index.
	 * @param boolean $bool
	 */
	public function setUnique($bool)
	{
		$this->_unique = true === $bool;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IColumn::getUnique()
	 */
	public function getUnique()
	{
		return $this->_unique;
	}
	
	/**
	 * Sets whether the column is part of a primary key.
	 * @param boolean $bool
	 */
	public function setPrimary($bool)
	{
		$this->_primary = true === $bool;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IColumn::getPrimary()
	 */
	public function getPrimary()
	{
		return $this->_primary;
	}
	
	/**
	 * Sets a comment for this column
	 * @param string $comment
	 */
	public function setComment($comment)
	{
		$this->_comment = (string) $comment;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IColumn::getComment()
	 */
	public function getComment()
	{
		return $this->_comment;
	}
	
	/**
	 * Adds a constraint to this column.
	 * @param IConstraint $constraint
	 */
	public function addConstraint(IConstraint $constraint)
	{
		$this->_constraints[] = $constraint;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IColumn::getConstraints()
	 */
	public function getConstraints()
	{
		return $this->_constraints;
	}
	
}

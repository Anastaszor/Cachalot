<?php

/**
 * DateDataType class file.
 * 
 * @author Anastaszor
 */
class DateDataType extends CachalotObject implements IDateDataType
{
	
	/**
	 * (non-PHPdoc)
	 * @see IDataType::getPhpType()
	 */
	public function getPhpType()
	{
		return 'Datetime';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IGenericDataType::toStdPhp()
	 */
	public function toStdPhp()
	{
		return new StdPhpDatetime();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IGenericDataType::toStatement()
	 */
	public function toStatement()
	{
		return new DateTypeStatement();
	}
	
}

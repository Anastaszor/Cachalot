<?php

/**
 * BinaryDataType class file.
 * 
 * @author Anastaszor
 */
class BinaryDataType extends CachalotObject implements IBinaryDataType
{
	
	/**
	 * 
	 * @var int
	 */
	private $_length = 255;
	
	/**
	 * (non-PHPdoc)
	 * @see IDataType::getPhpType()
	 */
	public function getPhpType()
	{
		return 'string';
	}
	
	/**
	 * 
	 * @param int $int
	 */
	public function setLength($int)
	{
		$this->_length = (int) $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IBinaryDataType::getLength()
	 */
	public function getLength()
	{
		return $this->_length;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IGenericDataType::toStdPhp()
	 */
	public function toStdPhp()
	{
		return new StdPhpString();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IGenericDataType::toStatement()
	 */
	public function toStatement()
	{
		$st = new BinaryTypeStatement();
		$st->setLength($this->getLength());
		return $st;
	}
	
}

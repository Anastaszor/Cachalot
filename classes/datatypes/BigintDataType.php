<?php

/**
 * BigintDataType class file.
 * 
 * @author Anastaszor
 */
class BigintDataType extends CachalotObject implements IBigintDataType
{
	/**
	 * 
	 * @var int
	 */
	private $_length = 20;
	/**
	 * 
	 * @var boolean
	 */
	private $_unsigned = false;
	/**
	 * 
	 * @var boolean
	 */
	private $_zerofill = false;
	
	/**
	 * (non-PHPdoc)
	 * @see IDataType::getPhpType()
	 */
	public function getPhpType()
	{
		return 'integer';
	}
	
	/**
	 * Sets the length of the data type.
	 * @param int $int
	 */
	public function setLength($int)
	{
		$this->_length = (int) $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IBigintDataType::getLength()
	 */
	public function getLength()
	{
		return $this->_length;
	}
	
	/**
	 * Sets whether the data type is unsigned.
	 * @param boolean $bool
	 */
	public function setUnsigned($bool)
	{
		$this->_unsigned = (bool) $bool;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IBigintDataType::getUnsigned()
	 */
	public function getUnsigned()
	{
		return $this->_unsigned;
	}
	
	/**
	 * Sets whether the data type should be zerofilled.
	 * @param boolean $bool
	 */
	public function setZerofill($bool)
	{
		$this->_zerofill = (bool) $bool;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IBigintDataType::getZerofill()
	 */
	public function getZerofill()
	{
		return $this->_zerofill;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IGenericDataType::toStdPhp()
	 */
	public function toStdPhp()
	{
		return new StdPhpInteger();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IGenericDataType::toStatement()
	 */
	public function toStatement()
	{
		$st = new BigintTypeStatement();
		$st->setLength($this->getLength());
		$st->setUnsigned($this->getUnsigned());
		$st->setZerofill($this->getZerofill());
		return $st;
	}
	
}

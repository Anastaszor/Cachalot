<?php

/**
 * VarcharDataType class file.
 * 
 * @author Anastaszor
 */
class VarcharDataType extends CachalotObject implements IVarcharDataType
{
	
	/**
	 * 
	 * @var int
	 */
	private $_length = null;
	/**
	 * 
	 * @var boolean
	 */
	private $_binary = false;
	/**
	 * 
	 * @var ICharset
	 */
	private $_charset = null;
	/**
	 * 
	 * @var ICollation
	 */
	private $_collation = null;
	
	/**
	 * 
	 * @param int $int
	 */
	public function setLength($int)
	{
		$this->_length = (int) $int;
	}
	/**
	 * (non-PHPdoc)
	 * @see IVarcharDataType::getLength()
	 */
	public function getLength()
	{
		return $this->_length;
	}
	
	/**
	 * 
	 * @param boolean $bool
	 */
	public function setBinary($bool)
	{
		$this->_binary = (bool) $bool;
	}
	/**
	 * (non-PHPdoc)
	 * @see IVarcharDataType::getBinary()
	 */
	public function getBinary()
	{
		return $this->_binary;
	}
	
	/**
	 * 
	 * @param ICharset $charset
	 */
	public function setCharset(ICharset $charset)
	{
		$this->_charset = $charset;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IVarcharDataType::getCharset()
	 */
	public function getCharset()
	{
		return $this->_charset;
	}
	
	/**
	 * 
	 * @param ICollation $collation
	 */
	public function setCollation(ICollation $collation)
	{
		$this->_collation = $collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IVarcharDataType::getCollation()
	 */
	public function getCollation()
	{
		return $this->_collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataType::getPhpType()
	 */
	public function getPhpType()
	{
		return 'string';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IGenericDataType::toStdPhp()
	 */
	public function toStdPhp()
	{
		return new StdPhpString();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IGenericDataType::toStatement()
	 */
	public function toStatement()
	{
		$st = new VarcharTypeStatement();
		$st->setBinary($this->getBinary());
		$st->setLength($this->getLength());
		if($this->getCharset() !== null)
			$st->setCharset($this->getCharset());
		if($this->getCollation() !== null)
			$st->setCollation($this->getCollation());
		return $st;
	}
	
}

<?php

/**
 * LongblobDataType class file.
 * 
 * @author Anastaszor
 */
class LongblobDataType extends CachalotObject implements ILongBlobDataType
{
	
	/**
	 * (non-PHPdoc)
	 * @see IDataType::getPhpType()
	 */
	public function getPhpType()
	{
		return 'string';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IGenericDataType::toStdPhp()
	 */
	public function toStdPhp()
	{
		return new StdPhpString();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IGenericDataType::toStatement()
	 */
	public function toStatement()
	{
		return new LongblobTypeStatement();
	}
	
	
}

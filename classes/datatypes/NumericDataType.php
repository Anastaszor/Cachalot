<?php

/**
 * NumericDataType class file.
 * 
 * @author Anastaszor
 */
class NumericDataType extends CachalotObject implements INumericDataType
{
	
	/**
	 * 
	 * @var int
	 */
	private $_length = 13;
	/**
	 * 
	 * @var int
	 */
	private $_decimals = 2;
	/**
	 * 
	 * @var boolean
	 */
	private $_unsigned = false;
	/**
	 * 
	 * @var boolean
	 */
	private $_zerofill = false;
	
	/**
	 * 
	 * @param int $int
	 */
	public function setLength($int)
	{
		$this->_length = (int) $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see INumericDataType::getLength()
	 */
	public function getLength()
	{
		return $this->_length;
	}
	
	/**
	 * 
	 * @param int $int
	 */
	public function setDecimals($int)
	{
		$this->_decimals = (int) $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see INumericDataType::getDecimals()
	 */
	public function getDecimals()
	{
		return $this->_decimals;
	}
	
	/**
	 * 
	 * @param boolean $bool
	 */
	public function setUnsigned($bool)
	{
		$this->_unsigned = (bool) $bool;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see INumericDataType::getUnsigned()
	 */
	public function getUnsigned()
	{
		return $this->_unsigned;
	}
	
	/**
	 * 
	 * @param boolean $bool
	 */
	public function setZerofill($bool)
	{
		$this->_zerofill = (bool) $bool;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see INumericDataType::getZerofill()
	 */
	public function getZerofill()
	{
		return $this->_zerofill;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataType::getPhpType()
	 */
	public function getPhpType()
	{
		return 'double';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IGenericDataType::toStdPhp()
	 */
	public function toStdPhp()
	{
		return new StdPhpFloat();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IGenericDataType::toStatement()
	 */
	public function toStatement()
	{
		$st = new NumericTypeStatement();
		$st->setLength($this->getLength());
		$st->setDecimals($this->getDecimals());
		$st->setUnsigned($this->getUnsigned());
		$st->setZerofill($this->getZerofill());
		return $st;
	}
	
}

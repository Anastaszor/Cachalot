<?php

/**
 * DecimalDataType class file.
 * 
 * @author Anastaszor
 */
class DecimalDataType extends CachalotObject implements IDecimalDataType
{
	/**
	 * 
	 * @var int
	 */
	private $_length = 13;
	/**
	 * 
	 * @var int
	 */
	private $_decimals = 2;
	/**
	 * 
	 * @var boolean
	 */
	private $_unsigned = false;
	/**
	 * 
	 * @var boolean
	 */
	private $_zerofill = false;
	
	/**
	 * (non-PHPdoc)
	 * @see IDataType::getPhpType()
	 */
	public function getPhpType()
	{
		return 'float';
	}
	
	/**
	 * Sets the length of this data type.
	 * @param int $int
	 */
	public function setLength($int)
	{
		$this->_length = (int) $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDecimalTypeStatement::getLength()
	 */
	public function getLength()
	{
		return $this->_length;
	}
	
	/**
	 * Set sthe decimal length of this data type.
	 * @param int $int
	 */
	public function setDecimals($int)
	{
		$this->_decimals = (int) $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDecimalTypeStatement::getDecimals()
	 */
	public function getDecimals()
	{
		return $this->_decimals;
	}
	
	/**
	 * Sets whether this should be interpreted as unsigned data type
	 * @param boolean $bool
	 */
	public function setUnsigned($bool)
	{
		$this->_unsigned = (bool) $bool;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDecimalTypeStatement::getUnsigned()
	 */
	public function getUnsigned()
	{
		return $this->_unsigned;
	}
	
	/**
	 * Sets whether this should be zerofilled.
	 * @param boolean $bool
	 */
	public function setZerofill($bool)
	{
		$this->_zerofill = (bool) $bool;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDecimalTypeStatement::getZerofill()
	 */
	public function getZerofill()
	{
		return $this->_zerofill;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IGenericDataType::toStdPhp()
	 */
	public function toStdPhp()
	{
		return new StdPhpFloat();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IGenericDataType::toStatement()
	 */
	public function toStatement()
	{
		$st = new DecimalTypeStatement();
		$st->setLength($this->getLength());
		$st->setDecimals($this->getDecimals());
		$st->setUnsigned($this->getUnsigned());
		$st->setZerofill($this->getZerofill());
		return $st;
	}
	
}

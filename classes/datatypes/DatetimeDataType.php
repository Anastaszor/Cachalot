<?php

/**
 * DatetimeDataType class file.
 * 
 * @author Anastaszor
 */
class DatetimeDataType extends CachalotObject implements IDatetimeDataType
{
	
	/**
	 * (non-PHPdoc)
	 * @see IDataType::getPhpType()
	 */
	public function getPhpType()
	{
		return 'Datetime';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IGenericDataType::toStdPhp()
	 */
	public function toStdPhp()
	{
		return new StdPhpDatetime();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IGenericDataType::toStatement()
	 */
	public function toStatement()
	{
		return new DatetimeTypeStatement();
	}
	
}

<?php

/**
 * BitDataType class file.
 * 
 * @author Anastaszor
 */
class BitDataType extends CachalotObject implements IBitDataType
{
	
	/**
	 * 
	 * @var int
	 */
	private $_length = 255;
	
	/**
	 * (non-PHPdoc)
	 * @see IDataType::getPhpType()
	 */
	public function getPhpType()
	{
		return 'integer';
	}
	
	/**
	 * Sets the length of the data type.
	 * @param int $int
	 */
	public function setLength($int)
	{
		$this->_length = (int) $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IBitDataType::getLength()
	 */
	public function getLength()
	{
		return $this->_length;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IGenericDataType::toStdPhp()
	 */
	public function toStdPhp()
	{
		return new StdPhpInteger();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IGenericDataType::toStatement()
	 */
	public function toStatement()
	{
		$st = new BitTypeStatement();
		$st->setLength($this->getLength());
		return $st;
	}
	
}

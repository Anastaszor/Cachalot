<?php

/**
 * EnumDataType class file.
 * 
 * @author Anastaszor
 */
class EnumDataType extends CachalotObject implements IEnumDataType
{
	
	/**
	 * 
	 * @var string[]
	 */
	private $_values = array();
	/**
	 * 
	 * @var ICharset
	 */
	private $_charset = null;
	/**
	 * 
	 * @var ICollation
	 */
	private $_collation = null;
	
	/**
	 * 
	 * @param string $string
	 */
	public function addValue($string)
	{
		$this->_values[] = $string;
	}
	
	/**
	 * 
	 * @param string[] $strings
	 */
	public function setValues($strings)
	{
		$this->_values = $strings;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IEnumDataType::getValues()
	 */
	public function getValues()
	{
		return $this->_values;
	}
	
	/**
	 * 
	 * @param ICharset $charset
	 */
	public function setCharset(ICharset $charset)
	{
		$this->_charset = $charset;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IEnumDataType::getCharset()
	 */
	public function getCharset()
	{
		return $this->_charset;
	}
	
	/**
	 * 
	 * @param ICollation $collation
	 */
	public function setCollation(ICollation $collation)
	{
		$this->_collation = $collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IEnumDataType::getCollation()
	 */
	public function getCollation()
	{
		return $this->_collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataType::getPhpType()
	 */
	public function getPhpType()
	{
		return 'array';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IGenericDataType::toStdPhp()
	 */
	public function toStdPhp()
	{
		$pt = new StdPhpEnum();
		$pt->setValues($this->getValues());
		return $pt;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IGenericDataType::toStatement()
	 */
	public function toStatement()
	{
		$st = new EnumTypeStatement();
		$st->setValues($this->getValues());
		if($this->getCharset() !== null)
			$st->setCharset($this->getCharset());
		if($this->getCollation() !== null)
			$st->setCollation($this->getCollation());
		return $st;
	}
	
}

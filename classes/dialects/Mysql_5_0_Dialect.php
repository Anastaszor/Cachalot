<?php

/**
 * MysqlDialect class file.
 * 
 * This class represents the Mysql 5.0 variation about the SQL Language.
 * 
 * @author Anastaszor
 */
class Mysql_5_0_Dialect extends CachalotObject implements IDialect
{
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::quoteDatabaseName()
	 */
	public function quoteDatabaseName($dbname)
	{
		return '`'.str_replace(array('\\', '`'), array('\\\\', '\\`'), $dbname).'`';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::quoteTableName()
	 */
	public function quoteTableName($tablename)
	{
		return '`'.str_replace(array('\\', '`'), array('\\\\', '\\`'), $tablename).'`';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::quoteString()
	 */
	public function quoteString($string)
	{
		return '"'.str_replace(array('\\', '"'), array('\\\\', '\\"'), $string).'"';
	}
	
	// Data Definition Statements
	// http://dev.mysql.com/doc/refman/5.0/en/sql-syntax-data-definition.html
	
	/**
	 * ALTER {DATABASE | SCHEMA} [db_name] alter_specification ...
	 * 
	 * alter_specification:
	 *     [DEFAULT] CHARACTER SET [=] charset_name
	 *   | [DEFAULT] COLLATE [=] collation_name
	 * 
	 * @see IDialect::alterDatabase()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/alter-database.html
	 */
	public function alterDatabase(IAlterDatabaseStatement $statement)
	{
		$str = 'ALTER DATABASE ';
		$str .= $this->quoteDatabaseName($statement->getDatabaseName());
		return $str.' '.$statement->getSpecification()->toSQL($this).';';
	}
	
	/**
	 * ALTER FUNCTION func_name [characteristic ...]
	 * 
	 * characteristic:
	 *     COMMENT 'string'
	 *   | LANGUAGE SQL
	 *   | { CONTAINS SQL | NO SQL | READS SQL DATA | MODIFIES SQL DATA }
	 *   | SQL SECURITY { DEFINER | INVOKER }
	 * 
	 * @see IDialect::alterFunction()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/alter-function.html
	 */
	public function alterFunction(IAlterFunctionStatement $statement)
	{
		$str = 'ALTER FUNCTION ';
		$str .= $this->quoteTableName($statement->getFunctionName());
		$characsql = array();
		foreach($statement->getCharacteristics() as $charac)
		{
			$characsql[] = $charac->toSQL($this);
		}
		return $str.' '.implode(', ', $characsql).';';
	}
	
	/**
	 * ALTER PROCEDURE proc_name [characteristic ...]
	 * 
	 * characteristic:
	 *     COMMENT 'string'
	 *   | LANGUAGE SQL
	 *   | { CONTAINS SQL | NO SQL | READS SQL DATA | MODIFIES SQL DATA }
	 *   | SQL SECURITY { DEFINER | INVOKER }
	 * 
	 * @see IDialect::alterProcedure()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/alter-procedure.html
	 */
	public function alterProcedure(IAlterProcedureStatement $statement)
	{
		$str = 'ALTER PROCEDURE ';
		$str .= $this->quoteTableName($statement->getProcedureName());
		$characsql = array();
		foreach($statement->getCharacteristics() as $charac)
		{
			$characsql[] = $charac->toSQL($this);
		}
		return $str.' '.implode(', ', $characsql).';';
	}
	
	/**
	 * ALTER [IGNORE] TABLE tbl_name
	 *     [alter_specification [, alter_specification] ...]
	 * 
	 * alter_specification:
	 *     table_options
	 *   | ADD [COLUMN] col_name column_definition
	 *         [FIRST | AFTER col_name ]
	 *   | ADD [COLUMN] (col_name column_definition,...)
	 *   | ADD {INDEX|KEY} [index_name]
	 *         [index_type] (index_col_name,...) [index_type]
	 *   | ADD [CONSTRAINT [symbol]] PRIMARY KEY
	 *         [index_type] (index_col_name,...) [index_type]
	 *   | ADD [CONSTRAINT [symbol]]
	 *         UNIQUE [INDEX|KEY] [index_name]
	 *         [index_type] (index_col_name,...) [index_type]
	 *   | ADD [FULLTEXT|SPATIAL] [INDEX|KEY] [index_name]
	 *         (index_col_name,...) [index_type]
	 *   | ADD [CONSTRAINT [symbol]]
	 *         FOREIGN KEY [index_name] (index_col_name,...)
	 *         reference_definition
	 *   | ALTER [COLUMN] col_name {SET DEFAULT literal | DROP DEFAULT}
	 *   | CHANGE [COLUMN] old_col_name new_col_name column_definition
	 *         [FIRST|AFTER col_name]
	 *   | MODIFY [COLUMN] col_name column_definition
	 *         [FIRST | AFTER col_name]
	 *   | DROP [COLUMN] col_name
	 *   | DROP PRIMARY KEY
	 *   | DROP {INDEX|KEY} index_name
	 *   | DROP FOREIGN KEY fk_symbol
	 *   | DISABLE KEYS
	 *   | ENABLE KEYS
	 *   | RENAME [TO|AS] new_tbl_name
	 *   | ORDER BY col_name [, col_name] ...
	 *   | CONVERT TO CHARACTER SET charset_name [COLLATE collation_name]
	 *   | [DEFAULT] CHARACTER SET [=] charset_name [COLLATE [=] collation_name]
	 *   | DISCARD TABLESPACE
	 *   | IMPORT TABLESPACE
	 * 
	 * index_col_name:
	 *     col_name [(length)] [ASC | DESC]
	 * 
	 * index_type:
	 *     USING {BTREE | HASH}
	 * 
	 * table_options:
	 *     table_option [[,] table_option] ...  (see CREATE TABLE options)
	 * 
	 * @see IDialect::alterTable()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/alter-table.html
	 */
	public function alterTable(IAlterTableStatement $statement)
	{
		$sql = 'ALTER';
		if($statement->getIgnore())
			$sql .=' IGNORE';
		$sql .= ' TABLE '.$this->quoteTableName($statement->getTableName());
		$tablessql = array();
		foreach($statement->getTableSpecifications() as $tablespec)
		{
			$tablessql[] = $tablespec->toSQL($this);
		}
		return $sql.' '.implode(', ', $tablessql).';';
	}
	
	/**
	 * ALTER
	 *     [ALGORITHM = {UNDEFINED | MERGE | TEMPTABLE}]
	 *     [DEFINER = { user | CURRENT_USER }]
	 *     [SQL SECURITY { DEFINER | INVOKER }]
	 *     VIEW view_name [(column_list)]
	 *     AS select_statement
	 *     [WITH [CASCADED | LOCAL] CHECK OPTION]
	 * 
	 * @see IDialect::alterView()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/alter-view.html
	 */
	public function alterView(IAlterViewStatement $statement)
	{
		$sql = 'ALTER';
		if($statement->getAlgorithm() !== null)
			$sql .= ' ALGORITHM = '.$statement->getAlgorithm();
		if($statement->getDefiner() !== null)
		{
			$sql .= ' DEFINER = ';
			if($statement->getDefiner() === IAlterViewStatement::DEFINER_CU)
				$sql .= $statement->getDefiner();
			else 
				$sql .= $this->quoteString($statement->getDefiner());
		}
		if($statement->getSecurity() !== null)
			$sql .= ' SQL SECURITY '.$statement->getSecurity();
		$sql .= ' VIEW '.$this->quoteTableName($statement->getViewName());
		if(count($statement->getColumnList()) > 0)
		{
			$sql .= ' ('.implode(', ', array_map(
				array($this, 'quoteTableName'), 
				$statement->getColumnList()
			)).')';
		}
		$sql .= ' AS '.$statement->getSelectStatement()->toSQL($this);
		if($statement->getCheckOption())
		{
			$sql = rtrim($sql, ';');
			$sql .= ' WITH';
			if($statement->getCheckOptionMode() !== null)
				$sql .= ' '.$statement->getCheckOptionMode();
			$sql .= ' CHECK OPTION;';
		}
		return $sql;
	}
	
	/**
	 * CREATE {DATABASE | SCHEMA} [IF NOT EXISTS] db_name
	 *     [create_specification] ...
	 * 
	 * create_specification:
	 *     [DEFAULT] CHARACTER SET [=] charset_name
	 *   | [DEFAULT] COLLATE [=] collation_name
	 * 
	 * @see IDialect::createDatabase()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/create-database.html
	 */
	public function createDatabase(ICreateDatabaseStatement $statement)
	{
		$sql = 'CREATE DATABASE ';
		if($statement->getIfNotExists())
			$sql .= 'IF NOT EXISTS ';
		$sql .= $this->quoteDatabaseName($statement->getDatabaseName());
		if(count($statement->getSpecifications()) > 0)
		{
			$specsql = array();
			foreach($statement->getSpecifications() as $specification)
			{
				$specsql[] = $specification->toSQL($this);
			}
			return $sql.' '.implode(' ', $specsql).';';
		}
		return $sql.';';
	}
	
	/**
	 * CREATE
	 *     [DEFINER = { user | CURRENT_USER }]
	 *     FUNCTION sp_name ([func_parameter[,...]])
	 *     RETURNS type
	 *     [characteristic ...] routine_body
	 * 
	 * func_parameter:
	 *     param_name type
	 * 
	 * type:
	 *     Any valid MySQL data type
	 * 
	 * characteristic:
	 *     COMMENT 'string'
	 *   | LANGUAGE SQL
	 *   | [NOT] DETERMINISTIC
	 *   | { CONTAINS SQL | NO SQL | READS SQL DATA | MODIFIES SQL DATA }
	 *   | SQL SECURITY { DEFINER | INVOKER }
	 * 
	 * routine_body:
	 *     Valid SQL routine statement
	 * 
	 * @see IDialect::createFunction()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/create-procedure.html
	 */
	public function createFunction(ICreateFunctionStatement $statement)
	{
		$sql = 'CREATE';
		if($statement->getDefiner() !== null)
		{
			$sql .= ' DEFINER = ';
			if($statement->getDefiner() === ICreateFunctionStatement::DEFINER_CU)
				$sql .= $statement->getDefiner();
			else 
				$sql .= $this->quoteString($statement->getDefiner());
		}
		$sql .= ' FUNCTION '.$this->quoteTableName($statement->getFunctionName());
		$paramsql = array();
		foreach($statement->getParams() as $param)
		{
			$paramsql[] = $param->toSql($this);
		}
		$sql .= ' ('.implode(', ', $paramsql).')';
		$sql .= ' RETURNS '.$statement->getReturnType()->toSQL($this);
		foreach($statement->getCharacteristics() as $characteristic)
		{
			$sql .= ' '.$characteristic->toSQL($this);
		}
		return $sql.' BEGIN '.$statement->getRoutineBody()->toSql($this).' END;';
	}
	
	/**
	 * CREATE [UNIQUE|FULLTEXT|SPATIAL] INDEX index_name
	 *     [index_type]
	 *     ON tbl_name (index_col_name,...)
	 *     [index_type]
	 * 
	 * index_col_name:
	 *     col_name [(length)] [ASC | DESC]
	 * 
	 * index_type:
	 *     USING {BTREE | HASH}
	 * 
	 * @see IDialect::createIndex()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/create-index.html
	 */
	public function createIndex(ICreateIndexStatement $statement)
	{
		$sql = 'CREATE';
		if($statement->getIndexSpace() !== null)
			$sql .= ' '.$statement->getIndexSpace();
		$sql .= ' INDEX '.$this->quoteTableName($statement->getIndexName());
		$sql .= ' ON '.$this->quoteTableName($statement->getTableName());
		if(count($statement->getColumnIndexStatements()) > 0)
		{
			$solumnsql = array();
			foreach($statement->getColumnIndexStatements() as $colstat)
			{
				$solumnsql[] = $colstat->toSQL($this);
			}
			$sql .= ' ('.implode(', ', $solumnsql).')';
		}
		if($statement->getIndexType() !== null)
			$sql .= ' USING '.$statement->getIndexType();
		return $sql.';';
	}
	
	/**
	 * CREATE
	 *     [DEFINER = { user | CURRENT_USER }]
	 *     PROCEDURE sp_name ([proc_parameter[,...]])
	 *     [characteristic ...] routine_body
	 * 
	 * proc_parameter:
	 *     [ IN | OUT | INOUT ] param_name type
	 * 
	 * type:
	 *     Any valid MySQL data type
	 * 
	 * characteristic:
	 *     COMMENT 'string'
	 *   | LANGUAGE SQL
	 *   | [NOT] DETERMINISTIC
	 *   | { CONTAINS SQL | NO SQL | READS SQL DATA | MODIFIES SQL DATA }
	 *   | SQL SECURITY { DEFINER | INVOKER }
	 * 
	 * routine_body:
	 *     Valid SQL routine statement
	 * 
	 * @see IDialect::createProcedure()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/create-procedure.html
	 */
	public function createProcedure(ICreateProcedureStatement $statement)
	{
		$sql = 'CREATE';
		if($statement->getDefiner() !== null)
		{
			$sql .= ' DEFINER = ';
			if($statement->getDefiner() === ICreateProcedureStatement::DEFINER_CU)
				$sql .= $statement->getDefiner();
			else 
				$sql .= $this->quoteString($statement->getDefiner());
		}
		$sql .= ' PROCEDURE '.$this->quoteTableName($statement->getProcedureName());
		$paramsql = array();
		foreach($statement->getParams() as $param)
		{
			$paramsql[] = $param->toSql($this);
		}
		$sql .= ' ('.implode(', ', $paramsql).')';
		foreach($statement->getCharacteristics() as $charac)
		{
			$sql .= ' '.$charac->toSQL($this);
		}
		return $sql.' BEGIN '.$statement->getRoutineBody()->toSql($this).' END;';
	}
	
	/**
	 * CREATE [TEMPORARY] TABLE [IF NOT EXISTS] tbl_name
	 *     (create_definition,...)
	 *     [table_options]
	 * 
	 * CREATE [TEMPORARY] TABLE [IF NOT EXISTS] tbl_name
	 *     [(create_definition,...)]
	 *     [table_options]
	 *     select_statement
	 * 
	 * CREATE [TEMPORARY] TABLE [IF NOT EXISTS] tbl_name
	 *     { LIKE old_tbl_name | (LIKE old_tbl_name) }
	 * 
	 * create_definition:
	 *     col_name column_definition
	 *   | [CONSTRAINT [symbol]] PRIMARY KEY [index_type] (index_col_name,...)
	 *         [index_type]
	 *   | {INDEX|KEY} [index_name] [index_type] (index_col_name,...)
	 *         [index_type]
	 *   | [CONSTRAINT [symbol]] UNIQUE [INDEX|KEY]
	 *         [index_name] [index_type] (index_col_name,...)
	 *         [index_type]
	 *   | {FULLTEXT|SPATIAL} [INDEX|KEY] [index_name] (index_col_name,...)
	 *         [index_type]
	 *   | [CONSTRAINT [symbol]] FOREIGN KEY
	 *         [index_name] (index_col_name,...) reference_definition
	 *   | CHECK (expr)
	 * 
	 * column_definition:
	 *     data_type [NOT NULL | NULL] [DEFAULT default_value]
	 *       [AUTO_INCREMENT] [UNIQUE [KEY] | [PRIMARY] KEY]
	 *       [COMMENT 'string'] [reference_definition]
	 * 
	 * data_type:
	 *     BIT[(length)]
	 *   | TINYINT[(length)] [UNSIGNED] [ZEROFILL]
	 *   | SMALLINT[(length)] [UNSIGNED] [ZEROFILL]
	 *   | MEDIUMINT[(length)] [UNSIGNED] [ZEROFILL]
	 *   | INT[(length)] [UNSIGNED] [ZEROFILL]
	 *   | INTEGER[(length)] [UNSIGNED] [ZEROFILL]
	 *   | BIGINT[(length)] [UNSIGNED] [ZEROFILL]
	 *   | REAL[(length,decimals)] [UNSIGNED] [ZEROFILL]
	 *   | DOUBLE[(length,decimals)] [UNSIGNED] [ZEROFILL]
	 *   | FLOAT[(length,decimals)] [UNSIGNED] [ZEROFILL]
	 *   | DECIMAL[(length[,decimals])] [UNSIGNED] [ZEROFILL]
	 *   | NUMERIC[(length[,decimals])] [UNSIGNED] [ZEROFILL]
	 *   | DATE
	 *   | TIME
	 *   | TIMESTAMP
	 *   | DATETIME
	 *   | YEAR
	 *   | CHAR[(length)] [BINARY]
	 *       [CHARACTER SET charset_name] [COLLATE collation_name]
	 *   | VARCHAR(length) [BINARY]
	 *       [CHARACTER SET charset_name] [COLLATE collation_name]
	 *   | BINARY[(length)]
	 *   | VARBINARY(length)
	 *   | TINYBLOB
	 *   | BLOB
	 *   | MEDIUMBLOB
	 *   | LONGBLOB
	 *   | TINYTEXT [BINARY]
	 *       [CHARACTER SET charset_name] [COLLATE collation_name]
	 *   | TEXT [BINARY]
	 *       [CHARACTER SET charset_name] [COLLATE collation_name]
	 *   | MEDIUMTEXT [BINARY]
	 *       [CHARACTER SET charset_name] [COLLATE collation_name]
	 *   | LONGTEXT [BINARY]
	 *       [CHARACTER SET charset_name] [COLLATE collation_name]
	 *   | ENUM(value1,value2,value3,...)
	 *       [CHARACTER SET charset_name] [COLLATE collation_name]
	 *   | SET(value1,value2,value3,...)
	 *       [CHARACTER SET charset_name] [COLLATE collation_name]
	 *   | spatial_type
	 * 
	 * index_col_name:
	 *     col_name [(length)] [ASC | DESC]
	 * 
	 * index_type:
	 *     USING {BTREE | HASH}
	 * 
	 * reference_definition:
	 *     REFERENCES tbl_name (index_col_name,...)
	 *       [MATCH FULL | MATCH PARTIAL | MATCH SIMPLE]
	 *       [ON DELETE reference_option]
	 *       [ON UPDATE reference_option]
	 * 
	 * reference_option:
	 *     RESTRICT | CASCADE | SET NULL | NO ACTION
	 * 
	 * table_options:
	 *     table_option [[,] table_option] ...
	 * 
	 * table_option:
	 *     {ENGINE|TYPE} [=] engine_name
	 *   | AUTO_INCREMENT [=] value
	 *   | AVG_ROW_LENGTH [=] value
	 *   | [DEFAULT] CHARACTER SET [=] charset_name
	 *   | CHECKSUM [=] {0 | 1}
	 *   | [DEFAULT] COLLATE [=] collation_name
	 *   | COMMENT [=] 'string'
	 *   | CONNECTION [=] 'connect_string'
	 *   | DATA DIRECTORY [=] 'absolute path to directory'
	 *   | DELAY_KEY_WRITE [=] {0 | 1}
	 *   | INDEX DIRECTORY [=] 'absolute path to directory'
	 *   | INSERT_METHOD [=] { NO | FIRST | LAST }
	 *   | MAX_ROWS [=] value
	 *   | MIN_ROWS [=] value
	 *   | PACK_KEYS [=] {0 | 1 | DEFAULT}
	 *   | PASSWORD [=] 'string'
	 *   | ROW_FORMAT [=] {DEFAULT|DYNAMIC|FIXED|COMPRESSED|REDUNDANT|COMPACT}
	 *   | UNION [=] (tbl_name[,tbl_name]...)
	 * 
	 * select_statement:
	 *     [IGNORE | REPLACE] [AS] SELECT ...   (Some legal select statement)
	 * 
	 * @see IDialect::createTable()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/create-table.html
	 */
	public function createTable(ICreateTableStatement $statement)
	{
		$sql = 'CREATE ';
		if($statement->getTemporary())
		{
			$sql .= 'TEMPORARY ';
		}
		$sql .= 'TABLE ';
		if($statement->getIfNotExists())
		{
			$sql .= 'IF NOT EXISTS ';
		}
		if($statement->getDatabaseName() !== null)
		{
			$sql .= $this->quoteDatabaseName($statement->getDatabaseName());
		}
		$sql .= $this->quoteTableName($statement->getTableName());
		if($statement->getLikeStatement() !== null)
		{
			$sql .= $statement->getLikeStatement()->toSQL($dialect);
			return $sql.';';
		}
		$sql .= ' (';
		$sttsql = array();
		foreach($statement->getCreateDefinitionStatements() as $child)
		{
			$sttqsl[] = $child->toSQL($dialect);
		}
		$sql .= implode(', ', $sttqsl);
		$sql .= ')';
		if($statement->getTableOptionsStatement() !== null)
		{
			$sql .= $statement->getTableOptionsStatement()->toSQL($dialect);
		}
		if($statement->getPartitionOptionsStatement() !== null)
		{
			$sql .= $statement->getPartitionOptionsStatement()->toSQL($dialect);
		}
		if($statement->getSelectOptionStatement() !== null)
		{
			$sql .= $statement->getSelectOptionStatement()->toSQL($dialect);
		}
		else
		{
			$sql .= ';';
		}
		return $sql;
	}
	
	/**
	 * CREATE
	 *     [DEFINER = { user | CURRENT_USER }]
	 *     TRIGGER trigger_name
	 *     trigger_time trigger_event
	 *     ON tbl_name FOR EACH ROW
	 *     trigger_body
	 * 
	 * trigger_time: { BEFORE | AFTER }
	 * 
	 * trigger_event: { INSERT | UPDATE | DELETE }
	 * 
	 * @see IDialect::createTrigger()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/create-trigger.html
	 */
	public function createTrigger(ICreateTriggerStatement $statement)
	{
		$sql = 'CREATE';
		if($statement->getDefiner() !== null)
			$sql .= ' DEFINER = '.$statement->getDefiner();
		$sql .= ' TRIGGER '.$this->quoteTableName($statement->getTriggerName());
		$sql .= ' '.$statement->getTriggerTime().' '.$statement->getTriggerEvent();
		$sql .= ' ON '.$this->quoteTableName($statement->getTargetTableName());
		return $sql.' FOR EACH ROW '.$statement->getTriggerBody()->toSql($this);
	}
	
	/**
	 * CREATE
	 *     [OR REPLACE]
	 *     [ALGORITHM = {UNDEFINED | MERGE | TEMPTABLE}]
	 *     [DEFINER = { user | CURRENT_USER }]
	 *     [SQL SECURITY { DEFINER | INVOKER }]
	 *     VIEW view_name [(column_list)]
	 *     AS select_statement
	 *     [WITH [CASCADED | LOCAL] CHECK OPTION]
	 * 
	 * @see IDialect::createView()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/create-view.html
	 */
	public function createView(ICreateViewStatement $statement)
	{
		$sql = 'CREATE';
		if($statement->getReplace())
			$sql .= ' OR REPLACE';
		if($statement->getAlgorithm() !== null)
			$sql .= ' ALGORITHM = '.$statement->getAlgorithm();
		if($statement->getDefiner() !== null)
			$sql .= ' DEFINER = '.$statement->getDefiner();
		if($statement->getSecurity() !== null)
			$sql .= ' SQL SECURITY '.$statement->getSecurity();
		$sql .= ' VIEW '.$this->quoteTableName($statement->getViewName());
		if(count($statement->getColumnList()) > 0)
		{
			$sql .= ' ('.implode(', ', array_map(
				array($this, 'quoteTableName' ),
				$statement->getColumnList()
			)).')';
		}
		$sql .= ' AS '.$statement->getSelectStatement()->toSQL($this);
		if($statement->getCheckOption())
		{
			$sql = rtrim($sql, ';');
			$sql .= ' WITH';
			if($statement->getCheckOptionMode() !== null)
				$sql .= ' '.$statement->getCheckOptionMode();
			$sql .= ' CHECK OPTION;';
		}
		return $sql;
	}
	
	/**
	 * DROP {DATABASE | SCHEMA} [IF EXISTS] db_name
	 * 
	 * @see IDialect::dropDatabase()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/drop-database.html
	 */
	public function dropDatabase(IDropDatabaseStatement $statement)
	{
		$sql = 'DROP DATABASE ';
		$sql .= $this->quoteDatabaseName($statement->getDatabaseName());
		if($statement->getIfExists())
		{
			$sql .= ' IF EXISTS';
		}
		return $sql.';';
	}
	
	/**
	 * DROP FUNCTION [IF EXISTS] function_name
	 * 
	 * @see IDialect::dropFunction()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/drop-procedure.html
	 */
	public function dropFunction(IDropFunctionStatement $statement)
	{
		$sql = 'DROP FUNCTION ';
		if($statement->getIfExists())
			$sql .= 'IF EXISTS ';
		return $sql.$this->quoteTableName($statement->getFunctionName()).';';
	}
	
	/**
	 * DROP INDEX index_name ON tbl_name
	 * 
	 * @see IDialect::dropIndex()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/drop-index.html
	 */
	public function dropIndex(IDropIndexStatement $statement)
	{
		$sql = 'DROP INDEX ';
		$sql .= $this->quoteTableName($statement->getIndexName());
		return $sql.' ON '.$this->quoteTableName($statement->getTableName()).';';
	}
	
	/**
	 * DROP PROCEDURE [IF EXISTS] proc_name
	 * 
	 * @see IDialect::dropProcedure()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/drop-procedure.html
	 */
	public function dropProcedure(IDropProcedureStatement $statement)
	{
		$sql = 'DROP PROCEDURE ';
		if($statement->getIfExists())
			$sql .= 'IF EXISTS ';
		return $sql.$this->quoteTableName($statement->getProcedureName()).';';
	}
	
	/**
	 * DROP [TEMPORARY] TABLE [IF EXISTS]
	 *     tbl_name [, tbl_name] ...
	 *     [RESTRICT | CASCADE]
	 * 
	 * @see IDialect::dropTable()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/drop-table.html
	 */
	public function dropTable(IDropTableStatement $statement)
	{
		$sql = 'DROP';
		if($statement->getTemporary())
			$sql .= ' TEMPORARY';
		$sql .= ' TABLE ';
		if($statement->getIfExists())
			$sql .= 'IF EXISTS ';
		$i = 0;
		$max = count($statement->getTableNames());
		foreach($statement->getTableNames() as $tablename)
		{
			$i++;
			if($tablename['database'] !== null)
			{
				$sql .= $this->quoteDatabaseName($tablename['database']).'.';
			}
			$sql .= $this->quoteTableName($tablename['table']);
			if($i < $max)
			{
				$sql .= ', ';
			}
		}
		$sql .= ';';
		return $sql;
	}
	
	/**
	 * DROP TRIGGER [IF EXISTS] [schema_name.]trigger_name
	 * 
	 * @see IDialect::dropTrigger()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/drop-trigger.html
	 */
	public function dropTrigger(IDropTriggerStatement $statement)
	{
		$sql = 'DROP TRIGGER ';
		if($statement->getIfExists())
			$sql .= 'IF EXISTS ';
		return $sql.$this->quoteTableName($statement->getTriggerName()).';';
	}
	
	/**
	 * DROP VIEW [IF EXISTS]
	 *     view_name [, view_name] ...
	 *     [RESTRICT | CASCADE]
	 * 
	 * @see IDialect::dropView()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/drop-view.html
	 */
	public function dropView(IDropViewStatement $statement)
	{
		$sql = 'DROP VIEW ';
		if($statement->getIfExists())
			$sql .= 'IF EXISTS ';
		foreach($statement->getViewNames() as $viewName)
		{
			$sql .= implode(', ', array_map(
				array($this, 'quoteTableName'),
				$statement->getViewNames()
			));
		}
		if($statement->getMode() !== null)
			$sql .= ' '.$statement->getMode();
		return $sql.';';
	}
	
	/**
	 * RENAME TABLE tbl_name TO new_tbl_name
	 *     [, tbl_name2 TO new_tbl_name2] ...
	 * 
	 * @see IDialect::renameTable()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/rename-table.html
	 */
	public function renameTable(IRenameTableStatement $statement)
	{
		$str = 'RENAME TABLE ';
		$parts = array();
		foreach($statement->getRenameMoveStatements() as $move)
		{
			$parts[] = $this->quoteTableName($move->getFromName())
				.' TO '.$this->quoteTableName($move->getToName());
		}
		return $str.implode(', ', $parts).';';
	}
	
	/**
	 * TRUNCATE [TABLE] tbl_name
	 * 
	 * @see IDialect::truncateTable()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/truncate-table.html
	 */
	public function truncateTable(ITruncateTableStatement $statement)
	{
		return 'TRUNCATE TABLE '.$this->quoteTableName($statement->getTableName()).';';
	}
	
	// Data Manipulation Statements
	// http://dev.mysql.com/doc/refman/5.0/en/sql-syntax-data-manipulation.html
	
	/**
	 * CALL sp_name([parameter[,...]])
	 * CALL sp_name[()]
	 * 
	 * @see IDialect::call()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/call.html
	 */
	public function call(ICallStatement $statement)
	{
		$str = 'CALL '.$this->quoteTableName($statement->getProcedureName());
		return $str.'('.implode(', ', $statement->getParamsValues()).');';
	}
	
	/**
	 * DELETE [LOW_PRIORITY] [QUICK] [IGNORE] FROM tbl_name
	 *     [WHERE where_condition]
	 *     [ORDER BY ...]
	 *     [LIMIT row_count]
	 * 
	 * DELETE [LOW_PRIORITY] [QUICK] [IGNORE]
	 *     tbl_name[.*] [, tbl_name[.*]] ...
	 *     { FROM | USING } table_references
	 *     [WHERE where_condition]
	 * 
	 * @see IDialect::delete()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/delete.html
	 */
	public function delete(IDeleteStatement $statement)
	{
		$sql = 'DELETE';
		if($statement->getLowPriority())
			$sql .= ' LOW_PRIORITY';
		if($statement->getQuick())
			$sql .= ' QUICK';
		if($statement->getIgnore())
			$sql .= ' IGNORE';
		$sql .= ' FROM ';
		$tablenamessql = array();
		foreach($statement->getTableNames() as $tablename)
		{
			$tablenamessql[] = $this->quoteTableName($tablename);
		}
		$sql .= implode(', ', $tablenamessql);
		if($statement->getWhereStatement() !== null)
			$sql .= ' '.$statement->getWhereStatement()->toSQL($this);
		if($statement->getOrderbyStatement() !== null)
			$sql .= ' '.$statement->getOrderbyStatement()->toSQL($this);
		if($statement->getLimitStatement() !== null)
			$sql .= ' '.$statement->getLimitStatement()->toSQL($this);
		return $sql.';';
	}
	
	/**
	 * DO expr [, expr] ...
	 * 
	 * @see IDialect::do_()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/do.html
	 */
	public function do_(IDoStatement $statement)
	{
		$sql = 'DO ';
		$statementssql = array();
		foreach($statement->getDoExpression() as $expression)
		{
			$statementssql[] = $expression->toSql($this);
		}
		return $sql.implode(', ', $statementssql).';';
	}
	
	/**
	 * HANDLER tbl_name OPEN [ [AS] alias]
	 * 
	 * HANDLER tbl_name READ index_name { = | <= | >= | < | > } (value1,value2,...)
	 *     [ WHERE where_condition ] [LIMIT ... ]
	 * HANDLER tbl_name READ index_name { FIRST | NEXT | PREV | LAST }
	 *     [ WHERE where_condition ] [LIMIT ... ]
	 * HANDLER tbl_name READ { FIRST | NEXT }
	 *     [ WHERE where_condition ] [LIMIT ... ]
	 * 
	 * HANDLER tbl_name CLOSE
	 * 
	 * @see IDialect::handler()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/handler.html
	 */
	public function handler(IHandlerStatement $statement)
	{
		return $statement->toSQL($this);
	}
	
	/**
	 * INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE]
	 *     [INTO] tbl_name [(col_name,...)]
	 *     {VALUES | VALUE} ({expr | DEFAULT},...),(...),...
	 *     [ ON DUPLICATE KEY UPDATE
	 *       col_name=expr
	 *         [, col_name=expr] ... ]
	 * 
	 * INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE]
	 *     [INTO] tbl_name
	 *     SET col_name={expr | DEFAULT}, ...
	 *     [ ON DUPLICATE KEY UPDATE
	 *       col_name=expr
	 *         [, col_name=expr] ... ]
	 * 
	 * INSERT [LOW_PRIORITY | HIGH_PRIORITY] [IGNORE]
	 *     [INTO] tbl_name [(col_name,...)]
	 *     SELECT ...
	 *     [ ON DUPLICATE KEY UPDATE
	 *       col_name=expr
	 *         [, col_name=expr] ... ]
	 * 
	 * @see IDialect::insert()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/insert.html
	 */
	public function insert(IInsertStatement $statement)
	{
		return $statement->toSQL($this);
	}
	
	/**
	 * LOAD DATA [LOW_PRIORITY | CONCURRENT] [LOCAL] INFILE 'file_name'
	 *     [REPLACE | IGNORE]
	 *     INTO TABLE tbl_name
	 *     [CHARACTER SET charset_name]
	 *     [{FIELDS | COLUMNS}
	 *         [TERMINATED BY 'string']
	 *         [[OPTIONALLY] ENCLOSED BY 'char']
	 *         [ESCAPED BY 'char']
	 *     ]
	 *     [LINES
	 *         [STARTING BY 'string']
	 *         [TERMINATED BY 'string']
	 *     ]
	 *     [IGNORE number LINES]
	 *     [(col_name_or_user_var,...)]
	 *     [SET col_name = expr,...]
	 * 
	 * @see IDialect::loadDataInfile()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/load-data.html
	 */
	public function loadDataInfile(ILoadDataInfileStatement $statement)
	{
		return $statement->toSQL($this);
	}
	
	/**
	 * REPLACE [LOW_PRIORITY | DELAYED]
	 *     [INTO] tbl_name [(col_name,...)]
	 *     {VALUES | VALUE} ({expr | DEFAULT},...),(...),...
	 * 
	 * REPLACE [LOW_PRIORITY | DELAYED]
	 *     [INTO] tbl_name
	 *     SET col_name={expr | DEFAULT}, ...
	 * 
	 * REPLACE [LOW_PRIORITY | DELAYED]
	 *     [INTO] tbl_name [(col_name,...)]
	 *     SELECT ...
	 * 
	 * @see IDialect::replace()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/replace.html
	 */
	public function replace(IReplaceStatement $statement)
	{
		return $statement->toSQL($this);
	}
	
	/**
	 * SELECT
	 *     [ALL | DISTINCT | DISTINCTROW ]
	 *       [HIGH_PRIORITY]
	 *       [STRAIGHT_JOIN]
	 *       [SQL_SMALL_RESULT] [SQL_BIG_RESULT] [SQL_BUFFER_RESULT]
	 *       [SQL_CACHE | SQL_NO_CACHE] [SQL_CALC_FOUND_ROWS]
	 *     select_expr [, select_expr ...]
	 *     [FROM table_references
	 *     [WHERE where_condition]
	 *     [GROUP BY {col_name | expr | position}
	 *       [ASC | DESC], ... [WITH ROLLUP]]
	 *     [HAVING where_condition]
	 *     [ORDER BY {col_name | expr | position}
	 *       [ASC | DESC], ...]
	 *     [LIMIT {[offset,] row_count | row_count OFFSET offset}]
	 *     [PROCEDURE procedure_name(argument_list)]
	 *     [INTO OUTFILE 'file_name' export_options
	 *       | INTO DUMPFILE 'file_name'
	 *       | INTO var_name [, var_name]]
	 *     [FOR UPDATE | LOCK IN SHARE MODE]]
	 * 
	 * @see IDialect::select()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/select.html
	 */
	public function select(ISelectStatement $statement)
	{
		$sql = 'SELECT ';
		if($statement->getDistinctMode() !== null)
			$sql .= $statement->getDistinctMode().' ';
		if($statement->getPriority() !== null)
			$sql .= $statement->getPriority().' ';
		// XXX STRAIGHT_JOIN not implemented ?
		if($statement->getResultMode() !== null)
			$sql .= $statement->getResultMode().' ';
		if($statement->getCacheMode() !== null)
			$sql .= $statement->getCacheMode().' ';
		// XXX SQL_CALC_FOUND_ROWS not implemented ?
		$selectexpsql = array();
		foreach($statement->getSelectExpressions() as $selectexp)
		{
			$selectexpsql[] = $selectexp->toSQL($this);
		}
		$sql .= implode(', ', $selectexpsql);
		if($statement->getFromStatement() !== null)
			$sql .= ' '.$statement->getFromStatement()->toSQL($this);
		return $sql.';';
	}
	
	/**
	 * UPDATE [LOW_PRIORITY] [IGNORE] table_reference
	 *     SET col_name1={expr1|DEFAULT} [, col_name2={expr2|DEFAULT}] ...
	 *     [WHERE where_condition]
	 *     [ORDER BY ...]
	 *     [LIMIT row_count]
	 * 
	 * UPDATE [LOW_PRIORITY] [IGNORE] table_references
	 *     SET col_name1={expr1|DEFAULT} [, col_name2={expr2|DEFAULT}] ...
	 *     [WHERE where_condition]
	 * 
	 * @see IDialect::update()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/update.html
	 */
	public function update(IUpdateStatement $statement)
	{
		$sql = 'UPDATE';
		if($statement->getLowPriority())
			$sql .= ' LOW_PRIORITY';
		if($statement->getIgnore())
			$sql .= ' IGNORE';
		$tablessql = array();
		foreach($statement->getTablesNames() as $tablename)
		{
			$tablessql[] = $this->quoteTableName($tablename);
		}
		$sql .= ' '.implode(', ', $tablessql);
		$setsql = array();
		foreach($statement->getSetColumnStatements() as $setstatement)
		{
			$setsql[] = $setstatement->toSQL($this);
		}
		$sql .= ' '.implode(', ', $setsql);
		if($statement->getWhereStatement() !== null)
			$sql .= ' '.$statement->getWhereStatement()->toSQL($this);
		if($statement->getOrderByStatement() !== null)
			$sql .= ' '.$statement->getOrderByStatement()->toSQL($this);
		if($statement->getLimitStatement() !== null)
			$sql .= ' '.$statement->getLimitStatement()->toSQL($this);
		return $sql.';';
	}
	
	// Transactional and Locking Statements
	// http://dev.mysql.com/doc/refman/5.0/en/sql-syntax-transactions.html
	
	/**
	 * START TRANSACTION [WITH CONSISTENT SNAPSHOT]
	 * 
	 * @see IDialect::startTransaction()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/commit.html
	 */
	public function startTransaction(IStartTransactionStatement $statement)
	{
		$sql = 'START TRANSACTION';
		if($statement->withConsistentSnapShot())
			return $sql.' WITH CONSISTENT SNAPSHOT;';
		else
			return $sql.';';
	}
	
	/**
	 * COMMIT [WORK] [AND [NO] CHAIN] [[NO] RELEASE]
	 * 
	 * @see IDialect::commit_()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/commit.html
	 */
	public function commit_(ICommitStatement $statement)
	{
		$sql = 'COMMIT';
		if($statement->getChain() !== null)
		{
			if($statement->getChain())
				$sql .= ' AND CHAIN';
			else
				$sql .= ' AND NO CHAIN';
		}
		if($statement->getRelease() !== null)
		{
			if($statement->getRelease())
				$sql .= ' RELEASE';
			else
				$sql .= ' NO RELEASE';
		}
		return $sql.';';
	}
	
	/**
	 * ROLLBACK [WORK] [AND [NO] CHAIN] [[NO] RELEASE]
	 * 
	 * @see IDialect::rollback_()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/commit.html
	 */
	public function rollback_(IRollbackStatement $statement)
	{
		$sql = 'ROLLBACK';
		if($statement->getChain() !== null)
		{
			if($statement->getChain())
				$sql .= ' AND CHAIN';
			else
				$sql .= ' AND NO CHAIN';
		}
		if($statement->getRelease() !== null)
		{
			if($statement->getRelease())
				$sql .= ' RELEASE';
			else
				$sql .= ' NO RELEASE';
		}
		return $sql.';';
	}
	
	/**
	 * SAVEPOINT identifier
	 * 
	 * @see IDialect::savepoint()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/savepoint.html
	 */
	public function savepoint(ISavepointStatement $statement)
	{
		return 'SAVEPOINT '.$this->quoteTableName($statement->getIdentifier()).';';
	}
	
	/**
	 * ROLLBACK [WORK] TO [SAVEPOINT] identifier
	 * 
	 * @see IDialect::rollbackToSavepoint()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/savepoint.html
	 */
	public function rollbackToSavepoint(IRollbackToSavepointStatement $statement)
	{
		return 'ROLLBACK TO '.$this->quoteTableName($statement->getIdentifier()).';';
	}
	
	/**
	 * RELEASE SAVEPOINT identifier
	 * 
	 * @see IDialect::releaseSavepoint()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/savepoint.html
	 */
	public function releaseSavepoint(IReleaseSavepointStatement $statement)
	{
		return 'RELEASE SAVEPOINT '.$this->quoteTableName($statement->getIdentifier()).';';
	}
	
	/**
	 * LOCK TABLES
	 *     tbl_name [[AS] alias] lock_type
	 *     [, tbl_name [[AS] alias] lock_type] ...
	 * 
	 * lock_type:
	 *     READ [LOCAL]
	 *   | [LOW_PRIORITY] WRITE
	 * 
	 * @see IDialect::lockTables()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/lock-tables.html
	 */
	public function lockTables(ILockTablesStatement $statement)
	{
		$sql = 'LOCK TABLES ';
		$tablessql = array();
		foreach($statement->getLockUnitTableStatements() as $unitStatement)
		{
			$tablessql[] = $this->quoteDatabaseName($unitStatement->getTableName())
				.' '.$unitStatement->getLockType()->toSQL($this);
		}
		return $sql.implode(', ', $tablessql).';';
	}
	
	/**
	 * UNLOCK TABLES
	 * 
	 * @see IDialect::unlockTables()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/lock-tables.html
	 */
	public function unlockTables(IUnlockTablesStatement $statement)
	{
		return 'UNLOCK TABLES;';
	}
	
	/**
	 * SET [GLOBAL | SESSION] TRANSACTION ISOLATION LEVEL
	 *   {
	 *        REPEATABLE READ
	 *      | READ COMMITTED
	 *      | READ UNCOMMITTED
	 *      | SERIALIZABLE
	 *    }
	 * 
	 * @see IDialect::setTransaction()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/set-transaction.html
	 */
	public function setTransaction(ISetTransactionStatement $statement)
	{
		$sql = 'SET ';
		if($statement->getScope() !== null)
			$sql .= $statement->getScope();
		return $sql.' TRANSACTION ISOLATION LEVEL '.$statement->getLevel().';';
	}
	
	// Prepared Statements
	// http://dev.mysql.com/doc/refman/5.0/en/sql-syntax-prepared-statements.html
	
	/**
	 * PREPARE stmt_name FROM preparable_stmt
	 * 
	 * @see IDialect::prepare_()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/prepare.html
	 */
	public function prepare_(IPrepareStatement $statement)
	{
		return 'PREPARE '.$this->quoteTableName($statement->getStatementName())
			.' FROM '.$statement->getStatement()->toSQL($this);
	}
	
	/**
	 * EXECUTE stmt_name [USING @var_name [, @var_name] ...]
	 * 
	 * @see IDialect::execute()
	 * @see dev.mysql.com/doc/refman/5.0/en/execute.html
	 */
	public function execute(IExecuteStatement $statement)
	{
		$sql = 'EXECUTE '.$this->quoteTableName($statement->getStatementName());
		$varssql = array();
		foreach($statement->getStatementVarNames() as $varname)
		{
			$varssql[] = '@'.$varname;
		}
		if(count($varssql) > 0)
			return $sql.' USING '.implode(', ', $varssql).';';
		else
			return $sql.';';
	}
	
	/**
	 * {DEALLOCATE | DROP} PREPARE stmt_name
	 * 
	 * @see IDialect::deallocatePrepare()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/deallocate-prepare.html
	 */
	public function deallocatePrepare(IDeallocatePrepareStatement $statement)
	{
		return 'DEALLOCATE PREPARE '.$this->quoteTableName($statement->getStatementName()).';';
	}
	
	// Database Administration Statements
	// http://dev.mysql.com/doc/refman/5.0/en/sql-syntax-server-administration.html
	
	/**
	 * ANALYZE [NO_WRITE_TO_BINLOG | LOCAL] TABLE
	 *     tbl_name [, tbl_name] ...
	 * 
	 * @see IDialect::analyseTable()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/analyze-table.html
	 */
	public function analyseTable(IAnalyseTableStatement $statement)
	{
		$sql = 'ANALYSE';
		if($statement->getMode() !== null)
			$sql .= ' '.$statement->getMode();
		$sql .= ' TABLE ';
		$tablessql = array();
		foreach($statement->getTableNames() as $tablename)
		{
			$tablessql[] = $this->quoteTableName($tablename);
		}
		return $sql.implode(', ', $tablessql).';';
	}
	
	/**
	 * BACKUP TABLE tbl_name [, tbl_name] ... TO '/path/to/backup/directory'
	 * 
	 * @see IDialect::backupTable()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/backup-table.html
	 */
	public function backupTable(IBackupTableStatement $statement)
	{
		$sql = 'BACKUP TABLES ';
		$tablessql = array();
		foreach($statement->getTableNames() as $tablename)
		{
			$tablessql[] = $this->quoteTableName($tablename);
		}
		return $sql.implode(', ', $tablessql)
			.' TO \''.$statement->getTargetFilePath().'\';';
	}
	
	/**
	 * CHECK TABLE tbl_name [, tbl_name] ... [option] ...
	 * 
	 * option = {
	 *     FOR UPGRADE
	 *   | QUICK
	 *   | FAST
	 *   | MEDIUM
	 *   | EXTENDED
	 *   | CHANGED
	 * }
	 * 
	 * @see IDialect::checkTable()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/check-table.html
	 */
	public function checkTable(ICheckTableStatement $statement)
	{
		$sql = 'CHECK TABLE ';
		$tablessql = array();
		foreach($statement->getTableNames() as $tablename)
		{
			$tablessql[] = $this->quoteTableName($tablename);
		}
		$optionssql = array();
		foreach($statement->getOptions() as $option)
		{
			$optionssql[] = $option;
		}
		return $sql.implode(', ', $tablessql).' '.implode(' ', $optionssql).';';
	}
	
	/**
	 * CHECKSUM TABLE tbl_name [, tbl_name] ... [ QUICK | EXTENDED ]
	 * 
	 * @see IDialect::checksumTable()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/checksum-table.html
	 */
	public function checksumTable(IChecksumTableStatement $statement)
	{
		$sql = 'CHECKSUM TABLE ';
		$tablessql = array();
		foreach($statement->getTableNames() as $tablename)
		{
			$tablessql[] = $this->quoteTableName($tablename);
		}
		$sql .= implode(', ', $tablessql);
		if($statement->getOption() !== null)
			return $sql.$statement->getOption().';';
		else
			return $sql.';';
	}
	
	/**
	 * OPTIMIZE [NO_WRITE_TO_BINLOG | LOCAL] TABLE tbl_name [, tbl_name] ...
	 * 
	 * @see IDialect::optimizeTable()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/optimize-table.html
	 */
	public function optimizeTable(IOptimizeTableStatement $statement)
	{
		$sql = 'OPTIMIZE';
		if($statement->getMode() !== null)
			$sql .= ' '.$statement->getMode();
		$sql .= ' TABLE ';
		$tablessql = array();
		foreach($statement->getTableNames() as $tablename)
		{
			$tablessql[] = $this->quoteTableName($tablename);
		}
		return $sql.implode(', ', $tablessql).';';
	}
	
	/**
	 * REPAIR [NO_WRITE_TO_BINLOG | LOCAL] TABLE
	 *     tbl_name [, tbl_name] ...
	 *     [QUICK] [EXTENDED] [USE_FRM]
	 * 
	 * @see IDialect::repairTable()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/repair-table.html
	 */
	public function repairTable(IRepairTableStatement $statement)
	{
		$sql = 'REPAIR';
		if($statement->getMode() !== null)
			$sql .= ' '.$statement->getMode();
		$sql .= ' TABLE ';
		$tablessql = array();
		foreach($statement->getTableNames() as $tablename)
		{
			$tablessql[] = $this->quoteTableName($tablename);
		}
		$sql .= implode(', ', $tablessql);
		if($statement->getQuick())
			$sql .= ' QUICK';
		if($statement->getExtended())
			$sql .= ' EXTENDED';
		if($statement->getUseFrm())
			$sql .= ' USE_FRM';
		return $sql.';';
	}
	
	/**
	 * RESTORE TABLE tbl_name [, tbl_name] ... FROM '/path/to/backup/directory'
	 * 
	 * @see IDialect::restoreTable()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/restore-table.html
	 */
	public function restoreTable(IRestoreTableStatement $statement)
	{
		$sql = 'RESTORE TABLE ';
		$tablessql = array();
		foreach($statement->getTableNames() as $tablename)
		{
			$tablessql[] = $this->quoteTableName($tablename);
		}
		return $sql.' FROM \''.$statement->getTargetFilePath().'\';';
	}
	
	/**
	 * SET variable_assignment [, variable_assignment] ...
	 * 
	 * variable_assignment:
	 *       user_var_name = expr
	 *     | [GLOBAL | SESSION] system_var_name = expr
	 *     | [@@global. | @@session. | @@]system_var_name = expr
	 * 
	 * @see IDialect::set()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/set-statement.html
	 */
	public function set(ISetStatement $statement)
	{
		$sql = 'SET ';
		$assignsql = array();
		foreach($statement->getVariableAssignmentStatements() as $assignment)
		{
			$assignsql[] = $assignment->getVariableName().' = '
				.$assignment->getExpression()->toSql($this);
		}
		return $sql.implode(', ', $assignsql).';';
	}
	
	/**
	 * SHOW BINARY LOGS
	 * SHOW MASTER LOGS
	 * 
	 * @see IDialect::showBinaryLogs()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-binary-logs.html
	 */
	public function showBinaryLogs(IShowBinaryLogsStatement $statement)
	{
		return 'SHOW BINARY LOGS;';
	}
	
	/**
	 * SHOW BINLOG EVENTS
	 *    [IN 'log_name'] [FROM pos] [LIMIT [offset,] row_count]
	 * 
	 * @see IDialect::showBinlogEvents()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-binlog-events.html
	 */
	public function showBinlogEvents(IShowBinlogEventsStatement $statement)
	{
		$sql = 'SHOW BINLOGS EVENTS';
		if($statement->getInLogName() !== null)
			$sql .= ' IN \''.$statement->getInLogName().'\'';
		if($statement->getFromPos() !== null)
			$sql .= ' FROM '.$statement->getFromPos();
		if($statement->getLimit() !== null)
		{
			$sql .= ' LIMIT ';
			if($statement->getOffset() !== null)
				$sql .= $statement->getOffset().', ';
			$sql .= $statement->getLimit();
		}
		return $sql.';';
	}
	
	/**
	 * SHOW CHARACTER SET [LIKE 'pattern' | WHERE expr]
	 * 
	 * @see IDialect::showCharacterSet()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-character-set.html
	 */
	public function showCharacterSet(IShowCharacterSetStatement $statement)
	{
		return $statement->toSQL($this);
	}
	
	/**
	 * SHOW COLLATION [LIKE 'pattern' | WHERE expr]
	 * 
	 * @see IDialect::showCollation()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-collation.html
	 */
	public function showCollation(IShowCollationStatement $statement)
	{
		return $statement->toSQL($this);
	}
	
	/**
	 * SHOW [FULL] COLUMNS {FROM | IN} tbl_name [{FROM | IN} db_name]
	 *     [LIKE 'pattern' | WHERE expr]
	 * 
	 * @see IDialect::showColumns()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-columns.html
	 */
	public function showColumns(IShowColumnsStatement $statement)
	{
		return $statement->toSQL($this);
	}
	
	/**
	 * SHOW CREATE {DATABASE | SCHEMA} [IF NOT EXISTS] db_name
	 * 
	 * @see IDialect::showCreateDatabase()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-create-database.html
	 */
	public function showCreateDatabase(IShowCreateDatabaseStatement $statement)
	{
		$sql = 'SHOW CREATE DATABASE ';
		if($statement->getIfNotExists())
			$sql .= 'IF NOT EXISTS ';
		return $sql.$this->quoteDatabaseName($statement->getDatabaseName()).';';
	}
	
	/**
	 * SHOW CREATE FUNCTION func_name
	 * 
	 * @see IDialect::showCreateFunction()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-create-function.html
	 */
	public function showCreateFunction(IShowCreateFunctionStatement $statement)
	{
		return 'SHOW CREATE FUNCTION '.$this->quoteTableName($statement->getFunctionName()).';';
	}
	
	/**
	 * SHOW CREATE PROCEDURE proc_name
	 * 
	 * @see IDialect::showCreateProcedure()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-create-procedure.html
	 */
	public function showCreateProcedure(IShowCreateProcedureStatement $statement)
	{
		return 'SHOW CREATE PROCEDURE '.$this->quoteTableName($statement->getProcedureName()).';';
	}
	
	/**
	 * SHOW CREATE TABLE tbl_name
	 * 
	 * @see IDialect::showCreateTable()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-create-table.html
	 */
	public function showCreateTable(IShowCreateTableStatement $statement)
	{
		return 'SHOW CREATE TABLE '.$this->quoteTableName($statement->getTableName()).';';
	}
	
	/**
	 * SHOW CREATE VIEW view_name
	 * 
	 * @see IDialect::showCreateView()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-create-view.html
	 */
	public function showCreateView(IShowCreateViewStatement $statement)
	{
		return 'SHOW CREATE VIEW '.$this->quoteTableName($statement->getViewName()).';';
	}
	
	/**
	 * SHOW {DATABASES | SCHEMAS} [LIKE 'pattern' | WHERE expr]
	 * 
	 * @see IDialect::showDatabases()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-databases.html
	 */
	public function showDatabases(IShowDatabasesStatement $statement)
	{
		$sql = 'SHOW DATABASES';
		if($statement->getShowLikeStatement() !== null)
		{
			$sql .= ' '.$statement->getShowLikeStatement()->toSQL($dialect);
		}
		if($statement->getWhereStatement() !== null)
		{
			$sql .= ' '.$statement->getWhereStatement()->toSQL($dialect);
		}
		$sql .= ";";
		return $sql;
	}
	
	/**
	 * SHOW ENGINE engine_name {LOGS | STATUS }
	 * 
	 * @see IDialect::showEngine()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-engine.html
	 */
	public function showEngine(IShowEngineStatement $statement)
	{
		return 'SHOW ENGINE '.$this->quoteTableName($statement->getEngineName())
			.' '.$statement->getCaracteristic().';';
	}
	
	/**
	 * SHOW [STORAGE] ENGINES
	 * 
	 * @see IDialect::showEngines()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-engines.html
	 */
	public function showEngines(IShowEnginesStatement $statement)
	{
		return 'SHOW ENGINES;';
	}
	
	/**
	 * SHOW ERRORS [LIMIT [offset,] row_count]
	 * SHOW COUNT(*) ERRORS
	 * 
	 * @see IDialect::showErrors()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-errors.html
	 */
	public function showErrors(IShowErrorsStatement $statement)
	{
		$sql = 'SHOW';
		if($statement->getCount())
			$sql .= ' COUNT(*)';
		$sql .= ' ERRORS';
		if($statement->getLimit() !== null)
		{
			$sql .= ' LIMIT ';
			if($statement->getOffset() !== null)
				$sql .= $statement->getOffset().', ';
			$sql .= $statement->getLimit();
		}
		return $sql.';';
	}
	
	/**
	 * SHOW FUNCTION CODE func_name
	 * 
	 * @see IDialect::showFunctionCode()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-function-code.html
	 */
	public function showFunctionCode(IShowFunctionCodeStatement $statement)
	{
		return 'SHOW FUNCTION CODE '.$this->quoteTableName($statement->getFunctionName()).';';
	}
	
	/**
	 * SHOW FUNCTION STATUS [LIKE 'pattern' | WHERE expr]
	 * 
	 * @see IDialect::showFunctionStatus()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-function-status.html
	 */
	public function showFunctionStatus(IShowFunctionStatusStatement $statement)
	{
		return $statement->toSQL($this);
	}
	
	/**
	 * SHOW GRANTS [FOR user]
	 * 
	 * @see IDialect::showGrants()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-grants.html
	 */
	public function showGrants(IShowGrantsStatement $statement)
	{
		$sql = 'SHOW GRANTS';
		if($statement->getUserName() !== null)
			$sql .= ' '.$statement->getUserName();
		return $sql.';';
	}
	
	/**
	 * SHOW {INDEX | INDEXES | KEYS}
	 *     {FROM | IN} tbl_name
	 *     [{FROM | IN} db_name]
	 *     [WHERE expr]
	 * 
	 * @see IDialect::showIndex()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-index.html
	 */
	public function showIndex(IShowIndexStatement $statement)
	{
		$sql = 'SHOW INDEX FROM '.$this->quoteTableName($statement->getTableName());
		if($statement->getDatabaseName() !== null)
			$sql .= ' IN '.$this->quoteDatabaseName($statement->getDatabaseName());
		if($statement->getWhereStatement() !== null)
			$sql .= $statement->getWhereStatement()->toSQL($this);
		return $sql.';';
	}
	
	/**
	 * SHOW [BDB] LOGS
	 * 
	 * @see IDialect::showLogs()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-logs.html
	 */
	public function showLogs(IShowLogsStatement $statement)
	{
		return 'SHOW LOGS;';
	}
	
	/**
	 * SHOW MASTER STATUS
	 * 
	 * @see IDialect::showMasterStatus()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-master-status.html
	 */
	public function showMasterStatus(IShowMasterStatusStatement $statement)
	{
		return 'SHOW MASTER STATUS;';
	}
	
	/**
	 * SHOW MUTEX STATUS
	 * 
	 * @see IDialect::showMutexStatus()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-mutex-status.html
	 */
	public function showMutexStatus(IShowMutexStatusStatement $statement)
	{
		return 'SHOW MUTEX STATUS;';
	}
	
	/**
	 * SHOW OPEN TABLES [{FROM | IN} db_name] [LIKE 'pattern' | WHERE expr]
	 * 
	 * @see IDialect::showOpenTables()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-open-tables.html
	 */
	public function showOpenTables(IShowOpenTablesStatement $statement)
	{
		return $statement->toSQL($this);
	}
	
	/**
	 * SHOW PRIVILEGES
	 * 
	 * @see IDialect::showPrivileges()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-privileges.html
	 */
	public function showPrivileges(IShowPrivilegesStatement $statement)
	{
		return 'SHOW PRIVILEGES;';
	}
	
	/**
	 * SHOW PROCEDURE CODE proc_name
	 * 
	 * @see IDialect::showProcedureCode()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-procedure-code.html
	 */
	public function showProcedureCode(IShowProcedureCodeStatement $statement)
	{
		return 'SHOW PROCEDURE CODE '.$this->quoteTableName($statement->getProcedureName()).';';
	}
	
	/**
	 * SHOW PROCEDURE STATUS [LIKE 'pattern' | WHERE expr]
	 * 
	 * @see IDialect::showProcedureStatus()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-procedure-status.html
	 */
	public function showProcedureStatus(IShowProcedureStatusStatement $statement)
	{
		return $statement->toSQL($this);
	}
	
	/**
	 * SHOW [FULL] PROCESSLIST
	 * 
	 * @see IDialect::showProcesslist()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-processlist.html
	 */
	public function showProcesslist(IShowProcesslistStatement $statement)
	{
		return 'SHOW PROCESSLIST;';
	}
	
	/**
	 * SHOW PROFILE [type [, type] ... ]
	 *     [FOR QUERY n]
	 *     [LIMIT row_count [OFFSET offset]]
	 * 
	 * type:
	 *     ALL
	 *   | BLOCK IO
	 *   | CONTEXT SWITCHES
	 *   | CPU
	 *   | IPC
	 *   | MEMORY
	 *   | PAGE FAULTS
	 *   | SOURCE
	 *   | SWAPS
	 * 
	 * @see IDialect::showProfile()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-profile.html
	 */
	public function showProfile(IShowProfileStatement $statement)
	{
		$sql = 'SHOW PROFILE '.implode(', ', $statement->getTypes());
		if($statement->getQueryNumber() !== null)
			$sql .= ' FOR QUERY '.$statement->getQueryNumber();
		if($statement->getLimit() !== null)
		{
			$sql .= ' LIMIT '.$statement->getLimit();
			if($statement->getOffset() !== null)
				$sql .= ' OFFSET '.$statement->getOffset();
		}
		return $sql.';';
	}
	
	/**
	 * SHOW PROFILES
	 * 
	 * @see IDialect::showProfiles()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-profiles.html
	 */
	public function showProfiles(IShowProfilesStatement $statement)
	{
		return 'SHOW PROFILES;';
	}
	
	/**
	 * SHOW [GLOBAL | SESSION] STATUS [LIKE 'pattern' | WHERE expr]
	 * 
	 * @see IDialect::showStatus()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-slave-hosts.html
	 */
	public function showStatus(IShowStatusStatement $statement)
	{
		return $statement->toSQL($this);
	}
	
	/**
	 * SHOW TABLE STATUS [{FROM | IN} db_name] [LIKE 'pattern' | WHERE expr]
	 * 
	 * @see IDialect::showTableStatus()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-table-status.html
	 */
	public function showTableStatus(IShowTableStatusStatement $statement)
	{
		return $statement->toSQL($this);
	}
	
	/**
	 * SHOW [FULL] TABLES [{FROM | IN} db_name] [LIKE 'pattern' | WHERE expr]
	 * 
	 * @see IDialect::showTables()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-tables.html
	 */
	public function showTables(IShowTablesStatement $statement)
	{
		$sql = 'SHOW TABLES';
		if(($name = $statement->getDatabaseName()) !== null)
		{
			$sql .= ' FROM '.$this->quoteDatabaseName($name);
		}
		if($statement->getShowLikeStatement() !== null)
		{
			$sql .= ' '.$statement->getShowLikeStatement()->toSQL($dialect);
		}
		if($statement->getWhereStatement() !== null)
		{
			$sql .= ' '.$statement->getWhereStatement()->toSQL($dialect);
		}
		$sql .= ';';
		return $sql;
	}
	
	/**
	 * SHOW TRIGGERS [{FROM | IN} db_name] [LIKE 'pattern' | WHERE expr]
	 * 
	 * @see IDialect::showTriggers()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-triggers.html
	 */
	public function showTriggers(IShowTriggersStatement $statement)
	{
		return $statement->toSQL($this);
	}
	
	/**
	 * SHOW [GLOBAL | SESSION] VARIABLES [LIKE 'pattern' | WHERE expr]
	 * 
	 * @see IDialect::showVariables()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-variables.html
	 */
	public function showVariables(IShowVariablesStatement $statement)
	{
		return $statement->toSQL($this);
	}
	
	/**
	 * SHOW WARNINGS [LIMIT [offset,] row_count]
	 * SHOW COUNT(*) WARNINGS
	 * 
	 * @see IDialect::showWarnings()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/show-warnings.html
	 */
	public function showWarnings(IShowWarningsStatement $statement)
	{
		$sql = 'SHOW';
		if($statement->getCount())
			$sql .= ' COUNT(*)';
		$sql .= ' WARNINGS';
		if($statement->getLimit() !== null)
		{
			$sql .= ' LIMIT ';
			if($statement->getOffset() !== null)
				$sql .= $statement->getOffset().', ';
			$sql .= $statement->getLimit();
		}
		return $sql.';';
	}
	
	/**
	 * CACHE INDEX
	 *   tbl_index_list [, tbl_index_list] ...
	 *   IN key_cache_name
	 * 
	 * tbl_index_list:
	 *   tbl_name [[INDEX|KEY] (index_name[, index_name] ...)]
	 * 
	 * @see IDialect::cacheIndex()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/cache-index.html
	 */
	public function cacheIndex(ICacheIndexStatement $statement)
	{
		$sql = 'CACHE INDEX ';
		$tableindexsql = array();
		foreach($statement->getTableIndexListStatements() as $tableindex)
		{
			$tsql = $this->quoteTableName($tableindex->getTableName());
			if(count($tableindex->getIndexNames()) > 0)
			{
				$indexessql = array();
				foreach($tableindex->getIndexNames() as $indexname)
				{
					$indexessql[] = $this->quoteTableName($indexname);
				}
				$tsql .= ' ('.implode(', ', $indexessql).')';
			}
			$tableindexsql[] = $tsql;
		}
		return $sql.implode(', ', $tableindexsql).' IN '
			.$this->quoteTableName($statement->getCacheKey()).';';
	}
	
	/**
	 * FLUSH [NO_WRITE_TO_BINLOG | LOCAL] flush_option [, flush_option] ...
	 * 
	 * flush_option = {
	 *      DES_KEY_FILE
	 *    | HOSTS
	 *    | LOGS
	 *    | MASTER
	 *    | PRIVILEGES
	 *    | QUERY CACHE
	 *    | SLAVE
	 *    | STATUS
	 *    | TABLES
	 *    | USER_RESOURCES
	 * }
	 * 
	 * @see IDialect::flush()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/flush.html
	 */
	public function flush(IFlushStatement $statement)
	{
		$sql = 'FLUSH ';
		if($statement->getMode() !== null)
			$sql .= $statement->getMode().' ';
		return $sql.implode(', ', $statement->getFlushOptions()).';';
	}
	
	/**
	 * KILL [CONNECTION | QUERY] processlist_id
	 * 
	 * @see IDialect::kill()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/kill.html
	 */
	public function kill(IKillStatement $statement)
	{
		return 'KILL '.$statement->getProcessId().';';
	}
	
	/**
	 * LOAD INDEX INTO CACHE
	 *   tbl_index_list [, tbl_index_list] ...
	 * 
	 * tbl_index_list:
	 *   tbl_name
	 *     [[INDEX|KEY] (index_name[, index_name] ...)]
	 *     [IGNORE LEAVES]
	 * 
	 * @see IDialect::loadIndexIntoCache()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/load-index.html
	 */
	public function loadIndexIntoCache(ILoadIndexIntoCacheStatement $statement)
	{
		$sql = 'LOAD INDEX INTO CACHE ';
		$tableindexsql = array();
		foreach($statement->getTableIndexListStatements() as $tableindex)
		{
			$tsql = $this->quoteTableName($tableindex->getTableName());
			if(count($tableindex->getIndexNames()) > 0)
			{
				$indexessql = array();
				foreach($tableindex->getIndexNames() as $indexname)
				{
					$indexessql[] = $this->quoteTableName($indexname);
				}
				$tsql .= ' ('.implode(', ', $indexessql).')';
			}
			if($tableindex->getIgnoreLeaves())
				$tsql .= ' IGNORE LEAVES';
			$tableindexsql[] = $tsql;
		}
		return $sql.implode(', ', $tableindexsql).';';
	}
	
	/**
	 * RESET reset_option [, reset_option] ...
	 * 
	 * reset_option = {
	 * 		  MASTER
	 * 		| QUERY CACHE
	 * 		| SLAVE
	 * }
	 * 
	 * @see IDialect::reset()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/reset.html
	 */
	public function reset(IResetStatement $statement)
	{
		$sql = 'RESET '.implode(', ', $statement->getResetOptions()).';';
	}
	
	// Utility Statements
	// http://dev.mysql.com/doc/refman/5.0/en/sql-syntax-utility.html
	
	/**
	 * {EXPLAIN | DESCRIBE | DESC} tbl_name [col_name | wild]
	 * 
	 * {EXPLAIN | DESCRIBE | DESC} [EXTENDED] SELECT select_options
	 * 
	 * @see IDialect::explain()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/explain.html
	 */
	public function explain(IExplainStatement $statement)
	{
		return $statement->toSQL($this);
	}
	
	/**
	 * HELP 'search_string'
	 * 
	 * @see IDialect::help()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/help.html
	 */
	public function help(IHelpStatement $statement)
	{
		return 'HELP '.$this->quoteString($statement->getSearchString()).';';
	}
	
	/**
	 * USE db_name
	 * 
	 * @see IDialect::use_()
	 * @see http://dev.mysql.com/doc/refman/5.0/en/use.html
	 */
	public function use_(IUseStatement $statement)
	{
		return 'USE '.$this->quoteDatabaseName($statement->getDatabaseName()).';';
	}
	
	
	// parts statements
	// used by all others statements to complete their sql status
	// used by double dispatch structures
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::charsetSpecification()
	 */
	public function charsetSpecification(ICharsetSpecificationStatement $statement)
	{
		return 'CHARACTER SET '.$statement->getCharset()->getName();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::collationSpecification()
	 */
	public function collationSpecification(ICollationSpecificationStatement $statement)
	{
		return 'COLLATE '.$statement->getCollation()->getName();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::commentCharacteristic()
	 */
	public function commentCharacteristic(ICommentCharacteristicStatement $statement)
	{
		return 'COMMENT '.$this->quoteString($statement->getCommentValue());
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::deterministicCharacteristic()
	 */
	public function deterministicCharacteristic(IDeterministicCharacteristicStatement $statement)
	{
		return ($statement->isNot() ? 'NOT ' : '').'DETERMINISTIC';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::languageCharacteristic()
	 */
	public function languageCharacteristic(ILanguageCharacteristicStatement $statement)
	{
		return 'LANGUAGE SQL';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::securityCharacteristic()
	 */
	public function securityCharacteristic(ISecurityCharacteristicStatement $statement)
	{
		return 'SQL SECURITY '.$statement->getSecurityParam();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::sqlCharacteristic()
	 */
	public function sqlCharacteristic(ISqlCharacteristicStatement $statement)
	{
		return $statement->getSqlModifier();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::tableOptionsSpecifications()
	 */
	public function tableOptionsSpecifications(ITableOptionsSpecificationStatement $statement)
	{
		$sql = array();
		foreach($statement->getTableOptionsStatements() as $chdstt)
		{
			$sql[] = $chdstt->toSQL($this);
		}
		return implode(', ', $sql);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::tableEngineOption()
	 */
	public function tableEngineOption(IEngineTableOptionStatement $statement)
	{
		return 'ENGINE '.$statement->getEngine()->getName();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::tableAutoIncrementOption()
	 */
	public function tableAutoIncrementOption(IAutoIncrementTableOptionStatement $statement)
	{
		return 'AUTO_INCREMENT '.$statement->getValue();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::tableAvgRowLengthOption()
	 */
	public function tableAvgRowLengthOption(IAvgRowLengthTableOptionStatement $statement)
	{
		return 'AVG_ROW_LENGTH '.$statement->getValue();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::tableCharsetOption()
	 */
	public function tableCharsetOption(ICharsetTableOptionStatement $statement)
	{
		return 'CHARSET '.$statement->getCharset()->getName();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::tableChecksumOption()
	 */
	public function tableChecksumOption(IChecksumTableOptionStatement $statement)
	{
		return 'CHECKSUM '.((int) $statement->getEnabled());
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::tableCollationOption()
	 */
	public function tableCollationOption(ICollationTableOptionStatement $statement)
	{
		return 'COLLATION '.$statement->getCollation()->getName();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::tableCommentOption()
	 */
	public function tableCommentOption(ICommentTableOptionStatement $statement)
	{
		$comment_val = $statement->getValue();
		if(empty($comment_val) || is_string($comment_val) && trim($comment_val) == '')
			return 'COMMENT NULL';
		else
			return 'COMMENT '.trim($this->quoteString($comment_val));
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::tableConnectionOption()
	 */
	public function tableConnectionOption(IConnectionTableOptionStatement $statement)
	{
		return 'CONNECTION '.$this->quoteString($statement->getConnectionString());
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::tableDataDirectoryOption()
	 */
	public function tableDataDirectoryOption(IDataDirectoryTableOptionStatement $statement)
	{
		return 'DATA DIRECTORY '.$this->quoteString($statement->getAbsolutePathToDirectory());
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::tableDelayKeyWriteOption()
	 */
	public function tableDelayKeyWriteOption(IDelayKeyWriteTableOptionStatement $statement)
	{
		return 'DELAY_KEY_WRITE '.((int) $statement->getEnabled());
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::tableIndexDirectoryOption()
	 */
	public function tableIndexDirectoryOption(IIndexDirectoryTableOptionStatement $statement)
	{
		return 'INDEX DIRECTORY '.$this->quoteString($statement->getAbsolutePathToDirectory());
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::tableInsertMethodOption()
	 */
	public function tableInsertMethodOption(IInsertMethodTableOptionStatement $statement)
	{
		return 'INSERT_METHOD '.$statement->getValue();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::tableMaxRowsOption()
	 */
	public function tableMaxRowsOption(IMaxRowsTableOptionStatement $statement)
	{
		return 'MAX_ROWS '.$statement->getValue();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::tableMinRowsOption()
	 */
	public function tableMinRowsOption(IMinRowsTableOptionStatement $statement)
	{
		return 'MIN_ROWS '.$statement->getValue();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::tablePackKeysOption()
	 */
	public function tablePackKeysOption(IPackKeysTableOptionStatement $statement)
	{
		return 'PACK_KEYS '.$statement->getValue();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::tablePasswordOption()
	 */
	public function tablePasswordOption(IPasswordTableOptionStatement $statement)
	{
		return 'PASSWORD '.$this->quoteString($statement->getValue());
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::tableRowFormatOption()
	 */
	public function tableRowFormatOption(IRowFormatTableOptionStatement $statement)
	{
		return 'ROW_FORMAT '.$statement->getValue();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::tableUnionOption()
	 */
	public function tableUnionOption(IUnionTableOptionStatement $statement)
	{
		$sql = array();
		foreach($statement->getTableNames() as $tablename)
		{
			$sql[] = $this->quoteTableName($tablename);
		}
		return 'UNION '.implode(', ', $sql);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::addColumnSpecification()
	 */
	public function addColumnSpecification(IAddColumnSpecificationStatement $statement)
	{
		$sql = 'ADD COLUMN '.$this->quoteTableName($statement->getColumnName());
		$sql .= ' '.$statement->getColumnDefinitionStatement()->toSQL($this);
		return $sql.' '.$statement->getColumnPositionStatement()->toSQL($this);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::columnPositionFirst()
	 */
	public function columnPositionFirst(IColumnPositionFirstStatement $statement)
	{
		return 'FIRST';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::columnPositionAfter()
	 */
	public function columnPositionAfter(IColumnPositionAfterStatement $statement)
	{
		return 'AFTER '.$this->quoteTableName($statement->getColumnName());
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::addIndexSpecification()
	 */
	public function addIndexSpecification(IAddIndexSpecificationStatement $statement)
	{
		$sql = 'ADD';
		if($statement->getIndexSpecification() !== null)
			$sql .= ' '.$statement->getIndexSpecification();
		$sql .= ' INDEX '.$this->quoteTableName($statement->getIndexName());
		$colnamessql = array();
		foreach($statement->getIndexColNames() as $columnname)
		{
			$colnamessql[] = $columnname->toSQL($this);
		}
		$sql .= ' ('.implode(', ', $colnamessql).')';
		if($statement->getIndexType() !== null)
			$sql .= ' USING '.$statement->getIndexType();
		return $sql;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::indexColumnSpecification()
	 */
	public function indexColumnSpecification(IIndexColumnNameSpecificationStatement $statement)
	{
		$sql = $this->quoteTableName($statement->getColumnName());
		if($statement->getLength() !== null)
			$sql .= ' ('.$statement->getLength().')';
		if($statement->getOrder() !== null)
			$sql .= ' '.$statement->getOrder();
		return $sql; 
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::simpleColumnDefinition()
	 */
	public function simpleColumnDefinition(ISimpleColumnDefinitionStatement $statement)
	{
		$str = $statement->getDataType()->toSQL($this);
		if($statement->getNotNull() && $statement->getDataType()->getDefaultValue() !== null)
		{
			$defaultvalue = $statement->getDataType()->getDefaultValue();
			if(is_string($defaultvalue) && trim($defaultvalue) === '')
				$str .= ' NOT NULL DEFAULT \'\'';
			elseif(is_string($defaultvalue))
				$str .= ' NOT NULL DEFAULT '.$this->quoteString($defaultvalue);
			elseif($defaultvalue instanceof DateTime)
				$str .= ' NOT NULL DEFAULT CURRENT_TIMESTAMP';
			else	// normally, should be numerical
				$str .= ' NOT NULL DEFAULT '.$defaultvalue;
		}
		else
			$str .= ' DEFAULT NULL';
		if($statement->getUnique() && !$statement->getPrimary())
			$str .= ' UNIQUE';
		if($statement->getPrimary())
			$str .= ' PRIMARY KEY';
		if($statement->getAutoIncrement())
			$str .= ' AUTO_INCREMENT';
		if($statement->getComment() !== null)
			$str .= ' COMMENT '.$this->quoteString($statement->getComment());
		if($statement->getColumnFormat() !== null)
			$str .= ' COLUMN FORMAT '.$statement->getColumnFormat();
		if($statement->getReferenceStatement() !== null)
			$str .= ' '.$statement->getReferenceStatement()->toSQL($this);
		return $str;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::addConstraintSpecification()
	 */
	public function addConstraintSpecification(IAddConstraintSpecificationStatement $statement)
	{
		$sql = 'ADD';
		if($statement->getSymbol() !== null)
			$sql .= ' CONSTRAINT '.$this->quoteTableName($statement->getSymbol());
		if($statement->getConstraintType() === IAddConstraintSpecificationStatement::PRIMARY)
			$sql .= ' PRIMARY KEY';
		else
			$sql .= ' UNIQUE KEY '.$this->quoteTableName($statement->getIndexName());
		$colsqls = array();
		foreach($statement->getIndexColNames() as $colname)
		{
			$colsqls[] = $colname->toSQL($this);
		}
		$sql .= ' ('.implode(', ', $colsqls).')';
		if($statement->getIndexType() !== null)
			$sql .= ' USING '.$statement->getIndexType();
		return $sql;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::addForeignKeySpecification()
	 */
	public function addForeignKeySpecification(IAddForeignKeySpecificationStatement $statement)
	{
		$sql = 'ADD CONSTRAINT '.$this->quoteTableName($statement->getSymbol());
		$sql .= ' FOREIGN KEY '.$this->quoteTableName($statement->getIndexName());
		$colsql = array();
		foreach($statement->getIndexColumnNames() as $column)
		{
			$colsql[] = $column->toSQL($this);
		}
		$sql .= ' ('.implode(', ', $colsql).')';
		$sql .= ' REFERENCES '.$this->quoteTableName($statement->getTargetTableName());
		$colsql = array();
		foreach($statement->getTargetIndexNames() as $column)
		{
			$colsql[] = $this->quoteTableName($column);
		}
		$sql .= ' ('.implode(', ', $colsql).')';
		$sql .= ' ON UPDATE '.$statement->getOnUpdateConstraint();
		$sql .= ' ON DELETE '.$statement->getOnDeleteConstraint();
		return $sql;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::alterColumnSpecification()
	 */
	public function alterColumnSpecification(IAlterColumnSpecificationStatement $statement)
	{
		$sql = 'ALTER COLUMN '.$this->quoteTableName($statement->getColumnName());
		if($statement->getDefaultValue() === null)
			$sql .= ' DROP DEFAULT';
		else
		{
			$sql .= ' SET DEFAULT ';
			switch(gettype($statement->getDefaultValue()))
			{
				case 'boolean':
					$sql .= $statement->getDefaultValue() ? '1' : '0';
					break;
				case 'integer':
				case 'double':
					$sql .= $statement->getDefaultValue();
					break;
				case 'string':
					$sql .= $this->quoteString($statement->getDefaultValue());
					break;
				case 'array':
					$varssql = array();
					foreach($statement->getDefaultValue() as $value)
					{
						switch(gettype($value))
						{
							case 'boolean':
								$varssql[] = $value ? '1' : '0';
								break;
							case 'integer':
							case 'double':
								$varssql[] = $value;
								break;
							case 'string':
								$varssql[] = $this->quoteString($value);
							default:
								// does nothing
						}
					}
					if(count($varssql) > 0)
					{
						$sql .= '('.implode(', ', $varssql).')';
					}
				default:
					// does nothing
			}
		}
		return $sql;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::changeColumnSpecification()
	 */
	public function changeColumnSpecification(IChangeColumnSpecificationStatement $statement)
	{
		$sql = 'CHANGE COLUMN ';
		$sql .= $this->quoteTableName($statement->getOldColumnName());
		$sql .= ' '.$this->quoteTableName($statement->getNewColumnName());
		$sql .= ' '.$statement->getColumnDefinition()->toSQL($this);
		if($statement->getPositionStatement() !== null)
			$sql .= ' '.$statement->getPositionStatement()->toSQL($this);
		return $sql;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::modifyColumnSpecification()
	 */
	public function modifyColumnSpecification(IModifyColumnSpecificationStatement $statement)
	{
		$sql = 'MODIFY COLUMN ';
		$sql .= $this->quoteTableName($statement->getColumnName());
		$sql .= ' '.$statement->getColumnDefinition()->toSQL($this);
		if($statement->getPositionStatement() !== null)
			$sql .= ' '.$statement->getPositionStatement()->toSQL($this);
		return $sql;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::dropColumnSpecification()
	 */
	public function dropColumnSpecification(IDropColumnSpecificationStatement $statement)
	{
		return 'DROP COLUMN '.$this->quoteTableName($statement->getColumnName());
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::dropPrimarySpecification()
	 */
	public function dropPrimarySpecification(IDropPrimarySpecificationStatement $statement)
	{
		return 'DROP PRIMARY KEY';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::dropIndexSpecification()
	 */
	public function dropIndexSpecification(IDropIndexSpecificationStatement $statement)
	{
		return 'DROP INDEX '.$this->quoteTableName($statement->getIndexName());
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::dropForeignSpecification()
	 */
	public function dropForeignSpecification(IDropForeignSpecificationStatement $statement)
	{
		return 'DROP FOREIGN KEY '.$this->quoteTableName($statement->getForeignKeySymbol());
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::disableKeysSpecification()
	 */
	public function disableKeysSpecification(IDisableKeysSpecificationStatement $statement)
	{
		return 'DISABLE KEYS';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::enableKeysSpecification()
	 */
	public function enableKeysSpecification(IEnableKeysSpecificationStatement $statement)
	{
		return 'ENABLE KEYS';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::renameSpecification()
	 */
	public function renameSpecification(IRenameSpecificationStatement $statement)
	{
		return 'RENAME TO '.$this->quoteTableName($statement->getNewTableName());
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::orderBySpecification()
	 */
	public function orderBySpecification(IOrderBySpecificationStatement $statement)
	{
		$sql = 'ORDER BY ';
		$tablessql = array();
		foreach($statement->getTableNames() as $tablename)
		{
			$tablessql[] = $this->quoteTableName($tablename);
		}
		return $sql.implode(', ', $tablessql);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::convertCharsetSpecification()
	 */
	public function convertCharsetSpecification(IConvertCharsetSpecificationStatement $statement)
	{
		$sql = 'CONVERT TO CHARACTER SET '.$statement->getCharset()->getName();
		if($statement->getCollation() !== null)
			$sql .= ' COLLATE '.$statement->getCollation()->getName();
		return $sql;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::defaultCharsetSpecification()
	 */
	public function defaultCharsetSpecification(IDefaultCharsetSpecificationStatement $statement)
	{
		$sql = 'DEFAULT CHARACTER SET '.$statement->getCharset()->getName();
		if($statement->getCollation() !== null)
			$sql .= ' COLLATE '.$statement->getCollation()->getName();
		return $sql;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::discardTablespaceSpecification()
	 */
	public function discardTablespaceSpecification(IDiscardTablespaceSpecificationStatement $statement)
	{
		return 'DISCARD TABLESPACE';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::importTablespaceSpecification()
	 */
	public function importTablespaceSpecification(IImportTablespaceSpecificationStatement $statement)
	{
		return 'IMPORT TABLESPACE';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::routineBody()
	 */
	public function routineBody(IRoutineBodyStatement $statement)
	{
		if(count($statement->getInstructions()) > 0)
		{
			$sql = array();
			foreach($statement->getInstructions() as $instruction)
			{
				$sql[] = $instruction->toSQL($this);
			}
			return implode('; ', $sql).';';
		}
		return '';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::functionParam()
	 */
	public function functionParam(IFunctionParamStatement $statement)
	{
		return $this->quoteTableName($statement->getParamName()).' '
			.$statement->getDataType()->toSQL($this);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::columnIndex()
	 */
	public function columnIndex(IColumnIndexStatement $statement)
	{
		$sql = $this->quoteTableName($statement->getColumnName());
		if($statement->getLength() !== null)
			$sql .= ' ('.$statement->getLength().')';
		if($statement->getOrder() !== null)
			$sql .= ' '.$statement->getOrder();
		return $sql;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDialect::procedureParam()
	 */
	public function procedureParam(IProcedureParamStatement $statement)
	{
		if($statement->getType() !== null)
			return $statement->getType().' '.$this->functionParam($statement);
		else
			return $this->functionParam($statement);
	}
	
}

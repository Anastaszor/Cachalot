<?php

/**
 * Cachalot class file.
 * 
 * @author Anastaszor
 */
class Cachalot extends CachalotObject implements DBMS, Executor, APP
{
	/**
	 * 
	 * @var StdPhpTranslator
	 */
	private static $_translator = null;
	
	/**
	 * 
	 * @var string
	 */
	private $_path = null;
	
	/**
	 * 
	 * @var CachalotObject[]
	 */
	private $_components = array();
	/**
	 * 
	 * @var mixed[]
	 */
	private $_options = array();
	
	/**
	 * 
	 * @var IDatabase
	 */
	private $_currentdb = null;
	
	/**
	 * Builds a new instance of the Cachalot DBMS according to the given
	 * parameters.
	 * @param string $dsn the directory name, then all additional parameters,
	 * 	like the database to set up first, or the default charset, etc...
	 * @return Cachalot a new instance of the DBMS
	 */
	public static function connect($dsn, $options = array())
	{
		if(strpos($dsn, 'file://') === 0)
		{
			$dsn = str_replace('file://', '', $dsn);
		}
		else
		{
			throw new CachalotException(Cachalot::t(
				'The given dsn does not point to a directory. It should begin by "file://", others shemes are not supported.',
				array('{dir}'=>$dsn)));
		}
		if(strpos($dsn, ';') !== false)
		{
			$dsndata = explode(';', $dsn);
			$directory = $dsndata[0];
			unset($dsndata[0]);
		}
		else
		{
			$dsndata = array();
			$directory = $dsn;
		}
		
		if(is_dir($directory))
		{
			if(is_writable($directory))
			{
				$app = new Cachalot($directory);
				$app->_options = $options + $app->getDefaultOptions();
				return $app;
			}
			throw new CachalotException(Cachalot::t('Target directory "{dir}" is not writable.',
				array('{dir}'=>$directory)));
		}
		throw new CachalotException(Cachalot::t('Target directory "{dir}" does not exists. Please create it first.',
			array('{dir}'=>$directory)));
	}
	
	/**
	 * Translates a message to the specified language.
	 * @param string $message the original message
	 * @param array $params parameters to be applied to the message using <code>strtr</code>.
	 * @param string $language the target language. If null (default), the user's language will be used.
	 * @return string the translated message
	 */
	public static function t($message, array $params = array(), $language = null)
	{
		if(($source=self::getTranslator())!==null)
			$message=$source->translate($message,$language);
		
		return $params!==array() ? strtr($message,$params) : $message;
	}
	
	/**
	 * Gets a translator for translating all messages
	 * @return ITranslator
	 */
	private static function getTranslator()
	{
		if(self::$_translator === null)
		{
			$preco = new Cachalot('');
			$preco->_options = $preco->getDefaultOptions();
			self::$_translator = $preco->getComponent('translator');
		}
		return self::$_translator;
	}
	
	/**
	 * Gets an array with the options for this component. 
	 * The syntax is 
	 * array(
	 * 		[component_id] => array(
	 * 			'class' => <classname>,
	 * 			[field1] => [value1],
	 * 			[field2] => [value2],
	 * 			...
	 * 		),
	 * 		[component_id2] => array(
	 * 			'class' => [classname],
	 * 			[field1] => [value1],
	 * 			[field2] => [value2],
	 * 			...
	 * 		),
	 * 		...
	 * );
	 * @return mixed[][]
	 */
	protected function getDefaultOptions()
	{
		return array(
			'charsetFactory' => array(
				'class' => 'CharsetFactory',
			),
			'collationFactory' => array(
				'class' => 'CollationFactory',
			),
			'engineFactory' => array(
				'class' => 'EngineFactory',
			),
			'databaseFactory' => array(
				'class' => 'DatabaseFactory',
				'fileName' => 'db_repository',
			),
			'tableFactory' => array(
				'class' => 'TableFactory',
				'fileName' => 'tb_repository',
			),
			'parser' => array(
				'class' => 'CachalotParser',
				'cachalot' => $this,
				'dialects' => array(
					new Mysql_5_0_Dialect(),
				),
			),
			'defaultDialect' => array(
				'class' => 'MysqlDialect',
			),
			'translator' => array(
				'class' => 'StdPhpTranslator',
				'basePath' => dirname(__DIR__).DIRECTORY_SEPARATOR.'messages',
				'appLocale' => 'en_us',
			),
		);
	}
	
	/**
	 * Creates a CachalotObject according to the options. Throws an 
	 * exception if something goes wrong or if the component is not found
	 * into the options.
	 * @param string $id
	 * @throws InvalidArgumentException
	 * @return CachalotObject
	 */
	public function getComponent($id)
	{
		if(isset($this->_components[$id]))
			return $this->_components[$id];
		if(!isset($this->_options[$id]))
		{
			throw new InvalidArgumentException(
				Cachalot::t('Impossible to find component "{name}" into the options.',
					array('{name}' => $id)
				));
		}
		$config = $this->_options[$id];
		return $this->_components[$id] = $this->createComponentFromConfig($config);
	}
	
	/**
	 * Creates a CachalotObject from array configuration. Throws an exception
	 * if the array is wrongly written or if the class is not found.
	 * @param array $config
	 * @throws InvalidArgumentException if the config is not well written
	 * @throws CachalotException if the config contains parameters impossible
	 * 		to set to the new object
	 * @return CachalotObject
	 */
	public function createComponentFromConfig(array $config=array())
	{
		$id = serialize($config);
		if(!isset($config['class']))
		{
			throw new InvalidArgumentException(
				Cachalot::t('Impossible to find the class of component "{name}" into the options.',
					array('{name}' => $id)
				));
		}
		$classname = $config['class'];
		unset($config['class']);
		if(!class_exists($classname))
		{
			throw new InvalidArgumentException(
				Cachalot::t('Impossible to find the class "{class}" for component "{id}".',
					array('{class}' => $classname, '{id}' => $id)
				));
		}
		$object = new $classname();
		if(!($object instanceof CachalotObject))
		{
			throw new InvalidArgumentException(
				Cachalot::t('The given class "{class}" for component "{id}" is not a CachalotObject.',
					array('{class}' => $classname, '{id}' => $id)
				));
		}
		foreach($config as $param => $value)
		{
			$object->$param = $value;
		}
		if($object->canSetProperty('cachalot'))
		{
			$object->cachalot = $this;
		}
		$object->init();
		return $object;
	}
	
	/**
	 * 
	 * @param string $path where to work
	 */
	private function __construct($path)
	{
		$this->_path = $path;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see APP::getBasePath()
	 */
	public function getBasePath()
	{
		return $this->_path;
		}
	
	/**
	 * (non-PHPdoc)
	 * @see APP::getCharsetFactory()
	 * @return ICharsetFactory
	 */
	public function getCharsetFactory()
	{
		return $this->getComponent('charsetFactory');
		}
	/**
	 * (non-PHPdoc)
	 * @see APP::getCollationFactory()
	 * @return ICollationFactory
	 */
	public function getCollationFactory()
	{
		return $this->getComponent('collationFactory');
		}
	/**
	 * (non-PHPdoc)
	 * @see APP::getEngineFactory()
	 * @return IEngineFactory
	 */
	public function getEngineFactory()
	{
		return $this->getComponent('engineFactory');
	}
	/**
	 * (non-PHPdoc)
	 * @see APP::getDatabaseFactory()
	 * @return IDatabaseFactory
	 */
	public function getDatabaseFactory()
	{
		return $this->getComponent('databaseFactory');
	}
	/**
	 * (non-PHPdoc)
	 * @see APP::getTableFactory()
	 * @return ITableFactory
	 */
	public function getTableFactory()
	{
		return $this->getComponent('tableFactory');
	}
	/**
	 * (non-PHPdoc)
	 * @see APP::getParser()
	 * @return CachalotParser
	 */
	public function getParser()
	{
		return $this->getComponent('parser');
	}
	
	/**
	 * Gets the default dialect used by this dbms to behave like. This is set
	 * at the instanciation of Cachalot and cannot be changed afterwards.
	 * @return IDialect
	 * @see Cachalot::getDefaultOptions()
	 */
	public function getDefaultDialect()
	{
		return $this->getComponent('defaultDialect');
	}
	
	/**
	 * (non-PHPdoc)
	 * @see DBMS::beginTransaction()
	 */
	public function beginTransaction()
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see DBMS::commit()
	 */
	public function commit()
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see DBMS::exec()
	 */
	public function exec($statement)
	{
		/* @var $statement DMBSStatement */
		$statement = $this->query($statement);
		$statement->execute();
		return $statement->rowCount();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see DBMS::inTransaction()
	 */
	public function inTransaction()
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see DBMS::lastInsertId()
	 */
	public function lastInsertId($name = null)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see DBMS::prepare()
	 */
	public function prepare($statement)
	{
		return $this->query($statement);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see DBMS::query()
	 */
	public function query($statement)
	{
		$parser = $this->getParser();
		$commands = $parser->parse($statement);
		$dbmss = new DBMSStatementImpl();
		$dbmss->setCommands($commands);
		return $dbmss;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see DBMS::quote()
	 */
	public function quote($string)
	{
		return $this->getDefaultDialect()->quoteString($string);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see DBMS::rollBack()
	 */
	public function rollBack()
	{
		throw new NotImplementedException();
	}
	
	// ----- statements execution ----- \\
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::alterDatabase()
	 */
	public function alterDatabase(IAlterDatabaseStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::alterFunction()
	 */
	public function alterFunction(IAlterFunctionStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::alterProcedure()
	 */
	public function alterProcedure(IAlterProcedureStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::alterTable()
	 */
	public function alterTable(IAlterTableStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::alterView()
	 */
	public function alterView(IAlterViewStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::createDatabase()
	 */
	public function createDatabase(ICreateDatabaseStatement $statement)
	{
		return $this->getDatabaseFactory()->createDatabase($statement);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::createFunction()
	 */
	public function createFunction(ICreateFunctionStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::createIndex()
	 */
	public function createIndex(ICreateIndexStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::createProcedure()
	 */
	public function createProcedure(ICreateProcedureStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::createTable()
	 */
	public function createTable(ICreateTableStatement $statement)
	{
		$database = null;
		if($statement->getDatabaseName() === null)
		{
			if($this->_currentdb === null)
			{
				return $this->failExecution(Cachalot::t(
					'Impossible to create table : no database selected.'
				));
			}
			$database = $this->_currentdb;
		}
		else
		{
			$database = $this->getDatabaseFactory()->getDatabase($statement->getDatabaseName());
			if($database === null)
			{
				return $this->failExecution(Cachalot::t(
					'Impossible to create table : database "{name}" does not exists.',
					array('{name}'=>$statement->getDatabaseName())
				));
			}
		}
		return $this->getTableFactory()->createTable($database, $statement);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::createTrigger()
	 */
	public function createTrigger(ICreateTriggerStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::createView()
	 */
	public function createView(ICreateViewStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::dropDatabase()
	 */
	public function dropDatabase(IDropDatabaseStatement $statement)
	{
		return $this->getDatabaseFactory()->dropDatabase($statement);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::dropFunction()
	 */
	public function dropFunction(IDropFunctionStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::dropIndex()
	 */
	public function dropIndex(IDropIndexStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::dropProcedure()
	 */
	public function dropProcedure(IDropProcedureStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::dropTable()
	 */
	public function dropTable(IDropTableStatement $statement)
	{
		foreach($statement->getTableNames() as $key =>  $tablename)
		{
			if(isset($tablename['database']))
			{
				$database = $this->getDatabaseFactory()->getDatabase($tablename['database']);
				if($database === null)
				{
					return $this->failExecution(Cachalot::t(
						'Impossible to find database "{name}".',
							array('{name}'=>$tablename['database'])
					));
				}
			}
			else
			{
				$database = $this->_currentdb;
				if($database === null)
				{
					return $this->failExecution(Cachalot::t(
						'No database selected.'
					));
				}
			}
			$this->getTableFactory()->dropTable($database, $key, $statement);
		}
		$result = new StatementExecutionResult();
		$result->success();
		return $result;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::dropTrigger()
	 */
	public function dropTrigger(IDropTriggerStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::dropView()
	 */
	public function dropView(IDropViewStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::renameTable()
	 */
	public function renameTable(IRenameTableStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::truncateTable()
	 */
	public function truncateTable(ITruncateTableStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::call()
	 */
	public function call(ICallStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::delete()
	 */
	public function delete(IDeleteStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::do_()
	 */
	public function do_(IDoStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::handler()
	 */
	public function handler(IHandlerStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::insert()
	 */
	public function insert(IInsertStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::loadDataInfile()
	 */
	public function loadDataInfile(ILoadDataInfileStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::replace()
	 */
	public function replace(IReplaceStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::select()
	 */
	public function select(ISelectStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::update()
	 */
	public function update(IUpdateStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::startTransaction()
	 */
	public function startTransaction(IStartTransactionStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::commit_()
	 */
	public function commit_(ICommitStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::rollback()
	 */
	public function rollback_(IRollbackStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::savepoint()
	 */
	public function savepoint(ISavepointStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::rollbackToSavepoint()
	 */
	public function rollbackToSavepoint(IRollbackToSavepointStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::releaseSavepoint()
	 */
	public function releaseSavepoint(IReleaseSavepointStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::lockTables()
	 */
	public function lockTables(ILockTablesStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::unlockTables()
	 */
	public function unlockTables(IUnlockTablesStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::setTransaction()
	 */
	public function setTransaction(ISetTransactionStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::prepare_()
	 */
	public function prepare_(IPrepareStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::execute()
	 */
	public function execute(IExecuteStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::deallocatePrepare()
	 */
	public function deallocatePrepare(IDeallocatePrepareStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::analyseTable()
	 */
	public function analyseTable(IAnalyseTableStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::backupTable()
	 */
	public function backupTable(IBackupTableStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::checkTable()
	 */
	public function checkTable(ICheckTableStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::checksumTable()
	 */
	public function checksumTable(IChecksumTableStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::optimizeTable()
	 */
	public function optimizeTable(IOptimizeTableStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::repairTable()
	 */
	public function repairTable(IRepairTableStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::restoreTable()
	 */
	public function restoreTable(IRestoreTableStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::set()
	 */
	public function set(ISetStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showBinaryLogs()
	 */
	public function showBinaryLogs(IShowBinaryLogsStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showBinlogEvents()
	 */
	public function showBinlogEvents(IShowBinlogEventsStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showCharacterSet()
	 */
	public function showCharacterSet(IShowCharacterSetStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showCollation()
	 */
	public function showCollation(IShowCollationStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showColumns()
	 */
	public function showColumns(IShowColumnsStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showCreateDatabase()
	 */
	public function showCreateDatabase(IShowCreateDatabaseStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showCreateFunction()
	 */
	public function showCreateFunction(IShowCreateFunctionStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showCreateProcedure()
	 */
	public function showCreateProcedure(IShowCreateProcedureStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showCreateTable()
	 */
	public function showCreateTable(IShowCreateTableStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showCreateView()
	 */
	public function showCreateView(IShowCreateViewStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showDatabases()
	 */
	public function showDatabases(IShowDatabasesStatement $statement)
	{
		return $this->getDatabaseFactory()->showDatabases($statement);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showEngine()
	 */
	public function showEngine(IShowEngineStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showEngines()
	 */
	public function showEngines(IShowEnginesStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showErrors()
	 */
	public function showErrors(IShowErrorsStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showFunctionCode()
	 */
	public function showFunctionCode(IShowFunctionCodeStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showFunctionStatus()
	 */
	public function showFunctionStatus(IShowFunctionStatusStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showGrants()
	 */
	public function showGrants(IShowGrantsStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showIndex()
	 */
	public function showIndex(IShowIndexStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showLogs()
	 */
	public function showLogs(IShowLogsStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showMasterStatus()
	 */
	public function showMasterStatus(IShowMasterStatusStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showMutexStatus()
	 */
	public function showMutexStatus(IShowMutexStatusStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showOpenTables()
	 */
	public function showOpenTables(IShowOpenTablesStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showPrivileges()
	 */
	public function showPrivileges(IShowPrivilegesStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showProcedureCode()
	 */
	public function showProcedureCode(IShowProcedureCodeStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showProcedureStatus()
	 */
	public function showProcedureStatus(IShowProcedureStatusStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showProcesslist()
	 */
	public function showProcesslist(IShowProcesslistStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showProfile()
	 */
	public function showProfile(IShowProfileStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showProfiles()
	 */
	public function showProfiles(IShowProfilesStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showStatus()
	 */
	public function showStatus(IShowStatusStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showTableStatus()
	 */
	public function showTableStatus(IShowTableStatusStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showTables()
	 */
	public function showTables(IShowTablesStatement $statement)
	{
		if(($name = $statement->getDatabaseName()) !== null)
		{
			$database = $this->getDatabaseFactory()->getDatabase($name);
			if($database === null)
			{
				return $this->failExecution(Cachalot::t(
					'Impossible to find database "{name}".',
						array('{name}'=>$name)
				));
			}
		}
		else
		{
			$database = $this->_currentdb;
			if($database === null)
			{
				return $this->failExecution(Cachalot::t(
					'No database selected.'
				));
			}
		}
		return $this->getTableFactory()->showTables($database, $statement);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showTriggers()
	 */
	public function showTriggers(IShowTriggersStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showVariables()
	 */
	public function showVariables(IShowVariablesStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::showWarnings()
	 */
	public function showWarnings(IShowWarningsStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::cacheIndex()
	 */
	public function cacheIndex(ICacheIndexStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::flush()
	 */
	public function flush(IFlushStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::kill()
	 */
	public function kill(IKillStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::loadIndexIntoCache()
	 */
	public function loadIndexIntoCache(ILoadIndexIntoCacheStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::reset()
	 */
	public function reset(IResetStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::explain()
	 */
	public function explain(IExplainStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::help()
	 */
	public function help(IHelpStatement $statement)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Executor::use_()
	 */
	public function use_(IUseStatement $statement)
	{
		$return = new StatementExecutionResult();
		$database = $this->getDatabaseFactory()->getDatabase($statement->getDatabaseName());
		if($database === null)
		{
			$return->fail();
			return $return;
		}
		$this->_currentdb = $database;
		$return->success();
		return $return;
	}
	
	/**
	 * Creates a fail execution result message.
	 * @param string $message
	 * @return StatementExecutionResult
	 */
	protected function failExecution($message)
	{
		$result = new StatementExecutionResult();
		$result->fail();
		$result->setMessage($message);
		return $result;
	}
	
}

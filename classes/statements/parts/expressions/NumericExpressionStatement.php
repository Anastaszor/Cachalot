<?php

/**
 * NumericExpressionStatement class file.
 * 
 * @author Anastaszor
 */
class NumericExpressionStatement 
		extends CachalotObject implements ISelectExpressionStatement
{
	
	/**
	 * 
	 * @var number
	 */
	private $_value = null;
	/**
	 * 
	 * @var string
	 */
	private $_alias = null;
	
	/**
	 * 
	 * @param unknown $value
	 * @param string $alias
	 */
	public function __construct($value, $alias = null)
	{
		if(is_numeric($value))
			$this->_value = $value;
		$this->_alias = $alias;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExpressionStatement::getValue()
	 */
	public function getValue()
	{
		return $this->_value;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExpressionStatement::getAlias()
	 */
	public function getAlias()
	{
		return $this->_alias;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return is_numeric($this->_value);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		if($this->_alias !== null)
		{
			return $this->_value.' AS '.$dialect->quoteString($this->_alias);
		}
		return $this->_value;
	}
	
}

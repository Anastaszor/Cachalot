<?php

/**
 * FunctionReturnStatement class file.
 * 
 * @author Anastaszor
 */
class FunctionReturnStatement 
		extends CachalotObject implements IInstructionStatement
{
	
	/**
	 * The value to return.
	 * 
	 * @var IExpressionStatement
	 */
	private $_value_statement = null;
	
	/**
	 * Builds a new return statement with an expression.
	 * 
	 * @param IExpressionStatement $statement
	 */
	public function __construct(IExpressionStatement $statement)
	{
		$this->_value_statement = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return $this->_value_statement !== null 
			&& $this->_value_statement->validate();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return 'RETURN '.$this->_value_statement->toSQL($dialect);
	}
	
}

<?php

/**
 * ColumnPositionAfterStatement class file.
 * 
 * @author Anastaszor
 */
class ColumnPositionAfterStatement 
		extends CachalotObject implements IColumnPositionAfterStatement
{
	
	/**
	 * The targeted before column name.
	 * 
	 * @var string
	 */
	private $_column_name = null;
	
	/**
	 * Sets the column name to place after.
	 * 
	 * @param string $name
	 */
	public function setColumnName($name)
	{
		$this->_column_name = $name;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IColumnPositionAfterStatement::getColumnName()
	 */
	public function getColumnName()
	{
		return $this->_column_name;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return is_string($this->_column_name) && strlen(trim($this->_column_name)) > 0;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->columnPositionAfter($this);
	}
	
}

<?php

/**
 * ColumnPositionFirstStatement class file.
 * 
 * @author Anastaszor
 */
class ColumnPositionFirstStatement 
		extends CachalotObject implements IColumnPositionFirstStatement
{
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->columnPositionFirst($this);
	}
	
}

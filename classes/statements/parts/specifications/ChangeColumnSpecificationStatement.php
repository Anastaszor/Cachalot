<?php

/**
 * ChangeColumnSpecificationStatement class file.
 * 
 * @author Anastaszor
 */
class ChangeColumnSpecificationStatement 
		extends CachalotObject implements IChangeColumnSpecificationStatement
{
	
	/**
	 * The old name of the column to change.
	 * 
	 * @var string
	 */
	private $_old_column_name = null;
	/**
	 * The new name of the column to change.
	 * 
	 * @var string
	 */
	private $_new_column_name = null;
	/**
	 * The definition of the column to change.
	 * 
	 * @var IColumnDefinitionStatement
	 */
	private $_column_definition = null;
	/**
	 * The position of the column to change.
	 * 
	 * @var IColumnPositionStatement
	 */
	private $_column_position = null;
	
	/**
	 * Sets the old name of the column to change.
	 * 
	 * @param string $string
	 */
	public function setOldColumnName($string)
	{
		$this->_old_column_name = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IChangeColumnSpecificationStatement::getOldColumnName()
	 */
	public function getOldColumnName()
	{
		return $this->_old_column_name;
	}
	
	/**
	 * Sets the new name of the column to change.
	 * 
	 * @param string $string
	 */
	public function setNewColumnName($string)
	{
		$this->_new_column_name = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IChangeColumnSpecificationStatement::getNewColumnName()
	 */
	public function getNewColumnName()
	{
		return $this->_new_column_name;
	}
	
	/**
	 * Sets the definition of the column to change.
	 * 
	 * @param IColumnDefinitionStatement $statement
	 */
	public function setColumnDefinition(IColumnDefinitionStatement $statement)
	{
		$this->_column_definition = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IChangeColumnSpecificationStatement::getColumnDefinition()
	 */
	public function getColumnDefinition()
	{
		return $this->_column_definition;
	}
	
	/**
	 * Sets the position of the column to change.
	 * 
	 * @param IColumnPositionStatement $statement
	 */
	public function setPositionStatement(IColumnPositionStatement $statement)
	{
		$this->_column_position = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IChangeColumnSpecificationStatement::getPositionStatement()
	 */
	public function getPositionStatement()
	{
		return $this->_column_position;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return is_string($this->_old_column_name) && strlen($this->_old_column_name) > 0
			&& is_string($this->_old_column_name) && strlen($this->_old_column_name) > 0
			&& $this->_column_definition !== null && $this->_column_definition->validate()
			&& $this->_column_position === null || $this->_column_position->validate();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->changeColumnSpecification($this);
	}
	
}

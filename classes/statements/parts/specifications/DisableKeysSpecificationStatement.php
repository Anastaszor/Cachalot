<?php

class DisableKeysSpecificationStatement 
		extends CachalotObject implements IDisableKeysSpecificationStatement
{
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->disableKeysSpecification($this);
	}
	
}

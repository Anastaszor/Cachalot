<?php

/**
 * DropColumnSpecificationStatement class file.
 * 
 * @author Anastaszor
 */
class DropColumnSpecificationStatement extends 
		CachalotObject implements IDropColumnSpecificationStatement
{
	
	/**
	 * The name of the column to drop
	 * 
	 * @var string
	 */
	private $_column_name = null;
	
	/**
	 * Sets the name of the column to drop.
	 * 
	 * @param string $string
	 */
	public function setColumnName($string)
	{
		$this->_column_name = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDropColumnSpecificationStatement::getColumnName()
	 */
	public function getColumnName()
	{
		return $this->_column_name;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return is_string($this->_column_name) && strlen(trim($this->_column_name)) > 0;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->dropColumnSpecification($this);
	}
	
	
}

<?php

/**
 * AddIndexSpecificationStatement class file.
 * 
 * @author Anastaszor
 */
class AddIndexSpecificationStatement 
		extends CachalotObject implements IAddIndexSpecificationStatement
{
	
	/**
	 * 
	 * @var enum('FULLTEXT', 'SPATIAL')
	 */
	private $_index_spec = null;
	/**
	 * The name of the index.
	 * 
	 * @var string
	 */
	private $_index_name = null;
	/**
	 * The type of the index.
	 * 
	 * @var enum('BTREE', 'HASH')
	 */
	private $_index_type = null;
	/**
	 * The column list on which the index is.
	 * 
	 * @var IIndexColumnNameSpecificationStatement[]
	 */
	private $_column_list = array();
	
	/**
	 * Sets the specification of the index.
	 * 
	 * @param enum('FULLTEXT', 'SPATIAL') $value
	 */
	public function setIndexSpecification($value)
	{
		if($value === self::FULLTEXT || $value === self::SPATIAL)
		{
			$this->_index_spec = $value;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAddIndexSpecificationStatement::getIndexSpecification()
	 */
	public function getIndexSpecification()
	{
		return $this->_index_spec;
	}
	
	/**
	 * Sets the name of the index.
	 * 
	 * @param string $name
	 */
	public function setIndexName($name)
	{
		$this->_index_name = $name;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAddIndexSpecificationStatement::getIndexName()
	 */
	public function getIndexName()
	{
		return $this->_index_name;
	}
	
	/**
	 * Sets the type of the index.
	 * 
	 * @param enum('BTREE', 'HASH') $type
	 */
	public function setIndexType($type)
	{
		if($type === self::BTREE || $type === self::HASH)
		{
			$this->_index_type = $type;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAddIndexSpecificationStatement::getIndexType()
	 */
	public function getIndexType()
	{
		return $this->_index_type;
	}
	
	/**
	 * Adds a column name to the list the index is on.
	 * 
	 * @param IIndexColumnNameSpecificationStatement $name
	 */
	public function addColumn(IIndexColumnNameSpecificationStatement $statement)
	{
		$this->_column_list[] = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAddIndexSpecificationStatement::getIndexColNames()
	 */
	public function getIndexColNames()
	{
		return $this->_column_list;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		$ok = is_string($this->_index_name) 
			&& strlen(trim($this->_index_name)) > 0
			&& count($this->_column_list) > 0;
		foreach($this->_column_list as $column)
		{
			$ok &= $column->validate();
		}
		return $ok;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->addIndexSpecification($this);
	}
	
}
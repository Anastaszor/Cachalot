<?php

/**
 * ImportTablespaceSpecificationStatement class file.
 * 
 * @author Anastaszor
 */
class ImportTablespaceSpecificationStatement 
		extends CachalotObject implements IImportTablespaceSpecificationStatement
{
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->importTablespaceSpecification($this);
	}
	
}

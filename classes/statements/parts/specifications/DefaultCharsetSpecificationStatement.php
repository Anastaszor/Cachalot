<?php

/**
 * DefaultCharsetSpecificationStatement class file.
 * 
 * @author Anastaszor
 */
class DefaultCharsetSpecificationStatement 
		extends CachalotObject implements IDefaultCharsetSpecificationStatement
{
	
	/**
	 *
	 * @var ICharset
	 */
	private $_charset = null;
	/**
	 *
	 * @var ICollation
	 */
	private $_collation = null;
	
	/**
	 *
	 * @param ICharset $charset
	 */
	public function setCharset(ICharset $charset)
	{
		$this->_charset = $charset;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IConvertCharsetSpecificationStatement::getCharset()
	 */
	public function getCharset()
	{
		if($this->_collation !== null)
			return $this->_collation->getCharset();
		return $this->_charset;
	}
	
	/**
	 *
	 * @param ICollation $collation
	 */
	public function setCollation(ICollation $collation)
	{
		$this->_collation = $collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IConvertCharsetSpecificationStatement::getCollation()
	 */
	public function getCollation()
	{
		return $this->_collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return $this->_charset !== null || $this->_collation !== null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->defaultCharsetSpecification($this);
	}
	
}

<?php

/**
 * AlterColumnSpecificationStatement class file.
 * 
 * @author Anastaszor
 */
class AlterColumnSpecificationStatement 
		extends CachalotObject implements IAlterColumnSpecificationStatement
{
	
	/**
	 * The name of the column to alter.
	 * 
	 * @var string
	 */
	private $_column_name = null;
	
	/**
	 * The default value to set.
	 * 
	 * @var mixed
	 */
	private $_default_value = null;
	
	/**
	 * Sets the name of the column to alter.
	 * 
	 * @param string $string
	 */
	public function setColumnName($string)
	{
		$this->_column_name = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAlterColumnSpecificationStatement::getColumnName()
	 */
	public function getColumnName()
	{
		return $this->_column_name;
	}
	
	/**
	 * Sets the default value for the column.
	 * 
	 * @param mixed $value
	 */
	public function setDefaultValue($value)
	{
		$this->_default_value = $value;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAlterColumnSpecificationStatement::getDefaultValue()
	 */
	public function getDefaultValue()
	{
		return $this->_default_value;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return is_string($this->_column_name) && strlen($this->_column_name) > 0;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->alterColumnSpecification($this);
	}
	
}

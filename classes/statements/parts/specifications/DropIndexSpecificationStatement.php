<?php

/**
 * DropIndexSpecificationStatement class file.
 * 
 * @author Anastaszor
 */
class DropIndexSpecificationStatement 
		extends CachalotObject implements IDropIndexSpecificationStatement
{
	
	/**
	 * The name of the index to be dropped.
	 * 
	 * @var string
	 */
	private $_index_name = null;
	
	/**
	 * Sets the name of the index to be dropped.
	 * 
	 * @param string $string
	 */
	public function setIndexName($string)
	{
		$this->_index_name = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDropIndexSpecificationStatement::getIndexName()
	 */
	public function getIndexName()
	{
		return $this->_index_name;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return is_string($this->_index_name) && strlen(trim($this->_index_name)) > 0;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->dropIndexSpecification($this);
	}
	
}

<?php

/**
 * EnableKeysSpecificationStatement class file.
 * 
 * @author Anastaszor
 */
class EnableKeysSpecificationStatement 
		extends CachalotObject implements IEnableKeysSpecificationStatement
{
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->enableKeysSpecification($this);
	}
	
}

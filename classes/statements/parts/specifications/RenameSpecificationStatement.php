<?php

/**
 * RenameSpecificationStatement class file.
 * 
 * @author Anastaszor
 */
class RenameSpecificationStatement 
		extends CachalotObject implements IRenameSpecificationStatement
{
	
	/**
	 * The new table name.
	 * 
	 * @var string
	 */
	private $_new_table_name = null;
	
	/**
	 * Sets the new name for the table to rename.
	 * 
	 * @param string $string
	 */
	public function setNewTableName($string)
	{
		$this->_new_table_name = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IRenameSpecificationStatement::getNewTableName()
	 */
	public function getNewTableName()
	{
		return $this->_new_table_name;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return is_string($this->_new_table_name) 
			&& strlen(trim($this->_new_table_name)) > 0;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->renameSpecification($this);
	}
	
}

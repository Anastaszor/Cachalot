<?php

/**
 * CollationSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
class CollationSpecificationStatement 
		extends CachalotObject implements ICollationSpecificationStatement
{
	
	/**
	 * 
	 * @var ICollation
	 */
	private $_collation = null;
	
	/**
	 * Sets the collation for this specification.
	 * 
	 * @param ICollation $collation
	 */
	public function setCollation(ICollation $collation)
	{
		$this->_collation = $collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICollationSpecificationStatement::getCollation()
	 */
	public function getCollation()
	{
		return $this->_collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return $this->_collation !== null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->collationSpecification($this);
	}
	
}

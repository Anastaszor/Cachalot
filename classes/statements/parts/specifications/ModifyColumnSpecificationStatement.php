<?php

/**
 * ModifyColumnSpecificationStatement class file.
 * 
 * @author Anastaszor
 */
class ModifyColumnSpecificationStatement 
		extends CachalotObject implements IModifyColumnSpecificationStatement
{
	
	/**
	 * The name of the column to modify.
	 * 
	 * @var string
	 */
	private $_column_name = null;
	/**
	 * The definition of the column to modify.
	 * 
	 * @var IColumnDefinitionStatement
	 */
	private $_column_definition = null;
	/**
	 * The position of the column to modify.
	 * 
	 * @var IColumnPositionStatement
	 */
	private $_column_position = null;
	
	/**
	 * Sets the name of the column to modify.
	 * 
	 * @param string $string
	 */
	public function setColumnName($string)
	{
		$this->_column_name = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IModifyColumnSpecificationStatement::getColumnName()
	 */
	public function getColumnName()
	{
		return $this->_column_name;
	}
	
	/**
	 * Sets the definition of the column to modify.
	 *
	 * @param IColumnDefinitionStatement $statement
	 */
	public function setColumnDefinition(IColumnDefinitionStatement $statement)
	{
		$this->_column_definition = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IChangeColumnSpecificationStatement::getColumnDefinition()
	 */
	public function getColumnDefinition()
	{
		return $this->_column_definition;
	}
	
	/**
	 * Sets the position of the column to modify.
	 *
	 * @param IColumnPositionStatement $statement
	 */
	public function setPositionStatement(IColumnPositionStatement $statement)
	{
		$this->_column_position = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IChangeColumnSpecificationStatement::getPositionStatement()
	 */
	public function getPositionStatement()
	{
		return $this->_column_position;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return is_string($this->_column_name) && strlen($this->_column_name) > 0
		&& $this->_column_definition !== null && $this->_column_definition->validate()
		&& $this->_column_position === null || $this->_column_position->validate();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->modifyColumnSpecification($this);
	}
	
}

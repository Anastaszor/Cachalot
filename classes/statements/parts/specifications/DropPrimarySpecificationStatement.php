<?php

/**
 * DropPrimarySpecificationStatement class file.
 * 
 * @author Anastaszor
 */
class DropPrimarySpecificationStatement 
		extends CachalotObject implements IDropPrimarySpecificationStatement
{
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->dropPrimarySpecification($this);
	}
	
}

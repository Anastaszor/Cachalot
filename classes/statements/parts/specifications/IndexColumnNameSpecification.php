<?php

/**
 * IndexColumnNameSpecificationStatement class file
 * 
 * @author Anastaszor
 */
class IndexColumnNameSpecificationStatement 
		extends CachalotObject implements IIndexColumnNameSpecificationStatement
{
	
	/**
	 * The name of the specified column.
	 * 
	 * @var string
	 */
	private $_column_name = null;
	/**
	 * The length of the specified column.
	 * 
	 * @var int
	 */
	private $_length = null;
	/**
	 * The order of the specified column.
	 * 
	 * @var enum('ASC', 'DESC')
	 */
	private $_order = null;
	
	/**
	 * Sets the column name of this specification.
	 * 
	 * @param string $string
	 */
	public function setColumnName($string)
	{
		$this->_column_name = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IIndexColumnNameSpecificationStatement::getColumnName()
	 */
	public function getColumnName()
	{
		return $this->_column_name;
	}
	
	/**
	 * Sets the length taken from the column value.
	 * 
	 * @param int $int
	 */
	public function setLength($int)
	{
		$this->_length = $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IIndexColumnNameSpecificationStatement::getLength()
	 */
	public function getLength()
	{
		return $this->_length;
	}
	
	/**
	 * Sets the order for the index to be.
	 * 
	 * @param enum('ASC', 'DESC') $order
	 */
	public function setOrder($order)
	{
		if($order === self::ASC || $order === self::DESC)
			$this->_order = $order;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IIndexColumnNameSpecificationStatement::getOrder()
	 */
	public function getOrder()
	{
		return $this->_order;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return is_string($this->_column_name) && strlen(trim($this->_column_name)) > 0;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->indexColumnSpecification($this);
	}
	
}

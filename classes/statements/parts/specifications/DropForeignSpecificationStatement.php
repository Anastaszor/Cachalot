<?php

/**
 * DropForeignSpecificationStatement class file.
 * 
 * @author Anastaszor
 */
class DropForeignSpecificationStatement 
		extends CachalotObject implements IDropForeignSpecificationStatement
{
	
	/**
	 * The name of the foreign key to drop.
	 * 
	 * @var string
	 */
	private $_fk_symbol = null;
	
	/**
	 * Sets the name of the foreign key to drop.
	 * 
	 * @param string $string
	 */
	public function setForeignKeySymbol($string)
	{
		$this->_fk_symbol = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDropForeignSpecificationStatement::getForeignKeySymbol()
	 */
	public function getForeignKeySymbol()
	{
		return $this->_fk_symbol;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return is_string($this->_fk_symbol) && strlen(trim($this->_fk_symbol)) > 0;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->dropForeignSpecification($this);
	}
	
}

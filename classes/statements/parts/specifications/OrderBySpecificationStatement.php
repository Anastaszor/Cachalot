<?php

/**
 * OrderBySpecificationStatement class file.
 * 
 * @author Anastaszor
 */
class OrderBySpecificationStatement 
		extends CachalotObject implements IOrderBySpecificationStatement
{
	
	/**
	 * The names of the tables to order by.
	 * 
	 * @var string
	 */
	private $_table_names = array();
	
	/**
	 * Adds a table name to the order list.
	 * 
	 * @param string $string
	 */
	public function addTableName($string)
	{
		$this->_table_names[] = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IOrderBySpecificationStatement::getTableNames()
	 */
	public function getTableNames()
	{
		return $this->_table_names;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return count($this->_table_names) > 0;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->orderBySpecification($this);
	}
	
}

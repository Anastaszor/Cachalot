<?php

/**
 * AddForeignKeySpecificationStatement class file.
 * 
 * @author Anastaszor
 */
class AddForeignKeySpecificationStatement 
		extends CachalotObject implements IAddForeignKeySpecificationStatement
{
	
	/**
	 * The name of the foreign key constraint.
	 * 
	 * @var string
	 */
	private $_symbol = null;
	
	/**
	 * The name of the index used by the foreign key.
	 * 
	 * @var string
	 */
	private $_index_name = null;
	
	/**
	 * The name of the columns used to form the foreign key.
	 * 
	 * @var IIndexColumnNameSpecificationStatement[]
	 */
	private $_index_columns = array();
	
	/**
	 * The name of the table targeted by the relation.
	 * 
	 * @var string
	 */
	private $_target_table_name = null;
	
	/**
	 * The name of the fields used by target table of the relation.
	 * 
	 * @var string[]
	 */
	private $_target_index_names = array();
	
	/**
	 * The ON UPDATE constraint for this foreign key.
	 * 
	 * @var enum('CASCADE', 'RESTRICT', 'NO ACTION', 'SET NULL')
	 */
	private $_on_update = self::RESTRICT;
	
	/**
	 * The ON DELETE constraint for this foreign key.
	 * 
	 * @var enum('CASCADE', 'RESTRICT', 'NO ACTION', 'SET NULL')
	 */
	private $_on_delete = self::RESTRICT;
	
	/**
	 * Sets the name of the foreign key constraint.
	 * 
	 * @param string $string
	 */
	public function setSymbol($string)
	{
		$this->_symbol = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAddForeignKeySpecificationStatement::getSymbol()
	 */
	public function getSymbol()
	{
		return $this->_symbol;
	}
	
	/**
	 * Sets the name on the index this foreign key lives on.
	 * 
	 * @param string $string
	 */
	public function setIndexName($string)
	{
		$this->_index_name = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAddForeignKeySpecificationStatement::getIndexName()
	 */
	public function getIndexName()
	{
		return $this->_index_name;
	}
	
	/**
	 * Adds a column name for the index to be on.
	 * 
	 * @param IIndexColumnNameSpecificationStatement $statement
	 */
	public function addIndexColumn(IIndexColumnNameSpecificationStatement $statement)
	{
		$this->_index_columns[] = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAddForeignKeySpecificationStatement::getIndexColumnNames()
	 */
	public function getIndexColumnNames()
	{
		return $this->_index_columns;
	}
	
	/**
	 * Sets the target table name of the relation.
	 * 
	 * @param string $string
	 */
	public function setTargetTableName($string)
	{
		$this->_target_table_name = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAddForeignKeySpecificationStatement::getTargetTableName()
	 */
	public function getTargetTableName()
	{
		return $this->_target_table_name;
	}
	
	/**
	 * Adds a target field name in the target table for the relation.
	 * 
	 * @param string $value
	 */
	public function addTargetIndexName($value)
	{
		$this->_target_index_names[] = $value;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAddForeignKeySpecificationStatement::getTargetIndexNames()
	 */
	public function getTargetIndexNames()
	{
		return $this->_target_index_names;
	}
	
	/**
	 * Sets the on update constraint for the foreign key.
	 * 
	 * @param enum('CASCADE', 'RESTRICT', 'NO ACTION', 'SET NULL') $value
	 */
	public function setOnUpdateConstraint($value)
	{
		if($value === self::CASCADE || $value === self::RESTRICT
			|| $value === self::NO_ACTION || $value === self::SET_NULL
		) {
			$this->_on_update = $value;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAddForeignKeySpecificationStatement::getOnUpdateConstraint()
	 */
	public function getOnUpdateConstraint()
	{
		return $this->_on_update;
	}
	
	/**
	 * Sets the on delete constraint for the foreign key.
	 * 
	 * @param enum('CASCADE', 'RESTRICT', 'NO ACTION', 'SET NULL') $value
	 */
	public function setOnDeleteConstraint($value)
	{
		if($value === self::CASCADE || $value === self::RESTRICT
			|| $value === self::NO_ACTION || $value === self::SET_NULL
		) {
			$this->_on_delete = $value;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAddForeignKeySpecificationStatement::getOnDeleteConstraint()
	 */
	public function getOnDeleteConstraint()
	{
		return $this->_on_delete;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		$ok = is_string($this->_symbol) && strlen($this->_symbol) > 0;
		$ok &= is_string($this->_index_name) && strlen($this->_index_name) > 0;
		$ok &= is_string($this->_target_table_name) && strlen($this->_target_table_name) > 0;
		$ok &= count($this->_index_columns) > 0;
		$ok &= count($this->_index_columns) === count($this->_target_index_names);
		foreach($this->_index_columns as $column)
		{
			$ok &= $column->validate();
		}
		return $ok;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->addForeignKeySpecification($this);
	}
	
}

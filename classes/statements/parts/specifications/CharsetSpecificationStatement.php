<?php

/**
 * CharsetSpecificationStatement class file.
 * 
 * @author Anastaszor
 */
class CharsetSpecificationStatement 
		extends CachalotObject implements ICharsetSpecificationStatement
{
	
	/**
	 * 
	 * @var ICharset
	 */
	private $_charset = null;
	
	/**
	 * Sets the charset for this specification.
	 * 
	 * @param ICharset $charset
	 */
	public function setCharset(ICharset $charset)
	{
		$this->_charset = $charset;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICharsetSpecificationStatement::getCharset()
	 */
	public function getCharset()
	{
		return $this->_charset;
	}
	
	/**
	 * Verifies if the statement contains enough information to be executed
	 * successfully.
	 * @return boolean
	 */
	public function validate()
	{
		return $this->_charset !== null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->charsetSpecification($this);
	}
	
}

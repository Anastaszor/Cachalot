<?php

/**
 * AddColumnSpecificationStatement class file.
 * 
 * @author Anastaszor
 */
class AddColumnSpecificationStatement 
		extends CachalotObject implements IAddColumnSpecificationStatement
{
	/**
	 * The name of this column.
	 * 
	 * @var string
	 */
	private $_column_name = null;
	/**
	 * The definition statement for this column.
	 * 
	 * @var IColumnDefinitionStatement
	 */
	private $_column_definition = null;
	/**
	 * The position statement for this column.
	 * 
	 * @var IColumnPositionStatement
	 */
	private $_column_position = null;
	
	/**
	 * Sets the name of this column.
	 * 
	 * @param string $string
	 */
	public function setColumnName($string)
	{
		$this->_column_name = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAddColumnSpecificationStatement::getColumnName()
	 */
	public function getColumnName()
	{
		return $this->_column_name;
	}
	
	/**
	 * Sets the definition statement for this column.
	 * 
	 * @param IColumnDefinitionStatement $statement
	 */
	public function setColumnDefinitionStatement(IColumnDefinitionStatement $statement)
	{
		$this->_column_definition = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAddColumnSpecificationStatement::getColumnDefinitionStatement()
	 */
	public function getColumnDefinitionStatement()
	{
		return $this->_column_definition;
	}
	
	/**
	 * Sets the position statement for this column.
	 * 
	 * @param IColumnPositionStatement $statement
	 */
	public function setColumnPositionStatement(IColumnPositionStatement $statement)
	{
		$this->_column_position = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAddColumnSpecificationStatement::getPositionStatement()
	 */
	public function getColumnPositionStatement()
	{
		return $this->_column_position;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return !empty($this->_column_name)
			&& $this->_column_definition !== null
			&& $this->_column_definition->validate()
			&& $this->_column_position !== null
			&& $this->_column_position->validate();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->addColumnSpecification($this);
	}
	
}

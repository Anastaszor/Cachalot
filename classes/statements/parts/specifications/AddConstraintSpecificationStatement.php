<?php

/**
 * AddConstraintSpecificationStatement class file.
 * 
 * @author Anastaszor
 */
class AddConstraintSpecificationStatement 
		extends CachalotObject implements IAddConstraintSpecificationStatement
{
	
	/**
	 * The symbol used by this constraint.
	 * 
	 * @var string
	 */
	private $_symbol = null;
	
	/**
	 * The type of constraint used.
	 * 
	 * @var enum('UNIQUE', 'PRIMARY')
	 */
	private $_constraint_type = null;
	
	/**
	 * The type of index used by this constraint.
	 * 
	 * @var enum('BTREE', 'HASH')
	 */
	private $_index_type = null;
	/**
	 * The name of this constraint
	 * 
	 * @var string
	 */
	private $_index_name = null;
	/**
	 * The columns of this constraint.
	 * 
	 * @var IIndexColumnNameSpecificationStatement[]
	 */
	private $_columns = array();
	
	/**
	 * Sets the symbol used to recognize this constraint.
	 * 
	 * @param string $string
	 */
	public function setSymbol($string)
	{
		$this->_symbol = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAddConstraintSpecificationStatement::getSymbol()
	 */
	public function getSymbol()
	{
		return $this->_symbol;
	}
	
	/**
	 * Sets the type of constraint used.
	 * 
	 * @param enum('UNIQUE', 'PRIMARY') $value
	 */
	public function setConstraintType($value)
	{
		if($value === self::UNIQUE || $value === self::PRIMARY)
		{
			$this->_constraint_type = $value;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAddConstraintSpecificationStatement::getConstraintType()
	 */
	public function getConstraintType()
	{
		return $this->_constraint_type;
	}
	
	/**
	 * Sets the type of index used by this constraint.
	 * 
	 * @param enum('BTREE', 'HASH') $value
	 */
	public function setIndexType($value)
	{
		if($value === self::BTREE || $value === self::HASH)
		{
			$this->_index_type = $value;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAddConstraintSpecificationStatement::getIndexType()
	 */
	public function getIndexType()
	{
		return $this->_index_type;
	}
	
	/**
	 * Sets the name of this constraint.
	 * 
	 * @param string $string
	 */
	public function setIndexName($string)
	{
		$this->_index_name = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAddConstraintSpecificationStatement::getIndexName()
	 */
	public function getIndexName()
	{
		return $this->_index_name;
	}
	
	/**
	 * Adds a new column name specification to this constraint.
	 * 
	 * @param IIndexColumnNameSpecificationStatement $statement
	 */
	public function addIndexColumn(IIndexColumnNameSpecificationStatement $statement)
	{
		$this->_columns[] = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAddConstraintSpecificationStatement::getIndexColNames()
	 */
	public function getIndexColNames()
	{
		return $this->_columns;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		$ok = $this->_constraint_type !== null 
			&& count($this->_columns) > 0
			&& is_string($this->_index_name)
			&& strlen($this->_index_name) > 0;
		foreach($this->_columns as $column)
		{
			$ok &= $column->validate();
		}
		return $ok;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->addConstraintSpecification($this);
	}
	
}

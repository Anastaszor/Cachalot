<?php

/**
 * VarcharTypeStatement class file.
 * 
 * @author Anastaszor
 */
class VarcharTypeStatement 
			extends CachalotObject implements IVarcharTypeStatement
{
	/**
	 * 
	 * @var int
	 */
	private $_length = 255;
	/**
	 * 
	 * @var boolean
	 */
	private $_binary = false;
	/**
	 * 
	 * @var ICharset
	 */
	private $_charset = null;
	/**
	 * 
	 * @var ICollation
	 */
	private $_collation = null;
	
	/**
	 * 
	 * @param int $int
	 */
	public function setLength($int)
	{
		$this->_length = (int) $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IVarcharTypeStatement::getLength()
	 */
	public function getLength()
	{
		return $this->_length;
	}
	
	/**
	 * 
	 * @param boolean $bool
	 */
	public function setBinary($bool)
	{
		$this->_binary = (bool) $bool;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IVarcharTypeStatement::getBinary()
	 */
	public function getBinary()
	{
		return $this->_binary;
	}
	
	/**
	 * 
	 * @param ICharset $charset
	 */
	public function setCharset(ICharset $charset)
	{
		$this->_charset = $charset;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IVarcharTypeStatement::getCharset()
	 */
	public function getCharset()
	{
		return $this->_charset;
	}
	
	/**
	 * 
	 * @param ICollation $collation
	 */
	public function setCollation(ICollation $collation)
	{
		$this->_collation = $collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IVarcharTypeStatement::getCollation()
	 */
	public function getCollation()
	{
		return $this->_collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::toDataType()
	 */
	public function toDataType()
	{
		$dt = new VarcharDataType();
		$dt->setLength($this->getLength());
		$dt->setBinary($this->getBinary());
		if($this->getCharset() !== null)
			$dt->setCharset($this->getCharset());
		if($this->getCollation() !== null)
			$dt->setCollation($this->getCollation());
		return $dt;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::getDefaultValue()
	 */
	public function getDefaultValue()
	{
		return '';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return $this->_length > 0 && $this->_length < 65537;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		$str = 'VARCHAR('.$this->getLength().')';
		if($this->getBinary())
			$str .= ' BINARY';
		if($this->getCharset() !== null)
			$str .= ' CHARSET '.$this->getCharset()->getName();
		if($this->getCollation() !== null)
			$str .= ' COLLATION '.$this->getCollation()->getName();
		return $str;
	}
	
}

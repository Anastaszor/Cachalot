<?php

/**
 * TextTypeStatement class file.
 * 
 * @author Anastaszor
 */
class TextTypeStatement extends CachalotObject implements ITextTypeStatement
{
	/**
	 * 
	 * @var boolean
	 */
	private $_binary = false;
	/**
	 * 
	 * @var ICharset
	 */
	private $_charset = null;
	/**
	 * 
	 * @var ICollation
	 */
	private $_collation = null;
	
	
	/**
	 * 
	 * @param boolean $bool
	 */
	public function setBinary($bool)
	{
		$this->_binary = (bool) $bool;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ITextTypeStatement::getBinary()
	 */
	public function getBinary()
	{
		return $this->_binary;
	}
	
	/**
	 * 
	 * @param ICharset $charset
	 */
	public function setCharset(ICharset $charset)
	{
		$this->_charset = $charset;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ITextTypeStatement::getCharset()
	 */
	public function getCharset()
	{
		return $this->_charset;
	}
	
	/**
	 * 
	 * @param ICollation $collation
	 */
	public function setCollation(ICollation $collation)
	{
		$this->_collation = $collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ITextTypeStatement::getCollation()
	 */
	public function getCollation()
	{
		return $this->_collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::toDataType()
	 */
	public function toDataType()
	{
		$dt = new TextDataType();
		$dt->setBinary($this->getBinary());
		if($this->getCharset() !== null)
			$dt->setCharset($this->getCharset());
		if($this->getCollation() !== null)
			$dt->setCollation($this->getCollation());
		return $dt;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::getDefaultValue()
	 */
	public function getDefaultValue()
	{
		return null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		$str = 'TEXT';
		if($this->getBinary())
			$str .= ' BINARY';
		if($this->getCharset() !== null)
			$str .= ' CHARSET '.$this->getCharset()->getName();
		if($this->getCollation() !== null)
			$str .= ' COLLATION '.$this->getCollation()->getName();
		return $str;
	}
	
}
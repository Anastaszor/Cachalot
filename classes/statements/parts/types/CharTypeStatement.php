<?php

/**
 * CharTypeStatement class file.
 *
 * This class represents the part of a statement where is declared a data
 * type CHAR.
 *
 * @author Anasastaszor
 */
class CharTypeStatement extends CachalotObject implements ICharTypeStatement
{
	/**
	 * 
	 * @var int
	 */
	private $_length = 255;
	/**
	 * 
	 * @var boolean
	 */
	private $_binary = false;
	/**
	 * 
	 * @var ICharset
	 */
	private $_charset = null;
	/**
	 * 
	 * @var ICollation
	 */
	private $_collation = null;
	
	/**
	 * Sets the length of the data type.
	 * @param int $int
	 */
	public function setLength($int)
	{
		$this->_length = (int) $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICharTypeStatement::getLength()
	 */
	public function getLength()
	{
		return $this->_length;
	}
	
	/**
	 * Sets whether the data type should be handled as binary string.
	 * @param boolean $bool
	 */
	public function setBinary($bool)
	{
		$this->_binary = (bool) $bool;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICharTypeStatement::getBinary()
	 */
	public function getBinary()
	{
		return $this->_binary;
	}
	
	/**
	 * Sets the charset for this data type.
	 * @param ICharset $charset
	 */
	public function setCharset(ICharset $charset)
	{
		$this->_charset = $charset;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICharTypeStatement::getCharset()
	 */
	public function getCharset()
	{
		return $this->_charset;
	}
	
	/**
	 * Sets the collation for this data type.
	 * @param ICollation $collation
	 */
	public function setCollation(ICollation $collation)
	{
		$this->_collation = $collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICharTypeStatement::getCollation()
	 */
	public function getCollation()
	{
		return $this->_collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::toDataType()
	 */
	public function toDataType()
	{
		$type = new CharDataType();
		$type->setLength($this->getLength());
		$type->setBinary($this->getBinary());
		if($this->getCharset() !== null)
			$type->setCharset($this->getCharset());
		if($this->getCollation() !== null)
			$type->setCollation($this->getCollation());
		return $type;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::getDefaultValue()
	 */
	public function getDefaultValue()
	{
		return '';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return $this->_length > 0 && $this->_length < 65537;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		$sql = 'CHAR('.$this->getLength().')';
		if($this->getBinary())
			$sql .= ' BINARY';
		if($this->getCharset() !== null)
			$sql .= ' CHARACTER SET '.$this->getCharset()->getName();
		if($this->getCollation() !== null)
			$sql .= ' COLLATE '.$this->getCollation()->getName();
		return $sql;
	}
	
}

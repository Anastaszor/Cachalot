<?php

/**
 * RealTypeStatement class file.
 * 
 * @author Anastaszor
 */
class RealTypeStatement extends CachalotObject implements IRealTypeStatement
{
	/**
	 * 
	 * @var int
	 */
	private $_length = 13;
	/**
	 * 
	 * @var int
	 */
	private $_decimals = 2;
	/**
	 * 
	 * @var boolean
	 */
	private $_unsigned = false;
	/**
	 * 
	 * @var boolean
	 */
	private $_zerofill = false;
	
	/**
	 * 
	 * @param int $int
	 */
	public function setLength($int)
	{
		$this->_length = (int) $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IRealTypeStatement::getLength()
	 */
	public function getLength()
	{
		return $this->_length;
	}
	
	/**
	 * 
	 * @param int $int
	 */
	public function setDecimals($int)
	{
		$this->_decimals = (int) $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IRealTypeStatement::getDecimals()
	 */
	public function getDecimals()
	{
		return $this->_decimals;
	}
	
	/**
	 * 
	 * @param boolean $bool
	 */
	public function setUnsigned($bool)
	{
		$this->_unsigned = (bool) $bool;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IRealTypeStatement::getUnsigned()
	 */
	public function getUnsigned()
	{
		return $this->_unsigned;
	}
	
	/**
	 * 
	 * @param boolean $bool
	 */
	public function setZerofill($bool)
	{
		$this->_zerofill = (bool) $bool;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IRealTypeStatement::getZerofill()
	 */
	public function getZerofill()
	{
		return $this->_zerofill;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::toDataType()
	 */
	public function toDataType()
	{
		$dt = new RealDataType();
		$dt->setLength($this->getLength());
		$dt->setDecimals($this->getDecimals());
		$dt->setUnsigned($this->getUnsigned());
		$dt->setZerofill($this->getZerofill());
		return $dt;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::getDefaultValue()
	 */
	public function getDefaultValue()
	{
		return 0;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return $this->_decimals >= 0 && $this->_decimals < 65537
			&& $this->_length > 0 && $this->_length < 65537;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		$str = 'REAL('.$this->getLength();
		if($this->getDecimals() !== null)
			$str .= ','.$this->getDecimals();
		$str .= ')';
		if($this->getUnsigned())
			$str .= ' UNSIGNED';
		if($this->getZerofill())
			$str .= ' ZEROFILL';
		return $str;
	}
	
}

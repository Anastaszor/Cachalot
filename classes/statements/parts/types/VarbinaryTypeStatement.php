<?php

/**
 * VarbinaryTypeStatement class file.
 * 
 * @author Anastaszor
 */
class VarbinaryTypeStatement 
			extends CachalotObject implements IVarbinaryTypeStatement
{
	
	/**
	 * 
	 * @var int
	 */
	private $_length = 1024;
	
	/**
	 * 
	 * @param int $int
	 */
	public function setLength($int)
	{
		$this->_length = (int) $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IVarbinaryTypeStatement::getLength()
	 */
	public function getLength()
	{
		return $this->_length;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::toDataType()
	 */
	public function toDataType()
	{
		$dt = new VarbinaryDataType();
		$dt->setLength($this->getLength());
		return $dt;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::getDefaultValue()
	 */
	public function getDefaultValue()
	{
		return '';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return $this->_length > 0 && $this->_length < 65537;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return 'VARBINARY('.$this->getLength().')';
	}
	
}

<?php

/**
 * SetTypeStatement class file.
 * 
 * @author Anastaszor
 */
class SetTypeStatement extends CachalotObject implements ISetTypeStatement
{
	
	/**
	 * 
	 * @var string[]
	 */
	private $_values = array();
	/**
	 * 
	 * @var ICharset
	 */
	private $_charset = null;
	/**
	 * 
	 * @var ICollation
	 */
	private $_collation = null;
	
	/**
	 * 
	 * @param string $string
	 */
	public function addValue($string)
	{
		$this->_values[] = $string;
	}
	
	/**
	 * 
	 * @param string[] $strings
	 */
	public function setValues(array $strings)
	{
		$this->_values = $strings;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ISetTypeStatement::getValues()
	 */
	public function getValues()
	{
		return $this->_values;
	}
	
	/**
	 * 
	 * @param ICharset $charset
	 */
	public function setCharset(ICharset $charset)
	{
		$this->_charset = $charset;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ISetTypeStatement::getCharset()
	 */
	public function getCharset()
	{
		return $this->_charset;
	}
	
	/**
	 * 
	 * @param ICollation $collation
	 */
	public function setCollation(ICollation $collation)
	{
		$this->_collation = $collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ISetTypeStatement::getCollation()
	 */
	public function getCollation()
	{
		return $this->_collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::toDataType()
	 */
	public function toDataType()
	{
		$dt = new SetDataType();
		$dt->setValues($this->getValues());
		if($this->getCharset() !== null)
			$dt->setCharset($this->getCharset());
		if($this->getCollation() !== null)
			$dt->setCollation($this->getCollation());
		return $dt;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::getDefaultValue()
	 */
	public function getDefaultValue()
	{
		if(count($this->_values) > 0)
			return $this->_values[0];
		return null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return count($this->_values) > 0;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		$values = array();
		foreach($this->getValues() as $value)
		{
			$values[] = $dialect->quoteString($value);
		}
		$str = 'SET('.implode(', ', $values).')';
		if($this->getCharset() !== null)
			$str .= ' CHARSET '.$this->getCharset()->getName();
		if($this->getCollation() !== null)
			$str .= ' COLLATION '.$this->getCollation()->getName();
		return $str;
	}
	
}

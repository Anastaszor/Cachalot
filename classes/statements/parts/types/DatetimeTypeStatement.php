<?php

/**
 * DatetimeTypeStatement class file.
 *
 * This class represents the part of a statement where is declared a data
 * type DATETIME.
 *
 * @author Anasastaszor
 */
class DatetimeTypeStatement 
		extends CachalotObject implements IDatetimeTypeStatement
{
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::toDataType()
	 */
	public function toDataType()
	{
		return new DateTimeDataType();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::getDefaultValue()
	 */
	public function getDefaultValue()
	{
		return new DateTime();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return 'DATETIME';
	}
	
}

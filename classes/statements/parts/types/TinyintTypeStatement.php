<?php

/**
 * TinyintTypeStatement class file.
 * 
 * @author Anastaszor
 */
class TinyintTypeStatement 
			extends CachalotObject implements ITinyintTypeStatement
{
	/**
	 * 
	 * @var int
	 */
	private $_length = 1;
	/**
	 * 
	 * @var boolean
	 */
	private $_unsigned = false;
	/**
	 * 
	 * @var boolean
	 */
	private $_zerofill = false;
	
	/**
	 * 
	 * @param int $int
	 */
	public function setLength($int)
	{
		$this->_length = (int) $int;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITinyintTypeStatement::getLength()
	 */
	public function getLength()
	{
		return $this->_length;
	}
	
	/**
	 * 
	 * @param boolean $bool
	 */
	public function setUnsigned($bool)
	{
		$this->_unsigned = (bool) $bool;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ITinyintTypeStatement::getUnsigned()
	 */
	public function getUnsigned()
	{
		return $this->_unsigned;
	}
	
	/**
	 * 
	 * @param boolean $bool
	 */
	public function setZerofill($bool)
	{
		$this->_zerofill = (bool) $bool;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ITinyintTypeStatement::getZerofill()
	 */
	public function getZerofill()
	{
		return $this->_zerofill;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::toDataType()
	 */
	public function toDataType()
	{
		$dt = new TinyintDataType();
		$dt->setLength($this->getLength());
		$dt->setUnsigned($this->getUnsigned());
		$dt->setZerofill($this->getZerofill());
		return $dt;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::getDefaultValue()
	 */
	public function getDefaultValue()
	{
		return 0;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return $this->_length > 0 && $this->_length < 65537;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		$str = 'TINYINT('.$this->getLength().')';
		if($this->getUnsigned())
			$str .= ' UNSIGNED';
		if($this->getZerofill())
			$str .= ' ZEROFILL';
		return $str;
	}
	
}

<?php

/**
 * BitTypeStatement class file.
 *
 * This class represents the part of a statement where is declared a data
 * type BIT.
 *
 * @author Anasastaszor
 */
class BitTypeStatement extends CachalotObject implements IBitTypeStatement
{
	/**
	 * 
	 * @var int
	 */
	private $_length = 255;
	
	/**
	 * Sets the length of the data type.
	 * @param int $int
	 */
	public function setLength($int)
	{
		$this->_length = (int) $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IBitTypeStatement::getLength()
	 */
	public function getLength()
	{
		return $this->_length;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::toDataType()
	 */
	public function toDataType()
	{
		$type = new BitDataType();
		$type->setlength($this->getLength());
		return $type;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::getDefaultValue()
	 */
	public function getDefaultValue()
	{
		return '';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return $this->_length > 0 && $this->_length < 65536;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return 'BIT('.$this->getLength().')';
	}
	
}

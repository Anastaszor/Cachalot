<?php

/**
 * DecimalTypeStatement class file.
 * 
 * @author Anastaszor
 */
class DecimalTypeStatement 
			extends CachalotObject implements IDecimalTypeStatement
{
	
	/**
	 * 
	 * @var int
	 */
	private $_length = 13;
	/**
	 * 
	 * @var int
	 */
	private $_decimals = 2;
	/**
	 * 
	 * @var boolean
	 */
	private $_unsigned = false;
	/**
	 * 
	 * @var boolean
	 */
	private $_zerofill = false;
	
	/**
	 * Sets the length of this data type.
	 * @param int $int
	 */
	public function setLength($int)
	{
		$this->_length = (int) $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDecimalTypeStatement::getLength()
	 */
	public function getLength()
	{
		return $this->_length;
	}
	
	/**
	 * Set sthe decimal length of this data type.
	 * @param int $int
	 */
	public function setDecimals($int)
	{
		$this->_decimals = (int) $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDecimalTypeStatement::getDecimals()
	 */
	public function getDecimals()
	{
		return $this->_decimals;
	}
	
	/**
	 * Sets whether this should be interpreted as unsigned data type
	 * @param boolean $bool
	 */
	public function setUnsigned($bool)
	{
		$this->_unsigned = (bool) $bool;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDecimalTypeStatement::getUnsigned()
	 */
	public function getUnsigned()
	{
		return $this->_unsigned;
	}
	
	/**
	 * Sets whether this should be zerofilled.
	 * @param boolean $bool
	 */
	public function setZerofill($bool)
	{
		$this->_zerofill = (bool) $bool;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDecimalTypeStatement::getZerofill()
	 */
	public function getZerofill()
	{
		return $this->_zerofill;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::toDataType()
	 */
	public function toDataType()
	{
		$dt = new DecimalDataType();
		$dt->setLength($this->getLength());
		$dt->setDecimals($this->getDecimals());
		$dt->setUnsigned($this->getUnsigned());
		$dt->setZerofill($this->getZerofill());
		return $dt;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::getDefaultValue()
	 */
	public function getDefaultValue()
	{
		return 0;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return $this->_length > 0 && $this->_length < 65537
			&& $this->_decimals >= 0 && $this->_decimals < 65537;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		$str = 'DECIMAL('.$this->getLength().','.$this->getDecimals().')';
		if($this->getUnsigned())
			$str .= ' UNSIGNED';
		if($this->getZerofill())
			$str .= ' ZEROFILL';
		return $str;
	}
	
}

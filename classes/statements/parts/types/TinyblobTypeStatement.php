<?php

/**
 * TinyblobTypeStatement class file.
 * 
 * @author Anastaszor
 */
class TinyblobTypeStatement 
			extends CachalotObject implements ITinyblobTypeStatement
{
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::toDataType()
	 */
	public function toDataType()
	{
		return new TinyblobDataType();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::getDefaultValue()
	 */
	public function getDefaultValue()
	{
		return null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return 'TINYBLOB';
	}
	
}

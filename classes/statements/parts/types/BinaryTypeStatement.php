<?php

/**
 * BinaryTypeStatement class file.
 * 
 * This class represents the part of a statement where is declared a data 
 * type BINARY.
 * 
 * @author Anasastaszor
 */
class BinaryTypeStatement 
			extends CachalotObject implements IBinaryTypeStatement
{
	
	/**
	 * 
	 * @var int
	 */
	private $_length = 1024;
	
	/**
	 * Sets the length of the data type.
	 * @param int $int
	 */
	public function setLength($int)
	{
		$this->_length = (int) $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IBinaryTypeStatement::getLength()
	 */
	public function getLength()
	{
		return $this->_length;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::toDataType()
	 */
	public function toDataType()
	{
		$type = new BinaryDataType();
		$type->setLength($this->getLength());
		return $type;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::getDefaultValue()
	 */
	public function getDefaultValue()
	{
		return '';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return $this->_length > 0 && $this->_length < 65536;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return 'BINARY('.$this->getLength().')';
	}
	
}

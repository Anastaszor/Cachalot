<?php

/**
 * MediumtextTypeStatement class file.
 * 
 * @author Anastaszor
 */
class MediumtextTypeStatement 
			extends CachalotObject implements IMediumtextTypeStatement
{
	/**
	 * 
	 * @var boolean
	 */
	private $_binary = false;
	/**
	 * 
	 * @var ICharset
	 */
	private $_charset = null;
	/**
	 * 
	 * @var ICollation
	 */
	private $_collation = null;
	
	/**
	 * 
	 * @param boolean $bool
	 */
	public function setBinary($bool)
	{
		$this->_binary = (bool) $bool;
	}
	/**
	 * (non-PHPdoc)
	 * @see IMediumtextTypeStatement::getBinary()
	 */
	public function getBinary()
	{
		return $this->_binary;
	}
	
	/**
	 * 
	 * @param ICharset $charset
	 */
	public function setCharset(ICharset $charset)
	{
		$this->_charset = $charset;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IMediumtextTypeStatement::getCharset()
	 */
	public function getCharset()
	{
		return $this->_charset;
	}
	
	/**
	 * 
	 * @param ICollation $collation
	 */
	public function setCollation(ICollation $collation)
	{
		$this->_collation = $collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IMediumtextTypeStatement::getCollation()
	 */
	public function getCollation()
	{
		return $this->_collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::toDataType()
	 */
	public function toDataType()
	{
		$dt = new MediumtextDataType();
		$dt->setBinary($this->_binary);
		if($this->getCharset() !== null)
			$dt->setCharset($this->getCharset());
		if($this->getCollation() !== null)
			$dt->setCollation($this->getCollation());
		return $dt;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataTypeStatement::getDefaultValue()
	 */
	public function getDefaultValue()
	{
		return null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		$str = 'MEDIUMTEXT';
		if($this->getBinary())
			$str .= ' BINARY';
		if($this->getCharset() !== null)
			$str .= ' CHARSET '.$this->getCharset()->getName();
		if($this->getCollation() !== null)
			$str .= ' COLLATION '.$this->getCollation()->getName();
		return $str;
	}
	
}

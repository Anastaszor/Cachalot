<?php

/**
 * ColumnIndexStatement class file.
 * 
 * @author Anastaszor
 */
class ColumnIndexStatement 
		extends CachalotObject implements IColumnIndexStatement
{
	
	/**
	 * The name of the column.
	 * 
	 * @var string
	 */
	private $_name = null;
	/**
	 * The length of the column.
	 * 
	 * @var int
	 */
	private $_length = null;
	/**
	 * The sort order of the column.
	 * 
	 * @var enum('ASC', 'DESC')
	 */
	private $_order = null;
	
	/**
	 * Sets the name of the column.
	 * 
	 * @param string $string
	 */
	public function setColumnName($string)
	{
		$this->_name = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IColumnIndexStatement::getColumnName()
	 */
	public function getColumnName()
	{
		return $this->_name;
	}
	
	/**
	 * Sets the length of the column.
	 * 
	 * @param int $int
	 */
	public function setLength($int)
	{
		$this->_length = (int) $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IColumnIndexStatement::getLength()
	 */
	public function getLength()
	{
		return $this->_length;
	}
	
	/**
	 * 
	 * @param enum('ASC', 'DESC') $value
	 */
	public function setOrder($value)
	{
		if($value === self::ASC || $value === self::DESC)
		{
			$this->_order = $value;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IColumnIndexStatement::getOrder()
	 */
	public function getOrder()
	{
		return $this->_order;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return is_string($this->_name) && strlen(trim($this->_name)) > 0
			&& $this->_length === null || $this->_length > 0;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->columnIndex($this);
	}
	
}

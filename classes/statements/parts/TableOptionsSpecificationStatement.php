<?php

/**
 * TableOptionsSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
class TableOptionsSpecificationStatement 
		extends CachalotObject implements ITableOptionsSpecificationStatement
{
	
	/**
	 * The table options for this statement.
	 * 
	 * @var ITableOptionStatement[]
	 */
	private $_option_statements = array();
	
	/**
	 * Adds a table option statement to the ones for this specification.
	 * Same class options are overridden, others are appened.
	 * 
	 * @param ITableOptionStatement $statement
	 */
	public function addTableOptionStatement(ITableOptionStatement $statement)
	{
		$stclass = get_class($statement);
		foreach($this->_option_statements as $i => $optionstt)
		{
			if(get_class($optionstt) == $stclass)
			{
				// found it, replace it
				$this->_option_statements[$i] = $statement;
				return;
			}
		}
		// not found, append it
		$this->_option_statements[] = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsSpecificationStatement::getTableOptionsStatements()
	 */
	public function getTableOptionsStatements()
	{
		return $this->_option_statements;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		$ok = count($this->_option_statements) > 0;
		foreach($this->_option_statements as $optstt)
		{
			$ok &= $optstt->validate();
		}
		return $ok;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->tableOptionsSpecifications($this);
	}
	
}

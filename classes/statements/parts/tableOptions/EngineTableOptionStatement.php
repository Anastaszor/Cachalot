<?php

/**
 * EngineTableOptionStatement interface file.
 * 
 * @author Anastaszor
 */
class EngineTableOptionStatement 
		extends CachalotObject implements IEngineTableOptionStatement
{
	
	/**
	 * The engine for this option.
	 * 
	 * @var IEngine
	 */
	private $_engine = null;
	
	/**
	 * Sets the engine for this option.
	 * 
	 * @param IEngine $engine
	 */
	public function setEngine(IEngine $engine)
	{
		$this->_engine = $engine;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IEngineTableOptionStatement::getEngine()
	 */
	public function getEngine()
	{
		return $this->_engine;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return $this->_engine !== null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->tableEngineOption($this);
	}
	
}

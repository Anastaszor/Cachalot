<?php

/**
 * CollationTableOptionStatement class file.
 * 
 * @author Anastaszor
 */
class CollationTableOptionStatement 
		extends CachalotObject implements ICollationTableOptionStatement
{
	
	/**
	 * The collation of this option.
	 * 
	 * @var ICollation
	 */
	private $_collation = null;
	
	/**
	 * Sets the collation for this option.
	 * 
	 * @param ICollation $collation
	 */
	public function setCollation(ICollation $collation)
	{
		$this->_collation = $collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICollationTableOptionStatement::getCollation()
	 */
	public function getCollation()
	{
		return $this->_collation;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return $this->_collation !== null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->tableCollationOption($this);
	}
	
}

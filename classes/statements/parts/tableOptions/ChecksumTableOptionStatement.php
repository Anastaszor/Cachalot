<?php

/**
 * ChecksumTableOptionStatement class file.
 * 
 * @author Anastaszor
 */
class ChecksumTableOptionStatement 
		extends CachalotObject implements IChecksumTableOptionStatement
{
	
	/**
	 * Whether the checksum should be enabled
	 * 
	 * @var boolean
	 */
	private $_enabled = true;
	
	/**
	 * Sets whether to enable the checksum option.
	 * 
	 * @param boolean $boolean
	 */
	public function setEnabled($boolean)
	{
		$this->_enabled = true === $boolean;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IChecksumTableOptionStatement::getEnabled()
	 */
	public function getEnabled()
	{
		return $this->_enabled;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->tableChecksumOption($this);
	}
	
}

<?php

/**
 * MinRowsTableOptionStatement class file.
 * 
 * @author Anastaszor
 */
class MinRowsTableOptionStatement 
		extends CachalotObject implements IMinRowsTableOptionStatement
{
	
	/**
	 * The value for the min rows option.
	 * 
	 * @var int
	 */
	private $_value = null;
	
	/**
	 * Sets the value for the min rows option.
	 * 
	 * @param unknown $int
	 */
	public function setValue($int)
	{
		$this->_value = (int) $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IMinRowsTableOptionStatement::getValue()
	 */
	public function getValue()
	{
		return $this->_value;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return $this->_value !== null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->tableMinRowsOption($this);
	}
	
}

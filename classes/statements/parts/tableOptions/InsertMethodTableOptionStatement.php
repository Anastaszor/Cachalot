<?php

/**
 * InsertMethodTableOptionStatement class file.
 * 
 * @author Anastaszor
 */
class InsertMethodTableOptionStatement 
		extends CachalotObject implements IInsertMethodTableOptionStatement
{
	
	/**
	 * The insert method value to evaluate.
	 * 
	 * @var enum('NO', 'FIRST', 'LAST')
	 */
	private $_value = null;
	
	/**
	 * Sets the insert method value of the option.
	 * 
	 * @param enum('NO', 'FIRST', 'LAST') $value
	 */
	public function setValue($value)
	{
		if($value === self::MTD_NO
			|| $value === self::MTD_LAST
			|| $value === self::MTD_FIRST
		) {
			$this->_value = $value;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IInsertMethodTableOptionStatement::getValue()
	 */
	public function getValue()
	{
		return $this->_value;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return $this->_value !== null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->tableInsertMethodOption($this);
	}
	
}

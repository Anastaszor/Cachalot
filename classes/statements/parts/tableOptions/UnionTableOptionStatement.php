<?php

/**
 * UnionTableOptionStatement class file.
 * 
 * @author Anastaszor
 */
class UnionTableOptionStatement 
		extends CachalotObject implements IUnionTableOptionStatement
{
	
	/**
	 * The table names to union with.
	 * 
	 * @var string[]
	 */
	private $_table_names = array();
	
	/**
	 * Adds a table name to the union list.
	 * 
	 * @param string $name
	 */
	public function addTableName($name)
	{
		$this->_table_names[] = $name;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IUnionTableOptionStatement::getTableNames()
	 */
	public function getTableNames()
	{
		return $this->_table_names;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return count($this->_table_names) > 0;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->tableUnionOption($this);
	}
	
}

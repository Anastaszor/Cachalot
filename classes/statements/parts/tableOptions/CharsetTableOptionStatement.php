<?php

/**
 * CharsetTableOptionStatement class file.
 * 
 * @author Anastaszor
 */
class CharsetTableOptionStatement 
		extends CachalotObject implements ICharsetTableOptionStatement
{
	
	/**
	 * The charset for this option
	 * 
	 * @var ICharset
	 */
	private $_charset = null;
	
	/**
	 * Sets the charset for this option.
	 * 
	 * @param ICharset $charset
	 */
	public function setCharset(ICharset $charset)
	{
		$this->_charset = $charset;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICharsetTableOptionStatement::getCharset()
	 */
	public function getCharset()
	{
		return $this->_charset;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return $this->_charset !== null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->tableCharsetOption($this);
	}
	
}

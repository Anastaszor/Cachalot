<?php

/**
 * RowFormatTableOptionStatement class file.
 * 
 * @author Anastaszor
 */
class RowFormatTableOptionStatement extends CachalotObject implements IRowFormatTableOptionStatement
{
	
	/**
	 * 
	 * @var enum('DEFAULT', 'DYNAMIC', 'FIXED', 'COMPRESSED', 'REDUNDANT', 'COMPACT')
	 */
	private $_value = null;
	
	/**
	 * Sets the value for this option.
	 * 
	 * @param enum('DEFAULT', 'DYNAMIC', 'FIXED', 'COMPRESSED', 'REDUNDANT', 'COMPACT') $value
	 */
	public function setValue($value)
	{
		if($value === self::_DEFAULT
			|| $value === self::DYNAMIC
			|| $value === self::FIXED
			|| $value === self::COMPRESSED
			|| $value === self::REDUNDANT
			|| $value === self::COMPACT
		) {
			$this->_value = $value;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IRowFormatTableOptionStatement::getValue()
	 */
	public function getValue()
	{
		return $this->_value;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return $this->_value !== null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->tableRowFormatOption($this);
	}
	
}

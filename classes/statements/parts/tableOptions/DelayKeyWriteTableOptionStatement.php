<?php

/**
 * DelayKeyWriteTableOptionStatement class file.
 * 
 * @author Anastaszor
 */
class DelayKeyWriteTableOptionStatement 
		extends CachalotObject implements IDelayKeyWriteTableOptionStatement
{
	
	/**
	 * Whether the delay key write option is enabled.
	 * 
	 * @var boolean
	 */
	private $_enabled = true;
	
	/**
	 * Sets whether the delay key write option is enabled.
	 * 
	 * @param boolean $boolean
	 */
	public function setEnabled($boolean)
	{
		$this->_enabled = true === $boolean;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDelayKeyWriteTableOptionStatement::getEnabled()
	 */
	public function getEnabled()
	{
		return $this->_enabled;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->tableDelayKeyWriteOption($this);
	}
	
}

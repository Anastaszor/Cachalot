<?php

/**
 * PasswordTableOptionStatement class file.
 * 
 * @author Anastaszor
 */
class PasswordTableOptionStatement 
		extends CachalotObject implements IPasswordTableOptionStatement
{
	
	/**
	 * The value of the password.
	 * 
	 * @var string
	 */
	private $_value = null;
	
	/**
	 * Sets the value of the password.
	 * 
	 * @param string $value
	 */
	public function setValue($value)
	{
		$this->_value = $value;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IPasswordTableOptionStatement::getValue()
	 */
	public function getValue()
	{
		return $this->_value;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return !empty($this->_value);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->tablePasswordOption($this);
	}
	
}

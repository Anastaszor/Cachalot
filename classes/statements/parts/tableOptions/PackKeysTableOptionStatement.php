<?php

/**
 * PackKeysTableOptionStatement class file.
 * 
 * @author Anastaszor
 */
class PackKeysTableOptionStatement 
		extends CachalotObject implements IPackKeysTableOptionStatement
{
	
	/**
	 * 
	 * @var enum('0', '1', 'DEFAULT')
	 */
	private $_value = null;
	
	public function setValue($value)
	{
		if($value === self::ENABLED
			|| $value === self::DISABLED
			|| $value === self::_DEFAULT
		) {
			$this->_value = $value;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IPackKeysTableOptionStatement::getValue()
	 */
	public function getValue()
	{
		return $this->_value;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return $this->_value !== null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->tablePackKeysOption($this);
	}
	
}

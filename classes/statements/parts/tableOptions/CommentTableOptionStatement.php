<?php

/**
 * CommentTableOptionStatement class file.
 * 
 * @author Anastaszor
 */
class CommentTableOptionStatement 
		extends CachalotObject implements ICommentTableOptionStatement
{
	
	/**
	 * The comment value.
	 * 
	 * @var string
	 */
	private $_value = null;
	
	/**
	 * Sets the value for this comment.
	 * 
	 * @param string $string
	 */
	public function setValue($string)
	{
		$this->_value = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICommentTableOptionStatement::getValue()
	 */
	public function getValue()
	{
		return $this->_value;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->tableCommentOption($this);
	}
	
}

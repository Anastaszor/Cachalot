<?php

class IndexDirectoryTableOptionStatement 
		extends CachalotObject implements IIndexDirectoryTableOptionStatement
{
	
	private $_absolute_path = null;
	
	/**
	 * The path to the directory of this option.
	 * 
	 * @param string $path
	 */
	public function setAbsolutePathToDirectory($path)
	{
		$this->_absolute_path = $path;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IIndexDirectoryTableOptionStatement::getAbsolutePathToDirectory()
	 */
	public function getAbsolutePathToDirectory()
	{
		return $this->_absolute_path;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		// TODO regexes to validate absolute paths on win/nux
		return $this->_absolute_path !== null && is_dir($this->_absolute_path);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->tableIndexDirectoryOption($this);
	}
	
}

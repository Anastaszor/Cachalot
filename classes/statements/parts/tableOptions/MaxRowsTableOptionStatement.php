<?php

/**
 * MaxRowsTableOptionStatement class file.
 * 
 * @author Anastaszor
 */
class MaxRowsTableOptionStatement 
		extends CachalotObject implements IMaxRowsTableOptionStatement
{
	
	/**
	 * The max rows value of the option.
	 * 
	 * @var int
	 */
	private $_value = null;
	
	/**
	 * Sets the max rows value of the option.
	 * 
	 * @param int $int
	 */
	public function setValue($int)
	{
		$this->_value = (int) $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IMaxRowsTableOptionStatement::getValue()
	 */
	public function getValue()
	{
		return $this->_value;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return !empty($this->_value); 
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->tableMaxRowsOption($this);
	}
	
}

<?php

/**
 * ConnectionTableOptionStatement class file.
 * 
 * @author Anastaszor
 */
class ConnectionTableOptionStatement 
		extends CachalotObject implements IConnectionTableOptionStatement
{
	
	/**
	 * The connection string
	 * 
	 * @var string
	 */
	private $_connection_string = null;
	
	/**
	 * Sets the connection string for this option.
	 * 
	 * @param string $string
	 */
	public function setConnectionString($string)
	{
		$this->_connection_string = (string) $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IConnectionTableOptionStatement::getConnectionString()
	 */
	public function getConnectionString()
	{
		return $this->_connection_string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		// TODO regex for connection string
		return $this->_connection_string !== null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->tableConnectionOption($this);
	}
	
}

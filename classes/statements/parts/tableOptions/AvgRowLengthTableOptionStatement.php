<?php

/**
 * AvgRowLengthTableOptionStatement class file.
 * 
 * @author Anastaszor
 */
class AvgRowLengthTableOptionStatement 
		extends CachalotObject implements IAvgRowLengthTableOptionStatement
{
	
	/**
	 * The Average Row Length value.
	 * 
	 * @var int
	 */
	private $_value = 255;
	
	/**
	 * Sets the average row length value.
	 * 
	 * @param unknown $int
	 */
	public function setValue($int)
	{
		$this->_value = (int) $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAvgRowLengthTableOptionStatement::getValue()
	 */
	public function getValue()
	{
		return $this->_value;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->tableAvgRowLengthOption($this);
	}
	
}

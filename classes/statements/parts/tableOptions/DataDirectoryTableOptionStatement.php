<?php

/**
 * DataDirectoryTableOptionStatement class file.
 * 
 * @author Anastaszor
 */
class DataDirectoryTableOptionStatement 
		extends CachalotObject implements IDataDirectoryTableOptionStatement
{
	
	/**
	 * The absolute path of the directory.
	 * 
	 * @var string
	 */
	private $_absolute_path = null;
	
	/**
	 * Sets the absolute path of this option.
	 * 
	 * @param string $path
	 */
	public function setAbsolutePathToDirectory($path)
	{
		$this->_absolute_path = $path;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataDirectoryTableOptionStatement::getAbsolutePathToDirectory()
	 */
	public function getAbsolutePathToDirectory()
	{
		return $this->_absolute_path;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		// TODO regexes win/nux for absolute paths
		return $this->_absolute_path !== null && is_dir($this->_absolute_path);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->tableDataDirectoryOption($this);
	}
	
}

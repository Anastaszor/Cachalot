<?php

/**
 * AutoIncrementTableOptionStatement class file.
 * 
 * @author Anastaszor
 */
class AutoIncrementTableOptionStatement 
		extends CachalotObject implements IAutoIncrementTableOptionStatement
{
	
	/**
	 * The AutoIncrement base value.
	 * 
	 * @var int
	 */
	private $_value = 0;
	
	/**
	 * Sets the value
	 * 
	 * @param int $int
	 */
	public function setValue($int)
	{
		$this->_value = (int) $int;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAutoIncrementTableOptionStatement::getValue()
	 */
	public function getValue()
	{
		return $this->_value;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->tableAutoIncrementOption($this);
	}
	
}

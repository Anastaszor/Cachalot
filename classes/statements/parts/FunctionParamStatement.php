<?php

/**
 * FunctionParamStatement class file.
 * 
 * @author Anastaszor
 */
class FunctionParamStatement 
		extends CachalotObject implements IFunctionParamStatement
{
	
	/**
	 * The name of the param for this function.
	 * 
	 * @var string
	 */
	private $_name = null;
	/**
	 * The type of the param for this function.
	 * 
	 * @var IDataTypeStatement
	 */
	private $_type = null;
	
	/**
	 * Sets the name of the param for the function.
	 * 
	 * @param string $string
	 */
	public function setParamName($string)
	{
		$this->_name = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IFunctionParamStatement::getParamName()
	 */
	public function getParamName()
	{
		return $this->_name;
	}
	
	/**
	 * Sets the type of the param for the function.
	 * 
	 * @param IDataTypeStatement $statement
	 */
	public function setDataType(IDataTypeStatement $statement)
	{
		$this->_type = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IFunctionParamStatement::getDataType()
	 */
	public function getDataType()
	{
		return $this->_type;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return is_string($this->_name) && strlen(trim($this->_name)) > 0
			&& $this->_type !== null && $this->_type->validate();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->functionParam($this);
	}
	
}

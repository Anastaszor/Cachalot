<?php

/**
 * ColumnDefinitionStatement class file.
 * 
 * @author Anastaszor
 */
class ColumnDefinitionStatement 
		extends CachalotObject implements ISimpleColumnDefinitionStatement
{
	
	/**
	 * 
	 * @var IDataTypeStatement
	 */
	private $_data_type = null;
	/**
	 * 
	 * @var boolean
	 */
	private $_nullable = false;
	/**
	 * 
	 * @var boolean
	 */
	private $_unique = false;
	/**
	 * 
	 * @var boolean
	 */
	private $_primary = false;
	/**
	 * 
	 * @var string
	 */
	private $_comment = null;
	/**
	 * 
	 * @var enum('FIXED', 'DYNAMIC', 'DEFAULT')
	 */
	private $_column_format = null;
	/**
	 * 
	 * @var boolean
	 */
	private $_auto_increment = false;
	/**
	 * 
	 * @var IReferenceStatement
	 */
	private $_ref_statement = null;
	
	/**
	 * Sets the data type of that column type.
	 * @param IDataTypeStatement $type
	 */
	public function setDataType(IDataTypeStatement $type)
	{
		$this->_data_type = $type;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IColumnTypeStatement::getDataType()
	 */
	public function getDataType()
	{
		return $this->_data_type;
	}
	
	/**
	 * Sets if the column can be null
	 * @param boolean $bool
	 */
	public function setNotNull($bool)
	{
		$this->_nullable = !(true === $bool);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IColumnTypeStatement::getNotNull()
	 */
	public function getNotNull()
	{
		return !$this->_nullable;
	}
	
	/**
	 * Sets whether all the keys in the column have to be unique.
	 * @param boolean $bool
	 */
	public function setUnique($bool)
	{
		$this->_unique = true === $bool;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IColumnTypeStatement::getUnique()
	 */
	public function getUnique()
	{
		return $this->_unique;
	}
	
	/**
	 * Sets whether this column is part of the primary key.
	 * @param boolean $bool
	 */
	public function setPrimary($bool)
	{
		$this->_primary = true === $bool;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IColumnTypeStatement::getPrimary()
	 */
	public function getPrimary()
	{
		return $this->_primary;
	}
	
	/**
	 * Sets the comment for this column
	 * @param string $str
	 */
	public function setComment($str)
	{
		$this->_comment = (string) $str;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IColumnTypeStatement::getComment()
	 */
	public function getComment()
	{
		return $this->_comment;
	}
	
	/**
	 * Sets whether this column has an auto-increment function.
	 * @param boolean $bool
	 */
	public function setAutoIncrement($bool)
	{
		$this->_auto_increment = true === $bool;
	}
	
	/**
	 *
	 * @return boolean
	 */
	public function getAutoIncrement()
	{
		return $this->_auto_increment;
	}
	
	/**
	 * Sets the format of this column
	 * @param enum('FIXED', 'DYNAMIC', 'DEFAULT') $format
	 */
	public function setColumnFormat($format)
	{
		if($format === null
			|| $format === self::COLUMN_FORMAT_DEFAULT
			|| $format === self::COLUMN_FORMAT_DYNAMIC
			|| $format === self::COLUMN_FORMAT_FIXED
		) {
			$this->_column_format = $format;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IColumnTypeStatement::getColumnFormat()
	 */
	public function getColumnFormat()
	{
		return $this->_column_format;
	}
	
	/**
	 * Sets the reference statement of this column.
	 * @param IReferenceStatement $statement
	 */
	public function setReferenceStatement(IReferenceStatement $statement)
	{
		$this->_ref_statement = $statement;
	}
	
	/**
	 *
	 * @return IReferenceStatement
	 */
	public function getReferenceStatement()
	{
		return $this->_ref_statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return $this->_data_type !== null
			&& $this->_data_type->validate();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->simpleColumnDefinition($this);
	}
	
}

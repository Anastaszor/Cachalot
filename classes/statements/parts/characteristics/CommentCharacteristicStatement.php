<?php

/**
 * CommentCharacteristicStatement class file.
 * 
 * @author Anastaszor
 */
class CommentCharacteristicStatement 
		extends CachalotObject implements ICommentCharacteristicStatement
{
	
	/**
	 * The comment value of the characteristic.
	 * 
	 * @var string
	 */
	private $_comment = null;
	
	/**
	 * Sets the comment to this characteristic.
	 * 
	 * @param unknown $comment
	 */
	public function setCommentValue($comment)
	{
		$this->_comment = $comment;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICommentCharacteristicStatement::getCommentValue()
	 */
	public function getCommentValue()
	{
		return $this->_comment;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		if(empty($this->_comment))
			$this->_comment = '';
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->commentCharacteristic($this);
	}
	
}

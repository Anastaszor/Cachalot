<?php

/**
 * SqlCharacteristicStatement class file.
 * 
 * @author Anastaszor
 */
class SqlCharacteristicStatement 
		extends CachalotObject implements ISqlCharacteristicStatement
{
	
	/**
	 * The modifier value for this characteristic.
	 * 
	 * @var enum('CONTAINS SQL', 'NO SQL', 'READS SQL DATA', 'MODIFIES SQL DATA')
	 */
	private $_sql_modifier = null;
	
	/**
	 * Sets the Modifier for this characteristic.
	 * 
	 * @param enum('CONTAINS SQL', 'NO SQL', 'READS SQL DATA', 'MODIFIES SQL DATA') $modifier
	 */
	public function setSqlModifier($modifier)
	{
		if($modifier === self::CONTAINS_SQL
			|| $modifier === self::NO_SQL
			|| $modifier === self::READS_SQL_DATA
			|| $modifier === self::MODIFIES_SQL_DATA
		) {
			$this->_sql_modifier = $modifier;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ISqlCharacteristicStatement::getSqlModifier()
	 */
	public function getSqlModifier()
	{
		return $this->_sql_modifier;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return $this->_sql_modifier !== null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->sqlCharacteristic($this);
	}
	
}

<?php

/**
 * DeterministicCharacteristicStatement class file.
 * 
 * @author Anastaszor
 */
class DeterministicCharacteristicStatement 
		extends CachalotObject implements IDeterministicCharacteristicStatement
{
	
	/**
	 * 
	 * @var boolean
	 */
	private $_not = null;
	
	/**
	 * 
	 * @param boolean $boolean
	 */
	public function setNot($boolean)
	{
		$this->_not = true === $boolean;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDeterministicCharacteristicStatement::isNot()
	 */
	public function isNot()
	{
		return $this->_not;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->deterministicCharacteristic($this);
	}
	
}

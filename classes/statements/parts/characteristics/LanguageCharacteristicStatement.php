<?php

/**
 * LanguageCharacteristicStatement interface file.
 * 
 * @author Anastaszor
 */
class LanguageCharacteristicStatement 
		extends CachalotObject implements ILanguageCharacteristicStatement
{
	
	/**
	 * The language characteristic value.
	 * 
	 * @var string
	 */
	private $_language = 'SQL';
	
	/**
	 * (non-PHPdoc)
	 * @see ILanguageCharacteristicStatement::getLanguageValue()
	 */
	public function getLanguageValue()
	{
		return $this->_language;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->languageCharacteristic($this);
	}
	
}

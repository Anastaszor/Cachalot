<?php

/**
 * SecurityCharacteristicStatement class file.
 * 
 * @author Anastaszor
 */
class SecurityCharacteristicStatement 
		extends CachalotObject implements ISecurityCharacteristicStatement
{
	
	/**
	 * The security param of this characteristic.
	 * 
	 * @var enum('DEFINER', 'INVOKER')
	 */
	private $_security = null;
	
	/**
	 * Sets the security param of this characteristic.
	 * 
	 * @param enum('DEFINER', 'INVOKER') $param
	 */
	public function setSecurityParam($param)
	{
		if($param === self::DEFINER || $param === self::INVOKER)
			$this->_security = $param;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ISecurityCharacteristicStatement::getSecurityParam()
	 */
	public function getSecurityParam()
	{
		return $this->_security;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return $this->_security !== null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->securityCharacteristic($this);
	}
	
}

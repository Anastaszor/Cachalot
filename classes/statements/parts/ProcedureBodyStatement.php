<?php

/**
 * ProcedureBodyStatement class file.
 * 
 * @author Anastaszor
 */
class ProcedureBodyStatement extends CachalotObject implements IRoutineBodyStatement
{
	
	/**
	 * All the instructions for the body to do something.
	 *
	 * @var IInstructionStatement[]
	 */
	private $_instructions = array();
	
	/**
	 * Adds an instruction to this body.
	 *
	 * @param IInstructionStatement $statement
	*/
	public function addInstruction(IInstructionStatement $statement)
	{
		$this->_instructions[] = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IRoutineBodyStatement::getInstructions()
	 */
	public function getInstructions()
	{
		return $this->_instructions;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		foreach($this->_instructions as $instruction)
		{
			if(!$instruction->validate())
				return false;
		}
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->routineBody($this);
	}
	
}

<?php

/**
 * CreateDefinitionStatement class file.
 * 
 * @author Anastaszor
 */
class CreateDefinitionStatement 
		extends CachalotObject implements ICreateDefinitionStatement
{
	
	/**
	 * 
	 * @var string
	 */
	private $_column_name = null;
	/**
	 * 
	 * @var IColumnDefinitionStatement
	 */
	private $_column_definition = null;
	/**
	 * 
	 * @var IConstraintStatement[]
	 */
	private $_constraints = array();
	
	/**
	 * Sets the name of the column
	 * @param string $name
	 */
	public function setColumnName($name)
	{
		$this->_column_name = (string) $name;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IColumnDefinitionStatement::getColumnName()
	 */
	public function getColumnName()
	{
		return $this->_column_name;
	}
	
	/**
	 * Sets the type of the column
	 * @param IColumnDefinitionStatement $definition
	 */
	public function setColumnDefinition(IColumnDefinitionStatement $definition)
	{
		$this->_column_definition = $definition;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IColumnDefinitionStatement::getColumnDefinition()
	 */
	public function getColumnDefinition()
	{
		return $this->_column_definition;
	}
	
	/**
	 * Adds a constraints to this statement
	 * @param IConstraintStatement $constraint
	 */
	public function addConstraint(IConstraintStatement $constraint)
	{
		$this->_constraints[] = $constraint;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IColumnDefinitionStatement::getConstraints()
	 */
	public function getConstraints()
	{
		return $this->_constraints;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		$str = '`'.$this->getColumnName().'` '.$this->getColumnDefinition()->toSQL($dialect);
		foreach($this->getConstraints() as $constraint)
		{
			$str .= ' '.$constraint->toSQL($dialect);
		}
		return $str;
	}
	
}

<?php

/**
 * CreateDatabaseStatement class file.
 *
 * @author Anastaszor
 * @deprecated
 */
class TableOptionsStatement 
		extends CachalotObject 
		implements ITableOptionsStatement
{
	/**
	 * 
	 * @var string
	 */
	private $_engine = null;
	
	private $_autoIncrement = null;
	private $_avgRowLength = null;
	private $_defaultCharset = null;
	private $_checksum = null;
	private $_defaultCollation = null;
	private $_comment = null;
	private $_connectionString = null;
	private $_dataDirectory = null;
	private $_delayKeyWrite = null;
	private $_indexDirectory = null;
	private $_insertMethod = null;
	private $_keyBlockSize = null;
	private $_maxRows = null;
	private $_minRows = null;
	private $_packKeys = null;
	private $_password = null;
	private $_rowFormat = null;
	private $_tablespace = null;
	private $_tablespaceStorage = null;
	private $_unionTableNames = array();
	
	/**
	 * Sets the engine this table option wants to take.
	 * @param string $name
	 */
	public function setEngine($name)
	{
		$this->_engine = $name;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsStatement::getEngine()
	 */
	public function getEngine()
	{
		return $this->_engine;
	}
	
	/**
	 * Sets current auto_increment value of the table. If not given, this
	 * will be automatically calculated.
	 * @param int $value
	 */
	public function setAutoIncrement($value)
	{
		$this->_autoIncrement = (int) $value;
		if($this->_autoIncrement <= 0)
			$this->_autoIncrement = null;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsStatement::getAutoIncrement()
	 */
	public function getAutoIncrement()
	{
		return $this->_autoIncrement;
	}
	
	/**
	 * Sets the average row length. This will be interpreted as a hint, not
	 * as a limit value.
	 * @param int $value
	 */
	public function setAverageRowLength($value)
	{
		$this->_avgRowLength = (int) $value;
		if($this->_avgRowLength <= 0)
			$this->_avgRowLength = null;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsStatement::getAverageRowLength()
	 */
	public function getAverageRowLength()
	{
		return $this->_avgRowLength;
	}
	
	/**
	 * Sets the character sets this table wants to have.
	 * @param string $name
	 */
	public function setDefaultCharacterSet($name)
	{
		$this->_defaultCharset = $name;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsStatement::getDefaultCharacterSet()
	 */
	public function getDefaultCharacterSet()
	{
		return $this->_defaultCharset;
	}
	
	/**
	 * Sets if the DBMS should keep a checksum of the line value, for integrity.
	 * @param string $value
	 * @throws InvalidArgumentException
	 */
	public function setCheckSum($value)
	{
		$trueval = (int) $value;
		if(!($trueval === ITable::CHECKSUM_OFF || $trueval === ITable::CHECKSUM_ON))
		{
			throw new InvalidArgumentException(Cachalot::t(
				'Invalid Checksum value, must be "{i}" (off) or "{v}" (on).', 
				array('{i}'=>ITable::CHECKSUM_OFF, '{v}'=>ITable::CHECKSUM_ON)
			));
		}
		$this->_checksum = $trueval;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsStatement::getChecksum()
	 */
	public function getChecksum()
	{
		return $this->_checksum;
	}
	
	/**
	 * Sets the collation this table wants to have.
	 * @param string $name
	 */
	public function setDefaultCollation($name)
	{
		$this->_defaultCollation = $name;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsStatement::getDefaultCollation()
	 */
	public function getDefaultCollation()
	{
		return $this->_defaultCollation;
	}
	
	/**
	 * Sets the comment for this table. This is free text.
	 * @param string $value
	 */
	public function setComment($value)
	{
		$this->_comment = $value;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsStatement::getComment()
	 */
	public function getComment()
	{
		return $this->_comment;
	}
	
	/**
	 * Sets the connectionString for this table.
	 * @param string $string
	 */
	public function setConnectionString($string)
	{
		$this->_connectionString = $string;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsStatement::getConnectionString()
	 */
	public function getConnectionString()
	{
		return $this->_connectionString;
	}
	
	/**
	 * Sets the data directory for this table, if the this is not canonical.
	 * @param string $path
	 */
	public function setDataDirectory($path)
	{
		$this->_dataDirectory = $path;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsStatement::getDataDirectory()
	 */
	public function getDataDirectory()
	{
		return $this->_dataDirectory;
	}
	
	/**
	 * Sets if there should be a delay between the write of the index and
	 * the write of the values into the table.
	 * @param string $value
	 * @throws InvalidArgumentException
	 */
	public function setDelayKeyWrite($value)
	{
		$trueval = (int) $value;
		if(!($value === ITable::DELAY_KEY_WRITE_OFF || $value === ITable::DELAY_KEY_WRITE_ON ))
		{
			throw new InvalidArgumentException(Cachalot::t(
				'Invalid Delay Key Write value, must be "{i}" (off) or "{v}" (on).', 
				array('{i}'=>ITable::DELAY_KEY_WRITE_OFF, '{v}'=>ITable::DELAY_KEY_WRITE_ON)
			));
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsStatement::getDelayKeyWrite()
	 */
	public function getDelayKeyWrite()
	{
		return $this->_delayKeyWrite;
	}
	
	/**
	 * Sets the index directory for this table, if this is not canonical.
	 * @param string $path
	 */
	public function setIndexDirectory($path)
	{
		$this->_indexDirectory = $path;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsStatement::getIndexDirectory()
	 */
	public function getIndexDirectory()
	{
		return $this->_indexDirectory;
	}
	
	/**
	 * Sets the insert method of the table.
	 * @param string $value
	 * @throws InvalidArgumentException
	 */
	public function setInsertMethod($value)
	{
		if(!($value === ITable::INSERT_METHOD_NO
			|| $value === ITable::INSERT_METHOD_FIRST
			|| $value === ITable::INSERT_METHOD_LAST
		)) {
			throw new InvalidArgumentException(Cachalot::t(
				'Invalid Insert Method value, must be one of "{1}", "{2}" or "{3}".',
				array('{1}'=>ITable::INSERT_METHOD_NO, 
					'{2}'=>ITable::INSERT_METHOD_FIRST,
					'{3}'=>ITable::INSERT_METHOD_LAST,
			)));
		}
		$this->_insertMethod = $value;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsStatement::getInsertMethod()
	 */
	public function getInsertMethod()
	{
		return $this->_insertMethod;
	}
	
	/**
	 * Sets the mean key block size of the table. This will be used only as
	 * an hint, not an absolute value.
	 * @param int $size
	 */
	public function setKeyBlockSize($size)
	{
		$this->_keyBlockSize = (int) $size;
		if($this->_keyBlockSize <= 0)
			$this->_keyBlockSize = null;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsStatement::getKeyBlockSize()
	 */
	public function getKeyBlockSize()
	{
		return $this->_keyBlockSize;
	}
	
	/**
	 * Sets the maximal number of rows of the table. This will be used only
	 * as an hint, not an absolute limit.
	 * @param int $value
	 */
	public function setMaxRows($value)
	{
		$this->_maxRows = (int) $value;
		if($this->_maxRows <= 0)
			$this->_maxRows = null;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsStatement::getMaxRows()
	 */
	public function getMaxRows()
	{
		return $this->_maxRows;
	}
	
	/**
	 * Sets the minimal number of rows of the table. This will be used only
	 * as an hint, not an absolute limit.
	 * @param int $value
	 */
	public function setMinRows($value)
	{
		$this->_minRows = (int) $value;
		if($this->_minRows <= 0)
			$this->_minRows = null;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsStatement::getMinRows()
	 */
	public function getMinRows()
	{
		return $this->_minRows;
	}
	
	/**
	 * Sets the order to pack the keys.
	 * @param string $value
	 * @throws InvalidArgumentException
	 */
	public function setPackKeys($value)
	{
		$trueval = (int) $value;
		if(!($trueval === ITable::PACK_KEYS_ON
			|| $trueval === ITable::PACK_KEYS_OFF
			|| $trueval === ITable::PACK_KEYS_DEFAULT
		)) {
			throw new InvalidArgumentException(Cachalot::t(
				'Invalid Pack Keys value, must be one of "{1}", "{2}" or "{3}".',
				array('{1}'=>ITable::PACK_KEYS_ON,
					'{2}'=>ITable::PACK_KEYS_OFF,
					'{3}'=>ITable::PACK_KEYS_DEFAULT,
			)));
		}
		$this->_packKeys = $value;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsStatement::getPackKeys()
	 */
	public function getPackKeys()
	{
		return $this->_packKeys;
	}
	
	/**
	 * Sets the password for this table.
	 * @param unknown $value
	 */
	public function setPassword($value)
	{
		$this->_password = $value;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsStatement::getPassword()
	 */
	public function getPassword()
	{
		return $this->_password;
	}
	
	/**
	 * Sets the row format for this table.
	 * @param string $format
	 * @throws InvalidArgumentException
	 */
	public function setRowFormat($format)
	{
		if(!($format === ITable::ROW_FORMAT_COMPACT
			|| $format === ITable::ROW_FORMAT_COMPRESSED
			|| $format === ITable::ROW_FORMAT_DEFAULT
			|| $format === ITable::ROW_FORMAT_DYNAMIC
			|| $format === ITable::ROW_FORMAT_FIXED
			|| $format === ITable::ROW_FORMAT_REDUNDANT
		)) {
			throw new InvalidArgumentException(Cachalot::t(
				'Invalid Pack Keys value, must be one of "{1}", "{2}", "{3}", "{4}", "{5}" or "{6}".',
				array('{1}'=>ITable::ROW_FORMAT_COMPACT,
					'{2}'=>ITable::ROW_FORMAT_COMPRESSED,
					'{3}'=>ITable::ROW_FORMAT_DEFAULT,
					'{4}'=>ITable::ROW_FORMAT_DYNAMIC,
					'{5}'=>ITable::ROW_FORMAT_FIXED,
					'{6}'=>ITable::ROW_FORMAT_REDUNDANT,
			)));
		}
		$this->_rowFormat = $format;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsStatement::getRowFormat()
	 */
	public function getRowFormat()
	{
		return $this->_rowFormat;
	}
	
	/**
	 * Sets the tablespace which owns this table.
	 * @param string $tablespace
	 */
	public function setTablespace($tablespace)
	{
		$this->_tablespace = $tablespace;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsStatement::getTablespace()
	 */
	public function getTablespace()
	{
		return $this->_tablespace;
	}
	
	/**
	 * Sets the storage space to the tablespace.
	 * @param string $storage
	 * @throws InvalidArgumentException
	 */
	public function setTableSpaceStorage($storage)
	{
		if(!($storage === ITable::TABLESPACE_STORAGE_DEFAULT
			|| $storage === ITable::TABLESPACE_STORAGE_DISK
			|| $storage === ITable::TABLESPACE_STORAGE_MEMORY
		)) {
			throw new InvalidArgumentException(Cachalot::t(
				'Invalid Pack Keys value, must be one of "{1}", "{2}" or "{3}".',
				array('{1}'=>ITable::TABLESPACE_STORAGE_DEFAULT,
					'{2}'=>ITable::TABLESPACE_STORAGE_DISK,
					'{3}'=>ITable::TABLESPACE_STORAGE_MEMORY,
			)));
		}
		$this->_tablespaceStorage = $storage;
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsStatement::getTablespaceStorage()
	 */
	public function getTablespaceStorage()
	{
		return $this->_tablespaceStorage;
	}
	
	/**
	 * Adds an union table name to the union table name array.
	 * @param string $name
	 */
	public function addUnionTableName($name)
	{
		if(!empty($name))
		{
			$this->_unionTableNames[] = $name;
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see ITableOptionsStatement::getUnionTableNames()
	 */
	public function getUnionTableNames()
	{
		return $this->_unionTableNames;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		$sql = '';
		if($this->getEngine() !== null)
			$sql .= ' ENGINE '.$this->getEngine();
		if($this->getAutoIncrement() !== null)
			$sql .= ' AUTO_INCREMENT '.$this->getAutoIncrement();
		if($this->getAverageRowLength() !== null)
			$sql .= ' AVG_ROW_LENGTH '.$this->getAverageRowLength();
		if($this->getDefaultCharacterSet() !== null)
			$sql .= ' CHARACTER SET '.$this->getDefaultCharacterSet();
		if($this->getChecksum() !== null)
			$sql .= ' CHECKSUM '.((int) $this->getChecksum());
		if($this->getDefaultCollation() !== null)
			$sql .= ' COLLATE '.$this->getDefaultCollation();
		if($this->getComment() !== null)
			$sql .= ' COMMENT '.$dialect->quoteString($this->getComment());
		if($this->getConnectionString() !== null)
			$sql .= ' CONNECTION '.$this->getConnectionString();
		if($this->getDataDirectory() !== null)
			$sql .= ' DATA DIRECTORY '.$this->getDataDirectory();
		if($this->getDelayKeyWrite() !== null)
			$sql .= ' DELAY_KEY_WRITE '.((int) $this->getDelayKeyWrite());
		if($this->getIndexDirectory() !== null)
			$sql .= ' INDEX DIRECTORY '.$this->getIndexDirectory();
		if($this->getInsertMethod() !== null)
			$sql .= ' INSERT_METHOD '.$this->getInsertMethod();
		if($this->getKeyBlockSize() !== null)
			$sql .= ' KEY_BLOCK_SIZE '.$this->getKeyBlockSize();
		if($this->getMaxRows() !== null)
			$sql .= ' MAX_ROWS '.$this->getMaxRows();
		if($this->getMinRows() !== null)
			$sql .= ' MIN_ROWS '.$this->getMinRows();
		if($this->getPackKeys() !== null)
			$sql .= ' PACK_KEYS '.($this->getPackKeys() === ITable::PACK_KEYS_DEFAULT 
					? ITable::PACK_KEYS_DEFAULT : (int) $this->getPackKeys());
		if($this->getPassword() !== null)
			$sql .= ' PASSWORD '.$dialect->quoteString($this->getPassword());
		if($this->getRowFormat() !== null)
			$sql .= ' ROW_FORMAT '.$this->getRowFormat();
		if($this->getTablespace() !== null)
		{
			$sql .= ' TABLESPACE '.$dialect->quoteTableName($this->getTablespace());
			if($this->getTablespaceStorage() !== null)
				$sql .= ' STORAGE '.$this->getTablespaceStorage();
		}
		return $sql;
	}
	
}

<?php

/**
 * ProcedureParamStatement class file.
 * 
 * @author Anastaszor
 */
class ProcedureParamStatement 
		extends FunctionParamStatement implements IProcedureParamStatement
{
	
	/**
	 * The type of the param.
	 * 
	 * @var enum('INOUT', 'IN', 'OUT')
	 */
	private $_type = null;
	
	/**
	 * Sets the type of the param.
	 * 
	 * @param enum('INOUT', 'IN', 'OUT') $value
	 */
	public function setType($value)
	{
		if($value === self::IN || $value === self::INOUT || $value === self::OUT)
		{
			$this->_type = $value;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IProcedureParamStatement::getType()
	 */
	public function getType()
	{
		return $this->_type;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see FunctionParamStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->procedureParam($this);
	}
	
}

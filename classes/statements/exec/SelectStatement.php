<?php

/**
 * SelectStatement class file.
 * 
 * @author Anastaszor
 */
class SelectStatement extends CachalotObject implements ISelectStatement
{
	
	/**
	 * 
	 * @var enum('ALL', 'DISTINCT', 'DISTINCTROW')
	 */
	private $_distinct_mode = null;
	/**
	 * 
	 * @var enum('HIGH_PRIORITY')
	 */
	private $_priority = null;
	/**
	 * 
	 * @var enum('SQL_SMALL_RESULT', 'SQL_BIG_RESULT', 'SQL_BUFFER_RESULT')
	 */
	private $_result_mode = null;
	/**
	 * 
	 * @var enum('SQL_CACHE', 'SQL_NO_CACHE')
	 */
	private $_cache_mode = null;
	/**
	 * 
	 * @var ISelectExpressionStatement[]
	 */
	private $_select_expressions = array();
	/**
	 * 
	 * @var IFromStatement
	 */
	private $_from_statement = null;
	
	/**
	 * 
	 * @param enum('ALL', 'DISTINCT', 'DISTINCTROW') $value
	 */
	public function setDistinctMode($value)
	{
		if($value === self::DISTINCTMODE_ALL
			|| $value === self::DISTINCTMODE_DISTINCT
			|| $value === self::DISTINCTMODE_DISTINCTROW
		) {
			$this->_distinct_mode = $value;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ISelectStatement::getDistinctMode()
	 */
	public function getDistinctMode()
	{
		return $this->_distinct_mode;
	}
	
	/**
	 * 
	 * @param enum('HIGH_PRIORITY') $value
	 */
	public function setPriority($value)
	{
		if($value === self::HIGH_PRIORITY)
		{
			$this->_priority = $value;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ISelectStatement::getPriority()
	 */
	public function getPriority()
	{
		return $this->_priority;
	}
	
	/**
	 * 
	 * @param enum('SQL_SMALL_RESULT', 'SQL_BIG_RESULT', 'SQL_BUFFER_RESULT') $value
	 */
	public function setResultMode($value)
	{
		if($value === self::SQL_SMALL_RESULT
			|| $value === self::SQL_BIG_RESULT
			|| $value === self::SQL_BUFFER_RESULT
		) {
			$this->_result_mode = $value;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ISelectStatement::getResultMode()
	 */
	public function getResultMode()
	{
		return $this->_result_mode;
	}
	
	/**
	 * 
	 * @param enum('SQL_CACHE', 'SQL_NO_CACHE') $value
	 */
	public function setCacheMode($value)
	{
		if($value === self::SQL_CACHE || $value === self::SQL_NO_CACHE)
		{
			$this->_cache_mode = $value;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ISelectStatement::getCacheMode()
	 */
	public function getCacheMode()
	{
		return $this->_cache_mode;
	}
	
	/**
	 * 
	 * @param ISelectExpressionStatement $statement
	 */
	public function addSelectExpression(ISelectExpressionStatement $statement)
	{
		$this->_select_expressions[] = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ISelectStatement::getSelectExpressions()
	 */
	public function getSelectExpressions()
	{
		return $this->_select_expressions;
	}
	
	/**
	 * 
	 * @param IFromStatement $statement
	 */
	public function setFromStatement(IFromStatement $statement)
	{
		$this->_from_statement = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ISelectStatement::getFromStatement()
	 */
	public function getFromStatement()
	{
		return $this->_from_statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExecutableStatement::execute()
	 */
	public function execute(Executor $executor)
	{
		return $executor->select($this);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		$ok = count($this->_select_expressions) > 0;
		foreach($this->_select_expressions as $statement)
		{
			$ok &= $statement->validate();
		}
		return $ok && $this->_from_statement === null 
			|| $this->_from_statement->validate();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->select($this);
	}
	
}

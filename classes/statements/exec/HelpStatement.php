<?php

/**
 * HelpStatement class file.
 * 
 * @author Anastaszor
 */
class HelpStatement extends CachalotObject implements IHelpStatement
{
	
	/**
	 * The search string on what to help for.
	 * 
	 * @var string
	 */
	private $_search_string = null;
	
	/**
	 * Sets the search string on what to help for.
	 * 
	 * @param string $string
	 */
	public function setSearchString($string)
	{
		$this->_search_string = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IHelpStatement::getSearchString()
	 */
	public function getSearchString()
	{
		return $this->_search_string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExecutableStatement::validate()
	 */
	public function validate()
	{
		return is_string($this->_search_string) && !empty(trim($this->_search_string));
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExecutableStatement::execute()
	 */
	public function execute(Executor $executor)
	{
		return $executor->help($this);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->help($this);
	}
	
}

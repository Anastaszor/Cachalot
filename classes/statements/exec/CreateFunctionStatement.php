<?php

/**
 * CreateFunctionStatement class file.
 * 
 * @author Anastaszor
 */
class CreateFunctionStatement 
		extends CachalotObject implements ICreateFunctionStatement
{
	
	/**
	 * 
	 * @var string
	 */
	private $_definer = null;
	/**
	 * 
	 * @var string
	 */
	private $_function_name = null;
	/**
	 * 
	 * @var IFunctionParamStatement[]
	 */
	private $_params = array();
	/**
	 * 
	 * @var IDataTypeStatement
	 */
	private $_return_type = null;
	/**
	 * 
	 * @var ICharacteristicStatement[]
	 */
	private $_characteristics = array();
	/**
	 * 
	 * @var IFunctionRoutineBodystatement
	 */
	private $_body = null;
	
	/**
	 * Sets the definer for this function to create.
	 * 
	 * @param string $string
	 */
	public function setDefiner($string)
	{
		$this->_definer = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateFunctionStatement::getDefiner()
	 */
	public function getDefiner()
	{
		return $this->_definer;
	}
	
	/**
	 * Sets the name of the function to create.
	 * 
	 * @param string $string
	 */
	public function setFunctionName($string)
	{
		$this->_function_name = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateFunctionStatement::getFunctionName()
	 */
	public function getFunctionName()
	{
		return $this->_function_name;
	}
	
	/**
	 * Adds a param to this function.
	 * 
	 * @param IFunctionParamStatement $statement
	 */
	public function addParam(IFunctionParamStatement $statement)
	{
		$this->_params[] = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateFunctionStatement::getParams()
	 */
	public function getParams()
	{
		return $this->_params;
	}
	
	/**
	 * Sets the return type for this function.
	 * 
	 * @param IDataTypeStatement $statement
	 */
	public function setReturnType(IDataTypeStatement $statement)
	{
		$this->_return_type = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateFunctionStatement::getReturnType()
	 */
	public function getReturnType()
	{
		return $this->_return_type;
	}
	
	/**
	 * Adds a characteristic to this function.
	 * 
	 * @param ICharacteristicStatement $statement
	 */
	public function addCharacteristics(ICharacteristicStatement $statement)
	{
		$this->_characteristics[] = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateFunctionStatement::getCharacteristics()
	 */
	public function getCharacteristics()
	{
		return $this->_characteristics;
	}
	
	/**
	 * Sets the body of this function.
	 * 
	 * @param IRoutineBodystatement $statement
	 */
	public function setBody(IRoutineBodystatement $statement)
	{
		$this->_body = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateFunctionStatement::getRoutineBody()
	 */
	public function getRoutineBody()
	{
		return $this->_body;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExecutableStatement::execute()
	 */
	public function execute(Executor $executor)
	{
		return $executor->createFunction($this);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		$ok = is_string($this->_function_name)
			&& strlen(trim($this->_function_name)) > 0
			&& $this->_return_type !== null && $this->_return_type->validate()
			&& $this->_body !== null && $this->_body->validate();
		foreach($this->_params as $param)
		{
			$ok &= $param->validate();
		}
		foreach($this->_characteristics as $charac)
		{
			$ok &= $charac->validate();
		}
		return $ok;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->createFunction($this);
	}
	
}

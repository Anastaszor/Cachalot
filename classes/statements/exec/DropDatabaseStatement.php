<?php

/**
 * DropDatabaseStatement class file.
 *
 * Statement for creating DROP DATABASE statements.
 *
 * @author Anastaszor
 * @see http://dev.mysql.com/doc/refman/5.5/en/drop-database.html
 */
class DropDatabaseStatement 
		extends CachalotObject 
		implements IDropDatabaseStatement
{
	
	/**
	 * 
	 * @var string
	 */
	private $_name = null;
	/**
	 * 
	 * @var boolean
	 */
	private $_ifExists = false;
	
	/**
	 * Sets the name of the database to delete
	 * @param string $name
	 */
	public function setDatabaseName($name)
	{
		$this->_name = $name;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDropDatabaseStatement::getDatabaseName()
	 */
	public function getDatabaseName()
	{
		return $this->_name;
	}
	
	/**
	 * Sets the if exists value. true if the IF EXISTS statement is present.
	 * @param bool $value
	 */
	public function setIfExists($value)
	{
		$this->_ifExists = $value === true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDropDatabaseStatement::getIfExists()
	 */
	public function getIfExists()
	{
		return $this->_ifExists;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExecutableStatement::validate()
	 */
	public function validate()
	{
		return !empty($this->getDatabaseName());
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::execute()
	 */
	public function execute(Executor $executor)
	{
		return $executor->dropDatabase($this);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->dropDatabase($this);
	}
	
}

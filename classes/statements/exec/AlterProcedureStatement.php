<?php

/**
 * AlterProcedureStatement class file.
 * 
 * @author Anastaszor
 */
class AlterProcedureStatement 
		extends CachalotObject implements IAlterProcedureStatement
{
	
	/**
	 * The name of the function to alter.
	 * 
	 * @var string
	 */
	private $_procedure_name = null;
	
	/**
	 * The characteristics of the procedure to alter.
	 * 
	 * @var ICharacteristicStatement[]
	 */
	private $_characteristics = array();
	
	/**
	 * Sets the name of the procedure to alter.
	 * 
	 * @param string $name
	 */
	public function setProcedureName($name)
	{
		$this->_procedure_name = $name;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAlterProcedureStatement::getProcedureName()
	 */
	public function getProcedureName()
	{
		return $this->_procedure_name;
	}
	
	/**
	 * Adds the Characteristic to the list for the procedure to alter.
	 * 
	 * @param ICharacteristicStatement $statement
	 */
	public function addCharacteristic(ICharacteristicStatement $statement)
	{
		$stclass = get_class($statement);
		foreach($this->_characteristics as $i => $characteristic)
		{
			if(get_class($characteristic) == $stclass)
			{
				// on remplace, on a trouvé
				$this->_characteristics[$i] = $statement;
				return;
			}
		}
		// on a pas trouvé, on append
		$this->_characteristics[] = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAlterProcedureStatement::getCharacteristics()
	 */
	public function getCharacteristics()
	{
		return $this->_characteristics;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExecutableStatement::execute()
	 */
	public function execute(Executor $executor)
	{
		return $executor->alterProcedure($this);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		$ok = !empty($this->_function_name)
		&& count($this->getCharacteristics()) > 0;
		foreach($this->getCharacteristics() as $charac)
		{
			$ok &= $charac->validate();
		}
		return $ok;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->alterProcedure($this);
	}
	
}

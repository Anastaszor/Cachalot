<?php

/**
 * AlterDatabaseStatement class file.
 * 
 * @author Anastaszor
 */
class AlterDatabaseStatement 
		extends CachalotObject implements IAlterDatabaseStatement
{
	
	/**
	 * The name of the database to alter.
	 * 
	 * @var string
	 */
	private $_database_name = null;
	
	/**
	 * The specification to alter.
	 * 
	 * @var IDatabaseSpecificationStatement
	 */
	private $_specification = null;
	
	/**
	 * Sets the database name with the value.
	 * 
	 * @param string $name
	 */
	public function setDatabaseName($name)
	{
		$this->_database_name = $name;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAlterDatabaseStatement::getDatabaseName()
	 */
	public function getDatabaseName()
	{
		return $this->_database_name;
	}
	
	/**
	 * Sets the specification option with the statement.
	 * 
	 * @param IDatabaseSpecificationStatement $specification
	 */
	public function setSpecification(IDatabaseSpecificationStatement $specification)
	{
		$this->_specification = $specification;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAlterDatabaseStatement::getSpecification()
	 */
	public function getSpecification()
	{
		return $this->_specification;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExecutableStatement::execute()
	 */
	public function execute(Executor $executor)
	{
		return $executor->alterDatabase($this);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExecutableStatement::validate()
	 */
	public function validate()
	{
		return !empty($this->_database_name) 
			&& $this->getSpecification() !== null
			&& $this->getSpecification()->validate();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->alterDatabase($this);
	}
	
}

<?php

/**
 * UseStatement class file.
 * 
 * @author Anastaszor
 */
class UseStatement extends CachalotObject implements IUseStatement
{
	
	/**
	 * The name of the database to use.
	 * 
	 * @var string
	 */
	private $_name = null;
	
	/**
	 * Sets the database name to use.
	 * 
	 * @param string $name
	 */
	public function setDatabaseName($name)
	{
		$this->_name = $name;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IUseStatement::getDatabaseName()
	 */
	public function getDatabaseName()
	{
		return $this->_name;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExecutableStatement::validate()
	 */
	public function validate()
	{
		return !empty($this->getDatabaseName());
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::execute()
	 */
	public function execute(Executor $executor)
	{
		return $executor->use_($this);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->use_($this);
	}
	
}

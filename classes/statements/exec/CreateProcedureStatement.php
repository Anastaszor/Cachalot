<?php

/**
 * CreateProcedureStatement class file.
 * 
 * @author Anastaszor
 */
class CreateProcedureStatement 
		extends CachalotObject implements ICreateProcedureStatement
{
	/**
	 *
	 * @var string
	 */
	private $_definer = null;
	/**
	 *
	 * @var string
	 */
	private $_procedure_name = null;
	/**
	 *
	 * @var IProcedureParamStatement[]
	 */
	private $_params = array();
	/**
	 *
	 * @var ICharacteristicStatement[]
	 */
	private $_characteristics = array();
	/**
	 *
	 * @var IProcedureRoutineBodystatement
	*/
	private $_body = null;
	
	/**
	 * Sets the definer for this function to create.
	 *
	 * @param string $string
	 */
	public function setDefiner($string)
	{
		$this->_definer = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateProcedureStatement::getDefiner()
	 */
	public function getDefiner()
	{
		return $this->_definer;
	}
	
	/**
	 * Sets the name of the function to create.
	 *
	 * @param string $string
	 */
	public function setProcedureName($string)
	{
		$this->_procedure_name = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateProcedureStatement::getProcedureName()
	 */
	public function getProcedureName()
	{
		return $this->_procedure_name;
	}
	
	/**
	 * Adds a param to this function.
	 *
	 * @param IProcedureParamStatement $statement
	 */
	public function addParam(IProcedureParamStatement $statement)
	{
		$this->_params[] = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateProcedureStatement::getParams()
	 */
	public function getParams()
	{
		return $this->_params;
	}
	
	/**
	 * Adds a characteristic to this function.
	 *
	 * @param ICharacteristicStatement $statement
	 */
	public function addCharacteristics(ICharacteristicStatement $statement)
	{
		$this->_characteristics[] = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateProcedureStatement::getCharacteristics()
	 */
	public function getCharacteristics()
	{
		return $this->_characteristics;
	}
	
	/**
	 * Sets the body of this function.
	 *
	 * @param IRoutineBodystatement $statement
	 */
	public function setBody(IRoutineBodystatement $statement)
	{
		$this->_body = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateProcedureStatement::getRoutineBody()
	 */
	public function getRoutineBody()
	{
		return $this->_body;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExecutableStatement::execute()
	 */
	public function execute(Executor $executor)
	{
		return $executor->createProcedure($this);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		$ok = is_string($this->_function_name)
		&& strlen(trim($this->_function_name)) > 0
		&& $this->_body !== null && $this->_body->validate();
		foreach($this->_params as $param)
		{
			$ok &= $param->validate();
		}
		foreach($this->_characteristics as $charac)
		{
			$ok &= $charac->validate();
		}
		return $ok;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->createProcedure($this);
	}
	
}

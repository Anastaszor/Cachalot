<?php

/**
 * AlterTableStatement class file.
 * 
 * @author Anastaszor
 */
class AlterTableStatement extends CachalotObject implements IAlterTableStatement
{
	
	/**
	 * Whether ignore fails for this statement.
	 * 
	 * @var boolean
	 */
	private $_ignore = null;
	
	/**
	 * Sets the name of the table to alter.
	 * 
	 * @var string
	 */
	private $_table_name = null;
	
	/**
	 * The table specifications for this statement.
	 * 
	 * @var ITableSpecificationStatement[]
	 */
	private $_alter_specifications = array();
	
	/**
	 * Sets the ignore value for this statement.
	 * 
	 * @param boolean $boolean
	 */
	public function setIgnore($boolean)
	{
		$this->_ignore = true === $boolean;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAlterTableStatement::getIgnore()
	 */
	public function getIgnore()
	{
		return $this->_ignore;
	}
	
	/**
	 * Sets the name of the table to alter.
	 * 
	 * @param string $string
	 */
	public function setTableName($string)
	{
		$this->_table_name = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAlterTableStatement::getTableName()
	 */
	public function getTableName()
	{
		return $this->_table_name;
	}
	
	/**
	 * Adds a new alter table specification to current statement.
	 * 
	 * @param ITableSpecificationStatement $statement
	 */
	public function addTableSpecification(ITableSpecificationStatement $statement)
	{
		$this->_alter_specifications[] = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAlterTableStatement::getAlterTableSpecifications()
	 */
	public function getTableSpecifications()
	{
		return $this->_alter_specifications;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExecutableStatement::execute()
	 */
	public function execute(Executor $executor)
	{
		return $executor->alterTable($this);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		$ok = !empty($this->_table_name) && count($this->_alter_specifications) > 1;
		foreach($this->getTableSpecifications() as $specification)
		{
			$ok &= $specification->validate();
		}
		return $ok;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->alterTable($this);
	}
	
}

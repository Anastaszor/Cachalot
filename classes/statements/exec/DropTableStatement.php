<?php

/**
 * DropTableStatement class file.
 *
 * Statement for creating DROP TABLE statements.
 *
 * @author Anastaszor
 * @see http://dev.mysql.com/doc/refman/5.5/en/drop-table.html
 */
class DropTableStatement
		extends CachalotObject
		implements IDropTableStatement
{
	
	/**
	 * 
	 * @var boolean
	 */
	private $_temporary = false;
	/**
	 * 
	 * @var string[]
	 */
	private $_tableNames = array();
	/**
	 * 
	 * @var boolean
	 */
	private $_ifExists = false;
	
	/**
	 * Sets the temporary part of the statement.
	 * @param boolean $value
	 */
	public function setTemporary($value)
	{
		$this->_temporary = $value === true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDropTableStatement::getTemporary()
	 */
	public function getTemporary()
	{
		return $this->_temporary;
	}
	
	/**
	 * Adds a table to the tables to drop.
	 * @param string $tablename
	 * @param string $databasename
	 */
	public function addTableName($tablename, $databasename=null)
	{
		$this->_tableNames[] = array(
			'database' => $databasename,
			'table' => $tablename,
		);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDropTableStatement::getTableNames()
	 */
	public function getTableNames()
	{
		return $this->_tableNames;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDropTableStatement::getFirstTableName()
	 */
	public function getFirstTableName()
	{
		if(count($this->_tableNames) > 0)
		{
			$tn = $this->_tableNames[0]['table'];
			unset($this->_tableNames[0]);
			return $tn;
		}
		return null;
	}
	
	/**
	 * Sets the if exists part of the statement.
	 * @param boolean $value
	 */
	public function setIfExists($value)
	{
		$this->_ifExists = $value === true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDropTableStatement::getIfExists()
	 */
	public function getIfExists()
	{
		return $this->_ifExists;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExecutableStatement::execute()
	 */
	public function execute(Executor $executor)
	{
		return $executor->dropTable($this);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->dropTable($this);
	}
	
}

<?php

/**
 * AlterFunctionStatement class file.
 * 
 * @author Anastaszor
 */
class AlterFunctionStatement 
		extends CachalotObject implements IAlterFunctionStatement
{
	
	/**
	 * The name of the function to alter.
	 * 
	 * @var string
	 */
	private $_function_name = null;
	
	/**
	 * The Characteristics of the function to alter.
	 * 
	 * @var ICharacteristicStatement[]
	 */
	private $_characteristics = array();
	
	/**
	 * Sets the name of the function to alter.
	 * 
	 * @param string $name
	 */
	public function setFunctionName($name)
	{
		$this->_function_name = $name;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAlterFunctionStatement::getFunctionName()
	 */
	public function getFunctionName()
	{
		return $this->_function_name;
	}
	
	/**
	 * Adds the Characteristic to the list for the function to alter.
	 * 
	 * @param ICharacteristicStatement $statement
	 */
	public function addCharacteristic(ICharacteristicStatement $statement)
	{
		$stclass = get_class($statement);
		foreach($this->_characteristics as $i => $characteristic)
		{
			if(get_class($characteristic) == $stclass)
			{
				// on remplace, on a trouvé
				$this->_characteristics[$i] = $statement;
				return;
			}
		}
		// on a pas trouvé, on append
		$this->_characteristics[] = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAlterFunctionStatement::getCharacteristic()
	 */
	public function getCharacteristics()
	{
		return $this->_characteristics;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExecutableStatement::execute()
	 */
	public function execute(Executor $executor)
	{
		return $executor->alterFunction($this);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		$ok = !empty($this->_function_name) 
			&& count($this->getCharacteristics()) > 0;
		foreach($this->getCharacteristics() as $charac)
		{
			$ok &= $charac->validate();
		}
		return $ok;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->alterFunction($this);
	}
	
}

<?php

/**
 * ShowDatabasesStatement class file.
 *
 * @author Anastaszor
 * @see http://dev.mysql.com/doc/refman/5.5/en/show-databases.html
 */
class ShowDatabasesStatement 
		extends CachalotObject 
		implements IShowDatabasesStatement
{
	
	/**
	 * 
	 * @var IShowLikeStatement $statement
	 */
	private $_like = null;
	/**
	 * 
	 * @var IWhereStatement
	 */
	private $_where = null;
	
	/**
	 * (non-PHPdoc)
	 * @see IShowDatabasesStatement::getShowLikeStatement()
	 */
	public function getShowLikeStatement()
	{
		return $this->_like;
	}
	
	/**
	 * Sets the like part of this statement.
	 * @param IShowLikeStatement $statement
	 */
	public function setShowLikeStatement(IShowLikeStatement $statement)
	{
		$this->_like = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IShowDatabasesStatement::getWhereStatement()
	 */
	public function getWhereStatement()
	{
		return $this->_where;
	}
	
	/**
	 * Sets the where part of this statment.
	 * @param IWhereStatement $statement
	 */
	public function setWhereStatement(IWhereStatement $statement)
	{
		$this->_where = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExecutableStatement::validate()
	 */
	public function validate()
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::execute()
	 */
	public function execute(Executor $executor)
	{
		return $executor->showDatabases($this);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->showDatabases($this);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CachalotObject::equals()
	 */
	public function equals(CachalotObject $obj)
	{
		if(!($obj instanceof self))
			return false;
		
		if($this->getShowLikeStatement() !== null)
		{
			if($obj->getShowLikeStatement() !== null)
			{
				if(!$this->getShowLikeStatement()->equals($obj->getShowLikeStatement()))
					return false;
			}
			else
				return false;
		}
		elseif($obj->getShowLikeStatement() !== null)
			return false;
		
		if($this->getWhereStatement() !== null)
		{
			if($obj->getWhereStatement() !== null)
			{
				if(!$this->getWhereStatement()->equals($obj->getWhereStatement()))
					return false;
			}
			else
				return false;
		}
		elseif($obj->getWhereStatement() !== null)
			return false;
		
		return true;
	}
	
}

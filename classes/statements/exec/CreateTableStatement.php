<?php

/**
 * CreateTableStatement class file.
 *
 * @author Anastaszor
 * @see http://dev.mysql.com/doc/refman/5.5/en/create-table.html
 */
class CreateTableStatement extends CachalotObject implements ICreateTableStatement
{
	
	/**
	 * 
	 * @var boolean
	 */
	private $_temporary = false;
	/**
	 * 
	 * @var boolean
	 */
	private $_ifNotExists = false;
	/**
	 * 
	 * @var IDatabase
	 */
	private $_databaseName = null;
	/**
	 * 
	 * @var string
	 */
	private $_tableName = null;
	/**
	 * 
	 * @var ICreateDefinitionStatements[]
	 */
	private $_createDefinitionStatements = array();
	/**
	 * 
	 * @var ITableOptionsStatement
	 */
	private $_tableOptionsStatement = null;
	/**
	 * 
	 * @var IPartitionOptionsStatement
	 */
	private $_partitionOptionsStatement = null;
	/**
	 * 
	 * @var ISelectOptionStatement
	 */
	private $_selectOptionsStatement = null;
	/**
	 * 
	 * @var ILikeStatement
	 */
	private $_likeStatement = null;
	
	/**
	 * 
	 * @param bool $value
	 */
	public function setTemporary($value)
	{
		$this->_temporary = $value === true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateTableStatement::getTemporary()
	 */
	public function getTemporary()
	{
		return $this->_temporary;
	}
	
	/**
	 * 
	 * @param bool $value
	 */
	public function setIfNotExists($value)
	{
		$this->_ifNotExists = $value === true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateTableStatement::getIfNotExists()
	 */
	public function getIfNotExists()
	{
		return $this->_ifNotExists;
	}
	
	/**
	 * 
	 * @param string $name
	 * @throws InvalidArgumentException
	 */
	public function setDatabaseName($name)
	{
		$this->_databaseName = $name;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateTableStatement::getDatabaseName()
	 */
	public function getDatabaseName()
	{
		return $this->_databaseName;
	}
	
	/**
	 * 
	 * @param string $name
	 * @throws InvalidArgumentException
	 */
	public function setTableName($name)
	{
		if(preg_match('[^\w\d_\-]', $name))
		{
			throw new InvalidArgumentException(Cachalot::t(
				'Invalid Database name "{name}": must be all alphanumeric characters (dash and underscore allowed).', array(
					'{name}' => $name,
				)));
		}
		$this->_tableName = $name;
	}
	/**
	 * (non-PHPdoc)
	 * @see ICreateTableStatement::getTableName()
	 */
	public function getTableName()
	{
		return $this->_tableName;
	}
	
	/**
	 * 
	 * @param ICreateDefinitionStatement $statement
	 */
	public function addCreateDefinitionStatement(ICreateDefinitionStatement $statement)
	{
		$this->_createDefinitionStatements[] = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateTableStatement::getCreateDefinitionStatements()
	 */
	public function getCreateDefinitionStatements()
	{
		return $this->_createDefinitionStatements;
	}
	
	/**
	 * 
	 * @param ITableOptionsStatement $statement
	 */
	public function setTableOptionsStatement(ITableOptionsStatement $statement)
	{
		$this->_tableOptionsStatement = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateTableStatement::getTableOptionsStatement()
	 */
	public function getTableOptionsStatement()
	{
		if($this->_tableOptionsStatement === null)
			$this->_tableOptionsStatement = new TableOptionsStatement();
		return $this->_tableOptionsStatement;
	}
	
	/**
	 * 
	 * @param IPartitionOptionsStatement $statement
	 */
	public function setPartitionOptionsStatement(IPartitionOptionsStatement $statement)
	{
		$this->_partitionOptionsStatement = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateTableStatement::getPartitionOptionsStatement()
	 */
	public function getPartitionOptionsStatement()
	{
		return $this->_partitionOptionsStatement;
	}
	
	/**
	 * 
	 * @param ISelectStatement $statement
	 */
	public function setSelectOptionStatement(ISelectStatement $statement)
	{
		$this->_selectOptionsStatement = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateTableStatement::getSelectOptionStatement()
	 */
	public function getSelectOptionStatement()
	{
		return $this->_selectOptionsStatement;
	}
	
	/**
	 * 
	 * @param ILikeStatement $statement
	 */
	public function setLikeStatement(ILikeStatement $statement)
	{
		$this->_likeStatement = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateTableStatement::getLikeStatement()
	 */
	public function getLikeStatement()
	{
		return $this->_likeStatement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExecutableStatement::validate()
	 */
	public function validate()
	{
		return !empty($this->getTableName()) && count($this->getCreateDefinitionStatements()) > 0;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::execute()
	 */
	public function execute(Executor $executor)
	{
		return $executor->createTable($this);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->createTable($this);
	}
	
}

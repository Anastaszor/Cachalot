<?php

/**
 * CreateDatabaseStatement class file.
 *
 * @author Anastaszor
 */
class CreateDatabaseStatement 
		extends CachalotObject implements ICreateDatabaseStatement
{
	
	/**
	 * 
	 * @var string
	 */
	private $_name = null;
	/**
	 * 
	 * @var boolean
	 */
	private $_ifNotExists = false;
	
	/**
	 * 
	 * @var ISpecificationStatement[]
	 */
	private $_specifications = array();
	
	/**
	 * true if the IF NOT EXISTS statement exists
	 * 
	 * @param boolean $value
	 */
	public function setIfNotExists($value)
	{
		$this->_ifNotExists = $value === true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateDatabaseStatement::getIfNotExists()
	 */
	public function getIfNotExists()
	{
		return $this->_ifNotExists;
	}
	
	/**
	 * Sets the Database Name (must be unescaped)
	 * 
	 * @param string $name
	 * @throws InvalidArgumentException
	 */
	public function setDatabaseName($name)
	{
		$this->_name = $name;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateDatabaseStatement::getDatabaseName()
	 */
	public function getDatabaseName()
	{
		return $this->_name;
	}
	
	/**
	 * Adds a specification for this statement.
	 * 
	 * @param IDatabaseSpecificationStatement $statement
	 */
	public function addSpecification(IDatabaseSpecificationStatement $statement)
	{
		$this->_specifications[] = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateDatabaseStatement::getSpecification()
	 */
	public function getSpecifications()
	{
		return $this->_specifications;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExecutableStatement::validate()
	 */
	public function validate()
	{
		$ok = !empty($this->getDatabaseName()) 
			&& count($this->_specifications) > 0;
		foreach($this->_specifications as $statement)
		{
			$ok &= $statement->validate();
		}
		return $ok;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::execute()
	 */
	public function execute(Executor $executor)
	{
		return $executor->createDatabase($this);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->createDatabase($this);
	}
	
}

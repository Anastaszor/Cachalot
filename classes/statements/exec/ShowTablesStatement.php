<?php

/**
 * ShowTablesStatement class file.
 *
 * @author Anastaszor
 * @see http://dev.mysql.com/doc/refman/5.5/en/show-tables.html
 */
class ShowTablesStatement
		extends CachalotObject
		implements IShowTablesStatement
{
	
	private $_dbname = null;
	private $_like = null;
	private $_where = null;
	
	/**
	 * (non-PHPdoc)
	 * @see IShowTablesStatement::getDatabaseName()
	 */
	public function getDatabaseName()
	{
		return $this->_dbname;
	}
	
	/**
	 * Sets the database name of this statement
	 * @param string $dbname
	 */
	public function setDatabaseName($dbname)
	{
		$this->_dbname = $dbname;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IShowTablesStatement::getShowLikeStatement()
	 */
	public function getShowLikeStatement()
	{
		return $this->_like;
	}
	
	/**
	 * Sets the like part of this statement.
	 * @param IShowLikeStatement $statement
	 */
	public function setShowLikeStatement(IShowLikeStatement $statement)
	{
		$this->_like = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IShowTablesStatement::getWhereStatement()
	 */
	public function getWhereStatement()
	{
		return $this->_where;
	}
	
	/**
	 * Sets the where part of this statment.
	 * @param IWhereStatement $statement
	 */
	public function setWhereStatement(IWhereStatement $statement)
	{
		$this->_where = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExecutableStatement::validate()
	 */
	public function validate()
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExecutableStatement::execute()
	 */
	public function execute(Executor $executor)
	{
		return $executor->showTables($this);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->showTables($this);
	}
	
}

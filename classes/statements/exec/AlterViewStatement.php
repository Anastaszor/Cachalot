<?php

/**
 * AlterViewStatement class file.
 * 
 * @author Anastaszor
 */
class AlterViewStatement extends CachalotObject implements IAlterViewStatement
{
	
	/**
	 * 
	 * @var enum('UNDEFINED', 'MERGE', 'TEMPTABLE')
	 */
	private $_algorithm = null;
	/**
	 * 
	 * @var string
	 */
	private $_definer = null;
	/**
	 * 
	 * @var enum('DEFINER', 'INVOKER')
	 */
	private $_security = null;
	/**
	 * 
	 * @var string
	 */
	private $_view_name = null;
	/**
	 * 
	 * @var string[]
	 */
	private $_column_list = array();
	/**
	 * 
	 * @var ISelectStatement
	 */
	private $_select_statement = null;
	/**
	 * 
	 * @var boolean
	 */
	private $_check_option = false;
	/**
	 * 
	 * @var enum('CASCADED', 'LOCAL')
	 */
	private $_check_option_mode = null;
	
	/**
	 * 
	 * @param enum('UNDEFINED', 'MERGE', 'TEMPTABLE') $value
	 */
	public function setAlgorithm($value)
	{
		if($value === self::ALGO_MERGE
			|| $value === self::ALGO_TEMPTABLE
			|| $value === self::ALGO_UNDEFINED
		) {
			$this->_algorithm = $value;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAlterViewStatement::getAlgorithm()
	 */
	public function getAlgorithm()
	{
		return $this->_algorithm;
	}
	
	/**
	 * 
	 * @param string $string
	 */
	public function setDefiner($string)
	{
		$this->_definer = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAlterViewStatement::getDefiner()
	 */
	public function getDefiner()
	{
		return $this->_definer;
	}
	
	/**
	 * 
	 * @param enum('DEFINER', 'INVOKER') $value
	 */
	public function setSecurity($value)
	{
		if($value === self::SEC_DEFINER || $value === self::SEC_INVOKER)
		{
			$this->_security = $value;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAlterViewStatement::getSecurity()
	 */
	public function getSecurity()
	{
		return $this->_security;
	}
	
	/**
	 * 
	 * @param string $string
	 */
	public function setViewName($string)
	{
		$this->_view_name = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAlterViewStatement::getViewName()
	 */
	public function getViewName()
	{
		return $this->_view_name;
	}
	
	/**
	 * 
	 * @param string $string
	 */
	public function addColumnName($string)
	{
		$this->_column_list[] = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAlterViewStatement::getColumnList()
	 */
	public function getColumnList()
	{
		return $this->_column_list;
	}
	
	/**
	 * 
	 * @param ISelectStatement $statement
	 */
	public function setSelectStatement(ISelectStatement $statement)
	{
		$this->_select_statement = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAlterViewStatement::getSelectStatement()
	 */
	public function getSelectStatement()
	{
		return $this->_select_statement;
	}
	
	/**
	 * 
	 * @param boolean $boolean
	 */
	public function setCheckOption($boolean)
	{
		$this->_check_option = true === $boolean;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAlterViewStatement::getCheckOption()
	 */
	public function getCheckOption()
	{
		return $this->_check_option;
	}
	
	/**
	 * 
	 * @param enum('CASCADED', 'LOCAL') $value
	 */
	public function setCheckOptionMode($value)
	{
		if($value === self::CHK_OPT_CASCADED || $value === self::CHK_OPT_LOCAL)
		{
			$this->_check_option_mode = $value;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IAlterViewStatement::getCheckOptionMode()
	 */
	public function getCheckOptionMode()
	{
		return $this->_check_option_mode;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExecutableStatement::execute()
	 */
	public function execute(Executor $executor)
	{
		return $executor->alterView($this);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return is_string($this->_view_name) 
			&& strlen(trim($this->_view_name)) > 0
			&& $this->_select_statement !== null 
			&& $this->_select_statement->validate();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->alterView($this);
	}
	
}

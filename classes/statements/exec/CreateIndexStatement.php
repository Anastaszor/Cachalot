<?php

/**
 * CreateIndexStatement class file.
 * 
 * @author Anastaszor
 */
class CreateIndexStatement 
		extends CachalotObject implements ICreateIndexStatement
{
	
	/**
	 * The spatial representation of this index.
	 * 
	 * @var enum('UNIQUE', 'FULLTEXT', 'SPATIAL')
	 */
	private $_index_space = null;
	/**
	 * The name of this index.
	 * 
	 * @var string
	 */
	private $_index_name = null;
	/**
	 * The type of this index.
	 * 
	 * @var enum('BTREE', 'HASH')
	 */
	private $_index_type = null;
	/**
	 * The target table name of this index.
	 * 
	 * @var string
	 */
	private $_table_name = null;
	/**
	 * The columns of this index.
	 * 
	 * @var IColumnIndexStatement[]
	 */
	private $_columns = array();
	
	/**
	 * Sets the spatial configuration for this index.
	 * 
	 * @param enum('UNIQUE', 'FULLTEXT', 'SPATIAL') $value
	 */
	public function setIndexSpace($value)
	{
		if($value === self::UNIQUE 
			|| $value === self::FULLTEXT 
			|| $value === self::SPATIAL
		) {
			$this->_index_space = $value;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateIndexStatement::getIndexSpace()
	 */
	public function getIndexSpace()
	{
		return $this->_index_space;
	}
	
	/**
	 * Sets the name of this index.
	 * 
	 * @param string $string
	 */
	public function setIndexName($string)
	{
		$this->_index_name = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateIndexStatement::getIndexName()
	 */
	public function getIndexName()
	{
		return $this->_index_name;
	}
	
	/**
	 * Sets the type of this index.
	 * 
	 * @param enum('BTREE', 'HASH') $value
	 */
	public function setIndexType($value)
	{
		if($value === self::BTREE || $value === self::HASH)
		{
			$this->_index_type = $value;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateIndexStatement::getIndexType()
	 */
	public function getIndexType()
	{
		return $this->_index_type;
	}
	
	/**
	 * Sets the target table name for this index.
	 * 
	 * @param string $string
	 */
	public function setTableName($string)
	{
		$this->_table_name = $string;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateIndexStatement::getTableName()
	 */
	public function getTableName()
	{
		return $this->_table_name;
	}
	
	/**
	 * Adds a column to this index.
	 * 
	 * @param IColumnIndexStatement $statement
	 */
	public function addColumnIndexStatement(IColumnIndexStatement $statement)
	{
		$this->_columns[] = $statement;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICreateIndexStatement::getColumnIndexStatements()
	 */
	public function getColumnIndexStatements()
	{
		return $this->_columns;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IExecutableStatement::execute()
	 */
	public function execute(Executor $executor)
	{
		return $executor->createIndex($this);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::validate()
	 */
	public function validate()
	{
		return is_string($this->_index_name)
			&& strlen(trim($this->_index_name)) > 0
			&& is_string($this->_table_name)
			&& strlen(trim($this->_table_name)) > 0;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IStatement::toSQL()
	 */
	public function toSQL(IDialect $dialect)
	{
		return $dialect->createIndex($this);
	}
	
}

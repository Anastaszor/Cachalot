<?php

/**
 * CachalotParser class file.
 * 
 * @author Anastaszor
 */
class CachalotParser extends CachalotObject implements ISqlParser
{
	
	const SPQT = "'";
	const DBQT = '"';
	const BKQT = '`';
	
	/**
	 * Dependancy Injection !
	 * @var Cachalot
	 */
	public $cachalot = null;
	
	/**
	 * Dependancy Injection !
	 * @var IDialect[]
	 */
	public $dialects = array();
	
	private $_str = null;
	private $_pos = null;
	private $_len = null;
	private $_chr = null;
	
	private $_functions = array(
		'ALTER DATABASE ' => 'parseAlterDatabase',
		'ALTER FUNCTION ' => 'parseAlterFunction',
		'ALTER PROCEDURE ' => 'parseAlterProcedure',
		'ALTER TABLE ' => 'parseAlterTable',
		'ALTER VIEW ' => 'parseAlterView',
		'CREATE DATABASE ' => 'parseCreateDatabase',
		'CREATE FUNCTION ' => 'parseCreateFunction',
		'CREATE INDEX ' => 'parseCreateIndex',
		'CREATE PROCEDURE ' => 'parseCreateProcedure',
		'CREATE TABLE ' => 'parseCreateTable',
		'CREATE TRIGGER ' => 'parseCreateTrigger',
		'CREATE VIEW ' => 'parseCreateView',
		'CREATE SCHEMA ' => 'parseCreateDatabase',
		'DROP DATABASE ' => 'parseDropDatabase',
		'DROP FUNCTION ' => 'parseDropFunction',
		'DROP INDEX ' => 'parseDropIndex',
		'DROP PROCEDURE ' => 'parseDropProcedure',
		'DROP TABLE ' => 'parseDropTable',
		'DROP TRIGGER ' => 'parseDropTrigger',
		'DROP VIEW ' => 'parseDropView',
		'RENAME TABLE ' => 'parseRenameTable',
		'TRUNCATE TABLE ' => 'parseTruncateTable',
		'CALL ' => 'parseCall',
		'DELETE ' => 'parseDelete',
		'DO ' => 'parseDo',
		'HANDLER ' => 'parseHandler',
		'INSERT ' => 'parseInsert',
		'LOAD DATA INFILE ' => 'parseLoadDataInfile',
		'REPLACE ' => 'parseReplace',
		'SELECT ' => 'parseSelect',
		'UPDATE' => 'parseUpdate',
		'START TRANSACTION ' => 'parseStartTransaction',
		'COMMIT' => 'parseCommit',
		'SAVEPOINT ' => 'parseSavepoint',
		'ROLLBACK TO SAVEPOINT' => 'parseRollbackToSavepoint',
		'ROLLBACK' => 'parseRollback',
		'RELEASE SAVEPOINT' => 'parseReleaseSavepoint',
		'LOCK TABLES ' => 'parseLockTables',
		'UNLOCK TABLES ' => 'parseUnlockTables',
		'SET TRANSACTION ' => 'parseSetTransaction',
		'PREPARE ' => 'parsePrepare',
		'EXECUTE ' => 'parseExecute',
		'DEALLOCATE PREPARE ' => 'parseDeallocatePrepare',
		'ANALYSE TABLE ' => 'parseAnalyseTable',
		'BACKUP TABLE ' => 'parseBackupTable',
		'CHECK TABLE ' => 'parseCheckTable',
		'CHECKSUM TABLE ' => 'parseChecksumTable',
		'OPTIMIZE TABLE ' => 'parseOptimizeTable',
		'REPAIR TABLE ' => 'parseRepairTable',
		'RESTORE TABLE ' => 'parseRestoreTable',
		'SET ' => 'parseSetTable',
		'SHOW BINARY LOGS ' => 'parseShowBinaryLogs',
		'SHOW BINLOGS EVENTS ' => 'parseShowBinlogsEvents',
		'SHOW CHARACTER SET ' => 'parseShowCharacterSet',
		'SHOW COLLATION ' => 'parseShowCollation',
		'SHOW COLUMNS ' => 'parseShowColumns',
		'SHOW CREATE DATABASE ' => 'parseShowCreateDatabase',
		'SHOW CREATE FUNCTION ' => 'parseShowCreateFunction',
		'SHOW CREATE PROCEDURE ' => 'parseShowCreateProcedure',
		'SHOW CREATE TABLE ' => 'parseShowCreateTable',
		'SHOW CREATE VIEW ' => 'parseShowCreateView',
		'SHOW DATABASES' => 'parseShowDatabases',
		'SHOW ENGINE ' => 'parseShowEngine',
		'SHOW ENGINES' => 'parseShowEngines',
		'SHOW ERRORS ' => 'parseShowErrors',
		'SHOW FUNCTION CODE ' => 'parseShowFunctionCode',
		'SHOW FUNCTION STATUS ' => 'parseShowFunctionStatus',
		'SHOW GRANTS ' => 'parseShowGrants',
		'SHOW INDEX ' => 'parseShowIndex',
		'SHOW LOGS ' => 'parseShowLogs',
		'SHOW MASTER STATUS ' => 'parseShowMasterStatus',
		'SHOW MUTEX STATUS ' => 'parseShowMutexStatus',
		'SHOW OPEN TABLES ' => 'parseShowOpenTables',
		'SHOW PRIVILEGES ' => 'parseShowPrivileges',
		'SHOW PROCEDURE CODE ' => 'parseShowProcedureCode',
		'SHOW PROCEDURE STATUS ' => 'parseShowProcedureStatus',
		'SHOW PROCESS LIST' => 'parseShowProcessList',
		'SHOW PROFILE ' => 'parseShowProfile',
		'SHOW PROFILES ' => 'parseShowProfiles',
		'SHOW STATUS ' => 'parseShowStatus',
		'SHOW TABLE STATUS ' => 'parseShowTableStatus',
		'SHOW TABLES' => 'parseShowTables',
		'SHOW FULL TABLES' => 'parseShowTables',
		'SHOW TRIGGERS ' => 'parseShowTriggers',
		'SHOW VARIABLES ' => 'parseShowVariables',
		'SHOW WARNINGS ' => 'parseShowWarnings',
		'CACHE INDEX ' => 'parseCacheIndex',
		'FLUSH ' => 'parseFlush',
		'KILL' => 'parseKill',
		'LOAD INDEX INTO CACHE ' => 'parseLoadIndexIntoCache',
		'RESET ' => 'parseReset',
		'DESCRIBE ' => 'parseDescribe',
		'EXPLAIN ' => 'parseExplain',
		'HELP ' => 'parseHelp',
		'USE ' => 'parseUse',
	);
	
	/**
	 * (non-PHPdoc)
	 * @see ISqlParser::parse()
	 */
	public function parse($string)
	{
		$this->_str = $string;
		$this->_len = strlen($this->_str);
		
		if($this->_len < 1)
			return array();
		
		$this->_pos = -1;
		
		$statements = array();
		while($this->getChar())
		{
			if(!$this->skipSpaces())
				break;	// we're at the end of the string
			
			foreach($this->_functions as $stn => $fcn)
			{
				// if, at the current position, the statement is equal to
				// one of the above statements...
				if(stripos($this->_str, $stn, $this->_pos) === $this->_pos)
				{
					$this->getChar(strlen($stn));
					if(method_exists($this, $fcn))
					{
						$statements[] = $this->$fcn();
					}
					else 
						throw new NotImplementedException("The function ''$fcn'' is not implemented.");
					
					break;
				}
			}
		}
		
		if(count($statements) === 0)
			throw new ParseException($this->_str, 0, $this->_len);
		
		$this->_str = null;
		$this->_pos = null;
		$this->_len = null;
		$this->_chr = null;
		
		return $statements;
	}
	
	/**
	 * Increments $qty of the position and synchronizes the given character.
	 * @return boolean whether the end of the string is reached or not
	 * 		(false = end is here, true = you can continue)
	 */
	public function getChar($qty = 1)
	{
		$this->_pos = $this->_pos + $qty;
		if($this->_pos < $this->_len)
		{
			$this->_chr = $this->_str[$this->_pos];
			return true;
		}
		return false;
	}
	
	/**
	 * Increments the position counter for as long as it points to a space
	 * character.
	 * @return boolean whether the end of the string is reached or not
	 * 		(false = end is here, true = you can continue)
	 */
	public function skipSpaces()
	{
		while($this->_chr === ' ' 		// space
			|| $this->_chr === "\n" 	// newline
			|| $this->_chr === "\r" 	// carriage return
			|| $this->_chr === "\t" 	// tabulation
			|| $this->_chr === "\v" 	// vertical tabulation
			|| $this->_chr === " " 		// non breaking space
		) {
			// prevent infinite loop when string ends with space char
			if(!$this->getChar())
				return false;
		}
		return true;
	}
	
	// ---------- Individual Statement Parsers ---------- \\
	
	/**
	 * Creates a statement to execute a create database query.
	 * 
	 * @throws ParseException
	 * @return CreateDatabaseStatement
	 */
	public function parseCreateDatabase()
	{
		// "CREATE DATABASE" already parsed
		$this->skipSpaces();
		
		$start = $this->_pos;
		
		$statement = new CreateDatabaseStatement($this->cachalot);
		// CREATE DATABASE IF NOT EXISTS ...
		$statement->setIfNotExists($this->parseIfNotExists());
		// CREATE DATABASE [IF NOT EXISTS ]`dbname` ...
		$statement->setDatabaseName($this->parseDatabaseName());
		// CREATE DATABASE [IF NOT EXISTS ]`dbname` [DEFAULT] CHARACTER SET ...
		if(($charset = $this->parseCharacterSet()) !== null)
			$statement->setCharacterSet($charset);
		// CREATE DATABASE [IF NOT EXISTS ]`dbname` [[DEFAULT] CHARACTER SET `chrname` ][DEFAULT] COLLATION ...
		if(($collation = $this->parseCollation()) !== null)
			$statement->setCollation($collation);
		// CREATE DATABSE [IF NOT EXISTS ]`dbname` [[DEFAULT] CHARACTER SET `chrname` ][[DEFAULT COLLATION `colname` ]
		if(($engine = $this->parseEngine()) !== null)
			$statement->setEngine($engine);
		
		$this->skipSpaces();
		
		$end = $this->_pos;
		
		if($this->_chr === ';' || $this->_pos === $this->_len)
		{
			$this->getChar();
		}
		else
			throw new ParseException($this->_str, $start, $end - $start + 1);
		
		$end = $this->_pos;
		
		if($statement->validate())
			return $statement;
		else
			throw new ParseException($this->_str, $start, $end - $start + 1);
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return CreateTableStatement
	 */
	public function parseCreateTable()
	{
		// CREATE TABLE already parsed
		$this->skipSpaces();
		
		$start = $this->_pos;
		
		$statement = new CreateTableStatement($this->cachalot);
		// CREATE TABLE IF NOT EXISTS ...
		$statement->setIfNotExists($this->parseIfNotExists());
		// CREATE TABLE [IF NOT EXISTS ] [dbname].tbname
		$tablecoords = $this->parseTableName();
		if(isset($tablecoords['db']))
			$statement->setDatabaseName($tablecoords['db']);
		$statement->setTableName($tablecoords['tb']);
		
		$this->skipSpaces();
		
		// CREATE TABLE [IF NOT EXISTS] [dbname].tbname ( ... create definition ... )
		if($this->_chr !== '(')
		{
			$end = $this->_pos;
			throw new ParseException($this->_str, $start, $end - $start + 1);
		}
		else
			$this->getChar();
		
		while($this->_chr !== ')' && $this->_len > $this->_pos)
		{
			var_dump($this->_chr." >> ".$this->_pos);
			if($this->_chr === ',')
				$this->getChar();
			
			$this->skipSpaces();
			
			$substt = $this->parseCreateDefinition();
			if($substt !== null)
				$statement->addCreateDefinitionStatement($substt);
			$this->getChar();
			var_dump($this->_chr." >> ".$this->_pos);
		}
		
		$this->skipSpaces();
		
		$tbostt = $this->parseTableOptions();
		if($tbostt !== null)
			$statement->setTableOptionsStatement($tbostt);
		
		$this->skipSpaces();
		
		$parstt = $this->parsePartitionOptions();
		if($parstt !== null)
			$statement->setPartitionOptionsStatement($parstt);
		
		$this->skipSpaces();
		
		$selstt = $this->parseSelect();
		if($selstt !== null)
			$statement->setSelectOptionStatement($selstt);
		
		$this->skipSpaces();
		
		$end = $this->_pos;
		
		if($this->_chr === ';' || $this->_chr === ')' || $this->_pos === $this->_len)
		{
			$this->getChar();
		}
		else
			throw new ParseException($this->_str, $start, $end - $start + 1);
		
		$end = $this->_pos;
		
		if($statement->validate())
			return $statement;
		else
			throw new ParseException($this->_str, $start, $end - $start + 1);
	}
	
	/**
	 * Creates a statement to execute a drop database query.
	 * 
	 * @throws ParseException
	 * @return DropDatabaseStatement
	 */
	public function parseDropDatabase()
	{
		// DROP DATABASE already parsed
		$this->skipSpaces();
		
		$start = $this->_pos;
		
		$statement = new DropDatabaseStatement($this->cachalot);
		// DROP DATABASE IF EXISTS ...
		$statement->setIfExists($this->parseIfExists());
		// DROP DATABASE [IF EXISTS ]`dbname` ...
		$statement->setDatabaseName($this->parseDatabaseName());
		
		$this->skipSpaces();
		
		$end = $this->_pos;
		
		if($this->_chr === ';' || $this->_pos === $this->_len)
		{
			$this->getChar();
		}
		else
			throw new ParseException($this->_str, $start, $end - $start + 1);
		
		$end = $this->_pos;
		
		if($statement->validate())
			return $statement;
		else
			throw new ParseException($this->_str, $start, $end - $start + 1);
	}
	
	/**
	 * Creates a statement to execute a show database query.
	 * 
	 * @throws ParseException
	 * @return ShowDatabasesStatement
	 */
	public function parseShowDatabases()
	{
		// "SHOW DATABASES" already parsed
		$this->skipSpaces();
		
		// case "SHOW DATABASES;"
		if($this->_chr === ';' || $this->_pos === $this->_len)
			return new ShowDatabasesStatement($this->cachalot);
		
		// case "SHOW DATABASES LIKE ..."
		if(stripos($this->_str, 'LIKE ', $this->_pos) === $this->_pos)
		{
			throw new NotImplementedException('Like statements are not implemented.');
		}
		
		// case "SHOW DATABASES WHERE ... "
		if(stripos($this->_str, 'WHERE ', $this->_pos) === $this->_pos)
		{
			throw new NotImplementedException('Where statements are not implemented.');
		}
		
		throw new ParseException($this->_str, $this->_pos - 10, 20);
	}
	
	public function parseShowTables()
	{
		// SHOW [FULL] TABLES already parsed
		$this->skipSpaces();
		
		// case SHOW TABLES;
		if($this->_chr === ';' || $this->_pos === $this->_len)
			return new ShowTablesStatement($this->cachalot);
		
		// case "SHOW TABLES FROM ... "
		if(stripos($this->_str, 'FROM ', $this->_pos) === $this->_pos)
		{
			throw new NotImplementedException('from statements are not implemented.');
		}
		
		// case "SHOW TABLES IN ... "
		if(stripos($this->_str, 'IN ', $this->_pos) === $this->_pos)
		{
			throw new NotImplementedException('in statements are not implemented.');
		}
		
		// case "SHOW TABLES LIKE ... "
		if(stripos($this->_str, 'LIKE ', $this->_pos) === $this->_pos)
		{
			throw new NotImplementedException('Like statements are not implemented.');
		}
		
		// case "SHOW TABLES WHERE ... "
		if(stripos($this->_str, 'WHERE ', $this->_pos) === $this->_pos)
		{
			throw new NotImplementedException('where statements are not implemented.');
		}
		
		throw new ParseException($this->_str, $start, $end - $start + 1);
	}
	
	/**
	 * 
	 * @return 
	 */
	public function parseSelect()
	{
		
	}
	
	/**
	 * Creates a statement to execute a use database query.
	 * 
	 * @throws ParseException
	 * @return UseStatement
	 */
	public function parseUse()
	{
		// USE already parsed
		$this->skipSpaces();
		
		$start = $this->_pos;
		
		$statement = new UseStatement($this->cachalot);
		$statement->setDatabaseName($this->parseDatabaseName());
		
		$this->skipSpaces();
		
		$end = $this->_pos;
		
		if($this->_chr === ';' || $this->_pos === $this->_len)
		{
			$this->getChar();
		}
		else
			throw new ParseException($this->_str, $start, $end - $start + 1);
		
		$end = $this->_pos;
		
		if($statement->validate())
			return $statement;
		else
			throw new ParseException($this->_str, $start, $end - $start + 1);
	}
	
	
	// ---------- Utility Parsers ---------- \\
	
	/**
	 * Gets a string for this statement part. A name is a string that is inside
	 * two similar quote characters, or which begins here and finishes at the
	 * next space. In case of quotes, the quotes are not part of the name.
	 * Escaped quotes are part of the name.
	 * @return string
	 */
	public function parseString()
	{
		$this->skipSpaces();
		$quote = ' ';
		$string = "";
		
		if($this->_chr === self::BKQT
			|| $this->_chr === self::SPQT
			|| $this->_chr === self::DBQT
		) {
			$quote = $this->_chr;
		}
		else
			$string .= $this->_chr;
		
		while($this->getChar())
		{
			if($quote === ' ' && (	// is control character
				$this->_chr === ';' || $this->_chr === '.' 
				|| $this->_chr === ',' || $this->_chr === '(' 
				|| $this->_chr === ')'
			))
			{
				// end of statement or end of string
				return $string;
			}
			if($this->_chr !== $quote && $this->_chr !== '\\')
			{
				// normal character, add to string
				$string .= $this->_chr;
			}
			else
			{
				if($this->_chr === '\\')
					continue;	// joker character, ignore
		
				// quote character, verify if not joker
				if($this->_pos > 0)
					$prevchr = $this->_str[$this->_pos - 1];
				else
					$prevchr = '';
				if($prevchr === '\\')
				{
					// joker : include char to name
					$string .= $this->_chr;
				}
				else
				{
					// quote character no joker, ends the name
					$this->getChar();
					break;
				}
			}
		}
		return $string;
	}
	
	/**
	 * 
	 * @return string
	 */
	public function parseDatabaseName()
	{
		return $this->parseString();
	}
	
	/**
	 * 
	 * @return string[2]
	 */
	public function parseTableName()
	{
		$name = $this->parseString();
		if($this->_chr === '.')
		{
			$this->getChar();
			// qualified name
			$tbname = $this->parseString();
			return array('db' => $name, 'tb' => $tbname);
		}
		return array('db'=>null, 'tb'=>$name);
	}
	
	/**
	 * Gets if IF EXISTS part of a statement is present
	 * @return boolean
	 */
	public function parseIfExists()
	{
		$this->skipSpaces();
		
		if(stripos($this->_str, 'IF EXISTS ', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('IF EXISTS '));
			return true;
		}
		return false;
	}
	
	/**
	 * Gets if the IF NOT EXISTS part of a statement is present
	 * @return boolean
	 */
	public function parseIfNotExists()
	{
		$this->skipSpaces();
		
		if(stripos($this->_str, 'IF NOT EXISTS ', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('IF NOT EXISTS '));
			return true;
		}
		return false;
	}
	
	
	/**
	 * Gets the charset if exists and if it is supported by this dbms.
	 * @throws ParseException
	 * @return ICharset
	 */
	public function parseCharacterSet()
	{
		$this->skipSpaces();
		
		if(stripos($this->_str, 'DEFAULT CHARACTER SET ', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('DEFAULT CHARACTER SET '));
		}
		else if(stripos($this->_str, 'CHARACTER SET ', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('CHARACTER SET '));
		}
		else 
			return null;
		
		$start = $this->_pos;
		$charname = "";
		while($this->getChar() && $this->_chr !== ' ')
		{
			$charname .= $this->_chr;
		}
		$end = $this->_pos;
		
		$charset = $this->cachalot->getCharsetFactory()->getCharset($charname);
		if($charset === null)
			throw new ParseException($this->_str, $start, $end - $start + 1);
		
		return $charset;
	}
	
	/**
	 * Gets the collation if exists and if it is supported by this dbms.
	 * @throws ParseException
	 * @return ICollation
	 */
	public function parseCollation()
	{
		$this->skipSpaces();
		
		if(stripos($this->_str, 'DEFAULT COLLATION ', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('DEFAULT COLLATION '));
		}
		else if(stripos($this->_str, 'COLLATION ', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('COLLATION '));
		}
		else
			return null;
		
		$start = $this->_pos;
		$colname = "";
		while($this->getChar() && $this->_chr !== ' ')
		{
			$colname .= $this->_chr;
		}
		$end = $this->_pos;
		
		$collation = $this->cachalot->getCollationFactory()->getCollation($colname);
		if($collation === null)
			throw new ParseException($this->_str, $start, $end - $start + 1);
		
		return $collation;
	}
	
	/**
	 * Gets the engine if exists and if it is supported by this dbms.
	 * @throws ParseException
	 * @return IEngine
	 */
	public function parseEngine()
	{
		$this->skipSpaces();
		
		if(stripos($this->_str, 'DEFAULT ENGINE ', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('DEFAULT ENGINE '));
		}
		else if(stripos($this->_str, 'ENGINE ', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('ENGINE '));
		}
		else
			return null;
		
		$start = $this->_pos;
		$engname = "";
		while($this->getChar() && $this->_chr !== ' ')
		{
			$engname .= $this->_chr;
		}
		$end = $this->_pos;
		
		$engine = $this->cachalot->getEngineFactory()->getEngine($engname);
		if($engine === null)
			throw new ParseException($this->_str, $start, $end - $start +1);
		
		return $engine;
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return ICreateDefinitionStatement
	 */
	public function parseCreateDefinition()
	{
		$createdef = new CreateDefinitionStatement();
		// name column_definition [constraints...]
		$this->skipSpaces();
		
		$name = $this->parseTableName();
		$createdef->setColumnName($name['tb']);
		
		$this->skipSpaces();
		
		$definition = $this->parseColumnDefinition();
		$createdef->setColumnDefinition($definition);
		
		$this->skipSpaces();
		
		$constraints = $this->parseColumnConstraints();
		foreach($constraints as $constraint)
		{
			$createdef->addConstraint($constraint);
		}
		return $createdef;
	}
	
	/**
	 * 
	 * @return IColumnDefinitionStatement
	 */
	public function parseColumnDefinition()
	{
		$def = new ColumnDefinitionStatement();
		
		$this->skipSpaces();
		
		$dt = $this->parseDataType();
		$def->setDataType($dt);
		$this->getChar();
		
		$this->skipSpaces();
		
		
		// TODO...
		
		$next_comma = strpos($this->_str, ',', $this->_pos);
		$next_parenthesis = strpos($this->_str, ')', $this->_pos);
		if($next_parenthesis === false)
		{
			throw new ParseException($this->_str, $this->_pos - 10, 20);
		}
		if($next_comma === false)
		{
			// comma does not exists, last statement: go to closing parenthesis
			$this->_pos = $next_parenthesis;
		}
		else
		{
			// comma does exists, is it nearer that the parenthesis ?
			if($next_comma < $next_parenthesis)
			{
				// comma before parenthesis, more statements to come
				$this->_pos = $next_comma;
			}
			else
			{
				// parenthesis before comma, no more statement to come
				$this->_pos = $next_parenthesis;
			}
		}
		
		return $def;
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return IDataTypeStatement
	 */
	public function parseDataType()
	{
		$this->skipSpaces();
		
		$start = $this->_pos;
		
		if(stripos($this->_str, 'BIGINT', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('BIGINT'));
			return $this->parseBigintDataType();
		}
		if(stripos($this->_str, 'BINARY', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('BINARY'));
			return $this->parseBinaryDataType();
		}
		if(stripos($this->_str, 'BIT', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('BIT'));
			return $this->parseBitDataType();
		}
		if(stripos($this->_str, 'BLOB', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('BLOB'));
			return $this->parseBlobDataType();
		}
		if(stripos($this->_str, 'CHAR', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('CHAR'));
			return $this->parseCharDataType();
		}
		if(stripos($this->_str, 'DATETIME', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('DATETIME'));
			return $this->parseDatetimeDataType();
		}
		else if(stripos($this->_str, 'DATE', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('DATE'));
			return $this->parseDateDataType();
		}
		if(stripos($this->_str, 'DECIMAL', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('DECIMAL'));
			return $this->parseDecimalDataType();
		}
		if(stripos($this->_str, 'DOUBLE', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('DOUBLE'));
			return $this->parseDoubleDataType();
		}
		if(stripos($this->_str, 'ENUM', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('ENUM'));
			return $this->parseEnumDataType();
		}
		if(stripos($this->_str, 'FLOAT', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('FLOAT'));
			return $this->parseFloatDataType();
		}
		if(stripos($this->_str, 'INT', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('INT'));
			return $this->parseIntDataType();
		}
		if(stripos($this->_str, 'LONGBLOB', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('LONGBLOB'));
			return $this->parseLongblobDataType();
		}
		if(stripos($this->_str, 'LONGTEXT', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('LONGTEXT'));
			return $this->parseLongtextDataType();
		}
		if(stripos($this->_str, 'MEDIUMBLOB', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('MEDIUMBLOB'));
			return $this->parseMediumblobDataType();
		}
		if(stripos($this->_str, 'MEDIUMINT', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('MEDIUMINT'));
			return $this->parseMediumintDataType();
		}
		if(stripos($this->_str, 'MEDIUMTEXT', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('MEDIUMTEXT'));
			return $this->parseMediumtextDataType();
		}
		if(stripos($this->_str, 'NUMERIC', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('NUMERIC'));
			return $this->parseNumericDataType();
		}
		if(stripos($this->_str, 'REAL', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('REAL'));
			return $this->parseRealDataType();
		}
		if(stripos($this->_str, 'SET', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('SET'));
			return $this->parseSetDataType();
		}
		if(stripos($this->_str, 'SMALLINT', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('SMALLINT'));
			return $this->parseSmallintDataType();
		}
		if(stripos($this->_str, 'TEXT', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('TEXT'));
			return $this->parseTextDataType();
		}
		if(stripos($this->_str, 'TIMESTAMP', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('TIMESTAMP'));
			return $this->parseTimestampDataType();
		}
		else if(stripos($this->_str, 'TIME', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('TIME'));
			return $this->parseTimeDataType();
		}
		if(stripos($this->_str, 'TINYBLOB', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('TINYBLOB'));
			return $this->parseTinyblobDataType();
		}
		if(stripos($this->_str, 'TINYINT', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('TINYINT'));
			return $this->parseTinyintDataType();
		}
		if(stripos($this->_str, 'TINYTEXT', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('TINYTEXT'));
			return $this->parseTinytextDataType();
		}
		if(stripos($this->_str, 'VARBINARY', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('VARBINARY'));
			return $this->parseVarbinaryDataType();
		}
		if(stripos($this->_str, 'VARCHAR', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('VARCHAR'));
			return $this->parseVarcharDataType();
		}
		if(stripos($this->_str, 'YEAR', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('YEAR'));
			return $this->parseYearDataType();
		}
		
		throw new ParseException($this->_str, $start - 10, 20);
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return BigintTypeStatement
	 */
	public function parseBigintDataType()
	{
		// BIGINT[(length)] [UNSIGNED] [ZEROFILL]
		$dt = new BigintTypeStatement();
		
		$val = $this->parseSimpleLengthValue();
		if($val !== null)
			$dt->setLength($val);
		
		$this->skipSpaces();
		
		$dt->setUnsigned($this->parseUnsigned());
		
		$this->skipSpaces();
		
		$dt->setZerofill($this->parseZerofill());
		
		return $dt;
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return BinaryTypeStatement
	 */
	public function parseBinaryDataType()
	{
		// BINARY[(length)]
		$dt = new BinaryTypeStatement();
		
		$val = $this->parseSimpleLengthValue();
		if($val !== null)
			$dt->setLength($val);
		
		return $dt;
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return BitTypeStatement
	 */
	public function parseBitDataType()
	{
		// BIT[(length)]
		$dt = new BitTypeStatement();
		
		$val = $this->parseSimpleLengthValue();
		if($val !== null)
			$dt->setLength($val);
		
		return $dt;
	}
	
	/**
	 * 
	 * @return BlobTypeStatement
	 */
	public function parseBlobDataType()
	{
		// BLOB
		return new BlobTypeStatement();
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return CharTypeStatement
	 */
	public function parseCharDataType()
	{
		// CHAR[(length)] [BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]
		$dt = new CharTypeStatement();
		
		$val = $this->parseSimpleLengthValue();
		if($val !== null)
			$dt->setLength($val);
		
		$this->skipSpaces();
		
		$dt->setBinary($this->parseBinary());
		
		$ch = $this->parseCharacterSet();
		if($ch !== null)
			$dt->setCharset($ch);
		
		$this->skipSpaces();
		
		$col = $this->parseCollation();
		if($col !== null)
			$dt->setCollation($col);
		
		return $dt;
	}
	
	/**
	 * 
	 * @return DatetimeTypeStatement
	 */
	public function parseDatetimeDataType()
	{
		// DATETIME
		return new DatetimeTypeStatement();
	}
	
	/**
	 * 
	 * @return DateTypeStatement
	 */
	public function parseDateDataType()
	{
		// DATE
		return new DateTypeStatement();
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return DecimalTypeStatement
	 */
	public function parseDecimalDataType()
	{
		// DECIMAL[(length[,decimals])] [UNSIGNED] [ZEROFILL]
		$dt = new DecimalTypeStatement();
		
		$vals = $this->parseDoubleLengthValue();
		if($vals[0] !== null)
			$dt->setLength($vals[0]);
		if($vals[1] !== null)
			$dt->setDecimals($vals[1]);
		
		$this->skipSpaces();
		
		$dt->setUnsigned($this->parseUnsigned());
		
		$this->skipSpaces();
		
		$dt->setZerofill($this->parseZerofill());
		
		return $dt;
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return DoubleTypeStatement
	 */
	public function parseDoubleDataType()
	{
		// DOUBLE[(length,decimals)] [UNSIGNED] [ZEROFILL]
		$dt = new DoubleTypeStatement();
		
		$vals = $this->parseDoubleLengthValue();
		if($vals[0] !== null)
			$dt->setLength($vals[0]);
		if($vals[1] !== null)
			$dt->setDecimals($vals[1]);
		
		$this->skipSpaces();
		
		$dt->setUnsigned($this->parseUnsigned());
		
		$this->skipSpaces();
		
		$dt->setZerofill($this->parseZerofill());
		
		return $dt;
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return EnumTypeStatement
	 */
	public function parseEnumDataType()
	{
		// ENUM(value1,value2,value3,...) [CHARACTER SET charset_name] [COLLATE collation_name]
		$dt = new EnumTypeStatement();
		
		$dt->setValues($this->parseCollection());
		
		$this->skipSpaces();
		
		$cs = $this->parseCharacterSet();
		if($cs !== null)
			$dt->setCharset($cs);
		
		$this->skipSpaces();
		
		$cn = $this->parseCollation();
		if($cn !== null)
			$dt->setCollation($cn);
		
		return $dt;
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return FloatTypeStatement
	 */
	public function parseFloatDataType()
	{
		// FLOAT[(length,decimals)] [UNSIGNED] [ZEROFILL]
		$dt = new FloatTypeStatement();
		
		$vals = $this->parseDoubleLengthValue();
		if($vals[0] !== null)
			$dt->setLength($vals[0]);
		if($vals[1] !== null)
			$dt->setDecimals($vals[1]);
		
		$this->skipSpaces();
		
		$dt->setUnsigned($this->parseUnsigned());
		
		$this->skipSpaces();
		
		$dt->setZerofill($this->parseZerofill());
		
		return $dt;
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return IntTypeStatement
	 */
	public function parseIntDataType()
	{
		// INT[(length)] [UNSIGNED] [ZEROFILL]
		$dt = new IntTypeStatement();
		
		$val = $this->parseSimpleLengthValue();
		if($val !== null)
			$dt->setLength($val);
		
		$this->skipSpaces();
		
		$dt->setUnsigned($this->parseUnsigned());
		
		$this->skipSpaces();
		
		$dt->setZerofill($this->parseZerofill());
		
		return $dt;
	}
	
	/**
	 * 
	 * @return LongblobTypeStatement
	 */
	public function parseLongblobDataType()
	{
		// LONGBLOB
		return new LongblobTypeStatement();
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return LongtextTypeStatement
	 */
	public function parseLongtextDataType()
	{
		// LONGTEXT [BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]
		$dt = new LongtextTypeStatement();
		
		$this->skipSpaces();
		
		$dt->setBinary($this->parseBinary());
		
		$this->skipSpaces();
		
		$cs = $this->parseCharacterSet();
		if($cs !== null)
			$dt->setCharset($cs);
		
		$this->skipSpaces();
		
		$col = $this->parseCollation();
		if($col !== null)
			$dt->setCollation($col);
		
		return $dt;
	}
	
	/**
	 * 
	 * @return MediumblobTypeStatement
	 */
	public function parseMediumblobDataType()
	{
		// MEDIUMBLOB
		return new MediumblobTypeStatement();
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return MediumintTypeStatement
	 */
	public function parseMediumintDataType()
	{
		// MEDIUMINT[(length)] [UNSIGNED] [ZEROFILL]
		$dt = new MediumintTypeStatement();
		
		$val = $this->parseSimpleLengthValue();
		if($val !== null)
			$dt->setLength($val);
		
		$this->skipSpaces();
		
		$dt->setUnsigned($this->parseUnsigned());
		
		$this->skipSpaces();
		
		$dt->setZerofill($this->parseZerofill());
		
		return $dt;
	}
	
	/**
	 * @throws ParseException
	 * @return MediumtextTypeStatement
	 */
	public function parseMediumtextDataType()
	{
		// MEDIUMTEXT [BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]
		$dt = new MediumtextTypeStatement();
		
		$this->skipSpaces();
		
		$dt->setBinary($this->parseBinary());
		
		$this->skipSpaces();
		
		$cs = $this->parseCharacterSet();
		if($cs !== null)
			$dt->setCharset($cs);
		
		$this->skipSpaces();
		
		$col = $this->parseCollation();
		if($col !== null)
			$dt->setCollation($col);
		
		return $dt;
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return NumericTypeStatement
	 */
	public function parseNumericDataType()
	{
		// NUMERIC[(length[,decimals])] [UNSIGNED] [ZEROFILL]
		$dt = new NumericTypeStatement();
		
		$vals = $this->parseDoubleLengthValue();
		if($vals[0] !== null)
			$dt->setLength($vals[0]);
		if($vals[1] !== null)
			$dt->setDecimals($vals[1]);
		
		$this->skipSpaces();
		
		$dt->setUnsigned($this->parseUnsigned());
		
		$this->skipSpaces();
		
		$dt->setZerofill($this->parseZerofill());
		
		return $dt;
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return RealTypeStatement
	 */
	public function parseRealDataType()
	{
		// REAL[(length,decimals)] [UNSIGNED] [ZEROFILL]
		$dt = new RealTypeStatement();
		
		$vals = $this->parseDoubleLengthValue();
		if($vals[0] !== null)
			$dt->setLength($vals[0]);
		if($vals[1] !== null)
			$dt->setDecimals($vals[1]);
		
		$this->skipSpaces();
		
		$dt->setUnsigned($this->parseUnsigned());
		
		$this->skipSpaces();
		
		$dt->setZerofill($this->parseZerofill());
		
		return $dt;
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return SetTypeStatement
	 */
	public function parseSetDataType()
	{
		// SET(value1,value2,value3,...) [CHARACTER SET charset_name] [COLLATE collation_name]
		$dt = new SetTypeStatement();
		
		$dt->setValues($this->parseCollection());
		
		$this->skipSpaces();
		
		$cs = $this->parseCharacterSet();
		if($cs !== null)
			$dt->setCharset($cs);
		
		$this->skipSpaces();
		
		$cn = $this->parseCollation();
		if($cn !== null)
			$dt->setCollation($cn);
		
		return $dt;
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return SmallintTypeStatement
	 */
	public function parseSmallintDataType()
	{
		// SMALLINT[(length)] [UNSIGNED] [ZEROFILL]
		$dt = new SmallintTypeStatement();
		
		$val = $this->parseSimpleLengthValue();
		if($val !== null)
			$dt->setLength($val);
		
		$this->skipSpaces();
		
		$dt->setUnsigned($this->parseUnsigned());
		
		$this->skipSpaces();
		
		$dt->setZerofill($this->parseZerofill());
		
		return $dt;
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return TextTypeStatement
	 */
	public function parseTextDataType()
	{
		// TEXT [BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]
		$dt = new TextTypeStatement();
		
		$this->skipSpaces();
		
		$dt->setBinary($this->parseBinary());
		
		$this->skipSpaces();
		
		$cs = $this->parseCharacterSet();
		if($cs !== null)
			$dt->setCharset($cs);
		
		$this->skipSpaces();
		
		$col = $this->parseCollation();
		if($col !== null)
			$dt->setCollation($col);
		
		return $dt;
	}
	
	/**
	 * 
	 * @return TimestampTypeStatement
	 */
	public function parseTimestampDataType()
	{
		// TIMESTAMP
		return new TimestampTypeStatement();
	}
	
	/**
	 * 
	 * @return TimeTypeStatement
	 */
	public function parseTimeDataType()
	{
		// TIME
		return new TimeTypeStatement();
	}
	
	/**
	 * 
	 * @return TinyblobTypeStatement
	 */
	public function parseTinyblobDataType()
	{
		// TINYBLOB
		return new TinyblobTypeStatement();
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return TinyintTypeStatement
	 */
	public function parseTinyintDataType()
	{
		// TINYINT[(length)] [UNSIGNED] [ZEROFILL]
		$dt = new TinyintTypeStatement();
		
		$val = $this->parseSimpleLengthValue();
		if($val !== null)
			$dt->setLength($val);
		
		$this->skipSpaces();
		
		$dt->setUnsigned($this->parseUnsigned());
		
		$this->skipSpaces();
		
		$dt->setZerofill($this->parseZerofill());
		
		return $dt;
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return TinytextTypeStatement
	 */
	public function parseTinytextDataType()
	{
		// TINYTEXT [BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]
		$dt = new TinytextTypeStatement();
		
		$this->skipSpaces();
		
		$dt->setBinary($this->parseBinary());
		
		$this->skipSpaces();
		
		$cs = $this->parseCharacterSet();
		if($cs !== null)
			$dt->setCharset($cs);
		
		$this->skipSpaces();
		
		$col = $this->parseCollation();
		if($col !== null)
			$dt->setCollation($col);
		
		return $dt;
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return VarbinaryTypeStatement
	 */
	public function parseVarbinaryDataType()
	{
		// VARBINARY(length)
		$dt = new VarbinaryTypeStatement();
		
		$val = $this->parseSimpleLengthValue();
		if($val !== null)
			$dt->setLength($val);
		
		return $dt;
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return VarcharTypeStatement
	 */
	public function parseVarcharDataType()
	{
		// VARCHAR(length) [BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]
		$dt = new VarcharTypeStatement();
		
		$val = $this->parseSimpleLengthValue();
		if($val !== null)
			$dt->setLength($val);
		
		$this->skipSpaces();
		
		$dt->setBinary($this->parseBinary());
		
		$ch = $this->parseCharacterSet();
		if($ch !== null)
			$dt->setCharset($ch);
		
		$this->skipSpaces();
		
		$col = $this->parseCollation();
		if($col !== null)
			$dt->setCollation($col);
		
		return $dt;
	}
	
	/**
	 * 
	 * @return YearTypeStatement
	 */
	public function parseYearDataType()
	{
		// YEAR
		return new YearTypeStatement();
	}
	
	
	/**
	 * 
	 * @throws ParseException
	 * @return int
	 */
	public function parseSimpleLengthValue()
	{
		if($this->_chr === '(')
		{
			$this->getChar();
			$next = strpos($this->_str, ')', $this->_pos);
			if($next !== false)
			{
				$sub = substr($this->_str, $this->_pos, $next - $this->_pos);
				if(preg_match('#^\s*(\d+)\s*$#', $sub, $matches))
				{
					$this->getChar($next - $this->_pos);
					return $matches[1];
				}
				else
					throw new ParseException($this->_str, $this->_pos, $next - $this->_pos);
			}
			else
				throw new ParseException($this->_str, $this->_pos, 20);
		}
		throw new ParseException($this->_str, $this->_pos, 20);
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return int[2]
	 */
	public function parseDoubleLengthValue()
	{
		if($this->_chr === '(')
		{
			$next = strpos($this->_str, ')', $this->_pos);
			if($next !== false)
			{
				$sub = substr($this->_str, $this->_pos, $next - $this->_pos);
				if(preg_match('^\s*(\d+)\s*,\s*(\d+)\s*$', $sub, $matches))
				{
					$this->getChar($next - $this->_pos);
					return array($matches[1], $matches[2]);
				}
				else if(preg_match('^\s*(\d+)\s*,?\s*$', $sub, $matches))
				{
					$this->getChar($next - $this->_pos);
					return array($matches[1], null);
				}
				else
					throw new ParseException($this->_str, $this->_pos, $next - $this->_pos);
			}
			else
				throw new ParseException($this->_str, $this->_pos, 20);
		}
		return array(null, null);
	}
	
	/**
	 * 
	 * @return IConstraintStatement[]
	 */
	public function parseColumnConstraints()
	{
		$constraints = array();
		
		// TODO parseColumnConstraints ...
		
		return $constraints;
	}
	
	/**
	 * 
	 * @return ITableOptionsStatement
	 */
	public function parseTableOptions()
	{
		
	}
	
	/**
	 * 
	 * @return IPartitionOptionsStatement
	 */
	public function parsePartitionOptions()
	{
		
	}
	
	/**
	 * 
	 * @throws ParseException
	 * @return string[]
	 */
	public function parseCollection()
	{
		$collec = array();
		if($this->_chr === '(')
		{
			do
			{
				$this->getChar();
				$str = $this->parseString();
				if(!empty($str))
				{
					$collec[] = $str;
				}
				$this->skipSpaces();
			}
			while($this->_chr === ',');
			
			if($this->_chr === ')')
			{
				$this->getChar();
				return $collec;
			}
			
		}
		throw new ParseException($this->_str, $this->_pos - 10, 20);
	}
	
	/**
	 * 
	 * @return boolean
	 */
	public function parseBinary()
	{
		if(stripos($this->_str, 'BINARY', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('BINARY'));
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @return boolean
	 */
	public function parseUnsigned()
	{
		if(stripos($this->_str, 'UNSIGNED', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('UNSIGNED'));
			return true;
		}
		return false;
	}
	/**
	 * 
	 * @return boolean
	 */
	public function parseZerofill()
	{
		if(stripos($this->_str, 'ZEROFILL', $this->_pos) === $this->_pos)
		{
			$this->getChar(strlen('ZEROFILL'));
			return true;
		}
		return false;
	}
	
}

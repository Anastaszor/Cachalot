<?php

/**
 * ParseException class file.
 * 
 * @author Anastaszor
 */
class ParseException extends CachalotException
{
	
	/**
	 * 
	 * @param string $text
	 * @param int $offset
	 * @param int $length
	 */
	public function __construct($text, $offset, $length)
	{
		parent::__construct("Impossible to parse near '".substr($text, $offset, $length)."'.");
	}
	
}

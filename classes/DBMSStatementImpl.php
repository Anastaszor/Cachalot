<?php

/**
 * DBMSStatementImpl class file.
 * 
 * @author Anastaszor
 */
class DBMSStatementImpl extends CachalotObject implements DBMSStatement
{
	
	/**
	 * 
	 * @var IExecutableStatement[]
	 */
	private $_commands = array();
	/**
	 * 
	 * @var IStatementExecutionResult[]
	 */
	private $_results = array();
	
	/**
	 * 
	 * @param IExecutableStatement[] $commands
	 */
	public function setCommands(array $commands)
	{
		$this->_commands = $commands;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see DBMSStatement::bindColumn()
	 */
	public function bindColumn($column, $param)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see DBMSStatement::bindParam()
	 */
	public function bindParam($parameter, $variable)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see DBMSStatement::bindValue()
	 */
	public function bindValue($parameter, $value)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see DBMSStatement::closeCursor()
	 */
	public function closeCursor()
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see DBMSStatement::columnCount()
	 */
	public function columnCount()
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see DBMSStatement::execute()
	 */
	public function execute(array $input_parameters = array())
	{
		// TODO take into account input_params
		// TODO take into accounts bindings
		$success = true;
		foreach($this->_commands as $command)
		{
			$result = $command->execute();
			$success &= $result->isSuccessful();
			$this->_results[] = $result;
		}
		return $success;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see DBMSStatement::fetch()
	 */
	public function fetch($fetch_style, $cursor_orientation = true, $offset = 0)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see DBMSStatement::fetchAll()
	 */
	public function fetchAll(
		$fetch_style = PDO::ATTR_DEFAULT_FETCH_MODE, 
		$fetch_argument = PDO::FETCH_COLUMN, 
		array $ctor_args = array()
	) {
		if($fetch_style === PDO::ATTR_DEFAULT_FETCH_MODE)
			$fetch_style = PDO::FETCH_BOTH;
		
		if($fetch_style & PDO::FETCH_ASSOC)
		{
			$results = array();
			foreach($this->_results as $results_sets)
			{
				$results = array_merge($results, $results_sets->getValues());
			}
			return $results;
		}
		else
			throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see DBMSStatement::fetchColumn()
	 */
	public function fetchColumn($column_number = 0)
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see DBMSStatement::fetchObject()
	 */
	public function fetchObject($class_name = "stdClass", array $ctor_args = array())
	{
		throw new NotImplementedException();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see DBMSStatement::rowCount()
	 */
	public function rowCount()
	{
		$count = 0;
		foreach($this->_results as $result)
		{
			/* @var $result IStatementExecutionResult */
			$count += count($result->getValues());
		}
		return $count;
	}
	
}

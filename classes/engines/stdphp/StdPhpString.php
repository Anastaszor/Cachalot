<?php

/**
 * StdPhpString class file.
 * 
 * @author Anastaszor
 */
class StdPhpString extends CachalotObject implements IStdPhpDataType
{
	
	/**
	 * (non-PHPdoc)
	 * @see IDataType::getPhpType()
	 */
	public function getPhpType()
	{
		return 'string';
	}
	
}

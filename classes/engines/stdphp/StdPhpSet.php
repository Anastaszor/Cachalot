<?php

/**
 * StdPhpSet class file.
 * 
 * @author Anastaszor
 */
class StdPhpSet extends CachalotObject implements IStdPhpDataType
{
	
	/**
	 * 
	 * @var string[]
	 */
	private $_values = array();
	
	public function setValues(array $values)
	{
		$this->_values = $values;
	}
	
	public function getValues()
	{
		return $this->_values;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataType::getPhpType()
	 */
	public function getPhpType()
	{
		return 'array';
	}
	
}

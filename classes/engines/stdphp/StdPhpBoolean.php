<?php

/**
 * StdPhpBoolean class file.
 * 
 * @author Anastaszor
 */
class StdPhpBoolean extends CachalotObject implements IStdPhpDataType
{
	
	/**
	 * (non-PHPdoc)
	 * @see IDataType::getPhpType()
	 */
	public function getPhpType()
	{
		return 'boolean';
	}
	
}

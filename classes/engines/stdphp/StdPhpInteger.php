<?php

/**
 * StdPhpInteger class file.
 * 
 * @author Anastaszor
 */
class StdPhpInteger extends CachalotObject implements IStdPhpDataType
{
	
	/**
	 * (non-PHPdoc)
	 * @see IDataType::getPhpType()
	 */
	public function getPhpType()
	{
		return 'integer';
	}
	
}

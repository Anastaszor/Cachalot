<?php

/**
 * StdPhpDatetime class file.
 * 
 * @author Anastaszor
 */
class StdPhpDatetime extends CachalotObject implements IStdPhpDataType
{
	
	/**
	 * (non-PHPdoc)
	 * @see IDataType::getPhpType()
	 */
	public function getPhpType()
	{
		return 'Datetime';
	}
	
}

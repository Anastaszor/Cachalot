<?php

/**
 * StdPhpFloat class file.
 * 
 * @author Anastaszor
 */
class StdPhpFloat extends CachalotObject implements IStdPhpDataType
{
	
	/**
	 * (non-PHPdoc)
	 * @see IDataType::getPhpType()
	 */
	public function getPhpType()
	{
		return 'float';
	}
	
}

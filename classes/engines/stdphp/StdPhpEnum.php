<?php

/**
 * StdPhpEnum class file.
 * 
 * @author Anastaszor
 */
class StdPhpEnum extends CachalotObject implements IStdPhpDataType
{
	
	/**
	 *
	 * @var string
	 */
	private $_value = array();
	
	public function setValue($value)
	{
		$this->_value = $value;
	}
	
	public function getValue()
	{
		return $this->_value;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IDataType::getPhpType()
	 */
	public function getPhpType()
	{
		return 'string';
	}
	
}

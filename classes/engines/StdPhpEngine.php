<?php

/**
 * StdPhpEngine interface file.
 *
 * @author Anastaszor
 */
class StdPhpEngine extends CachalotObject implements IEngine
{
	
	/**
	 * (non-PHPdoc)
	 * @see IEngine::getName()
	 */
	public function getName()
	{
		return str_replace('Engine', '', get_class($this));
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IEngine::packDatabase()
	 */
	public function packDatabase(IDatabase $database)
	{
		return "<?php return ".var_export($database, true).";\n";
	}
	/**
	 * (non-PHPdoc)
	 * @see IEngine::unpackDatabase()
	 */
	public function unpackDatabase($data)
	{
		return eval($data);
	}
	/**
	 * (non-PHPdoc)
	 * @see IEngine::packTable()
	 */
	public function packTable(ITable $table)
	{
		return "<?php return ".var_export($table, true).";\n";
	}
	/**
	 * (non-PHPdoc)
	 * @see IEngine::unpackTable()
	 */
	public function unpackTable($data)
	{
		return eval($data);
	}
	/**
	 * (non-PHPdoc)
	 * @see IEngine::packRecord()
	 */
	public function packRecord(IRecord $record)
	{
		return "<?php return ".var_export($record, true).";\n";
	}
	/**
	 * (non-PHPdoc)
	 * @see IEngine::unpackRecord()
	 */
	public function unpackRecord($data)
	{
		return eval($data);
	}
	
	/**
	 *
	 * @return int
	 */
	public function getDefaultTableAutoIncrement()
	{
		return 1;
	}
	/**
	 *
	 * @return int
	*/
	public function getDefaultTableAverageRowLength()
	{
		return null;
	}
	/**
	 *
	 * @return boolean
	*/
	public function getDefaultTableChecksum()
	{
		return false;
	}
	/**
	 *
	 * @return string
	*/
	public function getDefaultTableComment()
	{
		return null;
	}
	/**
	 *
	 * @return string
	*/
	public function getDefaultTableConnectionString()
	{
		return null;
	}
	/**
	 *
	 * @return string
	*/
	public function getDefaultTableDataDirectory()
	{
		return null;
	}
	/**
	 *
	 * @return boolean
	*/
	public function getDefaultTableDelayKeyWrite()
	{
		return false;
	}
	/**
	 *
	 * @return string
	*/
	public function getDefaultTableIndexDirectory()
	{
		return null;
	}
	/**
	 *
	 * @return enum('NO', 'FIRST', 'LAST')
	*/
	public function getDefaultTableInsertMethod()
	{
		return ITable::INSERT_METHOD_FIRST;
	}
	/**
	 *
	 * @return int
	*/
	public function getDefaultTableKeyBlockSize()
	{
		return null;
	}
	/**
	 *
	 * @return int
	*/
	public function getDefaultTableMaxRows()
	{
		return null;
	}
	/**
	 *
	 * @return int
	*/
	public function getDefaultTableMinRows()
	{
		return null;
	}
	/**
	 *
	 * @return enum(true, false, 'DEFAULT')
	*/
	public function getDefaultTablePackKeys()
	{
		return false;
	}
	/**
	 *
	 * @return enum('DEFAULT', 'DYNAMIC', 'FIXED', 'COMPRESSED', 'REDUNDANT', 'COMPACT')
	*/
	public function getDefaultTableRowFormat()
	{
		return ITable::ROW_FORMAT_DEFAULT;
	}
	/**
	 *
	 * @return string
	*/
	public function getDefaultTablespace()
	{
		return null;
	}
	/**
	 *
	 * @return enum('DISK', 'MEMORY', 'DEFAULT')
	*/
	public function getDefaultTablespaceStorage()
	{
		return ITable::TABLESPACE_STORAGE_DEFAULT;
	}
	
}

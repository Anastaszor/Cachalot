<?php

/**
 * EngineFactory class file.
 * 
 * @author Anastaszor
 */
class EngineFactory extends CachalotObject implements IEngineFactory
{
	
	private $_names = array(
		'stdphp' => 'StdPhpEngine',
	);
	
	/**
	 * (non-PHPdoc)
	 * @see IEngineFactory::getDefaultEngine()
	 */
	public function getDefaultEngine()
	{
		return new StdPhpEngine();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IEngineFactory::getEngine()
	 */
	public function getEngine($name)
	{
		$sname = strtolower($name);
		if(isset($this->_names[$sname]))
		{
			$classname = $this->_names[$sname];
			return new $classname();
		}
		return null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IEngineFactory::getEngines()
	 */
	public function getEngines()
	{
		$objects = array();
		foreach(array_unique(array_values($this->_names)) as $name)
		{
			$objects[] = new $name();
		}
		return $objects;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IEngineFactory::isEngineAvailable()
	 */
	public function isEngineAvailable($name)
	{
		$sname = strtolower($name);
		return isset($this->_names[$sname]);
	}
	
}

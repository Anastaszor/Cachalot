<?php

/**
 * ICharsetFactory interface file.
 *
 * @author Anastaszor
 */
interface ICharsetFactory
{
	
	/**
	 * 
	 * @return ICharset
	 */
	public function getDefaultCharset();
	/**
	 * 
	 * @param string $name
	 * @return ICharset
	 */
	public function getCharset($name);
	/**
	 * 
	 * @return ICharset[]
	 */
	public function getCharsets();
	/**
	 * 
	 * @param string $name
	 * @return bool
	 */
	public function isCharsetAvailable($name);
	
}

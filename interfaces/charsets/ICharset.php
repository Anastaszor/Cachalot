<?php

/**
 * ICharset interface file.
 * 
 * @author Anastaszor
 */
interface ICharset 
{
	
	/**
	 * 
	 * @return string
	 */
	public function getName();
	
}

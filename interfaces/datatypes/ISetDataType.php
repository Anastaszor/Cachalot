<?php

/**
 * ISetDataType interface file.
 * 
 * @author Anastaszor
 */
interface ISetDataType extends IGenericDataType
{
	
	/**
	 *
	 * @return string[]
	 */
	public function getValues();
	/**
	 *
	 * @return ICharset
	 */
	public function getCharset();
	/**
	 *
	 * @return ICollation
	 */
	public function getCollation();
	
}

<?php

/**
 * ITinytextDataType interface file.
 * 
 * @author Anastaszor
 */
interface ITinytextDataType extends IGenericDataType
{
	
	/**
	 *
	 * @return boolean
	 */
	public function getBinary();
	/**
	 *
	 * @return ICharset
	 */
	public function getCharset();
	/**
	 *
	 * @return ICollation
	 */
	public function getCollation();
	
}

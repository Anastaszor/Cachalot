<?php

/**
 * IMediumtextDataType interface file.
 * 
 * @author Anastaszor
 */
interface IMediumtextDataType extends IGenericDataType
{
	
	/**
	 *
	 * @return boolean
	 */
	public function getBinary();
	/**
	 *
	 * @return ICharset
	 */
	public function getCharset();
	/**
	 *
	 * @return ICollation
	 */
	public function getCollation();
	
}

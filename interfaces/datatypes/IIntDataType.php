<?php

/**
 * IIntDataType interface file.
 * 
 * @author Anastaszor
 */
interface IIntDataType extends IGenericDataType
{
	
	/**
	 *
	 * @return int
	 */
	public function getLength();
	/**
	 *
	 * @return boolean
	*/
	public function getUnsigned();
	/**
	 *
	 * @return boolean
	*/
	public function getZerofill();
	
}

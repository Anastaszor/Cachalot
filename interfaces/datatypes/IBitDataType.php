<?php

/**
 * IBitDataType interface file.
 * 
 * @author Anastaszor
 */
interface IBitDataType extends IGenericDataType
{
	
	/**
	 *
	 * @return int
	 */
	public function getLength();
	
}

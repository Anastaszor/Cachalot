<?php

/**
 * IBinaryDataType interface file.
 * 
 * @author Anastaszor
 */
interface IBinaryDataType extends IGenericDataType
{
	
	/**
	 *
	 * @return int
	 */
	public function getLength();
	
}

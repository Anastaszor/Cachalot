<?php

/**
 * ILongtextDataType interface file.
 * 
 * @author Anastaszor
 */
interface ILongtextDataType extends IGenericDataType
{
	
	/**
	 *
	 * @return boolean
	 */
	public function getBinary();
	/**
	 *
	 * @return ICharset
	 */
	public function getCharset();
	/**
	 *
	 * @return ICollation
	 */
	public function getCollation();
	
}

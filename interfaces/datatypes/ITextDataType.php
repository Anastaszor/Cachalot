<?php

/**
 * ITextDataType interface file.
 * 
 * @author Anastaszor
 */
interface ITextDataType extends IGenericDataType
{
	
	/**
	 *
	 * @return boolean
	 */
	public function getBinary();
	/**
	 *
	 * @return ICharset
	 */
	public function getCharset();
	/**
	 *
	 * @return ICollation
	 */
	public function getCollation();
	
}

<?php

/**
 * IVarbinaryDataType interface file.
 * 
 * @author Anastaszor
 */
interface IVarbinaryDataType extends IGenericDataType
{
	
	/**
	 *
	 * @return int
	 */
	public function getLength();
	
}

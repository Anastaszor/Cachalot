<?php

/**
 * IEnumDataType interface file.
 * 
 * @author Anastaszor
 */
interface IEnumDataType extends IGenericDataType
{
	
	/**
	 *
	 * @return string[]
	 */
	public function getValues();
	/**
	 *
	 * @return ICharset
	*/
	public function getCharset();
	/**
	 *
	 * @return ICollation
	*/
	public function getCollation();
	
}

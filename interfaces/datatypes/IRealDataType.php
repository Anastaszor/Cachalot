<?php

/**
 * IRealDataType interface file.
 * 
 * @author Anastaszor
 */
interface IRealDataType extends IGenericDataType
{
	
	/**
	 *
	 * @return int
	 */
	public function getLength();
	/**
	 *
	 * @return int
	 */
	public function getDecimals();
	/**
	 *
	 * @return boolean
	 */
	public function getUnsigned();
	/**
	 *
	 * @return boolean
	 */
	public function getZerofill();
	
}

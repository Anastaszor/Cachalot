<?php

/**
 * IVarcharDataType interface file.
 * 
 * @author Anastaszor
 */
interface IVarcharDataType extends IGenericDataType
{
	
	/**
	 *
	 * @return int
	 */
	public function getLength();
	/**
	 *
	 * @return boolean
	 */
	public function getBinary();
	/**
	 *
	 * @return ICharset
	 */
	public function getCharset();
	/**
	 *
	 * @return ICollation
	 */
	public function getCollation();
	
}

<?php

/**
 * IMediumintDataType interface file.
 * 
 * @author Anastaszor
 */
interface IMediumintDataType extends IGenericDataType
{
	
	/**
	 *
	 * @return int
	 */
	public function getLength();
	/**
	 *
	 * @return boolean
	*/
	public function getUnsigned();
	/**
	 *
	 * @return boolean
	*/
	public function getZerofill();
	
}

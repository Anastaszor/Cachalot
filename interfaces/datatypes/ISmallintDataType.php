<?php

/**
 * ISmallintDataType interface file.
 * 
 * @author Anastaszor
 */
interface ISmallintDataType extends IGenericDataType
{
	
	/**
	 *
	 * @return int
	 */
	public function getLength();
	/**
	 *
	 * @return boolean
	 */
	public function getUnsigned();
	/**
	 *
	 * @return boolean
	 */
	public function getZerofill();
	
}

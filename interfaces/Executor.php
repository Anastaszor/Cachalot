<?php

/**
 * Executor interface class file.
 *
 * An Executor is a class which will execute sql statement and return the
 * results, if applicable. 
 *
 * @author Anastaszor
 * @see http://dev.mysql.com/doc/refman/5.0/en/sql-syntax.html
 */
interface Executor
{
	// Data Definition Statements
	// http://dev.mysql.com/doc/refman/5.0/en/sql-syntax-data-definition.html
	
	public function alterDatabase(IAlterDatabaseStatement $statement);
	
	public function alterFunction(IAlterFunctionStatement $statement);
	
	public function alterProcedure(IAlterProcedureStatement $statement);
	
	public function alterTable(IAlterTableStatement $statement);
	
	public function alterView(IAlterViewStatement $statement);
	
	public function createDatabase(ICreateDatabaseStatement $statement);
	
	public function createFunction(ICreateFunctionStatement $statement);
	
	public function createIndex(ICreateIndexStatement $statement);
	
	public function createProcedure(ICreateProcedureStatement $statement);
	
	public function createTable(ICreateTableStatement $statement);
	
	public function createTrigger(ICreateTriggerStatement $statement);
	
	public function createView(ICreateViewStatement $statement);
	
	public function dropDatabase(IDropDatabaseStatement $statement);
	
	public function dropFunction(IDropFunctionStatement $statement);
	
	public function dropIndex(IDropIndexStatement $statement);
	
	public function dropProcedure(IDropProcedureStatement $statement);
	
	public function dropTable(IDropTableStatement $statement);
	
	public function dropTrigger(IDropTriggerStatement $statement);
	
	public function dropView(IDropViewStatement $statement);
	
	public function renameTable(IRenameTableStatement $statement);
	
	public function truncateTable(ITruncateTableStatement $statement);
	
	// Data Manipulation Statements
	// http://dev.mysql.com/doc/refman/5.0/en/sql-syntax-data-manipulation.html
	
	public function call(ICallStatement $statement);
	
	public function delete(IDeleteStatement $statement);
	
	public function do_(IDoStatement $statement);
	
	public function handler(IHandlerStatement $statement);
	
	public function insert(IInsertStatement $statement);
	
	public function loadDataInfile(ILoadDataInfileStatement $statement);
	
	public function replace(IReplaceStatement $statement);
	
	public function select(ISelectStatement $statement);
	
	public function update(IUpdateStatement $statement);
	
	// Transactional and Locking Statements
	// http://dev.mysql.com/doc/refman/5.0/en/sql-syntax-transactions.html
	
	public function startTransaction(IStartTransactionStatement $statement);
	
	public function commit_(ICommitStatement $statement);
	
	public function rollback_(IRollbackStatement $statement);
	
	public function savepoint(ISavepointStatement $statement);
	
	public function rollbackToSavepoint(IRollbackToSavepointStatement $statement);
	
	public function releaseSavepoint(IReleaseSavepointStatement $statement);
	
	public function lockTables(ILockTablesStatement $statement);
	
	public function unlockTables(IUnlockTablesStatement $statement);
	
	public function setTransaction(ISetTransactionStatement $statement);
	
	// Prepared Statements
	// http://dev.mysql.com/doc/refman/5.0/en/sql-syntax-prepared-statements.html
	
	public function prepare_(IPrepareStatement $statement);
	
	public function execute(IExecuteStatement $statement);
	
	public function deallocatePrepare(IDeallocatePrepareStatement $statement);
	
	// Database Administration Statements
	// http://dev.mysql.com/doc/refman/5.0/en/sql-syntax-server-administration.html
	
	public function analyseTable(IAnalyseTableStatement $statement);
	
	public function backupTable(IBackupTableStatement $statement);
	
	public function checkTable(ICheckTableStatement $statement);
	
	public function checksumTable(IChecksumTableStatement $statement);
	
	public function optimizeTable(IOptimizeTableStatement $statement);
	
	public function repairTable(IRepairTableStatement $statement);
	
	public function restoreTable(IRestoreTableStatement $statement);
	
	public function set(ISetStatement $statement);
	
	public function showBinaryLogs(IShowBinaryLogsStatement $statement);
	
	public function showBinlogEvents(IShowBinlogEventsStatement $statement);
	
	public function showCharacterSet(IShowCharacterSetStatement $statement);
	
	public function showCollation(IShowCollationStatement $statement);
	
	public function showColumns(IShowColumnsStatement $statement);
	
	public function showCreateDatabase(IShowCreateDatabaseStatement $statement);
	
	public function showCreateFunction(IShowCreateFunctionStatement $statement);
	
	public function showCreateProcedure(IShowCreateProcedureStatement $statement);
	
	public function showCreateTable(IShowCreateTableStatement $statement);
	
	public function showCreateView(IShowCreateViewStatement $statement);
	
	public function showDatabases(IShowDatabasesStatement $statement);
	
	public function showEngine(IShowEngineStatement $statement);
	
	public function showEngines(IShowEnginesStatement $statement);
	
	public function showErrors(IShowErrorsStatement $statement);
	
	public function showFunctionCode(IShowFunctionCodeStatement $statement);
	
	public function showFunctionStatus(IShowFunctionStatusStatement $statement);
	
	public function showGrants(IShowGrantsStatement $statement);
	
	public function showIndex(IShowIndexStatement $statement);
	
	public function showLogs(IShowLogsStatement $statement);
	
	public function showMasterStatus(IShowMasterStatusStatement $statement);
	
	public function showMutexStatus(IShowMutexStatusStatement $statement);
	
	public function showOpenTables(IShowOpenTablesStatement $statement);
	
	public function showPrivileges(IShowPrivilegesStatement $statement);
	
	public function showProcedureCode(IShowProcedureCodeStatement $statement);
	
	public function showProcedureStatus(IShowProcedureStatusStatement $statement);
	
	public function showProcesslist(IShowProcesslistStatement $statement);
	
	public function showProfile(IShowProfileStatement $statement);
	
	public function showProfiles(IShowProfilesStatement $statement);
	
	public function showStatus(IShowStatusStatement $statement);
	
	public function showTableStatus(IShowTableStatusStatement $statement);
	
	public function showTables(IShowTablesStatement $statement);
	
	public function showTriggers(IShowTriggersStatement $statement);
	
	public function showVariables(IShowVariablesStatement $statement);
	
	public function showWarnings(IShowWarningsStatement $statement);
	
	public function cacheIndex(ICacheIndexStatement $statement);
	
	public function flush(IFlushStatement $statement);
	
	public function kill(IKillStatement $statement);
	
	public function loadIndexIntoCache(ILoadIndexIntoCacheStatement $statement);
	
	public function reset(IResetStatement $statement);
	
	// Utility Statements
	// http://dev.mysql.com/doc/refman/5.0/en/sql-syntax-utility.html
	
	public function explain(IExplainStatement $statement);
	
	public function help(IHelpStatement $statement);
	
	public function use_(IUseStatement $statement);
	
}

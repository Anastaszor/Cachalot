<?php

/**
 * IStatementExecutionResult interface file.
 * 
 * @author Anastaszor
 */
interface IStatementExecutionResult
{
	
	/**
	 * 
	 * @return boolean
	 */
	public function isSuccessful();
	/**
	 * 
	 * @return string
	 */
	public function getMessage();
	/**
	 * 
	 * @return IResultIterator
	 */
	public function getResultIterator();
	/**
	 * 
	 * @return string[][]
	 */
	public function getValues();
	
}

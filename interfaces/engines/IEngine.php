<?php

/**
 * IEngine interface file.
 *
 * @author Anastaszor
 */
interface IEngine
{
	/**
	 * 
	 * @return string
	 */
	public function getName();
	
	/**
	 * 
	 * @param IDatabase $database
	 * @return string to write
	 */
	public function packDatabase(IDatabase $database);
	/**
	 * 
	 * @param string $data
	 * @return IDatabase
	 */
	public function unpackDatabase($data);
	/**
	 * 
	 * @param ITable $table
	 * @return int bytes written
	 */
	public function packTable(ITable $table);
	/**
	 * 
	 * @param string $data
	 * @return ITable
	 */
	public function unpackTable($data);
	/**
	 * 
	 * @param IRecord $record
	 * @return int bytes written
	 */
	public function packRecord(IRecord $record);
	/**
	 * 
	 * @param string $data
	 * @return IRecord
	 */
	public function unpackRecord($data);
	
	
	// table options \\
	
	/**
	 * 
	 * @return int
	 */
	public function getDefaultTableAutoIncrement();
	/**
	 * 
	 * @return int
	 */
	public function getDefaultTableAverageRowLength();
	/**
	 * 
	 * @return boolean
	 */
	public function getDefaultTableChecksum();
	/**
	 * 
	 * @return string
	 */
	public function getDefaultTableComment();
	/**
	 * 
	 * @return string
	 */
	public function getDefaultTableConnectionString();
	/**
	 * 
	 * @return string
	 */
	public function getDefaultTableDataDirectory();
	/**
	 * 
	 * @return boolean
	 */
	public function getDefaultTableDelayKeyWrite();
	/**
	 * 
	 * @return string
	 */
	public function getDefaultTableIndexDirectory();
	/**
	 * 
	 * @return enum('NO', 'FIRST', 'LAST')
	 */
	public function getDefaultTableInsertMethod();
	/**
	 * 
	 * @return int
	 */
	public function getDefaultTableKeyBlockSize();
	/**
	 * 
	 * @return int
	 */
	public function getDefaultTableMaxRows();
	/**
	 * 
	 * @return int
	 */
	public function getDefaultTableMinRows();
	/**
	 * 
	 * @return enum(true, false, 'DEFAULT')
	 */
	public function getDefaultTablePackKeys();
	/**
	 * 
	 * @return enum('DEFAULT', 'DYNAMIC', 'FIXED', 'COMPRESSED', 'REDUNDANT', 'COMPACT')
	 */
	public function getDefaultTableRowFormat();
	/**
	 * 
	 * @return string
	 */
	public function getDefaultTablespace();
	/**
	 * 
	 * @return enum('DISK', 'MEMORY', 'DEFAULT')
	 */
	public function getDefaultTablespaceStorage();
	
}

<?php

/**
 * IEngineFactory interface file.
 *
 * @author Anastaszor
 */
interface IEngineFactory
{
	
	/**
	 *
	 * @return IEngine
	 */
	public function getDefaultEngine();
	/**
	 * 
	 * @param IEngine $name
	 */
	public function getEngine($name);
	/**
	 *
	 * @return IEngine[]
	 */
	public function getEngines();
	/**
	 *
	 * @param string $name
	 * @return bool
	 */
	public function isEngineAvailable($name);
	
}

<?php

/**
 * IDataType interface file.
 * 
 * @author Anastaszor
 */
interface IDataType
{
	
	/**
	 * 
	 * @return string
	 */
	public function getPhpType();
	
}

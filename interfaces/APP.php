<?php

/**
 * APP interface file.
 *
 * @author Anastaszor
 */
interface APP
{
	
	/**
	 *
	 * @return string
	 */
	public function getBasePath();
	/**
	 *
	 * @return ICharsetFactory
	 */
	public function getCharsetFactory();
	/**
	 *
	 * @return ICollationFactory
	*/
	public function getCollationFactory();
	/**
	 *
	 * @return IEngineFactory
	*/
	public function getEngineFactory();
	/**
	 * 
	 * @return IDatabaseFactory
	 */
	public function getDatabaseFactory();
	/**
	 * 
	 * @return ITableFactory
	 */
	public function getTableFactory();
	/**
	 * 
	 * @return ISqlParser
	 */
	public function getParser();
	
}

<?php

/**
 * ITranslator interface file.
 *
 * @author Anastaszor
 */
interface ITranslator 
{
	
	public function translate($message, $language);
	
}

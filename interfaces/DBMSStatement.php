<?php

/**
 * DBMSStatement interface class file.
 *
 * DBMSStatement is an interface to define an API. This API is based on the API
 * of PDOStatement for MySQL(same method names, same arguments).
 *
 * @author Anastaszor
 * @see http://php.net/manual/en/class.pdostatement.php
 */
interface DBMSStatement 
{
	
	/**
	 * 
	 * @param string $column
	 * @param string $value
	 */
	public function bindColumn($column, $value);
	/**
	 * 
	 * @param string $parameter
	 * @param string $value
	 */
	public function bindParam($parameter, $value);
	/**
	 * 
	 * @param string $parameter
	 * @param string $value
	 */
	public function bindValue($parameter, $value);
	/**
	 * 
	 * @return bool
	 */
	public function closeCursor();
	/**
	 * 
	 * @return bool
	 */
	public function columnCount();
	/**
	 * 
	 * @param array $input_parameters
	 * @return bool
	 */
	public function execute(array $input_parameters = array());
	/**
	 * 
	 * @param string $fetch_style
	 * @param string $cursor_orientation
	 * @param int $offset
	 * @return string
	 */
	public function fetch($fetch_style, $cursor_orientation = true, $offset = 0);
	/**
	 * 
	 * @param string $fetch_style
	 * @param mixed $fetch_argument
	 * @param array $ctor_args
	 * @return array
	 * @see http://php.net/manual/en/pdostatement.fetchall.php
	 */
	public function fetchAll(
		$fetch_style = PDO::ATTR_DEFAULT_FETCH_MODE, 
		$fetch_argument = PDO::FETCH_COLUMN, 
		array $ctor_args = array()
	);
	/**
	 * 
	 * @param int $column_number
	 * @return array
	 */
	public function fetchColumn($column_number = 0);
	/**
	 * 
	 * @param string $class_name
	 * @param array $ctor_args
	 * @return array
	 */
	public function fetchObject($class_name = "stdClass", array $ctor_args = array());
	/**
	 * 
	 * @return int
	 */
	public function rowCount();
	
}

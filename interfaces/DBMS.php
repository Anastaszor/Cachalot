<?php

/**
 * DBMS interface class file.
 * 
 * DBMS is an interface to define an API. This API is based on the API
 * of PDO for MySQL (same method names, same arguments).
 * 
 * @author Anastaszor
 * @see http://php.net/manual/en/class.pdo.php
 */
interface DBMS
{
	/**
	 * 
	 * @return bool
	 */
	public function beginTransaction();
	/**
	 * 
	 * @return bool
	 */
	public function commit();
	/**
	 * 
	 * @param string $statement
	 * @return int
	 */
	public function exec($statement);
	/**
	 * 
	 * @return bool
	 */
	public function inTransaction();
	/**
	 * 
	 * @param string $name
	 * @return string
	 */
	public function lastInsertId($name = null);
	/**
	 * 
	 * @param string $statement
	 * @return DBMSStatement
	 */
	public function prepare($statement);
	/**
	 * 
	 * @param string $statement
	 * @return DBMSStatement
	 */
	public function query($statement);
	/**
	 * 
	 * @param string $string
	 * @return string
	 */
	public function quote($string);
	/**
	 * 
	 * @return bool
	 */
	public function rollBack();
	
}

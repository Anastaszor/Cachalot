<?php

/**
 * ISqlParser interface file.
 * 
 * This interface defines a parser for sql strings. This parser transforms
 * those strings into an array of statements executables by the engine.
 * 
 * @author Anastaszor
 */
interface ISqlParser
{
	
	/**
	 * Transforms a string into executable statements for the engine.
	 * This does not guarantee the statements are valid to execute, they just
	 * reflect what the user wants to do.
	 * @param IExecutableStatement[] $string
	 */
	public function parse($string);
	
}

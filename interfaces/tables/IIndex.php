<?php

/**
 * IIndex interface file.
 * 
 * @author Anastaszor
 */
interface IIndex
{
	
	/**
	 * 
	 * @return IColumn[]
	 */
	public function getColumns();
	
}

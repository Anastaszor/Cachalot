<?php

/**
 * IColumn interface file.
 * 
 * @author Anastaszor
 */
interface IColumn
{
	
	/**
	 * 
	 * @return string
	 */
	public function getName();
	/**
	 *
	 * @return IGenericDataType
	 */
	public function getGenericDataType();
	/**
	 *
	 * @return boolean
	 */
	public function getNotNull();
	/**
	 *
	 * @return boolean
	 */
	public function getUnique();
	/**
	 *
	 * @return boolean
	 */
	public function getPrimary();
	/**
	 *
	 * @return string
	 */
	public function getComment();
	/**
	 *
	 * @return IConstraint[]
	 */
	public function getConstraints();
	
}

<?php

/**
 * ISimpleColumn interface file.
 * 
 * @author Anastaszor
 */
interface ISimpleColumn extends IColumn
{
	
	const COLUMN_FORMAT_FIXED = 'FIXED';
	const COLUMN_FORMAT_DYNAMIC = 'DYNAMIC';
	const COLUMN_FORMAT_DEFAULT = 'DEFAULT';
	
	/**
	 *
	 * @return boolean
	 */
	public function getAutoIncrement();
	/**
	 *
	 * @return enum('FIXED', 'DYNAMIC', 'DEFAULT')
	*/
	public function getColumnFormat();
	/**
	 *
	 * @return IReference
	*/
	public function getReference();
	
}

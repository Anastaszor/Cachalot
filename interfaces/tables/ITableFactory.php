<?php

/**
 * ITableFactory interface file.
 *
 * @author Anastaszor
 */
interface ITableFactory
{
	
	/**
	 * 
	 * @param IDatabase $database
	 * @param string $tbname
	 * @return ITable
	 */
	public function getTable(IDatabase $database, $tbname);
	
	/**
	 * 
	 * @param IDatabase $database
	 * @return ITable[]
	 */
	public function getTables(IDatabase $database);
	
	/**
	 * Creates a new table.
	 * @param IDatabase $database
	 * @param ICreateTableStatement $statement
	 * @return StatementExecutionResult
	 * @throws CachalotException if the table cannot be created.
	 */
	public function createTable(IDatabase $database, ICreateTableStatement $statement);
	
	/**
	 * Drops a given table.
	 * @param IDatabase $database
	 * @param string $key the key to retrieve the right drop.
	 * @param IDropTableStatement $statement
	 * @return StatementExecutionResult
	 * @throws CachalotException if the table cannot be dropped.
	 */
	public function dropTable(IDatabase $database, $key, IDropTableStatement $statement);
	
	/**
	 * Shows a list of tables.
	 * @param IDatabase $database
	 * @param IShowTablesStatement $statement
	 * @return StatementExecutionResult
	 */
	public function showTables(IDatabase $database, IShowTablesStatement $statement);
	
}

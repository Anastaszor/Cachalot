<?php

/**
 * ITable interface file.
 *
 * @author Anastaszor
 */
interface ITable
{
	
	const CHECKSUM_OFF = 0;
	const CHECKSUM_ON = 1;
	const DELAY_KEY_WRITE_ON = 0;
	const DELAY_KEY_WRITE_OFF = 1;
	const INSERT_METHOD_NO = 'NO';
	const INSERT_METHOD_FIRST = 'FIRST';
	const INSERT_METHOD_LAST = 'LAST';
	const PACK_KEYS_OFF = 0;
	const PACK_KEYS_ON = 1;
	const PACK_KEYS_DEFAULT = 'DEFAULT';
	const ROW_FORMAT_DEFAULT = 'DEFAULT';
	const ROW_FORMAT_DYNAMIC = 'DYNAMIC';
	const ROW_FORMAT_FIXED = 'FIXED';
	const ROW_FORMAT_COMPRESSED = 'COMPRESSED';
	const ROW_FORMAT_REDUNDANT = 'REDUNDANT';
	const ROW_FORMAT_COMPACT =  'COMPACT';
	const TABLESPACE_STORAGE_DISK = 'DISK';
	const TABLESPACE_STORAGE_MEMORY = 'MEMORY';
	const TABLESPACE_STORAGE_DEFAULT = 'DEFAULT';
	
	/**
	 * 
	 * @return ICharset
	 */
	public function getCharset();
	/**
	 * 
	 * @return ICollation
	 */
	public function getCollation();
	/**
	 * 
	 * @return IEngine
	 */
	public function getEngine();
	
	/**
	 *
	 * @return the absolute physical path where this is stored.
	 */
	public function getPath();
	/**
	 *
	 * @return string
	 */
	public function getName();
	/**
	 * 
	 * @return string
	 */
	public function getHash();
	/**
	 * 
	 * @return bool
	 */
	public function getAutoIncrement();
	/**
	 * 
	 * @return int
	 */
	public function getAverageRowLength();
	/**
	 * 
	 * @return boolean
	 */
	public function getChecksum();
	/**
	 * 
	 * @return string
	 */
	public function getComment();
	/**
	 * 
	 * @return string
	 */
	public function getConnectionString();
	/**
	 * 
	 * @return string
	 */
	public function getDataDirectory();
	/**
	 * 
	 * @return boolean
	 */
	public function getDelayKeyWrite();
	/**
	 *
	 * @return string
	 */
	public function getIndexDirectory();
	/**
	 * 
	 * @return enum('NO', 'FIRST', 'LAST')
	 */
	public function getInsertMethod();
	/**
	 * 
	 * @return int
	 */
	public function getKeyBlockSize();
	/**
	 * 
	 * @return int
	 */
	public function getMaxRows();
	/**
	 * 
	 * @return int
	 */
	public function getMinRows();
	/**
	 * 
	 * @return enum(true, false, 'DEFAULT')
	 */
	public function getPackKeys();
	/**
	 * 
	 * @return string
	 */
	public function getPassword();
	/**
	 * 
	 * @return enum('COMPRESSED', 'REDUNDANT', 'COMPACT', 'DEFAULT', 'DYNAMIC', 'FIXED')
	 */
	public function getRowFormat();
	/**
	 * 
	 * @return string
	 */
	public function getTablespace();
	/**
	 * 
	 * @return enum('DISK', 'MEMORY', 'DEFAULT')
	 */
	public function getTablespaceStorage();
	
	/**
	 * 
	 * @return IColumn[]
	 */
	public function getColumns();
	/**
	 * 
	 * @param IColumn $column
	 */
	public function addColumn(IColumn $column);
	
}

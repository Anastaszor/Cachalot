<?php

/**
 * IGenericDataType interface file.
 * 
 * @author Anastaszor
 */
interface IGenericDataType extends IDataType
{
	/**
	 * 
	 * @return IStdPhpDataType
	 */
	public function toStdPhp();
	/**
	 * 
	 * @return IDataTypeStatement
	 */
	public function toStatement();
	
}

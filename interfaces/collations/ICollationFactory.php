<?php

/**
 * ICollationFactory interface file.
 *
 * @author Anastaszor
 */
interface ICollationFactory
{
	/**
	 * 
	 * @return ICollation
	 */
	public function getDefaultCollation();
	/**
	 * 
	 * @param string $name
	 * @return ICollation
	 */
	public function getCollation($name);
	/**
	 * 
	 * @param ICharset $charset
	 * @return ICollation[]
	 */
	public function getCollations(ICharset $charset = null);
	/**
	 * 
	 * @param string $name
	 * @return bool
	 */
	public function isCollationAvailable($name);
	
}

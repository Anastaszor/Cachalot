<?php

/**
 * ICollation interface file.
 * 
 * @author Anastaszor
 */
interface ICollation
{
	
	/**
	 * 
	 * @return string
	 */
	public function getName();
	/**
	 * 
	 * @return ICharset
	 */
	public function getCharset();
	
}

<?php

/**
 * IStatement interface file.
 * 
 * Base interface for every statement class.
 *
 * @author Anastaszor
 */
interface IStatement
{
	
	/**
	 * Verifies if the statement contains enough information to be executed
	 * successfully.
	 * @return boolean
	 */
	public function validate();
	
	/**
	 * Returns a correct sql statement to execute from given DBMS represented
	 * by the dialect argument.
	 * @param IDialect $dialect
	 * @return string
	 */
	public function toSQL(IDialect $dialect);
	
}

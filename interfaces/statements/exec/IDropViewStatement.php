<?php

/**
 * IDropViewStatement interface file.
 * 
 * @author Anastaszor
 */
interface IDropViewStatement extends IExecutableStatement
{
	
	const MODE_RESTRICT = 'RESTRICT';
	const MODE_CASCADE = 'CASCADE';
	
	/**
	 * 
	 * @return boolean
	 */
	public function getIfExists();
	
	/**
	 * 
	 * @return string[]
	 */
	public function getViewNames();
	
	/**
	 * 
	 * @return string
	 */
	public function getMode();
	
}

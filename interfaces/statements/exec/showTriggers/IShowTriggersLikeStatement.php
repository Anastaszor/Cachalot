<?php

/**
 * IShowTriggersLikeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowTriggersLikeStatement extends IShowTriggersStatement
{
	
	/**
	 * 
	 * @return ILikeStatement
	 */
	public function getLikeStatement();
	
}

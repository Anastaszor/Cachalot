<?php

/**
 * IShowTriggersWhereStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowTriggersWhereStatement extends IShowTriggersStatement
{
	
	/**
	 * 
	 * @return IWhereStatement
	 */
	public function getWhereStatement();
	
}

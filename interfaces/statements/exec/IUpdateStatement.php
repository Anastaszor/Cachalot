<?php

/**
 * IUpdateStatement interface file.
 * 
 * @author Anastaszor
 */
interface IUpdateStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return boolean
	 */
	public function getLowPriority();
	
	/**
	 * 
	 * @return boolean
	 */
	public function getIgnore();
	
	/**
	 * 
	 * @return string[]
	 */
	public function getTablesNames();
	
	/**
	 * 
	 * @return ISetColumnStatement
	 */
	public function getSetColumnStatements();
	
	/**
	 * 
	 * @return IWhereStatement
	 */
	public function getWhereStatement();
	
	/**
	 * 
	 * @return IOrderByStatement
	 */
	public function getOrderByStatement();
	
	/**
	 * 
	 * @return ILimitStatement
	 */
	public function getLimitStatement();
	
}

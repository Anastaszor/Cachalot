<?php

/**
 * IShowBinlogEventsStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowBinlogEventsStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getInLogName();
	
	/**
	 * 
	 * @return int
	 */
	public function getFromPos();
	
	/**
	 * 
	 * @return int
	 */
	public function getOffset();
	
	/**
	 * 
	 * @return int
	 */
	public function getLimit();
	
}

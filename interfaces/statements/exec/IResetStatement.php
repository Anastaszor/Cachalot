<?php

/**
 * IResetStatement interface file.
 * 
 * @author Anastaszor
 */
interface IResetStatement extends IExecutableStatement
{
	
	const OPT_MASTER = 'MASTER';
	const OPT_QUERY_CACHE = 'QUERY CACHE';
	const OPT_SLAVE = 'SLAVE';
	
	/**
	 * 
	 * @return enum('MASTER', 'QUERY CACHE', 'SLAVE')[]
	 */
	public function getResetOptions();
	
}

<?php

/**
 * ISetStatement interface file.
 * 
 * @author Anastaszor
 */
interface ISetStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return IVariableAssignmentStatement[]
	 */
	public function getVariableAssignmentStatements();
	
}

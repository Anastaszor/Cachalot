<?php

/**
 * IDropFunctionStatement interface file.
 * 
 * @author Anastaszor
 */
interface IDropFunctionStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return boolean
	 */
	public function getIfExists();
	
	/**
	 * 
	 * @return string
	 */
	public function getFunctionName();
	
}

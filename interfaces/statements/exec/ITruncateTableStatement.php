<?php

/**
 * ITruncateTableStatement interface file.
 * 
 * @author Anastaszor
 */
interface ITruncateTableStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getTableName();
	
}

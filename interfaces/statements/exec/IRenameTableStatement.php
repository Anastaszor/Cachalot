<?php

/**
 * IRenameTableStatement interface file.
 * 
 * @author Anastaszor
 */
interface IRenameTableStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return IRenameMoveStatement[]
	 */
	public function getRenameMoveStatements();
	
}

<?php

/**
 * IShowTableStatusStatement interface file.
 * 
 * Abstract Interface. Implementations are:
 * - IShowTableStatusLikeStatement
 * - IShowTableStatusWhereStatement
 * 
 * @author Anastaszor
 */
interface IShowTableStatusStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getDatabaseName();
	
}

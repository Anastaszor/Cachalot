<?php

/**
 * ICheckTableStatement interface file.
 * 
 * @author Anastaszor
 */
interface ICheckTableStatement extends IExecutableStatement
{
	
	const OPTION_UPGRADE = 'FOR UPGRADE';
	const OPTION_QUICK = 'QUICK';
	const OPTION_FAST = 'FAST';
	const OPTION_MEDIUM = 'MEDIUM';
	const OPTION_EXTENDED = 'EXTENDED';
	const OPTION_CHANGED = 'CHANGED';
	
	/**
	 * 
	 * @return string[]
	 */
	public function getTableNames();
	
	/**
	 * 
	 * @return enum('FOR UPGRADE', 'QUICK', 'FAST', 'MEDIUM', 'EXTENDED', 'CHANGED')[]
	 */
	public function getOptions();
	
}

<?php

/**
 * ISavepointStatement interface file.
 * 
 * @author Anastaszor
 */
interface ISavepointStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getIdentifier();
	
}

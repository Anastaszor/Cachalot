<?php

/**
 * IFlushStatement interface file.
 * 
 * @author Anastaszor
 */
interface IFlushStatement extends IExecutableStatement
{
	
	const MODE_NOBINLOG = 'NO_WRITE_TO_BINLOG';
	const MODE_LOCAL = 'LOCAL';
	
	const OPT_DES_KEY_FILE = 'DES_KEY_FILE';
	const OPT_HOSTS = 'HOSTS';
	const OPT_LOGS = 'LOGS';
	const OPT_MASTER = 'MASTER';
	const OPT_PRIVILEGES = 'PRIVILEGES';
	const OPT_QUERY_CACHE = 'QUERY CACHE';
	const OPT_SLAVE = 'SLAVE';
	const OPT_STATUS = 'STATUS';
	const OPT_TABLES = 'TABLES';
	const OPT_USER_RESOURCES = 'USER_RESOURCES';
	
	/**
	 * 
	 * @return enum('NO_WRITE_TO_BINLOG', 'LOCAl')
	 */
	public function getMode();
	
	/**
	 * 
	 * @return enum('DES_KEY_FILE', HOSTS', 'LOGS', 'MASTER', 'PRIVILEGES',
	 * 		'QUERY CACHE', 'SLAVE', 'STATUS', 'TABLES', 'USER_RESOURCES')[]
	 */
	public function getFlushOptions();
	
}

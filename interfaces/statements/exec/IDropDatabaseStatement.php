<?php

/**
 * IShowDatabasesStatement interface file.
 *
 * @author Anastaszor
 * @see http://dev.mysql.com/doc/refman/5.5/en/drop-database.html
 */
interface IDropDatabaseStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getDatabaseName();
	
	/**
	 * 
	 * @return bool
	 */
	public function getIfExists();
	
}

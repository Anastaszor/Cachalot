<?php

/**
 * IOptimizeTableStatement interface file.
 * 
 * @author Anastaszor
 */
interface IOptimizeTableStatement extends IExecutableStatement
{
	
	const MODE_NOBINLOG = 'NO_WRITE_TO_BINLOG';
	const MODE_LOCAL = 'LOCAL';
	
	/**
	 * 
	 * @return enum('NO_WRITE_TO_BINLOG', 'LOCAL')
	 */
	public function getMode();
	
	/**
	 * 
	 * @return string[]
	 */
	public function getTableNames();
	
}

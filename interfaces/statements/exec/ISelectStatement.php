<?php

/**
 * ISelectStatement interface file.
 * 
 * @author Anastaszor
 */
interface ISelectStatement extends IExecutableStatement
{
	
	const DISTINCTMODE_ALL = 'ALL';
	const DISTINCTMODE_DISTINCT = 'DISTINCT';
	const DISTINCTMODE_DISTINCTROW = 'DISTINCTROW';
	
	const HIGH_PRIORITY = 'HIGH_PRIORITY';
	
	const SQL_SMALL_RESULT = 'SQL_SMALL_RESULT';
	const SQL_BIG_RESULT = 'SQL_BIG_RESULT';
	const SQL_BUFFER_RESULT = 'SQL_BUFFER_RESULT';
	
	const SQL_CACHE = 'SQL_CACHE';
	const SQL_NO_CACHE = 'SQL_NO_CACHE';
	
	const FOR_UPDATE = 'FOR UPDATE';
	const LOCK_IN_SHARE_MODE = 'LOCK IN SHARE MODE';
	
	/**
	 * 
	 * @return enum('ALL', 'DISTINCT', 'DISTINCTROW')
	 */
	public function getDistinctMode();
	
	/**
	 * 
	 * @return enum('HIGH_PRIORITY')
	 */
	public function getPriority();
	
	/**
	 * 
	 * @return enum('SQL_SMALL_RESULT', 'SQL_BIG_RESULT', 'SQL_BUFFER_RESULT')
	 */
	public function getResultMode();
	
	/**
	 * 
	 * @return enum('SQL_CACHE', 'SQL_NO_CACHE')
	 */
	public function getCacheMode();
	
	/**
	 * 
	 * @return ISelectExpressionStatement[]
	 */
	public function getSelectExpressions();
	
	/**
	 * 
	 * @return IFromStatement
	 */
	public function getFromStatement();
	
}

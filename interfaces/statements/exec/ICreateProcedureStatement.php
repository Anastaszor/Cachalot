<?php

/**
 * ICreateProcedureStatement interface file.
 *
 * @author Anastaszor
 */
interface ICreateProcedureStatement extends IExecutableStatement
{
	
	const DEFINER_CU = 'CURRENT_USER';
	
	/**
	 *
	 * @return string
	 */
	public function getDefiner();
	
	/**
	 *
	 * @return string
	*/
	public function getProcedureName();
	
	/**
	 * 
	 * @return IProcedureParamsStatement[]
	 */
	public function getParams();
	
	/**
	 *
	 * @return ICharacteristicStatement[]
	*/
	public function getCharacteristics();
	
	/**
	 *
	 * @return IFunctionRoutineBodystatement
	*/
	public function getRoutineBody();

}
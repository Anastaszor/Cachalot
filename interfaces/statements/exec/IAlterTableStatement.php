<?php

/**
 * IAlterTableStatement interface file.
 * 
 * @author Anastaszor
 */
interface IAlterTableStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return boolean
	 */
	public function getIgnore();
	
	/**
	 * 
	 * @return string
	 */
	public function getTableName();
	
	/**
	 * 
	 * @return ITableSpecificationStatement[]
	 */
	public function getTableSpecifications();
	
}

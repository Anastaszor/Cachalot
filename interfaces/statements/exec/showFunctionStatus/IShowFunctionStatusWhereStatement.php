<?php

/**
 * IShowFunctionStatusWhereStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowFunctionStatusWhereStatement extends IShowFunctionStatusStatement
{
	
	/**
	 * 
	 * @return IWhereStatement
	 */
	public function getWhereStatement();
	
}

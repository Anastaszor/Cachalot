<?php

/**
 * IShowFunctionStatusLikeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowFunctionStatusLikeStatement extends IShowFunctionStatusStatement
{
	
	/**
	 * 
	 * @return ILikeStatement
	 */
	public function getLikeStatement();
	
}

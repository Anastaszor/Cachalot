<?php

/**
 * IShowCollationStatement interface file.
 * 
 * Abstract Interface, Implementations are:
 * - IShowCollationLikeStatement
 * - IShowCollationWhereStatement
 * 
 * @author Anastaszor
 */
interface IShowCollationStatement extends IExecutableStatement
{
	// nothing to do
}

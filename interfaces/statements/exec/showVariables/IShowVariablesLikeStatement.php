<?php

/**
 * IShowVariablesLikeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowVariablesLikeStatement extends IShowVariablesStatement
{
	
	/**
	 * 
	 * @return ILikeStatement
	 */
	public function getLikeStatement();
	
}

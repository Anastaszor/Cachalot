<?php

/**
 * IShowVariablesWhereStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowVariablesWhereStatement extends IShowVariablesStatement
{
	
	/**
	 * 
	 * @return IWhereStatement
	 */
	public function getWhereStatement();
	
}

<?php

/**
 * IShowVariablesStatement interface file.
 * 
 * Abstract Interface. Implementations are:
 * - IShowVariablesLikeStatement
 * - IShowVariablesWhereStatement
 * 
 * @author Anastaszor
 */
interface IShowVariablesStatement extends IExecutableStatement
{
	
	const SCOPE_GLOBAL = 'GLOBAL';
	const SCOPE_SESSION = 'SESSION';
	
	/**
	 * 
	 * @return enum('GLOBAL', 'SESSION')
	 */
	public function getScope();
	
}

<?php

/**
 * IShowProfilesStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowProfilesStatement extends IExecutableStatement
{
	// nothing to do
}

<?php

/**
 * IHelpStatement class file.
 * 
 * The HELP statement returns online information from the MySQL Reference 
 * manual. Its proper operation requires that the help tables in the mysql 
 * database be initialized with help topic information.
 * 
 * The HELP statement searches the help tables for the given search string and 
 * displays the result of the search. The search string is not case sensitive.
 * 
 * The search string can contain the wildcard characters "%" and "_". These 
 * have the same meaning as for pattern-matching operations performed with 
 * the LIKE operator. For example, HELP 'rep%' returns a list of topics that 
 * begin with 'rep'.
 * 
 * @author Anastaszor
 */
interface IHelpStatement extends IExecutableStatement
{
	
	/**
	 * Gets the search string.
	 * 
	 * @return string
	 */
	public function getSearchString();
	
}

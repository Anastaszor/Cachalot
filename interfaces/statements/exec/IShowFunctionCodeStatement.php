<?php

/**
 * IShowFunctionCodeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowFunctionCodeStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getFunctionName();
	
}

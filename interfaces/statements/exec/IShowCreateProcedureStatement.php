<?php

/**
 * IShowCreateProcedureStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowCreateProcedureStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getProcedureName();
	
}
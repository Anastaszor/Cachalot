<?php

/**
 * IExecuteStatement interface file.
 * 
 * @author Anastaszor
 */
interface IExecuteStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getStatementName();
	
	/**
	 * 
	 * @return string
	 */
	public function getStatementVarNames();
	
}

<?php

/**
 * IShowColumnsStatement interface file.
 * 
 * Abstract Interface, Implementations are:
 * - IShowColumnsLikeStatement
 * - IShowColumnsWhereStatement
 * 
 * @author Anastaszor
 */
interface IShowColumnsStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getTableName();
	
	/**
	 * 
	 * @return string
	 */
	public function getDatabaseName();
	
}

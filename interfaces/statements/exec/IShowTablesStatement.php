<?php

/**
 * IShowTablesStatement interface file.
 *
 * @author Anastaszor
 * @see http://dev.mysql.com/doc/refman/5.5/en/show-tables.html
 */
interface IShowTablesStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getDatabaseName();
	/**
	 *
	 * @return IShowLikeStatement
	 */
	public function getShowLikeStatement();
	/**
	 *
	 * @return IWhereStatement
	 */
	public function getWhereStatement();
	
}

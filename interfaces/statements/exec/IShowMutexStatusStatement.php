<?php

/**
 * IShowMutexStatusStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowMutexStatusStatement extends IExecutableStatement
{
	// nothing to do
}

<?php

/**
 * IRestoreTableStatement interface file.
 * 
 * @author Anastaszor
 */
interface IRestoreTableStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string[]
	 */
	public function getTableNames();
	
	/**
	 * 
	 * @return string
	 */
	public function getTargetFilePath();
	
}

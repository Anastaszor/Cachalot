<?php

/**
 * IShowEnginesStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowEnginesStatement extends IExecutableStatement
{
	// nothing to do
}

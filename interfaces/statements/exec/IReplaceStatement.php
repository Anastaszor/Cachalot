<?php

/**
 * IReplaceStatement interface file.
 * 
 * Abstract interface. Implementations are:
 * - IReplaceSelectStatement
 * - IReplaceSetStatement
 * - IReplaceValuesStatement
 * 
 * @author Anastaszor
 */
interface IReplaceStatement extends IExecutableStatement
{
	
	const LOW_PRIORITY = 'LOW_PRIORITY';
	const DELAYED = 'DELAYED';
	
	/**
	 * 
	 * @return enum('LOW_PRIORITY', 'DELAYED')
	 */
	public function getPriority();
	
	/**
	 *
	 * @return string
	 */
	public function getTableName();
	
	/**
	 *
	 * @return string[]
	*/
	public function getFieldsNames();
	
}

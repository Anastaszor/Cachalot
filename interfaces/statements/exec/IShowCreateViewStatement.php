<?php

/**
 * IShowCreateViewStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowCreateViewStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getViewName();
	
}

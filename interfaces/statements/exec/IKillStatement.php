<?php

/**
 * IKillStatement interface file.
 * 
 * @author Anastaszor
 */
interface IKillStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return int
	 */
	public function getProcessId();
	
}

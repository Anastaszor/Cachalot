<?php

/**
 * IShowLogsStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowLogsStatement extends IExecutableStatement
{
	// nothing to do
}

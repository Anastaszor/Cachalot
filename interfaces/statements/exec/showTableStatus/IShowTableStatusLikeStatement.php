<?php

/**
 * IShowTableStatusLikeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowTableStatusLikeStatement extends IShowTableStatusStatement
{
	
	/**
	 * 
	 * @return ILikeStatement
	 */
	public function getLikeStatement();
	
}

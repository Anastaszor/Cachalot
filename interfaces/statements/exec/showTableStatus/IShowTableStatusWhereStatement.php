<?php

/**
 * IShowTableStatusWhereStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowTableStatusWhereStatement extends IShowTableStatusStatement
{
	
	/**
	 * 
	 * @return IWhereStatement
	 */
	public function getWhereStatement();
	
}

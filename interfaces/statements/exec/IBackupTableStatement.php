<?php

/**
 * IBackupTableStatement interface file.
 * 
 * @author Anastaszor
 */
interface IBackupTableStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string[]
	 */
	public function getTableNames();
	
	/**
	 * 
	 * @return string
	 */
	public function getTargetFilePath();
	
}

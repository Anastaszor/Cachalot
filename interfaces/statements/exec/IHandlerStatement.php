<?php

/**
 * IHandlerStatement interface file.
 * 
 * Abstract interface. Implementations are:
 * - IHandlerOpenStatement
 * - IHandlerReadStatement
 * - IHandlerCloseStatement
 * 
 * @author Anastaszor
 */
interface IHandlerStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getTableName();
	
	/**
	 * 
	 * @return string
	 */
	public function getTableAlias();
	
}

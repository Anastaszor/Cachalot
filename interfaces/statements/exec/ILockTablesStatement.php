<?php

/**
 * ILockTablesStatement interface file.
 * 
 * @author Anastaszor
 */
interface ILockTablesStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return ILockUnitTableStatement[]
	 */
	public function getLockUnitTableStatements();
	
}

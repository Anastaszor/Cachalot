<?php

/**
 * IAnalyseTableStatement interface file.
 * 
 * @author Anastaszor
 */
interface IAnalyseTableStatement extends IExecutableStatement
{
	
	const MODE_LOCAL = 'LOCAL';
	const MODE_NOBINLOG = 'NO WRITE TO BINLOG';
	
	/**
	 * 
	 * @return enum('LOCAL', 'NO WRITE TO BINLOG')
	 */
	public function getMode();
	
	/**
	 * 
	 * @return string[]
	 */
	public function getTableNames();
	
}

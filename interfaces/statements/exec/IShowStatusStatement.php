<?php

/**
 * IShowStatusStatement interface file.
 * 
 * Abstract Interface. Implementations are:
 * - IShowStatusLikeStatement
 * - IShowStatusWhereStatement
 * 
 * @author Anastaszor
 */
interface IShowStatusStatement extends IExecutableStatement
{
	
	const SCOPE_GLOBAL = 'GLOBAL';
	const SCOPE_SESSION = 'SESSION';
	
	/**
	 * 
	 * @return enum('GLOBAL', 'SESSION')
	 */
	public function getScope();
	
}

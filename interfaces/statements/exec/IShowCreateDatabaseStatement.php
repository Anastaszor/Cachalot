<?php

/**
 * IShowCreateDatabaseStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowCreateDatabaseStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return boolean
	 */
	public function getIfNotExists();
	
	/**
	 * 
	 * @return string
	 */
	public function getDatabaseName();
	
}

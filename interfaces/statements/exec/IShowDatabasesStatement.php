<?php

/**
 * IShowDatabasesStatement interface file.
 *
 * @author Anastaszor
 * @see http://dev.mysql.com/doc/refman/5.5/en/show-databases.html
 */
interface IShowDatabasesStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return IShowLikeStatement
	 */
	public function getShowLikeStatement();
	/**
	 * 
	 * @return IWhereStatement
	 */
	public function getWhereStatement();
	
}

<?php

/**
 * IAlterFunctionStatement interface file.
 * 
 * @author Anastaszor
 */
interface IAlterFunctionStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getFunctionName();
	
	/**
	 * 
	 * @return ICharacteristicStatement[]
	 */
	public function getCharacteristics();
	
}

<?php

/**
 * ICreateFunctionStatement interface file.
 * 
 * @author Anastaszor
 */
interface ICreateFunctionStatement extends IExecutableStatement
{
	
	const DEFINER_CU = 'CURRENT_USER';
	
	/**
	 * 
	 * @return string
	 */
	public function getDefiner();
	
	/**
	 * 
	 * @return string
	 */
	public function getFunctionName();
	
	/**
	 * 
	 * @return IFunctionParamStatement[]
	 */
	public function getParams();
	
	/**
	 * 
	 * @return IDataTypeStatement
	 */
	public function getReturnType();
	
	/**
	 * 
	 * @return ICharacteristicStatement[]
	 */
	public function getCharacteristics();
	
	/**
	 * 
	 * @return IRoutineBodystatement
	 */
	public function getRoutineBody();
	
}

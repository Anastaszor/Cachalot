<?php

/**
 * IAlterViewStatement interface file.
 * 
 * @author Anastaszor
 */
interface IAlterViewStatement extends IExecutableStatement
{
	
	const ALGO_UNDEFINED = 'UNDEFINED';
	const ALGO_MERGE = 'MERGE';
	const ALGO_TEMPTABLE = 'TEMPTABLE';
	
	const DEFINER_CU = 'CURRENT_USER';
	
	const SEC_DEFINER = 'DEFINER';
	const SEC_INVOKER = 'INVOKER';
	
	const CHK_OPT_CASCADED = 'CASCADED';
	const CHK_OPT_LOCAL = 'LOCAL';
	
	/**
	 * 
	 * @return enum('UNDEFINED', 'MERGE', 'TEMPTABLE')
	 */
	public function getAlgorithm();
	
	/**
	 * 
	 * @return string
	 */
	public function getDefiner();
	
	/**
	 * 
	 * @return enum('DEFINER', 'INVOKER')
	 */
	public function getSecurity();
	
	/**
	 * 
	 * @return string
	 */
	public function getViewName();
	
	/**
	 * 
	 * @return string[]
	 */
	public function getColumnList();
	
	/**
	 * 
	 * @return ISelectStatement
	 */
	public function getSelectStatement();
	
	/**
	 * 
	 * @return boolean
	 */
	public function getCheckOption();
	
	/**
	 * 
	 * @return enum('CASCADED', 'LOCAL')
	 */
	public function getCheckOptionMode();
	
}
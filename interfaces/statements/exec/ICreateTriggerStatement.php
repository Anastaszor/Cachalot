<?php

/**
 * ICreateTriggerStatement interface file.
 * 
 * @author Anastaszor
 */
interface ICreateTriggerStatement extends IExecutableStatement
{
	
	const DEFINER_CU = 'CURRENT_USER';
	
	const TIME_BEFORE = 'BEFORE';
	const TIME_AFTER = 'AFTER';
	
	const EVENT_INSERT = 'INSERT';
	const EVENT_UPDATE = 'UPDATE';
	const EVENT_DELETE = 'DELETE';
	
	/**
	 * 
	 * @return string
	 */
	public function getDefiner();
	
	/**
	 * 
	 * @return string
	 */
	public function getTriggerName();
	
	/**
	 * 
	 * @return enum('BEFORE', 'AFTER')
	 */
	public function getTriggerTime();
	
	/**
	 * 
	 * @return enum('INSERT', 'UPDATE', 'DELETE')
	 */
	public function getTriggerEvent();
	
	/**
	 * 
	 * @return string
	 */
	public function getTargetTableName();
	
	/**
	 * 
	 * @return ITriggerBodyStatement
	 */
	public function getTriggerBody();
	
}

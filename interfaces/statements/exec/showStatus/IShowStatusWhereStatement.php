<?php

/**
 * IShowStatusWhereStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowStatusWhereStatement extends IShowStatusStatement
{
	
	/**
	 * 
	 * @return IWhereStatement
	 */
	public function getWhereStatement();
	
}

<?php

/**
 * IShowStatusLikeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowStatusLikeStatement extends IShowStatusStatement
{
	
	/**
	 * 
	 * @return ILikeStatement
	 */
	public function getLikeStatement();
	
}

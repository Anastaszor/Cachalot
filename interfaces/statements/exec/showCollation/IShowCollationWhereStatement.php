<?php

/**
 * IShowCollationWhereStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowCollationWhereStatement extends IShowCollationStatement
{
	
	/**
	 * 
	 * @return IWhereStatement
	 */
	public function getWhereStatement();
	
}

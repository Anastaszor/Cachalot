<?php

/**
 * IShowCollationLikeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowCollationLikeStatement extends IShowCollationStatement
{
	
	/**
	 * 
	 * @return ILikeStatement
	 */
	public function getLikeStatement();
	
}

<?php

/**
 * IInsertSetStatement interface file.
 * 
 * @author Anastaszor
 */
interface IInsertSetStatement extends IInsertStatement
{
	
	/**
	 * 
	 * @return IInsertSetColumnExpressionStatement[]
	 */
	public function getColumnExpressions();
	
}

<?php

/**
 * IInsertSelectStatement interface file.
 * 
 * @author Anastaszor
 */
interface IInsertSelectStatement extends IInsertStatement
{
	
	/**
	 * 
	 * @return ISelectStatement
	 */
	public function getSelectStatement();
	
}

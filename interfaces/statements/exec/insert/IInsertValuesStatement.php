<?php

/**
 * IInsertValuesStatement interface file.
 * 
 * @author Anastaszor
 */
interface IInsertValuesStatement extends IInsertStatement
{
	
	/**
	 * 
	 * @return array(mixed)
	 */
	public function getRows();
	
}

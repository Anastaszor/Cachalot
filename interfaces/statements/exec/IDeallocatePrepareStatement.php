<?php

/**
 * IDeallocatePrepareStatement interface file.
 * 
 * @author Anastaszor
 */
interface IDeallocatePrepareStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getStatementName();
	
}

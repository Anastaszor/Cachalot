<?php

/**
 * IRollbackStatement class file.
 * 
 * @author Anastaszor
 */
interface IRollbackStatement extends IExecutableStatement
{
	
	/**
	 *
	 * @return null|boolean
	 */
	public function getChain();
	
	/**
	 *
	 * @return null|boolean
	*/
	public function getRelease();
	
}

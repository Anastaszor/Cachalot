<?php

/**
 * IDeleteStatement interface file.
 * 
 * @author Anastaszor
 */
interface IDeleteStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return boolean
	 */
	public function getLowPriority();
	
	/**
	 * 
	 * @return boolean
	 */
	public function getQuick();
	
	/**
	 * 
	 * @return boolean
	 */
	public function getIgnore();
	
	/**
	 * 
	 * @return string[]
	 */
	public function getTableNames();
	
	/**
	 * 
	 * @return IWhereStatement
	 */
	public function getWhereStatement();
	
	/**
	 * 
	 * @return IOrderByStatement
	 */
	public function getOrderbyStatement();
	
	/**
	 * 
	 * @return ILimitStatement
	 */
	public function getLimitStatement();
	
}

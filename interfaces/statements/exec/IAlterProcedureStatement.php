<?php

/**
 * IAlterProcedureStatement
 * 
 * @author Anastaszor
 */
interface IAlterProcedureStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getProcedureName();
	
	/**
	 * 
	 * @return ICharacteristicStatement[]
	 */
	public function getCharacteristics();
	
}

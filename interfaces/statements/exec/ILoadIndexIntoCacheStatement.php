<?php

/**
 * ILoadIndexIntoCacheStatement interface file.
 * 
 * @author Anastaszor
 */
interface ILoadIndexIntoCacheStatement extends IExecutableStatement
{
	
	/**
	 *
	 * @return ICacheTableIndexListStatement[]
	 */
	public function getTableIndexListStatements();
	
}

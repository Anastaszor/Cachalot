<?php

/**
 * IShowProcedureStatusWhereStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowProcedureStatusWhereStatement extends IShowProcedureStatusStatement
{
	
	/**
	 * 
	 * @return IWhereStatement
	 */
	public function getWhereStatement();
	
}

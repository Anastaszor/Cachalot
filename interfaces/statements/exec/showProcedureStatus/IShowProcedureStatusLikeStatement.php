<?php

/**
 * IShowProcedureStatusLikeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowProcedureStatusLikeStatement extends IShowProcedureStatusStatement
{
	
	/**
	 * 
	 * @return ILikeStatement
	 */
	public function getLikeStatement();
	
}

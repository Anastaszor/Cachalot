<?php

/**
 * IShowProfileStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowProfileStatement extends IExecutableStatement
{
	
	const TYPE_ALL = 'ALL';
	const TYPE_BLOCKIO = 'BLOCK IO';
	const TYPE_CTXSWH = 'CONTEXT SWITCHES';
	const TYPE_CPU = 'CPU';
	const TYPE_IPC = 'IPC';
	const TYPE_MEMORY = 'MEMORY';
	const TYPE_PFAULTS = 'PAGE FAULTS';
	const TYPE_SOURCE = 'SOURCE';
	const TYPE_SWAPS = 'SWAPS';
	
	/**
	 * 
	 * @return enum('ALL', 'BLOCK IO', 'CONTEXT SWITCHES', 'CPU', 'IPC', 
	 * 		'MEMORY', 'PAGE FAULTS', 'SOURCE', 'SWAPS')[]
	 */
	public function getTypes();
	
	/**
	 * 
	 * @return int
	 */
	public function getQueryNumber();
	
	/**
	 * 
	 * @return int
	 */
	public function getOffset();
	
	/**
	 * 
	 * @return int
	 */
	public function getLimit();
	
}

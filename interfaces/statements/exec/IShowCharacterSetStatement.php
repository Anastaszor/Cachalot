<?php

/**
 * IShowCharacterSetStatement interface file.
 * 
 * Abstract Interface, implementations are:
 * - IShowCharacterSetLikeStatement
 * - IShowCharacterSetWhereStatement
 * 
 * @author Anastaszor
 */
interface IShowCharacterSetStatement extends IExecutableStatement
{
	// nothing to do
}

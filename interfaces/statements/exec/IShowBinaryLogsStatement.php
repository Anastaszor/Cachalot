<?php

/**
 * IShowBinaryLogsStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowBinaryLogsStatement extends IExecutableStatement
{
	// nothing to do
}
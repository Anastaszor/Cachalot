<?php

/**
 * IShowWarningsStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowWarningsStatement extends IExecutableStatement
{
	
	/**
	 *
	 * @return boolean
	 */
	public function getCount();
	
	/**
	 *
	 * @return int
	*/
	public function getOffset();
	
	/**
	 *
	 * @return int
	*/
	public function getLimit();
	
}

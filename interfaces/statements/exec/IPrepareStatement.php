<?php

/**
 * IPrepareStatement interface file.
 * 
 * @author Anastaszor
 */
interface IPrepareStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getStatementName();
	
	/**
	 * 
	 * @return IExecutableStatement
	 */
	public function getStatement();
	
}
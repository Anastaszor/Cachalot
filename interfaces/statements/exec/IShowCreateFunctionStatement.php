<?php

/**
 * IShowCreateFunctionStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowCreateFunctionStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getFunctionName();
	
}

<?php

/**
 * IReleaseSavepointStatement interface file.
 * 
 * @author Anastaszor
 */
interface IReleaseSavepointStatement extends IExecutableStatement
{
	
	/**
	 *
	 * @return string
	 */
	public function getIdentifier();
	
}

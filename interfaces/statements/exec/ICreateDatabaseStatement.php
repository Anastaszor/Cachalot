<?php

/**
 * ICreateDatabaseStatement interface file.
 *
 * @author Anastaszor
 */
interface ICreateDatabaseStatement extends IExecutableStatement
{
	
	/**
	 * Returns true if the IF NOT EXISTS clause is present
	 * 
	 * @return boolean
	 */
	public function getIfNotExists();
	
	/**
	 * Gets the name of the database (not escaped)
	 * 
	 * @return string
	 */
	public function getDatabaseName();
	
	/**
	 * Gets the specifications for the creation of the database.
	 * 
	 * @return IDatabaseSpecificationStatement[]
	 */
	public function getSpecifications();
	
}

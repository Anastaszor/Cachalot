<?php

/**
 * IShowPrivilegesStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowPrivilegesStatement extends IExecutableStatement
{
	// nothing to do
}

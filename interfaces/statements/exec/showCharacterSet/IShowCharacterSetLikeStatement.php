<?php

/**
 * IShowCharacterSetLikeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowCharacterSetLikeStatement extends IShowCharacterSetStatement
{
	
	/**
	 * 
	 * @return ILikeStatement
	 */
	public function getLikeStatement();
	
}

<?php

/**
 * IShowCharacterSetWhereStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowCharacterSetWhereStatement extends IShowCharacterSetStatement
{
	
	/**
	 * 
	 * @return IWhereStatement
	 */
	public function getWhereStatement();
	
}

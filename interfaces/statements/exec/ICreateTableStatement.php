<?php

/**
 * ICreateTableStatement class file.
 *
 * @author Anastaszor
 * @see http://dev.mysql.com/doc/refman/5.5/en/create-database.html
 */
interface ICreateTableStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return boolean
	 */
	public function getTemporary();
	
	/**
	 * 
	 * @return boolean
	 */
	public function getIfNotExists();
	/**
	 * 
	 * @return string
	 */
	public function getDatabaseName();
	/**
	 * 
	 * @return string
	 */
	public function getTableName();
	
	/**
	 * 
	 * @return ICreateDefinitionStatement[]
	 */
	public function getCreateDefinitionStatements();
	
	/**
	 * 
	 * @return ITableOptionsStatement
	 */
	public function getTableOptionsStatement();
	
	/**
	 * 
	 * @return IPartitionOptionsStatement
	 */
	public function getPartitionOptionsStatement();
	
	/**
	 * 
	 * @return ISelectStatement
	 */
	public function getSelectOptionStatement();
	
	/**
	 * 
	 * @return ITableLikeStatement
	 */
	public function getLikeStatement();
	
}

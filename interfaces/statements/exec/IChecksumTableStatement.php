<?php

/**
 * IChecksumTableStatement interface file.
 * 
 * @author Anastaszor
 */
interface IChecksumTableStatement extends IExecutableStatement
{
	
	const OPTION_QUICK = 'QUICK';
	const OPTION_EXTENDED = 'EXTENDED';
	
	/**
	 * 
	 * @return string[]
	 */
	public function getTableNames();
	
	/**
	 * 
	 * @return enum('QUICK', 'EXTENDED')
	 */
	public function getOption();
	
}

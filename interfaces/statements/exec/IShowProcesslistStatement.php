<?php

/**
 * IShowProcesslistStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowProcesslistStatement extends IExecutableStatement
{
	// nothing to do
}

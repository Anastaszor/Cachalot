<?php

/**
 * ICommitStatement interface file.
 * 
 * @author Anastaszor
 */
interface ICommitStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return null|boolean
	 */
	public function getChain();
	
	/**
	 * 
	 * @return null|boolean
	 */
	public function getRelease();
	
}

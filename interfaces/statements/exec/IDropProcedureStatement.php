<?php

/**
 * IDropProcedureStatement interface file.
 * 
 * @author Anastaszor
 */
interface IDropProcedureStatement extends IExecutableStatement
{
	
	/**
	 *
	 * @return boolean
	 */
	public function getIfExists();
	
	/**
	 *
	 * @return string
	*/
	public function getProcedureName();
	
}

<?php

/**
 * IShowCreateTableStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowCreateTableStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getTableName();
	
}

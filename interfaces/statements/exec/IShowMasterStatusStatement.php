<?php

/**
 * IShowMasterStatusStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowMasterStatusStatement extends IExecutableStatement
{
	// nothing to do
}

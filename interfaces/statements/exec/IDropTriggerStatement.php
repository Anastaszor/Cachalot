<?php

/**
 * IDropTriggerStatement interface file.
 * 
 * @author Anastaszor
 */
interface IDropTriggerStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return boolean
	 */
	public function getIfExists();
	
	/**
	 * 
	 * @return boolean
	 */
	public function getTriggerName();
	
}

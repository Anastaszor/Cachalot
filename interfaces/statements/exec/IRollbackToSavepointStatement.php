<?php

/**
 * IRollbackToSavepointStatement interface file.
 * 
 * @author Anastaszor
 */
interface IRollbackToSavepointStatement extends IExecutableStatement
{
	
	/**
	 *
	 * @return string
	 */
	public function getIdentifier();
	
}

<?php

/**
 * IUseStatement interface file.
 * 
 * The USE db_name statement tells the DBMS to use the db_name database as the 
 * default (current) database for subsequent statements. The database remains 
 * the default until the end of the session or another USE statement is issued.
 * Making a particular database the default by means of the USE statement does 
 * not preclude you from accessing tables in other databases by using 
 * qualified table names.
 * 
 * @author Anastaszor
 */
interface IUseStatement extends IExecutableStatement
{
	
	/**
	 * Gets the name of the database which should be used.
	 * 
	 * @return string
	 */
	public function getDatabaseName();
	
}

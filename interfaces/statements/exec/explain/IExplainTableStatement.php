<?php

/**
 * IExplainTableStatement interface file.
 * 
 * @author Anastaszor
 */
interface IExplainTableStatement extends IExplainStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getTableName();
	
	/**
	 * 
	 * @return string
	 */
	public function getColumnPattern();
	
}

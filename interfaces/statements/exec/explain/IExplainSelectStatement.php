<?php

/**
 * IExplainSelectStatement interface file.
 * 
 * @author Anastaszor
 */
interface IExplainSelectStatement extends IExplainStatement
{
	
	/**
	 * 
	 * @return ISelectStatement
	 */
	public function getSelectStatement();
	
}

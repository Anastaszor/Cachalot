<?php

/**
 * ICreateIndexStatement interface file.
 * 
 * @author Anastaszor
 */
interface ICreateIndexStatement extends IExecutableStatement
{
	
	const UNIQUE = 'UNIQUE';
	const FULLTEXT = 'FULLTEXT';
	const SPATIAL = 'SPATIAL';
	
	const BTREE = 'BTREE';
	const HASH = 'HASH';
	
	/**
	 * 
	 * @return enum('UNIQUE', 'FULLTEXT', 'SPATIAL')
	 */
	public function getIndexSpace();
	
	/**
	 * 
	 * @return string
	 */
	public function getIndexName();
	
	/**
	 * 
	 * @return enum('BTREE', 'HASH')
	 */
	public function getIndexType();
	
	/**
	 * 
	 * @return string
	 */
	public function getTableName();
	
	/**
	 * 
	 * @return IColumnIndexStatement[]
	 */
	public function getColumnIndexStatements();
	
}

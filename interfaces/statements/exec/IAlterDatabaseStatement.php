<?php

/**
 * IAlterDatabaseStatement interface file.
 * 
 * @author Anastaszor
 */
interface IAlterDatabaseStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getDatabaseName();
	
	/**
	 * 
	 * @return IDatabaseSpecificationStatement
	 */
	public function getSpecification();
	
}

<?php

/**
 * IDropTableStatement interface file.
 *
 * @author Anastaszor
 * @see http://dev.mysql.com/doc/refman/5.5/en/drop-table.html
 */
interface IDropTableStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return bool
	 */
	public function getTemporary();
	
	/**
	 *
	 * @return array('database' => string, 'table' => string)
	 */
	public function getTableNames();
	/**
	 * Gets the first table name from the statement, and drops it from the
	 * current list of table names to drop.
	 * @return string
	 */
	public function getFirstTableName();
	
	/**
	 *
	 * @return bool
	 */
	public function getIfExists();
	
}

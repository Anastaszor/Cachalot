<?php

/**
 * IShowColumnsWhereStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowColumnsWhereStatement extends IShowColumnsStatement
{
	
	/**
	 * 
	 * @return IWhereStatement
	 */
	public function getWhereStatement();
	
}

<?php

/**
 * IShowColumnsLikeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowColumnsLikeStatement extends IShowColumnsStatement
{
	
	/**
	 * 
	 * @return ILikeStatement
	 */
	public function getLikeStatement();
	
}

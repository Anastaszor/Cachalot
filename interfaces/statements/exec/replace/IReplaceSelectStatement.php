<?php

/**
 * IReplaceSelectStatement interface file.
 * 
 * @author Anastaszor
 */
interface IReplaceSelectStatement extends IReplaceStatement
{
	
	/**
	 *
	 * @return ISelectStatement
	 */
	public function getSelectStatement();
	
}

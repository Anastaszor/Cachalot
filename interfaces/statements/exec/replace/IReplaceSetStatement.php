<?php

/**
 * IReplaceSetStatement interface file.
 * 
 * @author Anastaszor
 */
interface IReplaceSetStatement extends IReplaceStatement
{
	
	/**
	 *
	 * @return IInsertSetColumnExpressionStatement[]
	 */
	public function getColumnExpressions();
	
}

<?php

/**
 * IReplaceValuesStatement interface file.
 * 
 * @author Anastaszor
 */
interface IReplaceValuesStatement extends IReplaceStatement
{
	
	/**
	 *
	 * @return array(mixed)
	 */
	public function getRows();
	
}

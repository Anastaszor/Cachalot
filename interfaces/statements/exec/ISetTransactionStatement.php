<?php

interface ISetTransactionStatement extends IExecutableStatement
{
	
	const SCOPE_GLOBAL = 'GLOBAL';
	const SCOPE_SESSION = 'SESSION';
	
	const LEVEL_REPETABLE_READ = 'REPETABLE READ';
	const LEVEL_READ_COMMITED = 'READ COMMITED';
	const LEVEL_READ_UNCOMMITED = 'READ UNCOMMITED';
	const LEVEL_SERIALIZABLE = 'SERIALIZABLE';
	
	/**
	 * 
	 * @return enum('GLOBAL', 'SESSION)
	 */
	public function getScope();
	
	/**
	 * 
	 * @return enum('REPETABLE READ', 'READ COMMITED', 'READ UNCOMMITED', 'SERIALIZABLE')
	 */
	public function getLevel();
	
}

<?php

/**
 * IShowOpenTablesStatement interface file.
 * 
 * Abstract Interface. Implementations are:
 * - IShowOpenTablesLikeStatement
 * - IShowOpenTablesWhereStatement
 * 
 * @author Anastaszor
 */
interface IShowOpenTablesStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getDatabaseName();
	
}

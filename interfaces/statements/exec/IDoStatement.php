<?php

/**
 * IDoStatement interface file.
 * 
 * @author Anastaszor
 */
interface IDoStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return IExpressionStatement[]
	 */
	public function getDoExpression();
	
}

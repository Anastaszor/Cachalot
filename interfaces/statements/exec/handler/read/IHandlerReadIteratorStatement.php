<?php

/**
 * IHandlerReadIteratorStatement interface file.
 * 
 * @author Anastaszor
 */
interface IHandlerReadIteratorStatement extends IHandlerReadStatement
{
	
	const FIRST = 'FIRST';
	const NEXT = 'NEXT';
	const PREV = 'PREV';
	const LAST = 'LAST';
	
	/**
	 * 
	 * @return string
	 */
	public function getIndexName();
	
	/**
	 * 
	 * @return enum('FIRST', 'NEXT', 'PREV', 'LAST')
	 */
	public function getIterator();
	
}

<?php

/**
 * IHandlerReadComparatorStatement interface file.
 * 
 * @author Anastaszor
 */
interface IHandlerReadComparatorStatement extends IHandlerReadStatement
{
	
	const EQUALS = '=';
	const LOWER_THAN_AND_EQUALS = '<=';
	const UPPER_THAN_AND_EQUALS = '>=';
	const LOWER_THAN = '<';
	const UPPER_THAN = '>';
	
	/**
	 * 
	 * @return string
	 */
	public function getIndexName();
	
	/**
	 * 
	 * @return enum('=', '<=', '>=', '<', '>')
	 */
	public function getComparator();
	
	/**
	 * 
	 * @return mixed[]
	 */
	public function getValues();
	
}

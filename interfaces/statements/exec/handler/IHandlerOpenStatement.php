<?php

/**
 * IHandlerOpenStatement interface file.
 * 
 * @author Anastaszor
 */
interface IHandlerOpenStatement extends IHandlerStatement
{
	// no more information is needed
}

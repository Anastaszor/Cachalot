<?php

/**
 * IHandlerReadStatement interface file.
 * 
 * Abstract interface. Implementations are:
 * - IHandlerReadComparatorStatement
 * - IHandlerReadIteratorStatement
 * 
 * @author Anastaszor
 */
interface IHandlerReadStatement extends IHandlerStatement
{
	
	/**
	 * 
	 * @return IWhereStatement
	 */
	public function getWhereStatement();
	
}

<?php

/**
 * IHandlerCloseStatement interface file.
 * 
 * @author Anastaszor
 */
interface IHandlerCloseStatement extends IHandlerStatement
{
	// no more information is needed
}

<?php

/**
 * IStartTransactionStatement interface file.
 *
 * @author Anastaszor
 */
interface IStartTransactionStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return boolean
	 */
	public function withConsistentSnapShot();
	
}

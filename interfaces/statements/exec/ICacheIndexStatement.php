<?php

/**
 * ICacheIndexStatement interface file.
 * 
 * @author Anastaszor
 */
interface ICacheIndexStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return ICacheTableIndexListStatement[]
	 */
	public function getTableIndexListStatements();
	
	/**
	 * 
	 * @return string
	 */
	public function getCacheKey();
	
}

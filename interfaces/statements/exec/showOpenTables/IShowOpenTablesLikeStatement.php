<?php

/**
 * IShowOpenTablesLikeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowOpenTablesLikeStatement extends IShowOpenTablesStatement
{
	
	/**
	 * 
	 * @return ILikeStatement
	 */
	public function getLikeStatement();
	
}

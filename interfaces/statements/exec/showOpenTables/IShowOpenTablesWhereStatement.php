<?php

/**
 * IShowOpenTablesWhereStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowOpenTablesWhereStatement extends IShowOpenTablesStatement
{
	
	/**
	 * 
	 * @return IWhereStatement
	 */
	public function getWhereStatement();
	
}

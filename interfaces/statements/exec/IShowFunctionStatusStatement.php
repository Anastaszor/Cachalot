<?php

/**
 * IShowFunctionStatusStatement interface file.
 * 
 * Abstract Interface. Implementations are:
 * - IShowFunctionStatusLikeStatement
 * - IShowFunctionStatusWhereStatement
 * 
 * @author Anastaszor
 */
interface IShowFunctionStatusStatement extends IExecutableStatement
{
	// nothing to do
}

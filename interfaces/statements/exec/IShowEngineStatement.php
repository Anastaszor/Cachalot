<?php

/**
 * IShowEngineStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowEngineStatement extends IExecutableStatement
{
	
	const CHARAC_LOGS = 'LOGS';
	const CHARAC_STATUS = 'STATUS';
	
	/**
	 * 
	 * @return string
	 */
	public function getEngineName();
	
	/**
	 * 
	 * @return enum('LOGS', 'STATUS')
	 */
	public function getCaracteristic();
	
}

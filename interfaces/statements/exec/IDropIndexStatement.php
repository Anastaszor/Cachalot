<?php

/**
 * IDropIndexStatement interface file.
 * 
 * @author Anastaszor
 */
interface IDropIndexStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getIndexName();
	/**
	 * 
	 * @return string
	 */
	public function getTableName();
	
}

<?php

/**
 * IShowProcedureCodeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowProcedureCodeStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getProcedureName();
	
}

<?php

/**
 * IShowIndexStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowIndexStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getTableName();
	
	/**
	 * 
	 * @return string
	 */
	public function getDatabaseName();
	
	/**
	 * 
	 * @return IWhereStatement
	 */
	public function getWhereStatement();
	
}

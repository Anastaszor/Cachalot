<?php

/**
 * IInsertStatement interface file.
 * 
 * Abstract interface. Implementations are:
 * - IInsertValuesStatement
 * - IInsertSetStatement
 * - IInsertSelectStatement
 * 
 * @author Anastaszor
 */
interface IInsertStatement extends IExecutableStatement
{
	
	const LOW_PRIORITY = 'LOW_PRIORITY';
	const DELAYED = 'DELAYED';
	const HIGH_PRIORITY = 'HIGH_PRIORITY';
	
	/**
	 * 
	 * @return enum('LOW_PRIORITY', 'DELAYED', 'HIGH_PRIORITY')
	 */
	public function getPriority();
	
	/**
	 * 
	 * @return boolean
	 */
	public function getIgnore();
	
	/**
	 * 
	 * @return string
	 */
	public function getTableName();
	
	/**
	 * 
	 * @return string[]
	 */
	public function getFieldsNames();
	
	/**
	 * 
	 * @return IInsertDuplicateStatement
	 */
	public function getDuplicateStatement();
	
}

<?php

/**
 * ICallStatement interface file.
 * 
 * @author Anastaszor
 */
interface ICallStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getProcedureName();
	
	/**
	 * 
	 * @return mixed[]
	 */
	public function getParamsValues();
	
}

<?php

/**
 * ILoadDataInfileStatement interface file.
 * 
 * @author Anastaszor
 */
interface ILoadDataInfileStatement extends IExecutableStatement
{
	
	const LOW_PRIORITY = 'LOW_PRIORITY';
	const CONCURRENT = 'CONCURRENT';
	
	const REPLACE = 'REPLACE';
	const IGNORE = 'IGNORE';
	
	/**
	 * 
	 * @return enum('LOW_PRIORITY', 'CONCURRENT')
	 */
	public function getPriority();
	
	/**
	 * 
	 * @return boolean
	 */
	public function getLocal();
	
	/**
	 * 
	 * @return string
	 */
	public function getFilePath();
	
	/**
	 * 
	 * @return enum('REPLACE', 'IGNORE')
	 */
	public function getReplaceMode();
	
	/**
	 * 
	 * @return string
	 */
	public function getToTableName();
	
	/**
	 * 
	 * @return ICharset
	 */
	public function getCharacterSet();
	
	/**
	 * 
	 * @return ILoadDataInfileColumnAnalysisStatement
	 */
	public function getColumnsAnalysisStatement();
	
	/**
	 * 
	 * @return ILoadDataInfileLineAnalysisStatement
	 */
	public function getLineAnalysisStatement();
	
	/**
	 * 
	 * @return int
	 */
	public function getIgnoreQuantity();
	
	/**
	 * 
	 * @return IInsertSetColumnExpressionStatement[]
	 */
	public function getSetColumnExpressions();
	
}

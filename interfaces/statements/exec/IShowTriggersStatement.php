<?php

/**
 * IShowTriggersStatement interface file.
 * 
 * Abstract Interface. Implementations are:
 * - IShowTriggersLikeStatement
 * - IShowTriggersWhereStatement
 * 
 * @author Anastaszor
 */
interface IShowTriggersStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getDatabaseName();
	
}

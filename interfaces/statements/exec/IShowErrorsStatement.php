<?php

/**
 * IShowErrorsStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowErrorsStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return boolean
	 */
	public function getCount();
	
	/**
	 * 
	 * @return int
	 */
	public function getOffset();
	
	/**
	 * 
	 * @return int
	 */
	public function getLimit();
	
}

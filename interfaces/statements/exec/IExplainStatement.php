<?php

/**
 * IExplainStatement interface file.
 * 
 * Abstract Interface. Implementations are:
 * - IExplainTableStatement
 * - IExplainSelectStatement
 * 
 * @author Anastaszor
 */
interface IExplainStatement extends IExecutableStatement
{
	// nothing to do
}

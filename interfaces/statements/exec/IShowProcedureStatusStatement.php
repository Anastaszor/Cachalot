<?php

/**
 * IShowProcedureStatusStatement interface file.
 * 
 * Abstract Interface. Implementations are:
 * - IShowProcedureStatusLikeStatement
 * - IShowProcedureStatusWhereStatement
 * 
 * @author Anastaszor
 */
interface IShowProcedureStatusStatement extends IExecutableStatement
{
	// nothing to do
}

<?php

/**
 * IShowGrantsStatement interface file.
 * 
 * @author Anastaszor
 */
interface IShowGrantsStatement extends IExecutableStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getUserName();
	
}

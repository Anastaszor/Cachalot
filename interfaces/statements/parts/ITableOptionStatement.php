<?php

/**
 * ITableOptionStatement interface file.
 * 
 * Abstract Interface. Implementations are:
 * - IEngineTableOptionStatement
 * - IAutoIncrementTableOptionStatement
 * - IAvgRowLengthTableOptionStatement
 * - ICharsetTableOptionStatement
 * - IChecksumTableOptionStatement
 * - ICollationTableOptionStatement
 * - ICommentTableOptionStatement
 * - IConnectionTableOptionStatement
 * - IDataDirectoryTableOptionStatement
 * - IDelayKeyWriteTableOptionStatement
 * - IIndexDirectoryTableOptionStatement
 * - IInsertMethodTableOptionStatement
 * - IMaxRowsTableOptionStatement
 * - IMinRowsTableOptionStatement
 * - IPackKeysTableOptionStatement
 * - IPasswordTableOptionStatement
 * - IRowFormatTableOptionStatement
 * - IUnionTableOptionStatement
 * 
 * @author Anastaszor
 */
interface ITableOptionStatement extends IStatement
{
	// nothing to do
}

<?php

/**
 * ICacheTableIndexListStatement interface file.
 * 
 * @author Anastaszor
 */
interface ICacheTableIndexListStatement extends IStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getTableName();
	
	/**
	 * 
	 * @return string[]
	 */
	public function getIndexNames();
	
	/**
	 * 
	 * @return boolean
	 */
	public function getIgnoreLeaves();
	
}

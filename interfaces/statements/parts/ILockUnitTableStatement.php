<?php

/**
 * ILockUnitTableStatement interface file.
 * 
 * @author Anastaszor
 */
interface ILockUnitTableStatement extends IStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getTableName();
	
	/**
	 * 
	 * @return ILockTableTypeStatement
	 */
	public function getLockType();
	
}

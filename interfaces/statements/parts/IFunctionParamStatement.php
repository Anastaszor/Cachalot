<?php

/**
 * IFunctionParamStatement interface file.
 * 
 * @author Anastaszor
 */
interface IFunctionParamStatement extends IStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getParamName();
	
	/**
	 * 
	 * @return IDataTypeStatement
	 */
	public function getDataType();
	
}

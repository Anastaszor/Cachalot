<?php

/**
 * ICreateDefinitionStatement interface file.
 * 
 * @author Anastaszor
 */
interface ICreateDefinitionStatement extends IStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getColumnName();
	/**
	 * 
	 * @return IColumnDefinitionStatement
	 */
	public function getColumnDefinition();
	/**
	 * 
	 * @return IConstraintStatement[]
	 */
	public function getConstraints();
	
}

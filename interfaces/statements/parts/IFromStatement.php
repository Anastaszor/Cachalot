<?php

/**
 * IFromStatement interface file.
 * 
 * @author Anastaszor
 */
interface IFromStatement extends IStatement
{
	
	/**
	 * 
	 * @return ITableNameStatement
	 */
	public function getTableNameStatements();
	
	/**
	 *
	 * @return IWhereStatement
	 */
	public function getWhereStatement();
	
	/**
	 *
	 * @return IGroupByStatement
	 */
	public function getGroupByStatement();
	
	/**
	 *
	 * @return IHavingStatement
	 */
	public function getHavingStatement();
	
	/**
	 *
	 * @return IOrderByStatement
	 */
	public function getOrderByStatement();
	
	/**
	 *
	 * @return ILimitStatement
	 */
	public function getLimitStatement();
	
	/**
	 *
	 * @return IIntoStatement
	 */
	public function getIntoStatement();
	
	/**
	 *
	 * @return enum('FOR UPDATE', 'LOCK IN SHARE MODE')
	 */
	public function getUpdateStatus();
	
}

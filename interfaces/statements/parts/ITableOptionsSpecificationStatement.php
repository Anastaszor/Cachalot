<?php

/**
 * ITableOptionsSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface ITableOptionsSpecificationStatement extends ITableSpecificationStatement
{
	
	/**
	 * 
	 * @return ITableOptionStatement[]
	 */
	public function getTableOptionsStatements();
	
}

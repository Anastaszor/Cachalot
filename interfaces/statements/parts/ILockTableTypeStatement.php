<?php

/**
 * LockTableTypeStatement interface file.
 * 
 * Abstract interface. Implementations are:
 * - ILockTableReadTypeStatement
 * - ILockTableWriteTypeStatement
 * 
 * @author Anastaszor
 */
interface ILockTableTypeStatement extends IStatement
{
	// nothing to do
}

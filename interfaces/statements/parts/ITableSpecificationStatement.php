<?php

/**
 * ISpecification interface file.
 * 
 * Abstract Interface. Implementations are:
 * - IAddColumnSpecificationStatement
 * - IAddIndexSpecificationStatement
 * - IAddConstraintSpecificationStatement
 * - IAddForeignKeySpecificationStatement
 * - IAlterColumnSpecificationStatement
 * - IChangeColumnSpecificationStatement
 * - IModifyColumnSpecificationStatement
 * - IDropColumnSpecificationStatement
 * - IDropPrimarySpecificationStatement
 * - IDropIndexSpecificationStatement
 * - IDropForeignSpecificationStatement
 * - IDisableKeysSpecificationStatement
 * - IEnableKeysSpecificationStatement
 * - IRenameSpecificationStatement
 * - IOrderBySpecificationStatement
 * - IConvertCharsetSpecificationStatement
 * - IDefaultCharsetSpecificationStatement
 * - IDiscardTablespaceSpecificationStatement
 * - IImportTablespaceSpecificationStatement
 * - ITableOptionsSpecificationStatement
 * 
 * @author Anastaszor
 */
interface ITableSpecificationStatement extends IStatement
{
	// nothing to do
}

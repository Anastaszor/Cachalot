<?php

/**
 * IVarcharTypeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IVarcharTypeStatement extends IDataTypeStatement
{
	
	/**
	 *
	 * @return int
	 */
	public function getLength();
	/**
	 *
	 * @return boolean
	*/
	public function getBinary();
	/**
	 *
	 * @return ICharset
	*/
	public function getCharset();
	/**
	 *
	 * @return ICollation
	*/
	public function getCollation();
	
}
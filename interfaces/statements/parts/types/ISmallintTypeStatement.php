<?php

/**
 * ISmallintTypeStatement interface file.
 * 
 * @author Anastaszor
 */
interface ISmallintTypeStatement extends IDataTypeStatement
{
	
	/**
	 *
	 * @return int
	 */
	public function getLength();
	/**
	 *
	 * @return boolean
	*/
	public function getUnsigned();
	/**
	 *
	 * @return boolean
	*/
	public function getZerofill();
	
}

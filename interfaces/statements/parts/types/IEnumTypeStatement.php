<?php

/**
 * IEnumTypeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IEnumTypeStatement extends IDataTypeStatement
{
	
	/**
	 * 
	 * @return string[]
	 */
	public function getValues();
	/**
	 *
	 * @return ICharset
	*/
	public function getCharset();
	/**
	 *
	 * @return ICollation
	*/
	public function getCollation();
	
}

<?php

/**
 * IIntTypeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IIntTypeStatement extends IDataTypeStatement
{
	
	/**
	 *
	 * @return int
	 */
	public function getLength();
	/**
	 *
	 * @return boolean
	*/
	public function getUnsigned();
	/**
	 *
	 * @return boolean
	*/
	public function getZerofill();
	
}
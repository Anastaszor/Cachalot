<?php

/**
 * IBitTypeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IBitTypeStatement extends IDataTypeStatement
{
	
	/**
	 * 
	 * @return int
	 */
	public function getLength();
	
}

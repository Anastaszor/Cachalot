<?php

/**
 * ITextTypeStatement interface file.
 * 
 * @author Anastaszor
 */
interface ITextTypeStatement extends IDataTypeStatement
{
	
	/**
	 *
	 * @return boolean
	 */
	public function getBinary();
	/**
	 *
	 * @return ICharset
	*/
	public function getCharset();
	/**
	 *
	 * @return ICollation
	*/
	public function getCollation();
	
}

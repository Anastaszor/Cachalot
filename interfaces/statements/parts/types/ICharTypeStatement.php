<?php

/**
 * ICharTypeStatement interface file.
 * 
 * @author Anastaszor
 */
interface ICharTypeStatement extends IDataTypeStatement
{
	
	/**
	 *
	 * @return int
	 */
	public function getLength();
	/**
	 * 
	 * @return boolean
	 */
	public function getBinary();
	/**
	 * 
	 * @return ICharset
	 */
	public function getCharset();
	/**
	 *
	 * @return ICollation
	 */
	public function getCollation();
	
}

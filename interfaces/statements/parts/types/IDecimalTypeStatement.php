<?php

/**
 * IDecimalTypeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IDecimalTypeStatement extends IDataTypeStatement
{
	
	/**
	 *
	 * @return int
	 */
	public function getLength();
	/**
	 *
	 * @return int
	*/
	public function getDecimals();
	/**
	 *
	 * @return boolean
	*/
	public function getUnsigned();
	/**
	 *
	 * @return boolean
	*/
	public function getZerofill();
	
}
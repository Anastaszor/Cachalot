<?php

/**
 * ITinyintTypeStatement interface file.
 * 
 * @author Anastaszor
 */
interface ITinyintTypeStatement extends IDataTypeStatement
{
	
	/**
	 * 
	 * @return int
	 */
	public function getLength();
	/**
	 * 
	 * @return boolean
	 */
	public function getUnsigned();
	/**
	 * 
	 * @return boolean
	 */
	public function getZerofill();
	
}

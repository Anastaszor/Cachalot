<?php

/**
 * IVarbinaryTypeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IVarbinaryTypeStatement extends IDataTypeStatement
{
	
	/**
	 *
	 * @return int
	 */
	public function getLength();
	
}

<?php

/**
 * IMediumintTypeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IMediumintTypeStatement extends IDataTypeStatement
{
	
	/**
	 *
	 * @return int
	 */
	public function getLength();
	/**
	 *
	 * @return boolean
	*/
	public function getUnsigned();
	/**
	 *
	 * @return boolean
	*/
	public function getZerofill();
	
}

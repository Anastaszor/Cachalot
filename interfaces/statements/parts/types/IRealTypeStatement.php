<?php

/**
 * IRealTypeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IRealTypeStatement extends IDataTypeStatement
{
	
	/**
	 *
	 * @return int
	 */
	public function getLength();
	/**
	 * 
	 * @return int
	 */
	public function getDecimals();
	/**
	 *
	 * @return boolean
	 */
	public function getUnsigned();
	/**
	 *
	 * @return boolean
	 */
	public function getZerofill();
	
}
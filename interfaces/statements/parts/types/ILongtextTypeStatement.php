<?php

/**
 * ILongtextTypeStatement interface file.
 * 
 * @author Anastaszor
 */
interface ILongtextTypeStatement extends IDataTypeStatement
{
	
	/**
	 *
	 * @return boolean
	 */
	public function getBinary();
	/**
	 *
	 * @return ICharset
	*/
	public function getCharset();
	/**
	 *
	 * @return ICollation
	*/
	public function getCollation();
	
}

<?php

/**
 * IMediumtextTypeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IMediumtextTypeStatement extends IDataTypeStatement
{
	
	/**
	 *
	 * @return boolean
	 */
	public function getBinary();
	/**
	 *
	 * @return ICharset
	 */
	public function getCharset();
	/**
	 *
	 * @return ICollation
	 */
	public function getCollation();
	
}

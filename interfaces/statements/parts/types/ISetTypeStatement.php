<?php

/**
 * ISetTypeStatement interface file.
 * 
 * @author Anastaszor
 */
interface ISetTypeStatement extends IDataTypeStatement
{
	
	/**
	 *
	 * @return string[]
	 */
	public function getValues();
	/**
	 *
	 * @return ICharset
	 */
	public function getCharset();
	/**
	 *
	 * @return ICollation
	 */
	public function getCollation();
	
}

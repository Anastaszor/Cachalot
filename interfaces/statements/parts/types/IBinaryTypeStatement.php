<?php

/**
 * IBinaryTypeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IBinaryTypeStatement extends IDataTypeStatement
{
	
	/**
	 *
	 * @return int
	 */
	public function getLength();
	
}

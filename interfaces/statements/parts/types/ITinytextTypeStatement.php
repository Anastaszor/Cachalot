<?php

/**
 * ITinytextTypeStatement interface file.
 * 
 * @author Anastaszor
 */
interface ITinytextTypeStatement extends IDataTypeStatement
{
	
	/**
	 *
	 * @return boolean
	 */
	public function getBinary();
	/**
	 *
	 * @return ICharset
	 */
	public function getCharset();
	/**
	 *
	 * @return ICollation
	 */
	public function getCollation();
	
}

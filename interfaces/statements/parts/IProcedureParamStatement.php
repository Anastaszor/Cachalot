<?php

/**
 * IProcedureParamStatement interface file.
 * 
 * @author Anastaszor
 */
interface IProcedureParamStatement extends IFunctionParamStatement
{
	
	const INOUT = 'INOUT';
	const IN = 'IN';
	const OUT = 'OUT';
	
	/**
	 * 
	 * @return enum('INOUT', 'IN', 'OUT')
	 */
	public function getType();
	
}

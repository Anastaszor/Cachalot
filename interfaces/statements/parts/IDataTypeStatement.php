<?php

/**
 * IDataTypeStatement interface file.
 * 
 * @author Anastaszor
 */
interface IDataTypeStatement extends IStatement
{
	
	/**
	 * 
	 * @return IGenericDataType
	 */
	public function toDataType();
	
	/**
	 * 
	 * @return string|int
	 */
	public function getDefaultValue();
	
}

<?php

/**
 * IConstraintStatement interface file.
 * 
 * @author Anastaszor
 */
interface IConstraintStatement extends IStatement
{
	
	/**
	 * 
	 * @return IIndexType
	 */
	public function getIndexType();
	/**
	 * 
	 * @return IIndexColumn[]
	 */
	public function getColumnNames();
	/**
	 * 
	 * @return IIndexOption[]
	 */
	public function getIndexOptions();
	
}

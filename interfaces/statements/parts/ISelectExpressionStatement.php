<?php

/**
 * ISelectExpressionStatement class file.
 * 
 * @author Anastaszor
 */
interface ISelectExpressionStatement extends IExpressionStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getAlias();
	
}

<?php

/**
 * IRoutineBodystatement interface file.
 * 
 * @author Anastaszor
 */
interface IRoutineBodyStatement extends IStatement
{
	
	/**
	 * 
	 * @return IInstructionStatement
	 */
	public function getInstructions();
	
}

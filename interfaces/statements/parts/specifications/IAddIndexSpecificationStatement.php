<?php

/**
 * IAddIndexSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface IAddIndexSpecificationStatement extends ITableSpecificationStatement
{
	
	const FULLTEXT = 'FULLTEXT';
	const SPATIAL = 'SPATIAL';
	
	const BTREE = 'BTREE';
	const HASH = 'HASH';
	
	/**
	 * 
	 * @return enum('FULLTEXT', 'SPATIAL')
	 */
	public function getIndexSpecification();
	
	/**
	 * 
	 * @return string
	 */
	public function getIndexName();
	
	/**
	 * 
	 * @return enum('BTREE', 'HASH')
	 */
	public function getIndexType();
	
	/**
	 * 
	 * @return IIndexColumnNameSpecificationStatement[]
	 */
	public function getIndexColNames();
	
}

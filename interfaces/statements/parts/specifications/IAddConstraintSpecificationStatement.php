<?php

/**
 * IAddConstraintSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface IAddConstraintSpecificationStatement extends ITableSpecificationStatement
{
	
	const UNIQUE = 'UNIQUE';
	const PRIMARY = 'PRIMARY';
	
	const BTREE = 'BTREE';
	const HASH = 'HASH';
	
	/**
	 * 
	 * @return string
	 */
	public function getSymbol();
	
	/**
	 * 
	 * @return enum('UNIQUE', 'PRIMARY')
	 */
	public function getConstraintType();
	
	/**
	 * 
	 * @return enum('BTREE', 'HASH')
	 */
	public function getIndexType();
	
	/**
	 * 
	 * @return string
	 */
	public function getIndexName();
	
	/**
	 * 
	 * @return IIndexColumnNameSpecificationStatement[]
	 */
	public function getIndexColNames();
	
}

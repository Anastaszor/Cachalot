<?php

/**
 * IModifyColumnSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface IModifyColumnSpecificationStatement extends ITableSpecificationStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getColumnName();
	
	/**
	 * 
	 * @return IColumnDefinitionStatement
	 */
	public function getColumnDefinition();
	
	/**
	 * 
	 * @return IColumnPositionStatement
	 */
	public function getPositionStatement();
	
}

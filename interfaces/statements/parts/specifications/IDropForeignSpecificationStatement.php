<?php

/**
 * IDropForeignSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface IDropForeignSpecificationStatement extends ITableSpecificationStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getForeignKeySymbol();
	
}

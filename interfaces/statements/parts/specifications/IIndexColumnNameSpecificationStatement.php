<?php

/**
 * IIndexColumnNameSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface IIndexColumnNameSpecificationStatement extends IStatement
{
	
	const ASC = 'ASC';
	const DESC = 'DESC';
	
	/**
	 * 
	 * @return string
	 */
	public function getColumnName();
	
	/**
	 * 
	 * @return int
	 */
	public function getLength();
	
	/**
	 * 
	 * @return enum('ASC', 'DESC')
	 */
	public function getOrder();
	
}

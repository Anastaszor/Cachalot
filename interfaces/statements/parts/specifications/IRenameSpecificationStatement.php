<?php

/**
 * IRenameSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface IRenameSpecificationStatement extends ITableSpecificationStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getNewTableName();
	
}

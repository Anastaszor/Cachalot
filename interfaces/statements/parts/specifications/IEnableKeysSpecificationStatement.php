<?php

/**
 * IEnableKeysSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface IEnableKeysSpecificationStatement extends ITableSpecificationStatement
{
	// nothing to do
}

<?php

/**
 * ICharsetSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface ICharsetSpecificationStatement 
		extends IDatabaseSpecificationStatement
{
	
	/**
	 * 
	 * @return ICharset
	 */
	public function getCharset();
	
}

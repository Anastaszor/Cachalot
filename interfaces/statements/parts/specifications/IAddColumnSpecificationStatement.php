<?php

/**
 * IAddColumnSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface IAddColumnSpecificationStatement extends ITableSpecificationStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getColumnName();
	
	/**
	 * 
	 * @return IColumnDefinitionStatement
	 */
	public function getColumnDefinitionStatement();
	
	/**
	 * 
	 * @return IColumnPositionStatement
	 */
	public function getColumnPositionStatement();
	
}

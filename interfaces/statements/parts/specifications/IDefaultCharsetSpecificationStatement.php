<?php

/**
 * IDefaultCharsetSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface IDefaultCharsetSpecificationStatement extends ITableSpecificationStatement
{
	
	/**
	 * 
	 * @return ICharset
	 */
	public function getCharset();
	
	/**
	 * 
	 * @return ICollation
	 */
	public function getCollation();
	
}

<?php

/**
 * IDisableKeysSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface IDisableKeysSpecificationStatement extends ITableSpecificationStatement
{
	// nothing to do
}

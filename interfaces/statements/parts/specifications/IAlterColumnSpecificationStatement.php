<?php

/**
 * IAlterColumnSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface IAlterColumnSpecificationStatement extends ITableSpecificationStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getColumnName();
	
	/**
	 * 
	 * @return null|number|number[]|string|string[]
	 */
	public function getDefaultValue();
	
}

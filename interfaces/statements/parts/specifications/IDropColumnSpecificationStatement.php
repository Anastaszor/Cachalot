<?php

/**
 * IDropColumnSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface IDropColumnSpecificationStatement extends ITableSpecificationStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getColumnName();
	
}

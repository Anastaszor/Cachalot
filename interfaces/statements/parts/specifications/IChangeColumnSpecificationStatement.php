<?php

/**
 * IChangeColumnSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface IChangeColumnSpecificationStatement extends ITableSpecificationStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getOldColumnName();
	
	/**
	 * 
	 * @return string
	 */
	public function getNewColumnName();
	
	/**
	 * 
	 * @return IColumnDefinitionStatement
	 */
	public function getColumnDefinition();
	
	/**
	 * 
	 * @return IColumnPositionStatement
	 */
	public function getPositionStatement();
	
}

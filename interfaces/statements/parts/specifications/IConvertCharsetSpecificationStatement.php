<?php

/**
 * IConvertCharsetSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface IConvertCharsetSpecificationStatement extends ITableSpecificationStatement
{
	
	/**
	 * 
	 * @return ICharset
	 */
	public function getCharset();
	
	/**
	 * 
	 * @return ICollation
	 */
	public function getCollation();
	
}

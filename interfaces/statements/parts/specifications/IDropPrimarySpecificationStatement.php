<?php

/**
 * IDropPrimarySpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface IDropPrimarySpecificationStatement extends ITableSpecificationStatement
{
	// nothing to do
}

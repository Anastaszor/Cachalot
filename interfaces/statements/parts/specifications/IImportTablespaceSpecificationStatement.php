<?php

/**
 * IImportTablespaceSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface IImportTablespaceSpecificationStatement extends ITableSpecificationStatement
{
	// nothing to do
}

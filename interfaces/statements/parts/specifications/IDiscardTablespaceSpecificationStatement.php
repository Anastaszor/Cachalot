<?php

/**
 * IDiscardTablespaceSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface IDiscardTablespaceSpecificationStatement extends ITableSpecificationStatement
{
	// nothing to do
}

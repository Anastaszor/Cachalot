<?php

/**
 * IDropIndexSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface IDropIndexSpecificationStatement extends ITableSpecificationStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getIndexName();
	
}

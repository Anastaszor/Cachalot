<?php

/**
 * ICollationSpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface ICollationSpecificationStatement 
		extends IDatabaseSpecificationStatement
{
	
	/**
	 * 
	 * @return ICollation
	 */
	public function getCollation();
	
}

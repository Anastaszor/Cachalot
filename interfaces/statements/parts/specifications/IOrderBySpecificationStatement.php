<?php

/**
 * IOrderBySpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface IOrderBySpecificationStatement extends ITableSpecificationStatement
{
	
	/**
	 * 
	 * @return string[]
	 */
	public function getTableNames();
	
}

<?php

/**
 * IAddForeignKeySpecificationStatement interface file.
 * 
 * @author Anastaszor
 */
interface IAddForeignKeySpecificationStatement extends ITableSpecificationStatement
{
	
	const RESTRICT = 'RESTRICT';
	const CASCADE = 'CASCADE';
	const SET_NULL = 'SET NULL';
	const NO_ACTION = 'NO ACTION';
	
	/**
	 * 
	 * @return string
	 */
	public function getSymbol();
	
	/**
	 * 
	 * @return string
	 */
	public function getIndexName();
	
	/**
	 * 
	 * @return IIndexColumnNameSpecificationStatement[]
	 */
	public function getIndexColumnNames();
	
	/**
	 * 
	 * @return string
	 */
	public function getTargetTableName();
	
	/**
	 * 
	 * @return string[]
	 */
	public function getTargetIndexNames();
	
	/**
	 * 
	 * @return enum('RESTRICT', 'CASCADE', 'SET NULL', 'NO ACTION')
	 */
	public function getOnUpdateConstraint();
	
	/**
	 * 
	 * @return enum('RESTRICT', 'CASCADE', 'SET NULL', 'NO ACTION')
	 */
	public function getOnDeleteConstraint();
	
}

<?php

/**
 * IInstructionStatement interface file.
 * 
 * @author Anastaszor
 */
interface IInstructionStatement extends IStatement
{
	// nothing to do
}

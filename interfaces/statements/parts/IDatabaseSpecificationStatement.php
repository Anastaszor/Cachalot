<?php

/**
 * IDatabaseSpecificationStatement interface file.
 * 
 * Abstract interface. Implementations are: 
 * - ICharsetSpecificationStatement
 * - ICollationSpecificationStatement
 * 
 * @author Anastaszor
 */
interface IDatabaseSpecificationStatement extends IStatement
{
	
}

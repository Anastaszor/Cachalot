<?php

/**
 * IExpressionStatement interface file.
 * 
 * @author Anastaszor
 */
interface IExpressionStatement extends IInstructionStatement
{
	
	/**
	 *
	 * @return string
	 */
	public function getValue();
	
}

<?php

/**
 * ICharacteristicStatement interface file.
 * 
 * Abstract Interface. Implementations are:
 * - ICommentCharacteristicStatement
 * - IDeterministicCharacteristicStatement
 * - ILanguageCharacteristicStatement
 * - ISqlCharacteristicStatement
 * - ISecurityCharacteristicStatement
 * 
 * @author Anastaszor
 */
interface ICharacteristicStatement extends IStatement
{
	
}

<?php

/**
 * IVariableAssignmentStatement interface file.
 * 
 * @author Anastaszor
 */
interface IVariableAssignmentStatement extends IStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getVariableName();
	
	/**
	 * 
	 * @return IExpressionStatement
	 */
	public function getExpression();
	
}

<?php

/**
 * IRenameMoveStatement interface file.
 * 
 * @author Anastaszor
 */
interface IRenameMoveStatement extends IStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getFromName();
	
	/**
	 * 
	 * @return string
	 */
	public function getToName();
	
}

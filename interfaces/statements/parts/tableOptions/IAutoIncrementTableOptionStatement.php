<?php

/**
 * IAutoIncrementTableOptionStatement interface file.
 * 
 * @author Anastaszor
 */
interface IAutoIncrementTableOptionStatement extends ITableOptionStatement
{
	
	/**
	 * 
	 * @return int
	 */
	public function getValue();
	
}

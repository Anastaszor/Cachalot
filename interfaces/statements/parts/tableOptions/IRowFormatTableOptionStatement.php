<?php

/**
 * IRowFormatTableOptionStatement interface file.
 * 
 * @author Anastaszor
 */
interface IRowFormatTableOptionStatement extends ITableOptionStatement
{
	
	const _DEFAULT = 'DEFAULT';
	const DYNAMIC = 'DYNAMIC';
	const FIXED = 'FIXED';
	const COMPRESSED = 'COMPRESSED';
	const REDUNDANT = 'REDUNDANT';
	const COMPACT = 'COMPACT';
	
	/**
	 * 
	 * @return enum('DEFAULT', 'DYNAMIC', 'FIXED', 'COMPRESSED', 'REDUNDANT', 'COMPACT')
	 */
	public function getValue();
	
}

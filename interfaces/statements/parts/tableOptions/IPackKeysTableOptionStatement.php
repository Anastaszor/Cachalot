<?php

/**
 * IPackKeysTableOptionStatement interface file.
 * 
 * @author Anastaszor
 */
interface IPackKeysTableOptionStatement extends ITableOptionStatement
{
	
	const DISABLED = '0';
	const ENABLED = '1';
	const _DEFAULT = 'DEFAULT';
	
	/**
	 * 
	 * @return enum('0', '1', 'DEFAULT')
	 */
	public function getValue();
	
}

<?php

/**
 * ICharsetTableOptionStatement interface file.
 * 
 * @author Anastaszor
 */
interface ICharsetTableOptionStatement extends ITableOptionStatement
{
	
	/**
	 * 
	 * @return ICharset
	 */
	public function getCharset();
	
}

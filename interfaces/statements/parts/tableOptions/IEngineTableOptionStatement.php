<?php

/**
 * IEngineTableOptionStatement interface file.
 * 
 * @author Anastaszor
 */
interface IEngineTableOptionStatement extends ITableOptionStatement
{
	
	/**
	 * 
	 * @return IEngine
	 */
	public function getEngine();
	
}

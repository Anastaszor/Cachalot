<?php

/**
 * IChecksumTableOptionStatement interface file.
 * 
 * @author Anastaszor
 */
interface IChecksumTableOptionStatement extends ITableOptionStatement
{
	
	/**
	 * 
	 * @return boolean
	 */
	public function getEnabled();
	
}

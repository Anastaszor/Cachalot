<?php

/**
 * IUnionTableOptionStatement interface file.
 * 
 * @author Anastaszor
 */
interface IUnionTableOptionStatement extends ITableOptionStatement
{
	
	/**
	 * 
	 * @return string[]
	 */
	public function getTableNames();
	
}

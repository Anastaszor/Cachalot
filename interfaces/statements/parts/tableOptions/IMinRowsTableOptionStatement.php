<?php

/**
 * IMinRowsTableOptionStatement interface file.
 * 
 * @author Anastaszor
 */
interface IMinRowsTableOptionStatement extends ITableOptionStatement
{
	
	/**
	 * 
	 * @return int
	 */
	public function getValue();
	
}

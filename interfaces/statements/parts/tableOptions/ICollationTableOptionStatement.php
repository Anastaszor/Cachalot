<?php

/**
 * ICollationTableOptionStatement interface file.
 * 
 * @author Anastaszor
 */
interface ICollationTableOptionStatement extends ITableOptionStatement
{
	
	/**
	 * 
	 * @return ICollation
	 */
	public function getCollation();
	
}

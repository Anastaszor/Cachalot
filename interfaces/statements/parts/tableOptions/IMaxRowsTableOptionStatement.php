<?php

/**
 * IMaxRowsTableOptionStatement interface file.
 * 
 * @author Anastaszor
 */
interface IMaxRowsTableOptionStatement extends ITableOptionStatement
{
	
	/**
	 * 
	 * @return int
	 */
	public function getValue();
	
}

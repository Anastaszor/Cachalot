<?php

/**
 * IPasswordTableOptionStatement interface file.
 * 
 * @author Anastaszor
 */
interface IPasswordTableOptionStatement extends ITableOptionStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getValue();
	
}

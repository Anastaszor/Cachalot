<?php

/**
 * IConnectionTableOptionStatement interface file.
 * 
 * @author Anastaszor
 */
interface IConnectionTableOptionStatement extends ITableOptionStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getConnectionString();
	
}

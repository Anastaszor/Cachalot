<?php

/**
 * IIndexDirectoryTableOptionStatement interface file.
 * 
 * @author Anastaszor
 */
interface IIndexDirectoryTableOptionStatement extends ITableOptionStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getAbsolutePathToDirectory();
	
}

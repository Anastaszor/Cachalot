<?php

/**
 * IInsertMethodTableOptionStatement interface file.
 * 
 * @author Anastaszor
 */
interface IInsertMethodTableOptionStatement extends ITableOptionStatement
{
	
	const MTD_NO = 'NO';
	const MTD_FIRST = 'FIRST';
	const MTD_LAST = 'LAST';
	
	/**
	 * 
	 * @return enum('NO', 'FIRST', 'LAST')
	 */
	public function getValue();
	
}

<?php

/**
 * IAvgRowLengthTableOptionStatement interface file.
 * 
 * @author Anastaszor
 */
interface IAvgRowLengthTableOptionStatement extends ITableOptionStatement
{
	
	/**
	 * 
	 * @return int
	 */
	public function getValue();
	
}

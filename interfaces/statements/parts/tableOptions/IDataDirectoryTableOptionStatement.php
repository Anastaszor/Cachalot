<?php

/**
 * IDataDirectoryTableOptionStatement interface file.
 * 
 * @author Anastaszor
 */
interface IDataDirectoryTableOptionStatement extends ITableOptionStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getAbsolutePathToDirectory();
	
}

<?php

/**
 * IDelayKeyWriteTableOptionStatement interface file.
 * 
 * @author Anastaszor
 */
interface IDelayKeyWriteTableOptionStatement extends ITableOptionStatement
{
	
	/**
	 * 
	 * @return boolean
	 */
	public function getEnabled();
	
}

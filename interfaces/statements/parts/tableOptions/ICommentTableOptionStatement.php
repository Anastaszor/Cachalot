<?php

/**
 * ICommentTableOptionStatement interface file.
 * 
 * @author Anastaszor
 */
interface ICommentTableOptionStatement extends ITableOptionStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getValue();
	
}

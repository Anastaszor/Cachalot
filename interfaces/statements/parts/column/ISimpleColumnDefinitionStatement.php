<?php

/**
 * ISimpleColumnDefinitionStatement interface file.
 * 
 * @author Anastaszor
 */
interface ISimpleColumnDefinitionStatement extends IColumnDefinitionStatement
{
	
	const COLUMN_FORMAT_FIXED = 'FIXED';
	const COLUMN_FORMAT_DYNAMIC = 'DYNAMIC';
	const COLUMN_FORMAT_DEFAULT = 'DEFAULT';
	
	// TODO get user defined default value ?
	
	/**
	 * 
	 * @return boolean
	 */
	public function getAutoIncrement();
	/**
	 *
	 * @return enum('FIXED', 'DYNAMIC', 'DEFAULT')
	 */
	public function getColumnFormat();
	/**
	 * 
	 * @return IReferenceStatement
	 */
	public function getReferenceStatement();
	
}

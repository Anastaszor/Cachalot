<?php

/**
 * ILockTableWriteTypeStatement interface file.
 * 
 * @author Anastaszor
 */
interface ILockTableWriteTypeStatement extends ILockTableTypeStatement
{
	
	/**
	 * 
	 * @return boolean
	 */
	public function getLowPriority();
	
}

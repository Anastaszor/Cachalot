<?php

/**
 * ILockTableReadTypeStatement interface file.
 * 
 * @author Anastaszor
 */
interface ILockTableReadTypeStatement extends ILockTableTypeStatement
{
	
	/**
	 * 
	 * @return boolean
	 */
	public function getLocal();
	
}
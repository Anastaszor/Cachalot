<?php

/**
 * IColumnPositionStatement interface file.
 * 
 * Abstract Interface. Implementations are:
 * - IColumnPositionFirstStatement
 * - IColumnPositionAfterStatement
 * 
 * @author Anastaszor
 */
interface IColumnPositionStatement extends IStatement
{
	// nothing to do
}

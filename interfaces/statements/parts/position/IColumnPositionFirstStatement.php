<?php

/**
 * IColumnPositionFirstStatement interface file.
 * 
 * @author Anastaszor
 */
interface IColumnPositionFirstStatement extends IColumnPositionStatement
{
	// nothing to do
}

<?php

/**
 * IColumnPositionAfterStatement interface file.
 * 
 * @author Anastaszor
 */
interface IColumnPositionAfterStatement extends IColumnPositionStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getColumnName();
	
}

<?php

/**
 * IUniqueKeyConstraint interface file.
 * 
 * @author Anastaszor
 */
interface IUniqueKeyConstraint extends IConstraintStatement
{
	
	/**
	 *
	 * @return string
	 */
	public function getSymbol();
	
}

<?php

/**
 * ICheckConstraint interface file.
 * 
 * @author Anastaszor
 */
interface ICheckConstraint extends IConstraintStatement
{
	
	/**
	 * 
	 * @return string SQL
	 */
	public function getExpression();
	
}
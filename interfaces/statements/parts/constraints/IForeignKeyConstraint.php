<?php

/**
 * IForeignKeyConstraint interface file.
 * 
 * @author Anastaszor
 */
interface IForeignKeyConstraint
{
	
	/**
	 *
	 * @return string
	 */
	public function getSymbol();
	/**
	 * 
	 * @return IReferenceStatement
	 */
	public function getReference();
	
}
<?php

/**
 * IPrimaryKeyConstraint interface file.
 * 
 * @author Anastaszor
 */
interface IPrimaryKeyConstraint extends IConstraintStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getSymbol();
	
}

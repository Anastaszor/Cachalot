<?php

/**
 * ISqlCharacteristicStatement interface file.
 * 
 * @author Anastaszor
 */
interface ISqlCharacteristicStatement 
		extends ICharacteristicStatement
{
	
	const CONTAINS_SQL = 'CONTAINS SQL';
	const NO_SQL = 'NO SQL';
	const READS_SQL_DATA = 'READS SQL DATA';
	const MODIFIES_SQL_DATA = 'MODIFIES SQL DATA';
	
	/**
	 * 
	 * @return enum('CONTAINS SQL', 'NO SQL', 'READS SQL DATA', 'MODIFIES SQL DATA')
	 */
	public function getSqlModifier();
	
}

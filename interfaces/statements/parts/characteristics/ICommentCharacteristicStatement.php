<?php

/**
 * ICommentCharacteristicStatement interface file.
 * 
 * @author Anastaszor
 */
interface ICommentCharacteristicStatement 
		extends ICharacteristicStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getCommentValue();
	
}

<?php

/**
 * ILanguageCharacteristicStatement interface file.
 * 
 * @author Anastaszor
 */
interface ILanguageCharacteristicStatement 
		extends ICharacteristicStatement
{
	
	/**
	 * 
	 * @return string
	 */
	public function getLanguageValue();
	
}

<?php

/**
 * IDeterministicCharacteristicStatement interface file.
 * 
 * @author Anastaszor
 */
interface IDeterministicCharacteristicStatement 
		extends ICharacteristicStatement
{
	
	/**
	 * 
	 * @return boolean
	 */
	public function isNot();
	
}

<?php

/**
 * ISecurityCharacteristicStatement interface file.
 * 
 * @author Anastaszor
 */
interface ISecurityCharacteristicStatement 
		extends ICharacteristicStatement
{
	
	const DEFINER = 'DEFINER';
	const INVOKER = 'INVOKER';
	
	/**
	 * 
	 * @return enum('DEFINER', 'INVOKER')
	 */
	public function getSecurityParam();
	
}

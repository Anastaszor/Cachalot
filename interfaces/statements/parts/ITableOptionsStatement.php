<?php

/**
 * ITableOptionsStatement interface file.
 *
 * @author Anastaszor
 * @deprecated
 */
interface ITableOptionsStatement extends ITableSpecificationStatement
{
	/**
	 * 
	 * @return string
	 * @deprecated
	 */
	public function getEngine();
	/**
	 * 
	 * @return int
	 * @deprecated
	 */
	public function getAutoIncrement();
	/**
	 * 
	 * @return int
	 * @deprecated
	 */
	public function getAverageRowLength();
	/**
	 * 
	 * @return string
	 * @deprecated
	 */
	public function getDefaultCharacterSet();
	/**
	 * 
	 * @return boolean
	 * @deprecated
	 */
	public function getChecksum();
	/**
	 * 
	 * @return string
	 * @deprecated
	 */
	public function getDefaultCollation();
	/**
	 * 
	 * @return string
	 * @deprecated
	 */
	public function getComment();
	/**
	 * 
	 * @return string
	 * @deprecated
	 */
	public function getConnectionString();
	/**
	 * 
	 * @return string
	 * @deprecated
	 */
	public function getDataDirectory();
	/**
	 * 
	 * @return boolean
	 * @deprecated
	 */
	public function getDelayKeyWrite();
	/**
	 * 
	 * @return string
	 * @deprecated
	 */
	public function getIndexDirectory();
	/**
	 * 
	 * @return enum('NO', 'FIRST', 'LAST')
	 * @deprecated
	 */
	public function getInsertMethod();
	/**
	 * 
	 * @return int
	 * @deprecated
	 */
	public function getKeyBlockSize();
	/**
	 * 
	 * @return int
	 * @deprecated
	 */
	public function getMaxRows();
	/**
	 * 
	 * @return int
	 * @deprecated
	 */
	public function getMinRows();
	/**
	 * 
	 * @return enum(0, 1, 'DEFAULT')
	 * @deprecated
	 */
	public function getPackKeys();
	/**
	 * 
	 * @return string
	 * @deprecated
	 */
	public function getPassword();
	/**
	 * 
	 * @return enum('DEFAULT', 'DYNAMIC', 'FIXED', 'COMPRESSED', 'REDUNDANT', 'COMPACT')
	 * @deprecated
	 */
	public function getRowFormat();
	/**
	 * 
	 * @return string
	 * @deprecated
	 */
	public function getTablespace();
	/**
	 * 
	 * @return enum('DISK', 'MEMORY', 'DEFAULT')
	 * @deprecated
	 */
	public function getTablespaceStorage();
	/**
	 * 
	 * @return string[]
	 * @deprecated
	 */
	public function getUnionTableNames();
	
}

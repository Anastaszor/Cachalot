<?php

/**
 * IColumnDefinitionStatement interface file.
 * 
 * Abstract interface, implementations are:
 * - ISimpleColumnDefinitionStatement
 * - IGeneratedColumnDefinitionStatement
 * 
 * @author Anastaszor
 */
interface IColumnDefinitionStatement extends IStatement
{
	/**
	 * 
	 * @return IDataTypeStatement
	 */
	public function getDataType();
	/**
	 *
	 * @return boolean
	 */
	public function getNotNull();
	/**
	 *
	 * @return boolean
	 */
	public function getUnique();
	/**
	 *
	 * @return boolean
	 */
	public function getPrimary();
	/**
	 *
	 * @return string
	 */
	public function getComment();
	
}

<?php

/**
 * IStatement interface file.
 *
 * Base interface for every statement class that should be able to be
 * executed by the engine. Those statement contasts with non-executable
 * ones, which represents only a generic part of a larger statement.
 *
 * @author Anastaszor
 */
interface IExecutableStatement extends IStatement
{
	
	/**
	 * Executes the statement.
	 * @param Executor the instance which will execute this statement.
	 * @return StatementExecutionResult
	 */
	public function execute(Executor $executor);
	
}

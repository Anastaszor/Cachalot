<?php

/**
 * IDatabase interface file.
 *
 * @author Anastaszor
 */
interface IDatabase 
{
	
	/**
	 * 
	 * @return the absolute physical path where this is stored.
	 */
	public function getPath();
	/**
	 * 
	 * @return string
	 */
	public function getName();
	/**
	 * 
	 * @return string
	 */
	public function getHash();
	/**
	 * 
	 * @return string
	 */
	public function getCharacterSet();
	/**
	 * 
	 * @return string
	 */
	public function getCollation();
	/**
	 * 
	 * @return IEngine
	 */
	public function getEngine();
	
}

<?php

/**
 * IDatabaseManager interface file.
 *
 * @author Anastaszor
 */
interface IDatabaseFactory
{
	
	/**
	 * 
	 * @param string $dbname
	 * @return IDatabase
	 */
	public function getDatabase($dbname);
	/**
	 * 
	 * @return IDatabase[]
	 */
	public function getDatabases();
	/**
	 * Creates a new database.
	 * @param ICreateDatabaseStatement $statement
	 * @return StatementExecutionResult
	 * @throws CachalotException if the database cannot be created.
	 */
	public function createDatabase(ICreateDatabaseStatement $statement);
	
	/**
	 * Drops a given database.
	 * @param IDropDatabaseStatement $statement
	 * @return StatementExecutionResult
	 * @throws CachalotException if the database cannot be dropped.
	 */
	public function dropDatabase(IDropDatabaseStatement $statement);
	
	/**
	 * Shows a list of databases.
	 * @param IShowDatabasesStatement $statement
	 * @return StatementExecutionResult
	 */
	public function showDatabases(IShowDatabasesStatement $statement);
	
}

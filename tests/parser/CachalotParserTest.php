<?php

/**
 * CachalotParserTest class file.
 * 
 * @author Anastaszor
 */
class CachalotParserTest extends PHPUnit_Framework_TestCase
{
	
	/**
	 * 
	 * @var CachalotParser
	 */
	private $_parser = null;
	/**
	 * 
	 * @var Cachalot
	 */
	private $_cachalot = null;
	
	public function setUp()
	{
		$this->_cachalot = Cachalot::connect("file://".dirname(dirname(__DIR__)).'/runtime');
		$this->_parser = new CachalotParser();
		$this->_parser->cachalot = $this->_cachalot;
	}
	
	public function tearDown()
	{
		$this->_parser = null;
	}
	
	public function test_parseShowDatabases_ok1()
	{
		$stt = $this->_parser->parse("SHOW DATABASES");
		
		$real = new ShowDatabasesStatement($this->_cachalot);
		
		$this->assertEquals(1, count($stt));
		$this->assertEquals($real, $stt[0]);
	}
	
	public function test_parseShowDatabases_ok2()
	{
		$stt = $this->_parser->parse("SHOW DATABASES;");
		
		$real = new ShowDatabasesStatement($this->_cachalot);
		
		$this->assertEquals(1, count($stt));
		$this->assertEquals($real, $stt[0]);
	}
	
	public function test_parseShowDatabases_ok3()
	{
		$stt = $this->_parser->parse("   SHOW DATABASES   ");
		
		$real = new ShowDatabasesStatement($this->_cachalot);
		
		$this->assertEquals(1, count($stt));
		$this->assertEquals($real, $stt[0]);
	}
	
	public function testCreateDatabase_ok1()
	{
		$stt = $this->_parser->parse("CREATE DATABASE nameofthedb");
		
		$real = new CreateDatabaseStatement($this->_cachalot);
		$real->setDatabaseName('nameofthedb');
		
		$this->assertEquals(1, count($stt));
		$this->assertEquals($real, $stt[0]);
	}
	
	public function testCreateDatabase_ok2()
	{
		$stt = $this->_parser->parse('CREATE DATABASE "nameofthedb"');
		
		$real = new CreateDatabaseStatement($this->_cachalot);
		$real->setDatabaseName('nameofthedb');
		
		$this->assertEquals(1, count($stt));
		$this->assertEquals($real, $stt[0]);
	}
	
	public function testCreateDatabase_ok3()
	{
		$stt = $this->_parser->parse("CREATE DATABASE 'nameofthedb'");
		
		$real = new CreateDatabaseStatement($this->_cachalot);
		$real->setDatabaseName('nameofthedb');
		
		$this->assertEquals(1, count($stt));
		$this->assertEquals($real, $stt[0]);
	}
	
	public function testCreateDatabase_ok4()
	{
		$stt = $this->_parser->parse("CREATE DATABASE `nameofthedb`");
		
		$real = new CreateDatabaseStatement($this->_cachalot);
		$real->setDatabaseName('nameofthedb');
		
		$this->assertEquals(1, count($stt));
		$this->assertEquals($real, $stt[0]);
	}
	
	public function testCreateDatabase_ok5()
	{
		$stt = $this->_parser->parse("CREATE DATABASE nameofthedb;");
		
		$real = new CreateDatabaseStatement($this->_cachalot);
		$real->setDatabaseName('nameofthedb');
		
		$this->assertEquals(1, count($stt));
		$this->assertEquals($real, $stt[0]);
	}
	
	public function testCreateDatabase_ok6()
	{
		$stt = $this->_parser->parse("CREATE DATABASE nameofthedb ;");
		
		$real = new CreateDatabaseStatement($this->_cachalot);
		$real->setDatabaseName('nameofthedb');
		
		$this->assertEquals(1, count($stt));
		$this->assertEquals($real, $stt[0]);
	}
	
	public function testCreateDatabase_ok7()
	{
		$stt = $this->_parser->parse("CREATE DATABASE 'nameofthedb';");
		
		$real = new CreateDatabaseStatement($this->_cachalot);
		$real->setDatabaseName('nameofthedb');
		
		$this->assertEquals(1, count($stt));
		$this->assertEquals($real, $stt[0]);
	}
	
	public function testCreateDatabase_ok8()
	{
		$stt = $this->_parser->parse("CREATE DATABASE `nameofthedb`;");
		
		$real = new CreateDatabaseStatement($this->_cachalot);
		$real->setDatabaseName('nameofthedb');
		
		$this->assertEquals(1, count($stt));
		$this->assertEquals($real, $stt[0]);
	}
	
	public function test_create_table_bigint_20()
	{
		$stt = $this->_parser->parse('CREATE TABLE tablename_bigint ( fieldname_bigint bigint(20) )');
		
		$real = new CreateTableStatement($this->_cachalot);
		$real->setTableName('tablename_bigint');
			$col1 = new CreateDefinitionStatement();
			$col1->setColumnName('fieldname_bigint');
				$def1 = new ColumnDefinitionStatement();
					$type1 = new BigintTypeStatement();
					$type1->setLength(20);
				$def1->setDataType($type1);
			$col1->setColumnDefinition($def1);
		$real->addCreateDefinitionStatement($col1);
		
		$this->assertEquals(1, count($stt));
		$this->assertEquals($real, $stt[0]);
	}
	
	public function test_create_table_binary_1024()
	{
		$stt = $this->_parser->parse('CREATE TABLE tablename_binary ( fieldname_binary binary(1024) )');
		
		$real = new CreateTableStatement($this->_cachalot);
		$real->setTableName('tablename_binary');
			$col1 = new CreateDefinitionStatement();
			$col1->setColumnName('fieldname_binary');
				$def1 = new ColumnDefinitionStatement();
					$type1 = new BinaryTypeStatement();
					$type1->setLength(1024);
				$def1->setDataType($type1);
			$col1->setColumnDefinition($def1);
		$real->addCreateDefinitionStatement($col1);
		
		$this->assertEquals(1, count($stt));
		$this->assertEquals($real, $stt[0]);
	}
	
	public function test_create_table_bit_512()
	{
		$stt = $this->_parser->parse('CREATE TABLE tablename_bit(fieldname_bit bit(512))');
		
		$real = new CreateTableStatement($this->_cachalot);
		$real->setTableName('tablename_bit');
			$col1 = new CreateDefinitionStatement();
			$col1->setColumnName('fieldname_bit');
				$def1 = new ColumnDefinitionStatement();
					$type1 = new BitTypeStatement();
					$type1->setLength(512);
				$def1->setDataType($type1);
			$col1->setColumnDefinition($def1);
		$real->addCreateDefinitionStatement($col1);
		
		$this->assertEquals(1, count($stt));
		$this->assertEquals($real, $stt[0]);
	}
	
	public function test_create_table_blob()
	{
		$stt = $this->_parser->parse('create table tablename_blob (fieldname_blob blob));');
		
		$real = new CreateTableStatement($this->_cachalot);
		$real->setTableName('tablename_blob');
			$col1 = new CreateDefinitionStatement();
			$col1->setColumnName('fieldname_blob');
				$def1 = new ColumnDefinitionStatement();
					$type1 = new BlobTypeStatement();
				$def1->setDataType($type1);
			$col1->setColumnDefinition($def1);
		$real->addCreateDefinitionStatement($col1);
		
		$this->assertEquals(1, count($stt));
		$this->assertEquals($real, $stt[0]);
	}
	
	public function test_create_table_int_11()
	{
		$stt = $this->_parser->parse('CREATE TABLE tablename ( fieldname int(11) )');
		
		$real = new CreateTableStatement($this->_cachalot);
		$real->setTableName('tablename');
			$col1 = new CreateDefinitionStatement();
			$col1->setColumnName('fieldname');
				$def1 = new ColumnDefinitionStatement();
					$type1 = new IntTypeStatement();
					$type1->setLength(11);
				$def1->setDataType($type1);
			$col1->setColumnDefinition($def1);
		$real->addCreateDefinitionStatement($col1);
		
		$this->assertEquals(1, count($stt));
		$this->assertEquals($real, $stt[0]);
	}
	
}

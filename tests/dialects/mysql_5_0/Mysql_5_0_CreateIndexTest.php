<?php

/**
 * This class tests all variations of CREATE INDEX statement rendering
 * using the Mysql_5_0_Dialect.
 * 
 * @author Anastaszor
 */
class Mysql_5_0_CreateIndexTest extends Mysql_5_0_AbstractTest
{
	
	public function test_create_index_unique_nocol_btree()
	{
		$statement = new CreateIndexStatement();
		$statement->setIndexSpace(ICreateIndexStatement::UNIQUE);
		$statement->setIndexName('myindex');
		$statement->setTableName('mytable');
		$statement->setIndexType(ICreateIndexStatement::BTREE);
		
		$actual = $this->_dialect->createIndex($statement);
		
		$this->assertEquals('CREATE UNIQUE INDEX `myindex` ON `mytable` USING BTREE;', $actual);
	}
	
	public function test_create_index_cols()
	{
		$statement = new CreateIndexStatement();
		$statement->setIndexName('myindex');
		$statement->setTableName('mytable');
		$col1 = new ColumnIndexStatement();
		$col1->setColumnName('mycolumn1');
		$col1->setLength(10);
		$col1->setOrder(IColumnIndexStatement::DESC);
		$statement->addColumnIndexStatement($col1);
		$col2 = new ColumnIndexStatement();
		$col2->setColumnName('mycolumn2');
		$statement->addColumnIndexStatement($col2);
		
		$actual = $this->_dialect->createIndex($statement);
		
		$this->assertEquals('CREATE INDEX `myindex` ON `mytable` (`mycolumn1` (10) DESC, `mycolumn2`);', $actual);
	}
}

<?php

/**
 * This class tests all variations of CREATE DATABASE statement rendering
 * using the Mysql_5_0_Dialect.
 *
 * @author Anastaszor
 */
class Mysql_5_0_CreateDatabaseTest extends Mysql_5_0_AbstractTest
{
	
	public function test_create_database_ifnotexists()
	{
		$statement = new CreateDatabaseStatement();
		$statement->setDatabaseName('mydatabase');
		$statement->setIfNotExists(true);
		
		$actual = $this->_dialect->createDatabase($statement);
		
		$this->assertEquals('CREATE DATABASE IF NOT EXISTS `mydatabase`;', $actual);
	}
	
	public function test_create_database_charset()
	{
		$statement = new CreateDatabaseStatement();
		$statement->setDatabaseName('mydatabase');
		$spec = new CharsetSpecificationStatement();
		$spec->setCharset(new Utf8Charset());
		$statement->addSpecification($spec);
		
		$actual = $this->_dialect->createDatabase($statement);
		
		$this->assertEquals('CREATE DATABASE `mydatabase` CHARACTER SET utf-8;', $actual);
	}
	
	public function test_create_database_collation()
	{
		$statement = new CreateDatabaseStatement();
		$statement->setDatabaseName('mydatabase');
		$spec = new CollationSpecificationStatement();
		$spec->setCollation(new Utf8_general_csCollation());
		$statement->addSpecification($spec);
		
		$actual = $this->_dialect->createDatabase($statement);
		
		$this->assertEquals('CREATE DATABASE `mydatabase` COLLATE utf8_general_cs;', $actual);
	}
	
	public function test_create_database_charset_collation()
	{
		$statement = new CreateDatabaseStatement();
		$statement->setDatabaseName('mydatabase');
		$spec = new CharsetSpecificationStatement();
		$spec->setCharset(new Utf8Charset());
		$statement->addSpecification($spec);
		$spec = new CollationSpecificationStatement();
		$spec->setCollation(new Utf8_general_csCollation());
		$statement->addSpecification($spec);
		
		$actual = $this->_dialect->createDatabase($statement);
		
		$this->assertEquals('CREATE DATABASE `mydatabase` CHARACTER SET utf-8 COLLATE utf8_general_cs;', $actual);
	}
	
}

<?php

/**
 * This class tests all variations of ALTER PROCEDURE statement rendering
 * using the Mysql_5_0_Dialect.
 *
 * @author Anastaszor
 */
class Mysql_5_0_AlterProcedureTest extends Mysql_5_0_AbstractTest
{
	
	/**
	 * Tests the rendering of alter procedure with comment specification
	 */
	public function test_alter_procedure_comment()
	{
		$statement = new AlterProcedureStatement();
		$statement->setProcedureName('myprocedure');
		$fccarac = new CommentCharacteristicStatement();
		$fccarac->setCommentValue('Here\'s a comment for this procedure.');
		$statement->addCharacteristic($fccarac);
	
		$actual = $this->_dialect->alterProcedure($statement);
	
		$this->assertEquals(
			'ALTER PROCEDURE `myprocedure` COMMENT "Here\'s a comment for this procedure.";',
			$actual
		);
	}
	
	/**
	 * Tests the rendering of alter procedure with language specification
	 */
	public function test_alter_procedure_language()
	{
		$statement = new AlterProcedureStatement();
		$statement->setProcedureName('myprocedure');
		$statement->addCharacteristic(new LanguageCharacteristicStatement());
	
		$actual = $this->_dialect->alterProcedure($statement);
	
		$this->assertEquals(
			'ALTER PROCEDURE `myprocedure` LANGUAGE SQL;',
			$actual
		);
	}
	
	/**
	 * Tests the rendering of alter procedure with contains sql characteristic
	 */
	public function test_alter_procedure_contains_sql()
	{
		$statement = new AlterProcedureStatement();
		$statement->setProcedureName('myprocedure');
		$carac = new SqlCharacteristicStatement();
		$carac->setSqlModifier(ISqlCharacteristicStatement::CONTAINS_SQL);
		$statement->addCharacteristic($carac);
	
		$actual = $this->_dialect->alterProcedure($statement);
	
		$this->assertEquals(
			'ALTER PROCEDURE `myprocedure` CONTAINS SQL;',
			$actual
		);
	}
	
	/**
	 * Tests the rendering of alter procedure with no sql characteristic
	 */
	public function test_alter_procedure_no_sql()
	{
		$statement = new AlterProcedureStatement();
		$statement->setProcedureName('myprocedure');
		$carac = new SqlCharacteristicStatement();
		$carac->setSqlModifier(ISqlCharacteristicStatement::NO_SQL);
		$statement->addCharacteristic($carac);
	
		$actual = $this->_dialect->alterProcedure($statement);
	
		$this->assertEquals(
			'ALTER PROCEDURE `myprocedure` NO SQL;',
			$actual
		);
	}
	
	/**
	 * Tests the rendering of alter procedure with reads sql data characteristic
	 */
	public function test_alter_procedure_reads_sql_data()
	{
		$statement = new AlterProcedureStatement();
		$statement->setProcedureName('myprocedure');
		$carac = new SqlCharacteristicStatement();
		$carac->setSqlModifier(ISqlCharacteristicStatement::READS_SQL_DATA);
		$statement->addCharacteristic($carac);
	
		$actual = $this->_dialect->alterProcedure($statement);
	
		$this->assertEquals(
			'ALTER PROCEDURE `myprocedure` READS SQL DATA;',
			$actual
		);
	}
	
	/**
	 * Tests the rendering of alter procedure with modifies sql data characteristic
	 */
	public function test_alter_procedure_modifies_sql_data()
	{
		$statement = new AlterProcedureStatement();
		$statement->setProcedureName('myprocedure');
		$carac = new SqlCharacteristicStatement();
		$carac->setSqlModifier(ISqlCharacteristicStatement::MODIFIES_SQL_DATA);
		$statement->addCharacteristic($carac);
	
		$actual = $this->_dialect->alterProcedure($statement);
	
		$this->assertEquals(
			'ALTER PROCEDURE `myprocedure` MODIFIES SQL DATA;',
			$actual
		);
	}
	
	// should throw exception : unsupported statement (Deterministic) for my5.0
	/**
	 * Tests the rendering of alter procedure with deterministic characteristic
	 */
	// 	public function test_alter_procedure_deterministic()
	// 	{
	// 		$statement = new AlterProcedureStatement();
	// 		$statement->setProcedureName('myprocedure');
	// 		$carac = new DeterministicCharacteristicStatement();
	// 		$carac->setNot(false);
	// 		$statement->addCharacteristic($carac);
	
	
	// 		$actual = $this->_dialect->alterProcedure($statement);
	// 	}
	
	/**
	 * Tests the rendering of alter procedure with security definer characteristic
	 */
	public function test_alter_procedure_security_definer()
	{
		$statement = new AlterProcedureStatement();
		$statement->setProcedureName('myprocedure');
		$carac = new SecurityCharacteristicStatement();
		$carac->setSecurityParam(ISecurityCharacteristicStatement::DEFINER);
		$statement->addCharacteristic($carac);
	
		$actual = $this->_dialect->alterProcedure($statement);
	
		$this->assertEquals(
			'ALTER PROCEDURE `myprocedure` SQL SECURITY DEFINER;',
			$actual
		);
	}
	
	/**
	 * Tests the rendering of alter procedure with security invoker characteristic
	 */
	public function test_alter_procedure_security_invoker()
	{
		$statement = new AlterProcedureStatement();
		$statement->setProcedureName('myprocedure');
		$carac = new SecurityCharacteristicStatement();
		$carac->setSecurityparam(ISecurityCharacteristicStatement::INVOKER);
		$statement->addCharacteristic($carac);
	
		$actual = $this->_dialect->alterProcedure($statement);
	
		$this->assertEquals(
			'ALTER PROCEDURE `myprocedure` SQL SECURITY INVOKER;',
			$actual
		);
	}
	
}

<?php

/**
 * This class tests all variations of CREATE PROCEDURE statement rendering
 * using the Mysql_5_0_Dialect.
 *
 * @author Anastaszor
 */
class Mysql_5_0_CreateProcedureTest extends Mysql_5_0_AbstractTest
{
	
	public function test_create_procedure_definer_noarg_nocarac_sbody()
	{
		$statement = new CreateProcedureStatement();
		$statement->setDefiner(ICreateProcedureStatement::DEFINER_CU);
		$statement->setProcedureName('myprocedure');
		$body = new ProcedureBodyStatement();
		$statement->setBody($body);
		
		$actual = $this->_dialect->createProcedure($statement);
		
		$this->assertEquals('CREATE DEFINER = CURRENT_USER PROCEDURE `myprocedure` () BEGIN  END;', $actual);
	}
	
	public function test_create_procedure_user_args_carac_sbody()
	{
		$statement = new CreateProcedureStatement();
		$statement->setDefiner("iamthedefiner");
		$statement->setProcedureName('myprocedure');
		$statement->addCharacteristics(new DeterministicCharacteristicStatement());
		$statement->addCharacteristics(new LanguageCharacteristicStatement());
		$param1 = new ProcedureParamStatement();
		$param1->setParamName('param1');
		$param1->setDataType(new IntTypeStatement());
		$statement->addParam($param1);
		$param2 = new ProcedureParamStatement();
		$param2->setParamName('param2');
		$param2->setDataType(new FloatTypeStatement());
		$param2->setType(IProcedureParamStatement::INOUT);
		$statement->addParam($param2);
		$body = new ProcedureBodyStatement();
		$statement->setBody($body);
		
		$actual = $this->_dialect->createProcedure($statement);
		
		$this->assertEquals('CREATE DEFINER = "iamthedefiner" PROCEDURE `myprocedure` (`param1` INT(11), INOUT `param2` FLOAT(13,2)) DETERMINISTIC LANGUAGE SQL BEGIN  END;', $actual);
	}
	
}

<?php

/**
 * This class tests all variations of ALTER DATABASE statement rendering
 * using the Mysql_5_0_Dialect.
 * 
 * @author Anastaszor
 */
class Mysql_5_0_AlterDatabaseTest extends Mysql_5_0_AbstractTest
{
	
	/**
	 * Tests the rendering of alter database with character set specification
	 */
	public function test_alter_database_character_set()
	{
		$statement = new AlterDatabaseStatement();
		$statement->setDatabaseName('mydatabase');
		$charsetsp = new CharsetSpecificationStatement();
		$charsetsp->setCharset(new Utf8Charset());
		$statement->setSpecification($charsetsp);
		
		$actual = $this->_dialect->alterDatabase($statement);
		
		$this->assertEquals(
			"ALTER DATABASE `mydatabase` CHARACTER SET utf-8;", 
			$actual
		);
	}
	
	/**
	 * Tests the rendering of alter database with collation specification
	 */
	public function test_alter_database_collation()
	{
		$statement = new AlterDatabaseStatement();
		$statement->setDatabaseName('mydatabase');
		$collationsp = new CollationSpecificationStatement();
		$collationsp->setCollation(new Utf8_general_csCollation());
		$statement->setSpecification($collationsp);
		
		$actual = $this->_dialect->alterDatabase($statement);
		
		$this->assertEquals(
			"ALTER DATABASE `mydatabase` COLLATE utf8_general_cs;", 
			$actual
		);
	}
	
}

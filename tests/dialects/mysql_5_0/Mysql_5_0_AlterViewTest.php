<?php

/**
 * This class tests all variations of ALTER VIEW statement rendering
 * using the Mysql_5_0_Dialect.
 *
 * @author Anastaszor
 */
class Mysql_5_0_AlterViewTest extends Mysql_5_0_AbstractTest
{
	
	public function test_alter_view_merge_currentuser_definer_select1_cascaded()
	{
		$statement = new AlterViewStatement();
		$statement->setAlgorithm(IAlterViewStatement::ALGO_MERGE);
		$statement->setDefiner(IAlterViewStatement::DEFINER_CU);
		$statement->setSecurity(IAlterViewStatement::SEC_DEFINER);
		$statement->setViewName('myview');
		$select = new SelectStatement();
		$select->addSelectExpression(new NumericExpressionStatement("1"));
		$statement->setSelectStatement($select);
		$statement->setCheckOption(true);
		$statement->setCheckOptionMode(IAlterViewStatement::CHK_OPT_CASCADED);
		
		$actual = $this->_dialect->alterView($statement);
		
		$this->assertEquals('ALTER ALGORITHM = MERGE DEFINER = CURRENT_USER SQL SECURITY DEFINER VIEW `myview` AS SELECT 1 WITH CASCADED CHECK OPTION;', $actual);
	}
	
	public function test_alter_view_undefiner_username_invoker_select1_nooptionmode()
	{
		$statement = new AlterViewStatement();
		$statement->setAlgorithm(IAlterViewStatement::ALGO_UNDEFINED);
		$statement->setDefiner("iamthedefiner");
		$statement->setSecurity(IAlterViewStatement::SEC_INVOKER);
		$statement->setViewName('myview');
		$select = new SelectStatement();
		$select->addSelectExpression(new NumericExpressionStatement("1"));
		$statement->setSelectStatement($select);
		$statement->setCheckOption(true);
		
		$actual = $this->_dialect->alterView($statement);
		
		$this->assertEquals('ALTER ALGORITHM = UNDEFINED DEFINER = "iamthedefiner" SQL SECURITY INVOKER VIEW `myview` AS SELECT 1 WITH CHECK OPTION;', $actual);
	}
	
	public function test_alter_view_select1()
	{
		$statement = new AlterViewStatement();
		$statement->setViewName('myview');
		$select = new SelectStatement();
		$select->addSelectExpression(new NumericExpressionStatement("1"));
		$statement->setSelectStatement($select);
		
		$actual = $this->_dialect->alterView($statement);
		
		$this->assertEquals('ALTER VIEW `myview` AS SELECT 1;', $actual);
	}
	
}

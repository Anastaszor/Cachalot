<?php

/**
 * This class tests all variations of ALTER TABLE statement rendering
 * using the Mysql_5_0_Dialect.
 * 
 * @author Anastaszor
 */
class Mysql_5_0_AlterTableTest extends Mysql_5_0_AbstractTest
{
	
	public function test_alter_ignore_table_option_engine()
	{
		$statement = new AlterTableStatement();
		$statement->setIgnore(true);
		$statement->setTableName('mytable');
		$spec = new TableOptionsSpecificationStatement();
		$opt = new EngineTableOptionStatement();
		$opt->setEngine(new StdPhpEngine());
		$spec->addTableOptionStatement($opt);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER IGNORE TABLE `mytable` ENGINE StdPhp;', $actual);
	}
	
	public function test_alter_table_option_auto_increment()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new TableOptionsSpecificationStatement();
		$opt = new AutoIncrementTableOptionStatement();
		$opt->setValue(255);
		$spec->addTableOptionStatement($opt);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` AUTO_INCREMENT 255;', $actual);
	}
	
	public function test_alter_table_option_avg_row_length()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new TableOptionsSpecificationStatement();
		$opt = new AvgRowLengthTableOptionStatement();
		$opt->setValue(150);
		$spec->addTableOptionStatement($opt);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` AVG_ROW_LENGTH 150;', $actual);
	}
	
	public function test_alter_table_option_charset()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new TableOptionsSpecificationStatement();
		$opt = new CharsetTableOptionStatement();
		$opt->setCharset(new Utf8Charset());
		$spec->addTableOptionStatement($opt);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` CHARSET utf-8;', $actual);
	}
	
	public function test_alter_table_option_checksum()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new TableOptionsSpecificationStatement();
		$opt = new ChecksumTableOptionStatement();
		$opt->setEnabled(false);
		$spec->addTableOptionStatement($opt);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` CHECKSUM 0;', $actual);
	}
	
	public function test_alter_table_option_collation()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new TableOptionsSpecificationStatement();
		$opt = new CollationTableOptionStatement();
		$opt->setCollation(new Utf8_general_csCollation());
		$spec->addTableOptionStatement($opt);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` COLLATION utf8_general_cs;', $actual);
	}
	
	public function test_alter_table_option_comment()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new TableOptionsSpecificationStatement();
		$opt = new CommentTableOptionStatement();
		$opt->setValue('a comment for "my" table;');
		$spec->addTableOptionStatement($opt);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` COMMENT "a comment for \"my\" table;";', $actual);
	}
	
	public function test_alter_table_option_connection()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new TableOptionsSpecificationStatement();
		$opt = new ConnectionTableOptionStatement();
		$opt->setConnectionString('mysql:host=localhost;dbname=mydatabase');
		$spec->addTableOptionStatement($opt);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` CONNECTION "mysql:host=localhost;dbname=mydatabase";', $actual);
		
	}
	
	public function test_alter_table_option_data_directory()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new TableOptionsSpecificationStatement();
		$opt = new DataDirectoryTableOptionStatement();
		$opt->setAbsolutePathToDirectory('/var/www/path/to/directory');
		$spec->addTableOptionStatement($opt);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` DATA DIRECTORY "/var/www/path/to/directory";', $actual);
	}
	
	public function test_alter_table_option_delay_key_write()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new TableOptionsSpecificationStatement();
		$opt = new DelayKeyWriteTableOptionStatement();
		$opt->setEnabled(false);
		$spec->addTableOptionStatement($opt);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` DELAY_KEY_WRITE 0;', $actual);
	}
	
	public function test_alter_table_option_index_directory()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new TableOptionsSpecificationStatement();
		$opt = new IndexDirectoryTableOptionStatement();
		$opt->setAbsolutePathToDirectory('/var/www/path/to/directory');
		$spec->addTableOptionStatement($opt);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` INDEX DIRECTORY "/var/www/path/to/directory";', $actual);
	}
	
	public function test_alter_table_option_insert_method()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new TableOptionsSpecificationStatement();
		$opt = new InsertMethodTableOptionStatement();
		$opt->setValue(IInsertMethodTableOptionStatement::MTD_FIRST);
		$spec->addTableOptionStatement($opt);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` INSERT_METHOD FIRST;', $actual);
	}
	
	public function test_alter_table_option_max_rows()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new TableOptionsSpecificationStatement();
		$opt = new MaxRowsTableOptionStatement();
		$opt->setValue(200);
		$spec->addTableOptionStatement($opt);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` MAX_ROWS 200;', $actual);
	}
	
	public function test_alter_table_option_min_rows()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new TableOptionsSpecificationStatement();
		$opt = new MinRowsTableOptionStatement();
		$opt->setValue(200);
		$spec->addTableOptionStatement($opt);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` MIN_ROWS 200;', $actual);
	}
	
	public function test_alter_table_option_pack_keys()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new TableOptionsSpecificationStatement();
		$opt = new PackKeysTableOptionStatement();
		$opt->setValue(IPackKeysTableOptionStatement::_DEFAULT);
		$spec->addTableOptionStatement($opt);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` PACK_KEYS DEFAULT;', $actual);
	}
	
	public function test_alter_table_option_password()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new TableOptionsSpecificationStatement();
		$opt = new PasswordTableOptionStatement();
		$opt->setValue('vgyè"_9^$`\'\\c*');
		$spec->addTableOptionStatement($opt);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` PASSWORD "vgyè\"_9^$`\'\\\\c*";', $actual);
	}
	
	public function test_alter_table_option_row_format()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new TableOptionsSpecificationStatement();
		$opt = new RowFormatTableOptionStatement();
		$opt->setValue(IRowFormatTableOptionStatement::COMPRESSED);
		$spec->addTableOptionStatement($opt);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ROW_FORMAT COMPRESSED;', $actual);
	}
	
	public function test_alter_table_option_union()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new TableOptionsSpecificationStatement();
		$opt = new UnionTableOptionStatement();
		$opt->addTableName('yourtable1');
		$opt->addTableName('yourtable2');
		$spec->addTableOptionStatement($opt);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` UNION `yourtable1`, `yourtable2`;', $actual);
	}
	
	public function test_alter_table_add_column_bigint_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new BigintTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$place = new ColumnPositionFirstStatement();
		$spec->setColumnPositionStatement($place);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` BIGINT(20) NOT NULL DEFAULT 0 FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_binary_after_column()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new BinaryTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$place = new ColumnPositionAfterStatement();
		$place->setColumnName('othercolumn');
		$spec->setColumnPositionStatement($place);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` BINARY(1024) NOT NULL DEFAULT \'\' AFTER `othercolumn`;', $actual);
	}
	
	public function test_alter_table_add_column_bit_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new BitTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` BIT(255) NOT NULL DEFAULT \'\' FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_blob_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new BlobTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` BLOB DEFAULT NULL FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_char_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new CharTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` CHAR(255) NOT NULL DEFAULT \'\' FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_datetime_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new DatetimeTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_date_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new DateTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` DATE NOT NULL DEFAULT CURRENT_TIMESTAMP FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_decimal_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new DecimalTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` DECIMAL(13,2) NOT NULL DEFAULT 0 FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_double_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new DoubleTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` DOUBLE(13,2) NOT NULL DEFAULT 0 FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_enum_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$type = new EnumTypeStatement();
		$type->addValue('value_1');
		$type->addValue('value_2');
		$coldef->setDataType($type);
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` ENUM("value_1", "value_2") NOT NULL DEFAULT "value_1" FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_float_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new FloatTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` FLOAT(13,2) NOT NULL DEFAULT 0 FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_int_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new IntTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` INT(11) NOT NULL DEFAULT 0 FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_longblob_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new LongblobTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` LONGBLOB DEFAULT NULL FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_longtext_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new LongtextTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` LONGTEXT DEFAULT NULL FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_mediumblob_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new MediumblobTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
	
		$actual = $this->_dialect->alterTable($statement);
	
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` MEDIUMBLOB DEFAULT NULL FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_mediumtext_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new MediumtextTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` MEDIUMTEXT DEFAULT NULL FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_mediumint_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new MediumintTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` MEDIUMINT(6) NOT NULL DEFAULT 0 FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_numeric_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new NumericTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` NUMERIC(13,2) NOT NULL DEFAULT 0 FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_real_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new RealTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` REAL(13,2) NOT NULL DEFAULT 0 FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_set_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$type = new SetTypeStatement();
		$type->addValue('value_1');
		$type->addValue('value_2');
		$coldef->setDataType($type);
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` SET("value_1", "value_2") NOT NULL DEFAULT "value_1" FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_smallint_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new SmallintTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` SMALLINT(3) NOT NULL DEFAULT 0 FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_text_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new TextTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` TEXT DEFAULT NULL FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_timestamp_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new TimestampTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_time_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new TimeTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` TIME NOT NULL DEFAULT CURRENT_TIMESTAMP FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_tinyblob_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new TinyblobTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` TINYBLOB DEFAULT NULL FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_tinyint_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new TinyintTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` TINYINT(1) NOT NULL DEFAULT 0 FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_tinytext_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new TinytextTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` TINYTEXT DEFAULT NULL FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_varbinary_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new VarbinaryTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` VARBINARY(1024) NOT NULL DEFAULT \'\' FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_varchar_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new VarcharTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` VARCHAR(255) NOT NULL DEFAULT \'\' FIRST;', $actual);
	}
	
	public function test_alter_table_add_column_year_first()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new YearTypeStatement());
		$spec->setColumnDefinitionStatement($coldef);
		$spec->setColumnPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD COLUMN `mycolumn` YEAR NOT NULL DEFAULT '.date('Y').' FIRST;', $actual);
	}
	
	public function test_alter_table_add_index_hash()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddIndexSpecificationStatement();
		$spec->setIndexName('myindex');
		$spec->setIndexType(IAddIndexSpecificationStatement::HASH);
		$col1 = new IndexColumnNameSpecificationStatement();
		$col1->setColumnName('mycolumn1');
		$col1->setOrder(IIndexColumnNameSpecificationStatement::ASC);
		$spec->addColumn($col1);
		$col2 = new IndexColumnNameSpecificationStatement();
		$col2->setColumnName('mycolumn2');
		$col2->setLength(20);
		$spec->addColumn($col2);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD INDEX `myindex` (`mycolumn1` ASC, `mycolumn2` (20)) USING HASH;', $actual);
	}
	
	public function test_alter_table_add_index_btree()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddIndexSpecificationStatement();
		$spec->setIndexName('myindex');
		$spec->setIndexType(IAddIndexSpecificationStatement::BTREE);
		$col1 = new IndexColumnNameSpecificationStatement();
		$col1->setColumnName('mycolumn1');
		$col1->setOrder(IIndexColumnNameSpecificationStatement::DESC);
		$spec->addColumn($col1);
		$col2 = new IndexColumnNameSpecificationStatement();
		$col2->setColumnName('mycolumn2');
		$col2->setLength(40);
		$spec->addColumn($col2);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD INDEX `myindex` (`mycolumn1` DESC, `mycolumn2` (40)) USING BTREE;', $actual);
	}
	
	public function test_alter_table_add_constraint_unique_btree()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddConstraintSpecificationStatement();
		$spec->setSymbol('mysymbol');
		$spec->setConstraintType(IAddConstraintSpecificationStatement::UNIQUE);
		$spec->setIndexType(IAddConstraintSpecificationStatement::BTREE);
		$spec->setIndexName('myindex');
		$column1 = new IndexColumnNameSpecificationStatement();
		$column1->setColumnName('column_1');
		$spec->addIndexColumn($column1);
		$column2 = new IndexColumnNameSpecificationStatement();
		$column2->setColumnName('column_2');
		$spec->addIndexColumn($column2);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD CONSTRAINT `mysymbol` UNIQUE KEY `myindex` (`column_1`, `column_2`) USING BTREE;', $actual);
	}
	
	public function test_alter_table_add_constraint_primary_hash()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddConstraintSpecificationStatement();
		$spec->setSymbol('mysymbol');
		$spec->setConstraintType(IAddConstraintSpecificationStatement::PRIMARY);
		$spec->setIndexType(IAddConstraintSpecificationStatement::HASH);
		$spec->setIndexName('myindex');
		$column1 = new IndexColumnNameSpecificationStatement();
		$column1->setColumnName('column_1');
		$spec->addIndexColumn($column1);
		$column2 = new IndexColumnNameSpecificationStatement();
		$column2->setColumnName('column_2');
		$spec->addIndexColumn($column2);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD CONSTRAINT `mysymbol` PRIMARY KEY (`column_1`, `column_2`) USING HASH;', $actual);
	}
	
	public function test_alter_table_add_foreign_key_onup_cascade_ondel_restrict()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AddForeignKeySpecificationStatement();
		$spec->setSymbol('mysymbol');
		$spec->setIndexName('myindex');
		$column1 = new IndexColumnNameSpecificationStatement();
		$column1->setColumnName('column_1');
		$spec->addIndexColumn($column1);
		$column2 = new IndexColumnNameSpecificationStatement();
		$column2->setColumnName('column_2');
		$spec->addIndexColumn($column2);
		$spec->setTargetTableName('otable');
		$spec->addTargetIndexName('ocolumn1');
		$spec->addTargetIndexName('ocolumn2');
		$spec->setOnUpdateConstraint(IAddForeignKeySpecificationStatement::CASCADE);
		$spec->setOnDeleteConstraint(IAddForeignKeySpecificationStatement::RESTRICT);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ADD CONSTRAINT `mysymbol` FOREIGN KEY `myindex` (`column_1`, `column_2`) REFERENCES `otable` (`ocolumn1`, `ocolumn2`) ON UPDATE CASCADE ON DELETE RESTRICT;', $actual);
	}
	
	public function test_alter_table_alter_column_set_default_null()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AlterColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$spec->setDefaultValue(null);
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ALTER COLUMN `mycolumn` DROP DEFAULT;', $actual);
	}
	
	public function test_alter_table_alter_column_set_value()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AlterColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$spec->setDefaultValue('this is a string');
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ALTER COLUMN `mycolumn` SET DEFAULT "this is a string";', $actual);
	}
	
	public function test_alter_table_alter_column_set_array()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new AlterColumnSpecificationStatement();
		$spec->setColumnName('mycolumn');
		$spec->setDefaultValue(array('this is a string', 144));
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ALTER COLUMN `mycolumn` SET DEFAULT ("this is a string", 144);', $actual);
	}
	
	public function test_alter_table_change_column()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new ChangeColumnSpecificationStatement();
		$spec->setOldColumnName('oldcolumnname');
		$spec->setNewColumnName('newcolumnname');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new VarcharTypeStatement());
		$spec->setColumnDefinition($coldef);
		$spec->setPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` CHANGE COLUMN `oldcolumnname` `newcolumnname` VARCHAR(255) NOT NULL DEFAULT \'\' FIRST;', $actual);
	}
	
	public function test_alter_table_modify_column()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new ModifyColumnSpecificationStatement();
		$spec->setColumnName('columnname');
		$coldef = new ColumnDefinitionStatement();
		$coldef->setDataType(new VarcharTypeStatement());
		$spec->setColumnDefinition($coldef);
		$spec->setPositionStatement(new ColumnPositionFirstStatement());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` MODIFY COLUMN `columnname` VARCHAR(255) NOT NULL DEFAULT \'\' FIRST;', $actual);
	}
	
	public function test_alter_table_drop_column()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new DropColumnSpecificationStatement();
		$spec->setColumnName('droppedcolumn');
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` DROP COLUMN `droppedcolumn`;', $actual);
	}
	
	public function test_alter_table_drop_pk()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new DropPrimarySpecificationStatement();
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` DROP PRIMARY KEY;', $actual);
	}
	
	public function test_alter_table_drop_index()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new DropIndexSpecificationStatement();
		$spec->setIndexName('myindex');
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` DROP INDEX `myindex`;', $actual);
	}
	
	public function test_alter_table_drop_fk()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new DropForeignSpecificationStatement();
		$spec->setForeignKeySymbol('myforeignkey');
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` DROP FOREIGN KEY `myforeignkey`;', $actual);
	}
	
	public function test_alter_table_disable_keys()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new DisableKeysSpecificationStatement();
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` DISABLE KEYS;', $actual);
	}
	
	public function test_alter_table_enable_keys()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new EnableKeysSpecificationStatement();
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ENABLE KEYS;', $actual);
	}
	
	public function test_alter_table_rename()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new RenameSpecificationStatement();
		$spec->setNewTableName('newmytable');
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` RENAME TO `newmytable`;', $actual);
	}
	
	public function test_alter_table_order_by()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new OrderBySpecificationStatement();
		$spec->addTableName('otable1');
		$spec->addTableName('otable2');
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` ORDER BY `otable1`, `otable2`;', $actual);
	}
	
	public function test_alter_table_convert_charset()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new ConvertCharsetSpecificationStatement();
		$spec->setCharset(new Utf8Charset());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` CONVERT TO CHARACTER SET utf-8;', $actual);
	}
	
	public function test_alter_table_convert_collation()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new ConvertCharsetSpecificationStatement();
		$spec->setCollation(new Utf8_general_csCollation());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` CONVERT TO CHARACTER SET utf-8 COLLATE utf8_general_cs;', $actual);
	}
	
	public function test_alter_table_default_charset()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new DefaultCharsetSpecificationStatement();
		$spec->setCharset(new Utf8Charset());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` DEFAULT CHARACTER SET utf-8;', $actual);
	}
	
	public function test_alter_table_default_collation()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$spec = new DefaultCharsetSpecificationStatement();
		$spec->setCollation(new Utf8_general_csCollation());
		$statement->addTableSpecification($spec);
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` DEFAULT CHARACTER SET utf-8 COLLATE utf8_general_cs;', $actual);
	}
	
	public function test_alter_table_discard_tablespace()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$statement->addTableSpecification(new DiscardTablespaceSpecificationStatement());
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` DISCARD TABLESPACE;', $actual);
	}
	
	public function test_alter_table_import_tablespace()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('mytable');
		$statement->addTableSpecification(new ImportTablespaceSpecificationStatement());
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `mytable` IMPORT TABLESPACE;', $actual);
	}
	
	public function test_alter_table_discard_import_tablespace()
	{
		$statement = new AlterTableStatement();
		$statement->setTableName('dualtable');
		$statement->addTableSpecification(new DiscardTablespaceSpecificationStatement());
		$statement->addTableSpecification(new ImportTablespaceSpecificationStatement());
		
		$actual = $this->_dialect->alterTable($statement);
		
		$this->assertEquals('ALTER TABLE `dualtable` DISCARD TABLESPACE, IMPORT TABLESPACE;', $actual);
	}
	
}

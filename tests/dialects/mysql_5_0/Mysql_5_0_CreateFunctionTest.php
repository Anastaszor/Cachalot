<?php

/**
 * This class tests all variations of CREATE FUNCTION statement rendering
 * using the Mysql_5_0_Dialect.
 *
 * @author Anastaszor
 */
class Mysql_5_0_CreateFunctionTest extends Mysql_5_0_AbstractTest
{
	
	public function test_create_function_definer_noarg_nocarac_sbody()
	{
		$statement = new CreateFunctionStatement();
		$statement->setDefiner(ICreateFunctionStatement::DEFINER_CU);
		$statement->setFunctionName('myfunction');
		$statement->setReturnType(new IntTypeStatement());
		$body = new FunctionBodyStatement();
		$body->addInstruction(new FunctionReturnStatement(new NumericExpressionStatement("1")));
		$statement->setBody($body);
		
		$actual = $this->_dialect->createFunction($statement);
		
		$this->assertEquals('CREATE DEFINER = CURRENT_USER FUNCTION `myfunction` () RETURNS INT(11) BEGIN RETURN 1; END;', $actual);
	}
	
	public function test_create_function_user_args_carac_sbody()
	{
		$statement = new CreateFunctionStatement();
		$statement->setDefiner("iamthedefiner");
		$statement->setFunctionName('myfunction');
		$statement->setReturnType(new FloatTypeStatement());
		$statement->addCharacteristics(new DeterministicCharacteristicStatement());
		$statement->addCharacteristics(new LanguageCharacteristicStatement());
		$param1 = new FunctionParamStatement();
		$param1->setParamName('param1');
		$param1->setDataType(new IntTypeStatement());
		$statement->addParam($param1);
		$param2 = new FunctionParamStatement();
		$param2->setParamName('param2');
		$param2->setDataType(new FloatTypeStatement());
		$statement->addParam($param2);
		$body = new FunctionBodyStatement();
		$body->addInstruction(new FunctionReturnStatement(new NumericExpressionStatement("1.5")));
		$statement->setBody($body);
		
		$actual = $this->_dialect->createFunction($statement);
		
		$this->assertEquals('CREATE DEFINER = "iamthedefiner" FUNCTION `myfunction` (`param1` INT(11), `param2` FLOAT(13,2)) RETURNS FLOAT(13,2) DETERMINISTIC LANGUAGE SQL BEGIN RETURN 1.5; END;', $actual);
	}
	
}

<?php

/**
 * This class tests all variations of ALTER FUNCTION statement rendering
 * using the Mysql_5_0_Dialect.
 * 
 * @author Anastaszor
 */
class Mysql_5_0_AlterFunctionTest extends Mysql_5_0_AbstractTest
{
	
	/**
	 * Tests the rendering of alter function with comment specification
	 */
	public function test_alter_function_comment()
	{
		$statement = new AlterFunctionStatement();
		$statement->setFunctionName('myfunction');
		$fccarac = new CommentCharacteristicStatement();
		$fccarac->setCommentValue('Here\'s a comment for this function.');
		$statement->addCharacteristic($fccarac);
		
		$actual = $this->_dialect->alterFunction($statement);
		
		$this->assertEquals(
			'ALTER FUNCTION `myfunction` COMMENT "Here\'s a comment for this function.";', 
			$actual
		);
	}
	
	/**
	 * Tests the rendering of alter function with language specification
	 */
	public function test_alter_function_language()
	{
		$statement = new AlterFunctionStatement();
		$statement->setFunctionName('myfunction');
		$statement->addCharacteristic(new LanguageCharacteristicStatement());
		
		$actual = $this->_dialect->alterFunction($statement);
		
		$this->assertEquals(
			'ALTER FUNCTION `myfunction` LANGUAGE SQL;',
			$actual
		);
	}
	
	/**
	 * Tests the rendering of alter function with contains sql characteristic
	 */
	public function test_alter_function_contains_sql()
	{
		$statement = new AlterFunctionStatement();
		$statement->setFunctionName('myfunction');
		$carac = new SqlCharacteristicStatement();
		$carac->setSqlModifier(ISqlCharacteristicStatement::CONTAINS_SQL);
		$statement->addCharacteristic($carac);
		
		$actual = $this->_dialect->alterFunction($statement);
		
		$this->assertEquals(
			'ALTER FUNCTION `myfunction` CONTAINS SQL;',
			$actual
		);
	}
	
	/**
	 * Tests the rendering of alter function with no sql characteristic
	 */
	public function test_alter_function_no_sql()
	{
		$statement = new AlterFunctionStatement();
		$statement->setFunctionName('myfunction');
		$carac = new SqlCharacteristicStatement();
		$carac->setSqlModifier(ISqlCharacteristicStatement::NO_SQL);
		$statement->addCharacteristic($carac);
		
		$actual = $this->_dialect->alterFunction($statement);
		
		$this->assertEquals(
			'ALTER FUNCTION `myfunction` NO SQL;', 
			$actual
		);
	}
	
	/**
	 * Tests the rendering of alter function with reads sql data characteristic
	 */
	public function test_alter_function_reads_sql_data()
	{
		$statement = new AlterFunctionStatement();
		$statement->setFunctionName('myfunction');
		$carac = new SqlCharacteristicStatement();
		$carac->setSqlModifier(ISqlCharacteristicStatement::READS_SQL_DATA);
		$statement->addCharacteristic($carac);
		
		$actual = $this->_dialect->alterFunction($statement);
		
		$this->assertEquals(
			'ALTER FUNCTION `myfunction` READS SQL DATA;', 
			$actual
		);
	}
	
	/**
	 * Tests the rendering of alter function with modifies sql data characteristic
	 */
	public function test_alter_function_modifies_sql_data()
	{
		$statement = new AlterFunctionStatement();
		$statement->setFunctionName('myfunction');
		$carac = new SqlCharacteristicStatement();
		$carac->setSqlModifier(ISqlCharacteristicStatement::MODIFIES_SQL_DATA);
		$statement->addCharacteristic($carac);
		
		$actual = $this->_dialect->alterFunction($statement);
		
		$this->assertEquals(
			'ALTER FUNCTION `myfunction` MODIFIES SQL DATA;', 
			$actual
		);
	}
	
	// should throw exception : unsupported statement (Deterministic) for my5.0
	/**
	 * Tests the rendering of alter function with deterministic characteristic
	 */
// 	public function test_alter_function_deterministic()
// 	{
// 		$statement = new AlterFunctionStatement();
// 		$statement->setFunctionName('myfunction');
// 		$carac = new DeterministicCharacteristicStatement();
// 		$carac->setNot(false);
// 		$statement->addCharacteristic($carac);
		
		
// 		$actual = $this->_dialect->alterFunction($statement);
// 	}
	
	/**
	 * Tests the rendering of alter function with security definer characteristic
	 */
	public function test_alter_function_security_definer()
	{
		$statement = new AlterFunctionStatement();
		$statement->setFunctionName('myfunction');
		$carac = new SecurityCharacteristicStatement();
		$carac->setSecurityParam(ISecurityCharacteristicStatement::DEFINER);
		$statement->addCharacteristic($carac);
		
		$actual = $this->_dialect->alterFunction($statement);
		
		$this->assertEquals(
			'ALTER FUNCTION `myfunction` SQL SECURITY DEFINER;', 
			$actual
		);
	}
	
	/**
	 * Tests the rendering of alter function with security invoker characteristic
	 */
	public function test_alter_function_security_invoker()
	{
		$statement = new AlterFunctionStatement();
		$statement->setFunctionName('myfunction');
		$carac = new SecurityCharacteristicStatement();
		$carac->setSecurityparam(ISecurityCharacteristicStatement::INVOKER);
		$statement->addCharacteristic($carac);
		
		$actual = $this->_dialect->alterFunction($statement);
		
		$this->assertEquals(
			'ALTER FUNCTION `myfunction` SQL SECURITY INVOKER;', 
			$actual
		);
	}
	
}

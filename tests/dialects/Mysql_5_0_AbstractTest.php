<?php

/**
 * Abstract class to provide the dialect.
 * 
 * @author Anastaszor
 */
abstract class Mysql_5_0_AbstractTest extends PhpUnit_Framework_TestCase
{
	
	/**
	 * 
	 * @var Mysql_5_0_Dialect
	 */
	protected $_dialect = null;
	
	/**
	 * (non-PHPdoc)
	 * @see PhpUnit_Framework_TestCase::setUp()
	 */
	public function setUp()
	{
		$this->_dialect = new Mysql_5_0_Dialect();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see PhpUnit_Framework_TestCase::tearDown()
	 */
	public function tearDown()
	{
		$this->_dialect = null;
	}
	
}

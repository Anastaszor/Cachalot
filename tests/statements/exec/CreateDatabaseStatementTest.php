<?php

/**
 * CreateDatabaseStatementTest class file.
 * 
 * This test tests the CreateDatabaseStatement class.
 * 
 * @author Anastaszor
 */
class CreateDatabaseStatementTest extends AbstractStatementTest
{
	
	/**
	 * (non-PHPdoc)
	 * @see AbstractStatementTest::setUpStatement()
	 */
	protected function setUpStatement()
	{
		$statement = new CreateDatabaseStatement($this->_cachalot);
		$statement->setDatabaseName("test_database");
		return $statement;
	}
	
}

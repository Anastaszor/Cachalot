<?php

/**
 * AbstractStatementTest class file.
 * 
 * @author Anastaszor
 */
abstract class AbstractStatementTest extends PhpUnit_Framework_TestCase
{
	
	protected $_cachalot = null;
	
	protected $_statement = null;
	
	protected function setUp()
	{
		$bp = dirname(dirname(__DIR__));
		$testfolder = $bp.DIRECTORY_SEPARATOR.'runtime';
		CUtil::removeRecursiveDir($testfolder);
		mkdir($testfolder);
		chmod($testfolder, 0777);
		$this->_cachalot = Cachalot::connect("file://".$testfolder);
		$this->_statement = $this->setUpStatement();
	}
	
	protected function tearDown()
	{
		$this->_cachalot = null;
		$bp = dirname(dirname(__DIR__));
		$testfolder = $bp.DIRECTORY_SEPARATOR.'runtime';
		CUtil::removeRecursiveDir($testfolder);
	}
	
	/**
	 * 
	 * @return IStatement
	 */
	abstract protected function setUpStatement();
	
}
